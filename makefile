all: ./build/cloudKingdoms.dsk ./build/test.dsk

./build/cloudk.bin: cloudKingdoms.asm	./includes/cloudKingdoms_inc.asm ./includes/diskRoutines.asm ./includes/blockManager.asm ./includes/display.asm ./includes/compiledSprites.s ./includes/structs.asm ./includes/tunes.asm ./includes/mapLookups.asm ./includes/levels.asm
	awk '/^@version.*\.[0-9]+\\r/ { match($$0, /\.[0-9]+\\r/); build_number=sprintf(".%05d\\r",substr($$0,RSTART+1,RLENGTH-3)+1); sub(/\.[0-9]+\\r/, build_number); } 1' ./cloudKingdoms.asm > ./cloudKingdoms.r.asm
	mv ./cloudKingdoms.r.asm ./cloudKingdoms.asm
	lwasm --6309 --decb --format=decb --list=./build/cloudKingdoms.lst --symbol-dump=./build/cloudKingdoms.s --symbols --map=./build/cloudKingdoms.map --output=./build/cloudk.bin ./cloudKingdoms.asm

./build/sound.bin: sound.asm
	lwasm --6809 --decb --format=decb --list=./build/sound.lst --symbol-dump=./build/sound.s --symbols --map=./build/sound.map --output=./build/sound.bin ./sound.asm

./build/sound2.bin: sound2.asm
	lwasm --6809 --decb --format=decb --list=./build/sound2.lst --symbol-dump=./build/sound2.s --symbols --map=./build/sound2.map --output=./build/sound2.bin ./sound2.asm

./build/loadData.bin: loadData.asm	./includes/cloudKingdoms_inc.asm ./includes/diskRoutines.asm ./includes/blockManager.asm ./includes/display.asm ./includes/compiledSprites.asm
	awk '/^@version.*\.[0-9]+\\r/ { match($$0, /\.[0-9]+\\r/); build_number=sprintf(".%05d\\r",substr($$0,RSTART+1,RLENGTH-3)+1); sub(/\.[0-9]+\\r/, build_number); } 1' ./loadData.asm > ./loadData.r.asm
	mv ./loadData.r.asm ./loadData.asm
	lwasm --6309 --decb --format=decb --list=./build/loadData.lst --symbol-dump=./build/loadData.s --symbols --map=./build/loadData.map --output=./build/loadData.bin ./loadData.asm

./includes/compiledSprites.s: ./build/loadData.bin
	grep -e ^Hero\.[0-9][0-9] -e ^Hero\.SH -e ^PowerUps\.[0-9][0-9] -e ^Baddies\.[0-9][0-9] -e ^Chars\.[0-9][0-9] ./build/loadData.s > ./includes/compiledSprites.s

./build/cloudKingdoms.dsk: ./build/cloudk.bin ./build/loadData.bin ./cloudk.bas ./data/CKMAP.DTA ./data/PCBLOX.DAT ./build/sound.bin
ifeq ("$(wildcard ./build/cloudKingdoms.dsk)","")
	decb dskini ./build/cloudKingdoms.dsk
endif
	#../../../lst2cmt.exe /SYSTEM coco3h /OFFSET 21F7 /OVERWRITE ./cloudKingdoms.lst "D:\Emulators\Mame\comments\coco3h.cmt"
	decb copy -0 -a -r ./cloudk.bas ./build/cloudKingdoms.dsk,CLOUDK.BAS
	decb copy -2 -b -r ./build/cloudk.bin ./build/cloudKingdoms.dsk,CLOUDK.BIN
	decb copy -2 -b -r ./build/loadData.bin ./build/cloudKingdoms.dsk,LOADDATA.BIN
	decb copy -2 -b -r ./data/CKMAP.DTA ./build/cloudKingdoms.dsk,CKMAP.DTA
	decb copy -2 -b -r ./data/PCBLOX.DAT ./build/cloudKingdoms.dsk,PCBLOX.DAT

./build/test.dsk: ./build/sound.bin ./build/sound2.bin
ifeq ("$(wildcard ./build/test.dsk)","")
	decb dskini ./build/test.dsk
endif
	decb copy -2 -b -r ./build/sound.bin ./build/test.dsk,SOUND.BIN
	decb copy -2 -b -r ./build/sound2.bin ./build/test.dsk,SOUND2.BIN

clean:
	find ./build -type f \( -name "*.o" -o -name "*.bin" -o -name "*.map" -o -name "*.lst" -o -name "*.s" -o -name "*.dsk" \) -delete
