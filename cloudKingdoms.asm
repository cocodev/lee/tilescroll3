    pragma  undefextern
    pragma  newsource
    pragma  cescapes
    pragma  nodollarlocal
    pragma  cd

    include includes/compiledSprites.s

    include includes/structs.asm


    org     $0000
playfieldTopLeftX               rmb     2
playfieldTopLeftY               rmb     2
previousFramePlayfieldTopLeftY  rmb     2
vsyncCount                      rmb     1
firqCount                       rmb     2
splitScreen                     rmb     1


    org     $2000

debugData                       rmb     sizeof{DebugData}
debugDataIndex                  fcb     3

    include includes/display.asm
    include includes/blockManager.asm
    include includes/diskRoutines.asm
    include includes/cloudKingdoms_inc.asm
    include includes/keyscans.asm

;waveTables                      fdb     squareWaveTable,sawtoothWaveTable,sineWaveTable,midStepWaveTable,triangleWaveTable
;waveTablesCount                 equ     5

                            rmb     256-(*%256)
squareWaveTable             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

sineWaveTable               fcb     $7C,$80,$84,$84,$88,$8C,$90,$90,$94,$98,$9C,$9C,$A0,$A4,$A8,$A8
                            fcb     $AC,$B0,$B4,$B4,$B8,$BC,$BC,$C0,$C4,$C4,$C8,$C8,$CC,$D0,$D0,$D4
                            fcb     $D4,$D8,$D8,$DC,$DC,$E0,$E0,$E4,$E4,$E8,$E8,$E8,$EC,$EC,$F0,$F0
                            fcb     $F0,$F0,$F4,$F4,$F4,$F4,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8
                            fcb     $F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F4,$F4,$F4,$F4,$F0
                            fcb     $F0,$F0,$EC,$EC,$EC,$E8,$E8,$E4,$E4,$E4,$E0,$E0,$DC,$DC,$D8,$D8
                            fcb     $D4,$D4,$D0,$CC,$CC,$C8,$C8,$C4,$C0,$C0,$BC,$B8,$B8,$B4,$B0,$AC
                            fcb     $AC,$A8,$A4,$A4,$A0,$9C,$98,$98,$94,$90,$8C,$88,$88,$84,$80,$7C
                            fcb     $7C,$78,$74,$70,$70,$6C,$68,$64,$60,$60,$5C,$58,$54,$54,$50,$4C
                            fcb     $4C,$48,$44,$40,$40,$3C,$38,$38,$34,$30,$30,$2C,$2C,$28,$24,$24
                            fcb     $20,$20,$1C,$1C,$18,$18,$14,$14,$14,$10,$10,$0C,$0C,$0C,$08,$08
                            fcb     $08,$04,$04,$04,$04,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                            fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$04,$04,$04,$04,$08,$08
                            fcb     $08,$08,$0C,$0C,$10,$10,$10,$14,$14,$18,$18,$1C,$1C,$20,$20,$24
                            fcb     $24,$28,$28,$2C,$30,$30,$34,$34,$38,$3C,$3C,$40,$44,$44,$48,$4C
                            fcb     $50,$50,$54,$58,$5C,$5C,$60,$64,$68,$68,$6C,$70,$74,$74,$78,$7C

* sawtoothWaveTable           fcb     $00,$00,$00,$00,$04,$04,$04,$04,$08,$08,$08,$08,$0C,$0C,$0C,$0C
*                             fcb     $10,$10,$10,$10,$14,$14,$14,$14,$18,$18,$18,$18,$1C,$1C,$1C,$1C
*                             fcb     $20,$20,$20,$20,$24,$24,$24,$24,$28,$28,$28,$28,$2C,$2C,$2C,$2C
*                             fcb     $30,$30,$30,$30,$34,$34,$34,$34,$38,$38,$38,$38,$3C,$3C,$3C,$3C
*                             fcb     $40,$40,$40,$40,$44,$44,$44,$44,$48,$48,$48,$48,$4C,$4C,$4C,$4C
*                             fcb     $50,$50,$50,$50,$54,$54,$54,$54,$58,$58,$58,$58,$5C,$5C,$5C,$5C
*                             fcb     $60,$60,$60,$60,$64,$64,$64,$64,$68,$68,$68,$68,$6C,$6C,$6C,$6C
*                             fcb     $70,$70,$70,$70,$74,$74,$74,$74,$78,$78,$78,$78,$7C,$7C,$7C,$7C
*                             fcb     $80,$80,$80,$80,$84,$84,$84,$84,$88,$88,$88,$88,$8C,$8C,$8C,$8C
*                             fcb     $90,$90,$90,$90,$94,$94,$94,$94,$98,$98,$98,$98,$9C,$9C,$9C,$9C
*                             fcb     $A0,$A0,$A0,$A0,$A4,$A4,$A4,$A4,$A8,$A8,$A8,$A8,$AC,$AC,$AC,$AC
*                             fcb     $B0,$B0,$B0,$B0,$B4,$B4,$B4,$B4,$B8,$B8,$B8,$B8,$BC,$BC,$BC,$BC
*                             fcb     $C0,$C0,$C0,$C0,$C4,$C4,$C4,$C4,$C8,$C8,$C8,$C8,$CC,$CC,$CC,$CC
*                             fcb     $D0,$D0,$D0,$D0,$D4,$D4,$D4,$D4,$D8,$D8,$D8,$D8,$DC,$DC,$DC,$DC
*                             fcb     $E0,$E0,$E0,$E0,$E4,$E4,$E4,$E4,$E8,$E8,$E8,$E8,$EC,$EC,$EC,$EC
*                             fcb     $F0,$F0,$F0,$F0,$F4,$F4,$F4,$F4,$F8,$F8,$F8,$F8,$FC,$FC,$FC,$FC

* midStepWaveTable            fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80
*                             fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
*                             fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
*                             fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
*                             fcb     $00,$00,$00,$00
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80
*                             fcb     $80,$80,$80
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
*                             fcb     $FC,$FC,$FC

* ; triangle wave
* triangleWaveTable           fcb     $00,$01,$03,$05,$07,$09,$0B,$0D,$0F,$11,$13,$15,$17,$19,$1B,$1D
*                             fcb     $1F,$21,$23,$25,$27,$29,$2B,$2D,$2F,$31,$33,$35,$37,$39,$3B,$3D
*                             fcb     $3F,$40,$42,$44,$46,$48,$4A,$4C,$4E,$50,$52,$54,$56,$58,$5A,$5C
*                             fcb     $5E,$60,$62,$64,$66,$68,$6A,$6C,$6E,$70,$72,$74,$76,$78,$7A,$7C
*                             fcb     $7E,$7F,$81,$83,$85,$87,$89,$8B,$8D,$8F,$91,$93,$95,$97,$99,$9B
*                             fcb     $9D,$9F,$A1,$A3,$A5,$A7,$A9,$AB,$AD,$AF,$B1,$B3,$B5,$B7,$B9,$BB
*                             fcb     $BD,$BE,$C0,$C2,$C4,$C6,$C8,$CA,$CC,$CE,$D0,$D2,$D4,$D6,$D8,$DA
*                             fcb     $DC,$DE,$E0,$E2,$E4,$E6,$E8,$EA,$EC,$EE,$F0,$F2,$F4,$F6,$F8,$FA
*                             fcb     $FC,$FA,$F8,$F6,$F4,$F2,$F0,$EE,$EC,$EA,$E8,$E6,$E4,$E2,$E0,$DE
*                             fcb     $DC,$DA,$D8,$D6,$D4,$D2,$D0,$CE,$CC,$CA,$C8,$C6,$C4,$C2,$C0,$BE
*                             fcb     $BD,$BB,$B9,$B7,$B5,$B3,$B1,$AF,$AD,$AB,$A9,$A7,$A5,$A3,$A1,$9F
*                             fcb     $9D,$9B,$99,$97,$95,$93,$91,$8F,$8D,$8B,$89,$87,$85,$83,$81,$7F
*                             fcb     $7E,$7C,$7A,$78,$76,$74,$72,$70,$6E,$6C,$6A,$68,$66,$64,$62,$60
*                             fcb     $5E,$5C,$5A,$58,$56,$54,$52,$50,$4E,$4C,$4A,$48,$46,$44,$42,$40
*                             fcb     $3F,$3D,$3B,$39,$37,$35,$33,$31,$2F,$2D,$2B,$29,$27,$25,$23,$21
*                             fcb     $1F,$1D,$1B,$19,$17,$15,$13,$11,$0F,$0D,$0B,$09,$07,$05,$03,$01

scoreTemplate               fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, 'C,$32, 'l,$32, 'o,$32, 'u,$32, 'd,$32, ' ,$32, '0,$18, '0,$18, ' ,$32, 'K,$32, 'i,$32, 'n,$32, 'g,$32, 'd,$32, 'o,$32, 'm,$32, 's,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$00, ' ,$00, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$00, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$0F, ' ,$0F, 'G,$0F, 'e,$0F, 'm,$0F, 's,$0F, ':,$0F, '0,$18, '0,$18, ' ,$0F, ' ,$0F, ' ,$0F, 'S,$0F, 'c,$0F, 'o,$0F, 'r,$0F, 'e,$0F, ':,$0F, '0,$18, '0,$18, '0,$18, '0,$18, '0,$18, '0,$18, '0,$18, ' ,$0F, ' ,$0F, 'K,$0F, 'e,$0F, 'y,$0F, 's,$0F, ':,$0F, '0,$18, '0,$18, ' ,$0F, ' ,$0F, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$00, ' ,$00, ' ,$00 
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$0F, ' ,$0F, ' ,$0F, '>,$0F, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$32, ' ,$0F, ' ,$0F, ' ,$0F, ' ,$00, ' ,$00, ' ,$00, ' ,$00
                            fcb     ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$07, ' ,$00, ' ,$00, ' ,$00, ' ,$00, ' ,$00 

heroAccelerationAndDirectionInfo  ;  HeroAccelerationAndDirection
                                  ;  ┌─────────────── accelerationX
                                  ;  │   ┌─────────── accelerationY
                                  ;  │   │   ┌─────── heroIndex
                                  ;  │   │   │   ┌─── looking
                            fcb      0,  0, -1, -1      ; 00 = No Direction
                            fcb      0,  2,  6,  5      ; 01 = Down
                            fcb      0, -2,  2,  1      ; 02 = Up
                            fcb      0,  0,  0,  0      ; 03 = Up/Down
                            fcb      2,  0,  4,  3      ; 04 = Right
                            fcb      2,  2,  5,  4      ; 05 = Right/Down
                            fcb      2, -2,  3,  2      ; 06 = Right/Up
                            fcb      0,  0,  0,  0      ; 07 = Right/Up/Down
                            fcb     -2,  0,  8,  7      ; 08 = Left
                            fcb     -2,  2,  7,  6      ; 09 = Left/Down
                            fcb     -2, -2,  1,  8      ; 10 = Left/Up
                            fcb      0,  0,  0,  0      ; 11 = Left/Up/Down
                            fcb      0,  0,  0,  0      ; 12 = Left/Right
                            fcb      0,  0,  0,  0      ; 13 = Left/Right/Down
                            fcb      0,  0,  0,  0      ; 14 = Left/Right/Up
                            fcb      0,  0,  0,  0      ; 15 = Left/Right/Up/Down

heroSpeedAndControlFilterWhenAboutToFall
                            ;  AboutToFallSpeedAndControlFilter (indexed by quadrant flags)
                            ;    ┌───────────────── speedX
                            ;    │   ┌───────────── speedY
                            ;    │   │    ┌──────── controlFilter
                            ;    │   │    │  ┌───── filler
                            ;    │   │    │  │         ┌─────────────────────────────────── Quadrant Flags
                            ;    │   │    │  │         │                ┌───┬────────────── Speed x,y
                            ;    │   │    │  │         │                │   │      ┌─────── Control Filter (allowed controls)
                            fcb  0,  0, $1F, 0      ;  0 =          = ( 0,  0), 0x1F = Fire, Left, Right, Up, Down
                            fcb -1, -1, $0A, 0      ;  1 = ↖        = (-1, -1), 0x0A = Left, Up
                            fcb  1, -1, $06, 0      ;  2 = ↗        = ( 1, -1), 0x06 = Right, Up
                            fcb  0, -1, $0E, 0      ;  3 = ↖ ↗      = ( 0, -1), 0x0E = Left, Right, Up
                            fcb -1,  1, $09, 0      ;  4 = ↙        = (-1,  1), 0x09 = Left, Down
                            fcb -1,  0, $0B, 0      ;  5 = ↙ ↖      = (-1,  0), 0x0B = Left, Up, Down
                            fcb  0,  0, $1F, 0      ;  6 = ↙ ↗      = ( 0,  0), 0x1F = Fire, Left, Right, Up, Down
                            fcb -1, -1, $0A, 0      ;  7 = ↙ ↗ ↖    = (-1, -1), 0x0A = Left, Up
                            fcb  1,  1, $05, 0      ;  8 = ↘        = ( 1,  1), 0x05 = Right, Down
                            fcb  0,  0, $1F, 0      ;  9 = ↘ ↖      = ( 0,  0), 0x1F = Fire, Left, Right, Up, Down
                            fcb  1,  0, $07, 0      ; 10 = ↘ ↗      = ( 1,  0), 0x07 = Right, Up, Down
                            fcb  1, -1, $06, 0      ; 11 = ↘ ↗ ↖    = ( 1, -1), 0x06 = Right, Up
                            fcb  0,  1, $0D, 0      ; 12 = ↘ ↙      = ( 0,  1), 0x0D = Left, Right, Down
                            fcb -1,  1, $09, 0      ; 13 = ↘ ↙ ↖    = (-1,  1), 0x09 = Left, Down
                            fcb  1,  1, $05, 0      ; 14 = ↘ ↙ ↗    = ( 1,  1), 0x05 = Right, Down


routinesToBounceHeroFullSpeed                                           ;                      ↘ ↙ ↗↖
                                                                        ;                      0 0 0 0 
                                fdb BounceHeroNoDirection               ; %0000 = Bounced from no quadrant
                                fdb BounceHeroDownAndRightFullSpeed     ; %0001 = Bounced from ↖ quadrant
                                fdb BounceHeroDownAndLeftFullSpeed      ; %0010 = Bounced from ↗ quadrant
                                fdb BounceHeroDownFullSpeed             ; %0011 = Bounced from ↖ ↗ quadrants
                                fdb BounceHeroUpAndRightFullSpeed       ; %0100 = Bounced from ↙ quadrant
                                fdb BounceHeroRightFullSpeed            ; %0101 = Bounced from ↖ ↙ quadrants
                                fdb BounceHeroNoDirection               ; %0110 = Bounced from ↙ ↗ quadrants
                                fdb BounceHeroDownAndRightFullSpeed     ; %0111 = Bounced from ↙ ↗ ↖ quadrants
                                fdb BounceHeroUpAndLeftFullSpeed        ; %1000 = Bounced from ↘ quadrant
                                fdb BounceHeroNoDirection               ; %1001 = Bounced from ↘ ↖ quadrants
                                fdb BounceHeroLeftFullSpeed             ; %1010 = Bounced from ↘ ↗ quadrants
                                fdb BounceHeroDownAndLeftFullSpeed      ; %1011 = Bounced from ↘ ↗ ↖ quadrants
                                fdb BounceHeroUpFullSpeed               ; %1100 = Bounced from ↘ ↙ quadrants
                                fdb BounceHeroUpAndRightFullSpeed       ; %1101 = Bounced from ↘ ↙ ↖ quadrants
                                fdb BounceHeroUpAndLeftFullSpeed        ; %1110 = Bounced from ↘ ↙ ↗ quadrants
                                fdb BounceHeroNoDirection               ; %1111 = Bounced from ↘ ↙ ↗ ↖ quadrants

fullSpeedDirections         ;   ByteDirection
                            ;        ┌─────── x
                            ;        │   ┌─── y
                                fcb  0, -8 ; Up
                                fcb  8, -8 ; Right Up
                                fcb  8,  0 ; Right
                                fcb  8,  8 ; Right Down
                                fcb  0,  8 ; Down
                                fcb -8,  8 ; Left Down
                                fcb -8,  0 ; Left
                                fcb -8, -8 ; Left Up

baddiesDirectionData    ;       BaddieDirectionInfo
                        ;       ┌────────────────────────────────────────── leftDirection
                        ;       │  ┌─────────────────────────────────────── rightDirection
                        ;       │  │   ┌─────────────────────────────────── turnAroundDirection
                        ;       │  │   │   ┌─────────────────────────────── speedX
                        ;       │  │   │   │   ┌─────────────────────────── speedY
                        ;       │  │   │   │   │     ┌───────────────────── leftMapOffset
                        ;       │  │   │   │   │     │     ┌─────────────── rightMapOffset
                        ;       │  │   │   │   │     │     │     ┌───────── upMapOffset
                        ;       │  │   │   │   │     │     │     │     ┌─── downMapOffset
                            fcb 3, 2,  1, -4,  0,  $20, -$20,   -1,    1    ; Facing left
                            fcb 2, 3,  0,  4,  0, -$20,  $20,    1,   -1    ; Facing right
                            fcb 0, 1,  3,  0, -4,   -1,    1, -$20,  $20    ; Facing up
                            fcb 1, 0,  2,  0,  4,    1,   -1,  $20, -$20    ; facing down

    include includes/mapLookups.asm
    include includes/levels.asm
    include includes/tunes.asm

heroFrames                  ;            0         1           2       3            4          5              6         7            8
                            fdb     Hero.Idle,Hero.UpLeft,Hero.Up,Hero.UpRight,Hero.Right,Hero.DownRight,Hero.Down,Hero.DownLeft,Hero.Left
                            ;
Hero.EyesClose.Index        equ     9
Hero.EyesClose.FrameCount   equ     4
                            ;            9             10                  11                    12
                            fdb     Hero.EyesOpen,Hero.EyesMostlyOpen,Hero.EyesMostlyClosed,Hero.EyesClosed
                            ;
closeEyesAnimation          fcb     HERO_ACTION_DATA_PHYSICAL_BLOCK                     ; HeroAnimation.spritesPhysicalBlock
                            fdb     heroFrames                                          ; HeroAnimation.spritesLookup
                            fcb     Hero.EyesClose.Index                                ; HeroAnimation.firstFrameIndex
                            fcb     Hero.EyesClose.Index+Hero.EyesClose.FrameCount-1    ; HeroAnimation.lastFrameIndex
                            ;
openEyesAnimation           fcb     HERO_ACTION_DATA_PHYSICAL_BLOCK                     ; HeroAnimation.spritesPhysicalBlock
                            fdb     heroFrames                                          ; HeroAnimation.spritesLookup
                            fcb     Hero.EyesClose.Index+Hero.EyesClose.FrameCount-1    ; HeroAnimation.firstFrameIndex
                            fcb     Hero.EyesClose.Index                                ; HeroAnimation.lastFrameIndex
                            ;
Hero.Fall.FrameCount        equ     8
fallFrames                  fdb     Hero.Fall.Frame0,Hero.Fall.Frame1,Hero.Fall.Frame2,Hero.Fall.Frame3,Hero.Fall.Frame4,Hero.Fall.Frame5,Hero.Fall.Frame6,Hero.Fall.Frame7
                            ;
fallAnimation               fcb     HERO_ACTION_DATA_PHYSICAL_BLOCK     ; HeroAnimation.spritesPhysicalBlock
                            fdb     fallFrames                          ; HeroAnimation.spritesLookup
                            fcb     0                                   ; HeroAnimation.firstFrameIndex
                            fcb     Hero.Fall.FrameCount-1              ; HeroAnimation.lastFrameIndex
                            ;
Hero.Sparkles.FrameCount    equ     3
sparkleFrames               fdb     Hero.Sparkle.Frame0,Hero.Sparkle.Frame1,Hero.Sparkle.Frame2
                            ;
Hero.Wings.FrameCount       equ     4
wingFrames                  fdb     Hero.Wings.Frame0,Hero.Wings.Frame1,Hero.Wings.Frame2,Hero.Wings.Frame3
                            ;
Hero.Fade.FrameCount        equ     14
fadeFrames                  fdb     Hero.Fade.Frame0,Hero.Fade.Frame0,Hero.Fade.Frame1,Hero.Fade.Frame1,Hero.Fade.Frame2,Hero.Fade.Frame2,Hero.Fade.Frame3,Hero.Fade.Frame3,Hero.Fade.Frame4,Hero.Fade.Frame4,Hero.Fade.Frame5,Hero.Fade.Frame5,Hero.Fade.Frame6,Hero.Fade.Frame6
                            ;
fadeOutAnimation            fcb     HERO_FADE_DATA_PHYSICAL_BLOCK       ; HeroAnimation.spritesPhysicalBlock
                            fdb     fadeFrames                          ; HeroAnimation.spritesLookup
                            fcb     0                                   ; HeroAnimation.firstFrameIndex
                            fcb     Hero.Fade.FrameCount-1              ; HeroAnimation.lastFrameIndex
                            ;
fadeInAnimation             fcb     HERO_FADE_DATA_PHYSICAL_BLOCK       ; HeroAnimation.spritesPhysicalBlock
                            fdb     fadeFrames                          ; HeroAnimation.spritesLookup
                            fcb     Hero.Fade.FrameCount-1              ; HeroAnimation.firstFrameIndex
                            fcb     0                                   ; HeroAnimation.lastFrameIndex
                            ;
BaddieFrames                fdb     Baddie.Vertical1,Baddie.Vertical2,Baddie.Vertical3,Baddie.Vertical4,Baddie.Horizontal1,Baddie.Horizontal2,Baddie.Horizontal3,Baddie.Horizontal4

fontSprites                 fdb     Font.0,Font.1,Font.2,Font.3,Font.4,Font.5,Font.6,Font.7,Font.8,Font.9,Font.A,Font.B,Font.C,Font.D,Font.E,Font.F,Font.G,Font.H
                            fdb     Font.I,Font.J,Font.K,Font.L,Font.M,Font.N,Font.O,Font.P,Font.Q,Font.R,Font.S,Font.T,Font.U,Font.V,Font.W,Font.X,Font.Y,Font.Z
                            ;
redrawTile1                 rmb     sizeof{RedrawTile}
redrawTile2                 rmb     sizeof{RedrawTile}

tunes.currentTuneIndex      fcb     $00         ; Index into tunes.addresses and tunes.lengths of current tune playing
tunes.toneInTuneAddress     fdb     $0000       ; address of current tone in current tune
tunes.durationRemaining     fcb     $00         ; Current tone's duration remaining before we move to the next tone
tunes.tonesRemaining        fcb     $00         ; Number of tones remaining in the current tune before we stop playing

shouldSkipInitialPrerenderLevel         fcb 0   ; 0 = Prerender level 0 on start up, != 0 = Skip prerendering level 0 on start up
timerForTimeRemainingDecrement          fcb $50 ; 20 (frames per second) * 4 seconds = 80 ($50)
timerForTimeRemainingDecrementActive    fcb 1
bcdTimeRemaining            fcb     $00
bcdGems                     fcb     $00
bcdKeys                     fcb     $00
bcdScore                    fqb     $00000000
health                      fcb     100
paletteStore                rmb     16
keyColumnScans              fcb     1,1,1,1,1,1,1,1
previousKeyColumnScans      fcb     1,1,1,1,1,1,1,1

powerUpCompiledSprites      fdb     PowerUps.Key,PowerUps.StopWatch,PowerUps.Soda,PowerUps.Wings,PowerUps.Shield,PowerUps.Gem,PowerUps.Paint
currentMap                  rmb     512
currentWaveTable            fcb     $00
shouldQuit                  fcb     0
shouldCycleToNextLevel      fcb     0
shouldCycleToPreviousLevel  fcb     0
shouldReloadLevel           fcb     0
* shouldCycleToNextWaveTable  fcb     0
shouldDebugLT               fcb     0
shouldDebugGT               fcb     0
shouldPauseUnpause          fcb     0
shouldToggleNoFallCheat     fcb     0
shouldToggleNoDamageCheat   fcb     0
currentLevel                fcb     4
mapXHeroIsOn                fcb     $00
mapYHeroIsOn                fcb     $00
addressOfMapCellHeroIsOn    fdb     $0000
actionIndexForMapCellHeroIsOn   fcb $00
preventFrictionFromSlowingHero  fcb 1
heroIsOnMagnet              fcb     0
heroIsDead                  fcb     0
directionHeroIsLooking      fcb     0       ; 0=None, 1=Up, 2=Right Up, 3=Right, 4=Right Down, 5=Down, 6=Left Down, 7=Left, 8=Left Up
hammerTimer                 fcb     $00
wingTimer                   fcb     $00
wingFrameIndex              fcb     $00
wingFrameDirection          fcb     $01
sparkleTimer                fcb     $00
sparkleFrameIndex           fcb     $00
cheat.HeroNoDamage          fcb     $00
cheat.HeroNoFall            fcb     $00

controls                    ;        ┌─────── Bit 4 - Fire
                            ;        │┌────── Bit 3 - Left
                            ;        ││┌───── Bit 2 - Right
                            ;        │││┌──── Bit 1 - Up
                            ;        ││││┌─── Bit 0 - Down
                            fcb     %00000 
controlFilter               fcb     %11111

constantlyCyclingWord       fdb     $00

start:
;***********************
    lbsr    Width32
    lbsr    SetHsyncCountForGimeVersion
    lbsr    Width80
    lbsr    InitializeParameters
    lbsr    InitializeDiskIo
    lbsr    InitializeScreenBuffers
    bcc     @initializeDiskIoSuccess
    leax    noDiskSupport,pcr
    lbsr    PrintString
    lbra    @done
@initializeDiskIoSuccess
    lbsr    SetFastSpeed
    ldmd    #1
    lbsr    Set80x24TextMode
    leax    @version,pcr
    lbsr    PrintString
    * leax    @romsInStartingTakeover,pcr
    * lbsr    PrintString
    ;
    orcc    #%01010000
    lbsr    SwapRomsOut
    lda     #LOGICAL_C000_DFFF
    lbsr    SetSafeMmuScratchBlock
    andcc   #%10101111
    lbsr    OpenDebugFile
    * leax    @romsOutAboutToCallTests,pcr
    * lbsr    PrintString
    ;
    lbsr    InitializeSound
    lbsr    StartGameLoop
    lbsr    UninitializeSound
    * leax    @romsOutDoneWithTests,pcr
    * lbsr    PrintString
    ;
@doneWithTest
    lbsr    CloseDebugFile
    orcc    #%01010000
    lbsr    SwapRomsIn
    andcc   #%10101111
    lbsr    Set80x24TextMode
    * leax    @romsInDoneWithTakeover,pcr
    * lbsr    PrintString
    ldd     highSpeedTimerCount+1,pcr
    std     debugData+DebugData.highSpeedTimerCountFinal
@done
    lbsr    ResetRgbPalette
    lbsr    SetSlowSpeed
    ldmd    #0
    rts
@version                        fcn "Cloud Kingdoms\rVersion Alpha 0.1.0.00626\r\r"
@dataLoadError                  fcn "Error loading data\r"
* @romsInStartingTakeover         fcn "Roms in, starting takeover\r"
* @romsOutAboutToCallTests        fcn "Roms Out, about to call tests\r"
* @romsOutDoneWithTests           fcn "Roms Out, done with tests\r"
* @romsInDoneWithTakeover         fcn "Roms In, done with takeover\r"

OpenDebugFile
;***********************
    pshs    cc
    orcc    #%01010000
    lbsr    SwapRomsIn
    ldx     #@debugFilename
    lbsr    OpenFileForWrite
    bcs     @error
    bra     @success
@error
    ldx     #@errorMessageStart
    lbsr    PrintString
    tfr     b,a
    lbsr    PrintHexByte
    lda     #']
    lbsr    PrintChar80x24
    lda     #13
    lbsr    PrintChar80x24
    lbsr    SwapRomsOut
    coma
    bra     @done
    ;
@success
    lbsr    SwapRomsOut
    clra
    ;
@done
    puls    cc,pc
@debugFilename                  fcn "DEBUG   DAT";
@errorMessageStart              fcn '[%Error OF $'

WriteBytesToDebugFile
; ```plaintext
; > x = Address of bytes to write
; > b = Number of bytes to write
; ```
;***********************
    pshs    cc
    orcc    #%01010000
    lbsr    SwapRomsIn
@loop
    lda     ,x+
    lbsr    WriteByteToFile
    bcs     @error
    decb
    bne     @loop
    bra     @success
    ;
@error
    ldx     #@errorMessageStart
    lbsr    PrintString
    tfr     b,a
    lbsr    PrintHexByte
    lda     #']
    lbsr    PrintChar80x24
    lda     #13
    lbsr    PrintChar80x24
    coma
    bra     @done
    ;
@success
    clra
    ;
@done
    lbsr    SwapRomsOut
    puls    cc,pc
@errorMessageStart              fcn '[%Error WD $'

CloseDebugFile
;***********************
    pshs    cc
    orcc    #%01010000
    lbsr    SwapRomsIn
    lbsr    CloseAllFiles
    lbsr    SwapRomsOut
    puls    cc,pc

InitializeParameters
;***********************
    pshs    x,b,a
    pshsw
    ;
    clr     printChar80x24_CurrentCursorX,pcr
    clr     printChar80x24_CurrentCursorY,pcr
    ;
    clra
    pshs    a
    ldx     #debugData+DebugData.data
    ldw     #256
    tfm     s,x+
    puls    a
    lda     #3
    sta     debugDataIndex
    ;
    jsr     GetCurrentBasicCharacter
    cmpa    #':
    bne     @done
@ParameterLoop
    jsr     GetNextBasicCharacter
    beq     @done               ; Next character is 0, so we hit the end of the line.
    bsr     ProcessParameter
    bra     @ParameterLoop
    ;
@done
    pulsw
    puls    a,b,x,pc

ProcessParameter
; ```plaintext
; > a = parameter character
; ```
;***********************
    cmpa    #'X                 ; X = skip data
    bne     @done
    sta     shouldSkipInitialPrerenderLevel,pcr
@done
    rts

InitializeSound
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11110111;PIA0SideAControlRegister_FF01                        ; MSB & LSB of MUX set to 0 = Select DAC (Digital to Analog Converter) as sound source
    ;
    ;         PIA 0 Side B Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11110111;PIA0SideBControlRegister_FF03                        ; MSB & LSB set to 0 = Select DAC (Digital to Analog Converter) as sound source
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    oim     #%00001000;PIA1SideBControlRegister_FF23                        ; Enable Sound
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    aim     #%11111011;PIA1SideAControlRegister_FF21                        ; Make $FF20 configure direction for DAC, RS-323C, and Cassette
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%11111100
    sta     PIA1SideADataRegister_FF20                                      ; Set DAC to Output; Set RS-232C and Cassette to Input (so writes to those two bits do nothing)
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    oim     #%00000100;PIA1SideAControlRegister_FF21                        ; Make $FF20 function as input/output again
    rts

UninitializeSound
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    aim     #%11111011;PIA1SideAControlRegister_FF21                        ; Make $FF20 configure direction for DAC, RS-323C, and Cassette
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%11111110
    sta     PIA1SideADataRegister_FF20                                      ; DAC/RS-232C to Output, Cassette to Input
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    aim     #%11110111;PIA1SideBControlRegister_FF23                        ; Disable Sound
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    oim     #%00000100;PIA1SideAControlRegister_FF21                        ; Make $FF20 function as input/output again
    lda     #%00000010
    sta     PIA1SideADataRegister_FF20                                      ; Set RS-232C idle
    rts

InitializeScreenBuffers
    pshs    b,a,cc
    ;
    clrd
    orcc    #%01010000      ; Mask IRQ and FIRQ while we modifiy the screen buffers
    std     screenBuffers+ScreenBuffer.verticalOffset
    std     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.verticalOffset
    sta     screenBuffers+ScreenBuffer.leftPlayfieldXInSlice
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.leftPlayfieldXInSlice
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    sta     screenBuffers+ScreenBuffer.firstPhysicalBlockOfBuffer
    lda     #BUFFER2_FIRST_PHYSICAL_BLOCK
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.firstPhysicalBlockOfBuffer
    lda     #BUFFER1_VIDEO_BANK
    sta     screenBuffers+ScreenBuffer.videoBank
    lda     #BUFFER2_VIDEO_BANK
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.videoBank
    ;
    puls    cc,a,b,pc

Set80x24TextMode
;***********************
    pshs    a
    ;         Video Mode
    ;         ┌────────── Bit  7   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%00000011
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    lda     #%00000000
    sta     GIME_VideoBankSelect_FF9B
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00010101
    sta     GIME_VideoResolution_FF99
    ;
    ;         Border Color
    ;         ┌┬───────── Bits 7-6 - Unused
    ;         ││┌──┬───── Bits 5,2 - Red
    ;         │││┌─│┬──── Bits 4,1 - Green
    ;         ││││┌││┬─── Bits 3,0 - Blue
    lda     #%00000000
    sta     GIME_BorderColor_FF9A
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    lda     #%00000000
    sta     GIME_VideoBankSelect_FF9B
    ;
    ;         Vertical Scroll
    ;         ┌┬┬┬─────── Bits 7-4 - Unused
    ;         ││││┌┬┬┬─── Bits 3-0 - VSC (Vertical Scroll Register for smooth scrolling in text modes)
    lda     #%00000000
    sta     GIME_VerticalScroll_FF9C
    ;
    ;         Vertical Offset 1
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 18-11 of 19 bit Physical address for start of display
    lda     #%11011000
    sta     GIME_VerticalOffset1_FF9D
    ;
    ;         Vertical Offset 0
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 10-3 of 19 bit Physical address for start of display (bits 2-0 are always 0)
    lda     #%00000000  ; %11011000_00000000_000 = &h6C000
    sta     GIME_VerticalOffset0_FF9E
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen Enable
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     GIME_HorizontalOffset_FF9F
    ;
    puls    a,pc

Set320x200x16_GraphicsMode
;***********************
    pshs    x,a
    ;         VideoMode
    ;         ┌────────── Bit  7   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%10000001
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00111110
    sta     GIME_VideoResolution_FF99
    ;
    ;         Border Color
    ;         ┌┬───────── Bits 7-6 - Unused
    ;         ││┌──┬───── Bits 5,2 - Red
    ;         │││┌─│┬──── Bits 4,1 - Green
    ;         ││││┌││┬─── Bits 3,0 - Blue
    lda     #%00000000
    sta     GIME_BorderColor_FF9A
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    ;       #%00000000
    ldx     onScreenBuffer
    lda     ScreenBuffer.videoBank,x
    sta     GIME_VideoBankSelect_FF9B
    ;
    ;         Vertical Scroll
    ;         ┌┬┬┬─────── Bits 7-4 - Unused
    ;         ││││┌┬┬┬─── Bits 3-0 - VSC (Vertical Scroll Register for smooth scrolling in text modes)
    lda     #%00000000
    sta     GIME_VerticalScroll_FF9C
    ;
    ;         Vertical Offset 1
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 18-11 of 19 bit Physical address for start of display
    ;       #%00011100  ; %00011100_00000000_000 = %000_1000_0000_0000_0000 = &h0E000
    lda     ScreenBuffer.verticalOffset,x
    sta     GIME_VerticalOffset1_FF9D
    ;
    ;         Vertical Offset 0
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 10-3 of 19 bit Physical address for start of display (bits 2-0 are always 0)
    ;       #%00000000  ; %00011100_00000000_000 = %000_1000_0000_0000_0000 = &h0E000
    lda     ScreenBuffer.verticalOffset+1,x  ; %00000000_00000000_000 = %000_0000_0000_0000_0000 = &h00000
    sta     GIME_VerticalOffset0_FF9E
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen: 1 = Enabled, 0 = Disabled
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     GIME_HorizontalOffset_FF9F
    ;
    puls    a,x,pc

Enable256ByteWideMode
;***********************
    pshs    a
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen: 1 = Enabled, 0 = Disabled
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%10000000
    sta     GIME_HorizontalOffset_FF9F
    puls    a,pc

SetupInterruptSources
;***********************
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA0SideAControlRegister_FF01    ; Disable HSYNC IRQ
    ;
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA0SideBControlRegister_FF03    ; Disable VSYNC IRQ
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    aim     #%11111110;PIA1SideAControlRegister_FF21    ; Disable Cassette Data FIRQ
    ;
    ;        PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA1SideBControlRegister_FF23    ; Disable Cartridge FIRQ
    ;
    ;         Initialization Register 0 - INIT0 
    ;         ┌────────── Bit  7   - CoCo Bit (0 = CoCo 3 Mode, 1 = CoCo 1/2 Compatible)
    ;         │┌───────── Bit  6   - M/P (1 = MMU enabled, 0 = MMU disabled)
    ;         ││┌──────── Bit  5   - IEN (1 = GIME IRQ output enabled to CPU, 0 = disabled)
    ;         │││┌─────── Bit  4   - FEN (1 = GIME FIRQ output enabled to CPU, 0 = disabled)
    ;         ││││┌────── Bit  3   - MC3 (1 = Vector RAM at FEXX enabled, 0 = disabled)
    ;         │││││┌───── Bit  2   - MC2 (1 = Standard SCS (DISK) (0=expand 1=normal))
    ;         ││││││┌┬─── Bits 1-0 - MC1-0 (10 = ROM Map 32k Internal, 0x = 16K Internal/16K External, 11 = 32K External - Except Interrupt Vectors)
    lda     #%01111110
    sta     GIME_InitializeRegister0_FF90               ; CoCo 3 Mode; Enable MMU, IRQ, FIRQ; Vector RAM at $FFEx; ROM 32k internal
    ;
    ;         Initialization Register 1 - INIT1
    ;         ┌────────── Bit  7   - Unused
    ;         │┌───────── Bit  6   - Memory type (1=256K, 0=64K chips)
    ;         ││┌──────── Bit  5   - Timer input clock source (1=279.365 nsec, 0=63.695 usec)
    ;         │││┌┬┬┬──── Bits 4-1 - Unused
    ;         │││││││┌─── Bits 0   - MMU Task Register select (0=enable $FFA0-$FFA7, 1=enable $FFA8-$FFAF)
    lda     #%00100000
    sta     GIME_InitializeRegister1_FF91               ; Timer 63.695 usec (*10^-6, low speed); MMU $FFA0-$FFA7 task selected
    ;
    ;         Interrupt Request Enable Register - IRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00001000
    sta     GIME_InterruptReqEnable_FF92        ; Enable IRQS: VSYNC
                                                ; Disable IRQs: Timer, HSYNC, RS232, Keyboard, Cartridge
    ;
    ;         Fast Interrupt Request Enable Register - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00000000
    sta     GIME_FastInterruptReqEnable_FF93    ; Disable FIRQS: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    lbsr    SetupInterruptVectors
    rts

EnableSplitScreen
    pshs    a
    ;
    clr     <splitScreen
    inc     <splitScreen
    ;
    puls    a,pc

DisableSplitScreen
    pshs    a
    ;
    clr     <splitScreen
    ;
    puls    a,pc

RestoreInterruptSources
;***********************
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    oim     #%00000001;PIA0SideBControlRegister_FF03    ; Enable VSYNC IRQ
    ;
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA0SideAControlRegister_FF01    ; Disable HSYNC IRQ
    ;
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    aim     #%11111110;PIA1SideAControlRegister_FF21    ; Disable the Cassette Data FIRQ
    ;
    ;        PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    aim     #%11111110;PIA1SideBControlRegister_FF23    ; Disable the Cartridge FIRQ
    ;
    ;         Initialization Register 0 - INIT0 
    ;         ┌────────── Bit  7   - CoCo Bit (0 = CoCo 3 Mode, 1 = CoCo 1/2 Compatible)
    ;         │┌───────── Bit  6   - M/P (1 = MMU enabled, 0 = MMU disabled)
    ;         ││┌──────── Bit  5   - IEN (1 = GIME IRQ output enabled to CPU, 0 = disabled)
    ;         │││┌─────── Bit  4   - FEN (1 = GIME FIRQ output enabled to CPU, 0 = disabled)
    ;         ││││┌────── Bit  3   - MC3 (1 = Vector RAM at FEXX enabled, 0 = disabled)
    ;         │││││┌───── Bit  2   - MC2 (1 = Standard SCS (DISK) (0=expand 1=normal))
    ;         ││││││┌┬─── Bits 1-0 - MC1-0 (10 = ROM Map 32k Internal, 0x = 16K Internal/16K External, 11 = 32K External - Except Interrupt Vectors)
    lda     #%01001110
    sta     GIME_InitializeRegister0_FF90
    ;
    ;         Fast/Interrupt Request Enable Registers - IRQENR/FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00000000
    sta     GIME_InterruptReqEnable_FF92        ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     GIME_FastInterruptReqEnable_FF93    ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    ;
    lda     GIME_InterruptReqEnable_FF92
    lda     GIME_FastInterruptReqEnable_FF93
    lbsr    RestoreInterruptVectors
    rts

endMessage              fcn     ".\r"

WaitForKeyEndMessageNoRom
;***********************
@clearKeyStart
    lbsr    IsKeyDownNoRom
    bne     @clearKeyStart
@pauseStart
    lbsr    IsKeyDownNoRom
    beq     @pauseStart
    leax    endMessage,pcr
    lbsr    PrintString
    rts

WaitForKeyEndMessageRom
@clearKeyStart
    lbsr    IsKeyDownRom
    bne     @clearKeyStart
@pauseStart
    lbsr    IsKeyDownRom
    beq     @pauseStart
    leax    endMessage,pcr
    lbsr    PrintString
    rts

romInStack              rmb 2
romOutStack             fdb $600
zeroPageInitialized     fcb 0

SwapRomsOut
;***********************
    pshs    x,b,a
    ;
    sta     RamMode_FFDF                ; Switch $8xxx-$Fxxx RAM mode
    ;
    ldb     #PHYSICAL_060000_061FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    mmu+LOGICAL_0000_1FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_068000_069FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    mmu+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06A000_06BFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    mmu+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06C000_06DFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    mmu+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06E000_06FFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    mmu+LOGICAL_E000_FFFF,pcr
    stb     ,x
    ;
    ;                           ; $7F50 = old_a, $7F51 = old_b, $7F52-3 = old_x, $7F54-5 = return
    tfr     s,x                 ; s = $7F50, x = $7F50
    leas    6,s                 ; s = $7F56
    sts     romInStack,pcr      ; romInStack = $7F56
    lds     romOutStack,pcr     ; s = $0600
    ldd     4,x                 ; d = return
    pshs    b,a                 ; $05FE-F = return, s = $05FE
    ldd     ,x                  ; d = old_d
    ldx     2,x                 ; x = old_x
    ;
    ldb     #PHYSICAL_066000_067FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    leax    mmu+LOGICAL_6000_7FFF,pcr
    stb     ,x
    ;
    ldb     #LOGICAL_C000_DFFF
    stb     safeMmuLogicalBlock,pcr
    ldx     #$C000
    stx     safeMmuLogicalBlockAddress,pcr
    ;
    tst     zeroPageInitialized,pcr
    bne     @zeroPageInitialized
    lbsr    DisableSplitScreen
    ldx     #squareWaveTable
    inc     zeroPageInitialized,pcr
@zeroPageInitialized
    lbsr    SetupInterruptSources
    ;
    rts

SwapRomsIn
;***********************
    pshs    x,b,a
    ;
    lbsr    RestoreInterruptSources
    ;
    ldb     #PHYSICAL_076000_077FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    leax    mmu+LOGICAL_6000_7FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_078000_079FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    mmu+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07A000_07BFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    mmu+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07C000_07DFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    mmu+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07E000_07FFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    mmu+LOGICAL_E000_FFFF,pcr
    stb     ,x
    ;
    lda     #LOGICAL_4000_5FFF
    lbsr    SetSafeMmuScratchBlock
    ;
    ;                           ; $05FA = old_a, $05FB = old_b, $05FC-D = old_x, $05FE-F = return
    tfr     s,x                 ; s = $05FA, x = $05FA
    leas    6,s                 ; s = $0600
    sts     romOutStack,pcr     ; romOutStack = $0600
    lds     romInStack,pcr      ; s = $7F56
    ldd     4,x                 ; d = return
    pshs    b,a                 ; $7F54-5 = return, s = $7F54
    ldd     ,x                  ; x = old_x
    ldx     2,x                 ; d = old_d
    ;
    pshs    x,b
    ldb     #PHYSICAL_070000_071FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    mmu+LOGICAL_0000_1FFF,pcr
    stb     ,x
    puls    b,x
    ;
    lbsr    UpdateRomCursorPosition
    ;
    rts

StartGameLoop:
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    lbsr    SavePalette
    jsr     InitializeState
@levelSelectionLoop
    jsr     GoToLevelSelectionScreen
@levelLoop
    lbsr    DisableSplitScreen
    jsr     DisplayLevelLoadingScreen
    lbsr    InitializeLevel
    tst     shouldSkipInitialPrerenderLevel
    beq     @continue
    clr     shouldSkipInitialPrerenderLevel
    bra     @donePrerenderingVerticalSlices
@continue
    * lbsr    PrenderAllTilesForDebug
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    clre
    clrb
    jsr     PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    jsr     PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    jsr     PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #4
    jsr     PrerenderLevel256ByteVerticalSlice
    ;
    clre
    adda    #16
    jsr     PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    jsr     PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    jsr     PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #4
    jsr     PrerenderLevel256ByteVerticalSlice
    ;
@donePrerenderingVerticalSlices
    jsr     WaitForVsync
    ;
    lda     currentLevel
    jsr     SetEgaPalette
    jsr     Clear80x24Line
    jsr     Set320x200x16_GraphicsMode
    jsr     Enable256ByteWideMode
    jsr     EnableSplitScreen
    lda     #tunes.levelSelectAndNewLevel.index
    jsr     StartTunePlaying
    jsr     FadeHeroInWithSparklesAndOpenEyes
    clr     <vsyncCount
@gameLoop
    ;
    tst     bcdGems
    bne     @skipWinLevel
    tst     currentJumpHeight+1
    bne     @skipWinLevel
    jsr     WinLevel
    lbra    @levelSelectionLoop
@skipWinLevel
    lbsr    CyclePalettes
    jsr     ApplyFriction
    jsr     GetControlsPressed
    jsr     HandlePauseAccelerateJumpOpenDoor
    jsr     UpdateGameState
    jsr     UpdateScreen
    lda     <vsyncCount
    sta     debugData+DebugData.lastFrameVsyncCount
    ldd     <firqCount
    std     debugData+DebugData.lastFrameFIRQCount
    ;lda     #%00000000      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    ;sta     GIME_BorderColor_FF9A
    ;
@timingDelay
    jsr     WaitForVsync
    ;jsr     ClearScoreForDebug
    lda     <vsyncCount
    cmpa    #GAME_FRAME_VSYNCS
    blt     @timingDelay
    clr     <vsyncCount
    ;lda     #%00100100      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    ;sta     GIME_BorderColor_FF9A
    lbsr    SwapVideoBuffers
    lbsr    RestoreOffScreenSpriteBackgrounds
    lbsr    ProgressTuneIfPlaying
@checkQuit
    tst     shouldQuit,pcr
    lbne    @quitGame
@checkCycleNextLevel
    tst     shouldCycleToNextLevel
    beq     @checkCyclePreviousLevel
    lda     currentLevel
    inca
    anda    #$1F
    sta     currentLevel
@checkCyclePreviousLevel
    tst     shouldCycleToPreviousLevel
    beq     @checkReloadLevel
    lda     currentLevel
    deca
    anda    #$1F
    sta     currentLevel
@checkReloadLevel
    tst     shouldReloadLevel
    beq     @checkDebugLT
    lbra    @levelLoop
@checkDebugLT
    tst     shouldDebugLT,pcr
    beq     @checkDebugGT
    ldd     highSpeedTimerCount+1,pcr
    decd
    std     highSpeedTimerCount+1
@checkDebugGT
    tst     shouldDebugGT,pcr
    beq     @doneCheckingKeys
    ldd     highSpeedTimerCount+1,pcr
    incd
    std     highSpeedTimerCount+1,pcr
@doneCheckingKeys
    lbra    @gameLoop
    ;
@quitGame
    lbsr    ClearGameKeys
    lbsr    SwapVideoBuffers
    lbsr    RestoreOffScreenSpriteBackgrounds
    lbsr    RestorePalette
    ;
@done
    pulsw
    puls    a,b,x,y,pc

DecrementHammerWingSparkleTimeRemainingIfNeeded
    inc     constantlyCyclingWord+1
    bne     @doneIncrementing
    inc     constantlyCyclingWord
@doneIncrementing
    ;
    tst     hammerTimer
    beq     @doneHammerTimer
    dec     hammerTimer
@doneHammerTimer
    tst     wingTimer
    beq     @doneWingTimer
    dec     wingTimer
@doneWingTimer
    tst     sparkleTimer
    beq     @doneSparkleTimer
    dec     sparkleTimer
@doneSparkleTimer
    tst     timerForTimeRemainingDecrementActive
    beq     @doneTimeRemainingTimer
    dec     timerForTimeRemainingDecrement
    bne     @doneTimeRemainingTimer
    lda     #1
    ldx     #bcdTimeRemaining
    lbsr    SubtractFromSingleByteBcd
    lda     #$50
    sta     timerForTimeRemainingDecrement
@doneTimeRemainingTimer
    rts

UpdateGameState
    jsr     GetHerosCurrentMapCellData
    clr     heroIsOnMagnet
    clr     heroIsDead
    ldx     #heroActionLookup
    jsr     [a,x]
    jsr     TurnAndMoveAllBaddies
    jsr     CheckHeroTouchedByBaddie
    jsr     DecrementHammerWingSparkleTimeRemainingIfNeeded
    jsr     MoveHeroAccordingToSpeed
    jsr     UpdatePlayfieldTopLeftBasedOnHeroLocation
    jsr     BounceHeroIfTouchingWallsOrBumpers
    jsr     PlayWingTuneAndProgressJumpSequenceIfNeeded
    rts

UpdateScreen
;***********************
    jsr     UpdateScroll
    jsr     UpdateScore
    jsr     UpdateSprites
    jsr     UpdateNextWingFrameIfWingsActive
    jsr     UpdateNextSparkleFrameIfSparklesActive
    jsr     DrawTilesFromMapToOffScreenBufferIfNeeded
    jsr     SaveOffScreenSpriteBackgrounds
    jsr     DrawOffScreenSprites
    ;
    rts

CyclePalettes
    pshs    x,b,a
    ;
    inc     @paletteCycleTimer
    lda     @paletteCycleTimer
    bita    #1
    beq     @done
    ;
    ldx     #ColorPaletteFirstRegister_FFB0
    lda     5,x
    ldb     13,x
    stb     5,x
    sta     13,x
    ;
@done
    puls    a,b,x,pc
@paletteCycleTimer          fcb 0

jumpHeights                 fcb 0,1,1,0,1,2,3,3,2,1,0,1,2,3,3,3,4,4,4,4,3,3,3,2,1
currentJumpIndex            fcb 0
currentJumpHeight           fdb 0

PlayWingTuneAndProgressJumpSequenceIfNeeded
    pshs    x,a
    ;
    tst     wingTimer
    beq     @progressJumpSequenceIfNeeded
    tst     tunes.tonesRemaining
    bne     @doneStartingWingTune
    lda     #tunes.hitWingItem.index
    jsr     StartTunePlaying
@doneStartingWingTune
    lda     currentJumpHeight+1
    cmpa    #4
    beq     @done
    ;
@progressJumpSequenceIfNeeded
    tst     currentJumpIndex
    beq     @done
    dec     currentJumpIndex
    ldx     #jumpHeights
    lda     currentJumpIndex
    lda     a,x
    sta     currentJumpHeight+1
    bne     @done
    lda     #tunes.land.index
    lbsr    StartTuneIfNoTuneAlreadyPlaying
@done
    ;
    puls    a,x,pc

spriteCount                 equ 7
spriteAddresses             fdb hero,heroShadow,baddies,baddies+sizeof{Baddie},baddies+sizeof{Baddie}*2,baddies+sizeof{Baddie}*3,baddies+sizeof{Baddie}*4

SaveOffScreenSpriteBackground
; ```plaintext
; > a = Sprite index
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #spriteAddresses
    lsla
    ldx     a,x
    ;
    lda     SpriteBase.isActive,x
    lbeq    @doneWithSprite
    ;
    ldd     SpriteBase.x,x
    addd    #32
    cmpd    <playfieldTopLeftX
    lble    @doneWithSprite
    subd    #(320+32)
    cmpd    <playfieldTopLeftX
    lbgt    @doneWithSprite
    ;
    ldd     SpriteBase.y,x
    addd    #32
    cmpd    <playfieldTopLeftY
    lble    @doneWithSprite
    subd    #(143+32)
    cmpd    <playfieldTopLeftY
    lbgt    @doneWithSprite
    ;
    ldy     SpriteBase.offScreenBackgroundAddress,x
    oim     1;SpriteBackground.isCaptured,y
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    ldb     #LOGICAL_8000_9FFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    inca
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    ldd     SpriteBackground.byteOffset,y
    leay    SpriteBackground.buffer,y
    ldx     #$8000
    leax    d,x
    ;
* #region Blit buffer to background
    ldw     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    leax    256-16,x
    ldf     #16
    tfm     x+,y+
    ;
* #endregion
    ;
    ldb     #LOGICAL_A000_BFFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    decb
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
@doneWithSprite
    ;
    pulsw
    puls    a,b,x,y,pc

SaveOffScreenSpriteBackgrounds
    pshs    a
    ;
    clra
@spriteLoop
    lbsr    SaveOffScreenSpriteBackground
    inca
    cmpa    #spriteCount
    blt     @spriteLoop
    ;
    puls    a,pc

RestoreOffScreenSpriteBackground
; ```plaintext
; > a = Sprite index
; ```
;***********************
    ; If sprite.isActive && sprite.background.isCaptured
    ;     Map $8000-$9FFF to sprite.background.firstOfTwoPhysicalBlocks
    ;     Map $A000-$BFFF to sprite.background.firstOfTwoPhysicalBlocks + 1
    ;     Restore sprite's background starting at $8000 + sprite.background.byteOffset
    ; End If
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #spriteAddresses
    lsla
    ldx     a,x
    ;
    lda     SpriteBase.isActive,x
    lbeq    @doneWithSprite
    ldy     SpriteBase.offScreenBackgroundAddress,x
    lda     SpriteBackground.isCaptured,y
    lbeq    @doneWithSprite
    clr     SpriteBackground.isCaptured,y
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    ldb     #LOGICAL_8000_9FFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    inca
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    ldd     SpriteBackground.byteOffset,y
    leay    SpriteBackground.buffer,y
    ldx     #$8000
    leax    d,x
    ;
* #region Blit background to buffer
    ;
    ldw     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    leax    256-16,x
    ldf     #16
    tfm     y+,x+
    ;
* #endregion
    ;
    ldb     #LOGICAL_A000_BFFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    decb
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
@doneWithSprite
    ;
    pulsw
    puls    a,b,x,y,pc

RestoreOffScreenSpriteBackgrounds
    pshs    a
    ;
    clra
@spriteLoop
    lbsr    RestoreOffScreenSpriteBackground
    inca
    cmpa    #spriteCount
    blt     @spriteLoop
    ;
    puls    a,pc

screenBuffers                       rmb sizeof{ScreenBuffer}*2
onScreenBuffer                      fdb screenBuffers
offScreenBuffer                     fdb screenBuffers+sizeof{ScreenBuffer}

UpdateScroll
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldw     offScreenBuffer,pcr
    ldb     ScreenBuffer.firstPhysicalBlockOfBuffer,w
    ;
    clra
    tfr     d,y
            * ; DEBUG START
            * pshs    y,x,b,a
            * pshsw
            * ldx     #debugData+DebugData.data
            * ldb     debugDataIndex
            * lde     #$F9
            * ste     b,x
            * incb
            * ldy     6,s
            * sty     b,x     ; log ScreenBuffer.firstPhysicalBlockOfBuffer
            * incb
            * incb
            * stb     debugDataIndex
            * pulsw
            * puls    a,b,x,y
            * ; DEBUG END
    clr     ScreenBuffer.leftPlayfieldXInSlice,w
    clr     ScreenBuffer.leftPlayfieldXInSlice+1,w
    ldd     <playfieldTopLeftX
    andb    #%11111100              ; We can only scroll horizontally to multiples of 4 x-coordinates, so zero out bits 0 and 1.
@checkForFirstSlice
    cmpd    #192                    ; Are we in slice 1? 192 = first x-coordinate of slice 2
    bge     @checkForSecondSlice    ; Nope, check if we're in the next slice
    ldx     #$0000                  ; We're in slice 1.  0 <= d <= 191.  d ÷ 2 = GIME Horizontal Offset.  $0000 = GIME Vertical Offset.
    bra     @updateScreen
@checkForSecondSlice
    cmpd    #384                    ; Are we in slice 2? 384 = first x-coordinate of slice 3
    bge     @checkForThirdSlice     ; Nope, check if we're in the next slice
    subd    #192                    ; We're in slice 2.  192 <= d <= 383.  (d-192) ÷ 2 = GIME Horizontal Offset.  $4000 = GIME Vertical Offset.
    ldx     #192
    stx     ScreenBuffer.leftPlayfieldXInSlice,w
    ldx     #$4000
    leay    $10,y
    bra     @updateScreen
@checkForThirdSlice
    cmpd    #512                    ; Are we in slice 3? 512 = first x-coordinate of slice 4
    bge     @fourthSlice            ; Nope, we're in slice 4
    subd    #384                    ; We're in slice 3.  384 <= d <= 511.  (d-384) ÷ 2 = GIME Horizontal Offset.  $8000 = GIME Vertical Offset.
    ldx     #384
    stx     ScreenBuffer.leftPlayfieldXInSlice,w
    ldx     #$8000
    leay    $20,y
    bra     @updateScreen
@fourthSlice
    subd    #512                    ; We're in slice 4.  512 <= d <= 704.  (d-512) ÷ 2 = GIME Horizontal Offset. $C000 = GIME Vertical Offset.
    ldx     #512
    stx     ScreenBuffer.leftPlayfieldXInSlice,w
    ldx     #$C000
    leay    $30,y
    ;
@updateScreen                       ; b = Horizontal Offset * 2, X = Vertical Offset for top of slice, @videoBank = Video Bank Select, y = first physical block of slice.
    exg     y,d
    stb     ScreenBuffer.firstPhysicalBlockOfVerticalStripe,w
    exg     y,d
    lsrb
    lsrb                            ; b = # pixels from left of vertical slice ÷ by 4 (horizontal offset is multiple of 2 bytes)
    stb     ScreenBuffer.horizontalOffset,w
    ldd     <playfieldTopLeftY      ; GIME Vertical Offset adjusts the start address of video in 8-byte increments.  256 ÷ 8 = 32.  We need to add 32 to Vertical Offset for each Playfield Y.
    lsld
    lsld
    lsld
    lsld
    lsld
    leax    d,x
    stx     ScreenBuffer.verticalOffset,w
    ;
    pulsw
    puls    a,b,x,y,pc

UpdateSprites
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldx     hero+SpriteBase.x
    stx     heroShadow+SpriteBase.x
    * lda     #'a
    * jsr     PrintChar80x24
    * jsr     PrintChar80x24
    * tfr     x,d
    * jsr     PrintHexWordWithSpace
    ldx     hero+SpriteBase.y
    stx     heroShadow+SpriteBase.y
    clre
@nextSprite
    ;
    ldu     offScreenBuffer
    ldy     #spriteAddresses
    tfr     e,a
    lsla
    ldy     a,y
    ;
    lda     SpriteBase.isActive,y
    lbeq    @doneWithSprite
    ;
    ldx     SpriteBase.offScreenBackgroundAddress,y
    ; Calculate first and second physical blocks and map them to $8000-$9FFF and $A000-$BFFF
    ldd     SpriteBase.y,y
    cmpe    #1
    ble     @updateHeroSprite
@updateBaddieSprite
    lsld
    lsld
    lsld                                        ; a = sprite.y ÷ 32
    adda    ScreenBuffer.firstPhysicalBlockOfVerticalStripe,u   ; a = firstPhysicalBlockOfVerticalStripe + (sprite.y ÷ 32)
    sta     SpriteBackground.firstOfTwoPhysicalBlocks,x
    ;
    ; Calculate sprite's byte offset from top of first block based on sprite.x, sprite.y
    ; (sprite.y & $001F) * 256 + (sprite.x - leftPlayfieldXInSlice) ÷ 2
    ldd     SpriteBase.x,y
    subd    ScreenBuffer.leftPlayfieldXInSlice,u
    lsrd
    pshs    b
    ldd     SpriteBase.y,y
    tfr     b,a
    puls    b
    anda    #$1F
    std     SpriteBackground.byteOffset,x
    bra     @doneWithSprite
@updateHeroSprite
    ldd     currentJumpHeight
    tste
    beq     @doneAdjustJumpHeight
    negd
@doneAdjustJumpHeight
    std     @jumpHeight,pcr
    ldd     SpriteBase.y,y
    subd    @jumpHeight,pcr
    subd    @jumpHeight,pcr
    lsld
    lsld
    lsld                                        ; a = sprite.y ÷ 32
    adda    ScreenBuffer.firstPhysicalBlockOfVerticalStripe,u   ; a = firstPhysicalBlockOfVerticalStripe + (sprite.y ÷ 32)
    sta     SpriteBackground.firstOfTwoPhysicalBlocks,x
    ;
    ; Calculate sprite's byte offset from top of first block based on sprite.x, sprite.y
    ; (sprite.y & $001F) * 256 + (sprite.x - leftPlayfieldXInSlice) ÷ 2
    ldd     SpriteBase.x,y
    subd    @jumpHeight,pcr
    subd    ScreenBuffer.leftPlayfieldXInSlice,u
    lsrd
    pshs    b
    ldd     SpriteBase.y,y
    subd    @jumpHeight,pcr
    subd    @jumpHeight,pcr
    tfr     b,a
    puls    b
    anda    #$1F
    std     SpriteBackground.byteOffset,x
    ;
@doneWithSprite
    ince
    cmpe    #spriteCount
    lblt    @nextSprite
    ;
    pulsw
    puls    a,b,x,y,pc
@jumpHeight         fdb   $0000

DrawOffScreenSprite
; ```plaintext
; > a = Sprite index
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #spriteAddresses
    lsla
    ldx     a,x
    ;
    lda     SpriteBase.isActive,x
    lbeq    @doneWithSprite
    ;
    ldy     SpriteBase.offScreenBackgroundAddress,x
    ldw     offScreenBuffer
    ldd     SpriteBase.x,x
    addd    #32
    cmpd    <playfieldTopLeftX
    lble    @doneWithSprite
    subd    #(320+32)
    cmpd    <playfieldTopLeftX
    lbgt    @doneWithSprite
    ;
    ldd     SpriteBase.y,x
    addd    #32
    cmpd    <playfieldTopLeftY
    lble    @doneWithSprite
    subd    #(143+32)
    cmpd    <playfieldTopLeftY
    lbgt    @doneWithSprite
    ;
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    ldb     #LOGICAL_8000_9FFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     SpriteBackground.firstOfTwoPhysicalBlocks,y
    inca
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     SpriteBase.compiledPhysicalBlock,x
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ;
    ldd     SpriteBackground.byteOffset,y
    tfr     d,u
    leau    $8000,u
    pshs    u
    jsr     [SpriteBase.compiled,x]
    puls    u
    tst     5,s                         ; Original pushed a (Sprite index)
    bne     @doneHeroAdornments
    ;
    jsr     DrawWingsIfNeeded
    jsr     DrawSparklesIfNeeded
    ;
@doneHeroAdornments
    puls    a
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ldb     #LOGICAL_A000_BFFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    decb
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
@doneWithSprite
    ;
    pulsw
    puls    a,b,x,y,pc

DrawWingsIfNeeded
    tst     wingTimer
    beq     @done
    ;
    lda     #HERO_ACTION_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ;
    ldx     #wingFrames
    lda     wingFrameIndex
    lsla
    pshs    u
    jsr     [a,x]
    puls    u
    ;
    puls    a
    lda     #HERO_ACTION_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
@done
    rts

DrawSparklesIfNeeded
    tst     sparkleTimer
    beq     @done
    ;
    lda     #HERO_ACTION_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ;
    ldx     #sparkleFrames
    lda     sparkleFrameIndex
    lsla
    pshs    u
    jsr     [a,x]
    puls    u
    ;
    puls    a
    lda     #HERO_ACTION_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
@done
    rts

DrawOffScreenSprites
    pshs    a
    ;
    lda     #2
@spriteLoop
    lbsr    DrawOffScreenSprite
    inca
    cmpa    #spriteCount
    blt     @spriteLoop
    jsr     DrawOffScreenHeroAndShadowWithAdornments
    ;
    puls    a,pc

DrawOffScreenSpritesWithoutHeroShadow
    pshs    a
    ;
    lda     #2
@spriteLoop
    lbsr    DrawOffScreenSprite
    inca
    cmpa    #spriteCount
    blt     @spriteLoop
    jsr     DrawOffScreenHeroWithAdornmentsWithoutShadow
    ;
    puls    a,pc

DrawOffScreenHeroAndShadowWithAdornments
    pshs    a
    ;
    lda     #1
    jsr     DrawOffScreenSprite     ; Draw Shadow
    deca
    jsr     DrawOffScreenSprite     ; Draw Hero and any adornments
    ;
    puls    a,pc

DrawOffScreenHeroWithAdornmentsWithoutShadow
    pshs    a
    ;
    clra
    jsr     DrawOffScreenSprite     ; Draw Hero and any adornments
    ;
    puls    a,pc


UpdateScore
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    lde     #$C0                                ; First block in 4th bank of 512K
    ldd     <playfieldTopLeftX
    cmpd    #192
    blt     @gotStartBlock
    adde    #16
    cmpd    #384
    blt     @gotStartBlock
    adde    #16
    cmpd    #512
    blt     @gotStartBlock
    adde    #16
@gotStartBlock
    ;
    ldd     <playfieldTopLeftY
    subd    <previousFramePlayfieldTopLeftY
    std     @verticalDifferenceFromLastFrame
    ldd     <playfieldTopLeftY
    addd    #143                                ; # lines down from current Y where score starts
    subd    @verticalDifferenceFromLastFrame
    ;
    lsrd                                        ; ÷ 2 (shifts bit 0 of a into bit 7 of b)
    lsrb                                        ; ÷ 4
    lsrb                                        ; ÷ 8
    lsrb                                        ; ÷ 16
    lsrb                                        ; b = low 9 bits of (playfieldTopLeftY + 143) ÷ 32 (# of 8K blocks (8k ÷ 512 bytes per line = 16) our score starts in)
    addr    e,b
    tfr     b,a                                 ; a = Physical block where our Y starts
    ;
    ldb     #LOGICAL_A000_BFFF
    tfr     a,e
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    tfr     e,a
    ;
    inca
    bne     @continue
    lda     #$C0
@continue
    ldb     #LOGICAL_C000_DFFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    ;
    ldd     <playfieldTopLeftY
    addd    #143
    subd    @verticalDifferenceFromLastFrame
    andb    #$1F
    tfr     b,a
    clrb
    ldx     #$A000
    leax    d,x
    leay    scoreTemplate,pcr
    ldw     #80*7
    tfm     y+,x+
    ;
    leax    -80*7,x
    jsr     PrintBcdValues
    jsr     DrawScoreBar
    ;
    puls    a
    ldb     #LOGICAL_C000_DFFF
    jsr     MapPhysicalBlockToLogicalBlock
    puls    a
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc
@verticalDifferenceFromLastFrame    fdb $0000

ClearScoreForDebug
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    lde     #$C0                                ; First block in 4th bank of 512K
    ldd     <playfieldTopLeftX
    cmpd    #192
    blt     @gotStartBlock
    adde    #16
    cmpd    #384
    blt     @gotStartBlock
    adde    #16
    cmpd    #512
    blt     @gotStartBlock
    adde    #16
@gotStartBlock
    ;
    ldd     <playfieldTopLeftY
    subd    <previousFramePlayfieldTopLeftY
    std     @verticalDifferenceFromLastFrame
    ldd     <playfieldTopLeftY
    addd    #143                                ; # lines down from current Y where score starts
    subd    @verticalDifferenceFromLastFrame
    ;
    lsrd                                        ; ÷ 2 (shifts bit 0 of a into bit 7 of b)
    lsrb                                        ; ÷ 4
    lsrb                                        ; ÷ 8
    lsrb                                        ; ÷ 16
    lsrb                                        ; b = low 9 bits of (playfieldTopLeftY + 143) ÷ 32 (# of 8K blocks (8k ÷ 512 bytes per line = 16) our score starts in)
    addr    e,b
    tfr     b,a                                 ; a = Physical block where our Y starts
    ;
    ldb     #LOGICAL_A000_BFFF
    tfr     a,e
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    tfr     e,a
    ;
    inca
    bne     @continue
    lda     #$C0
@continue
    ldb     #LOGICAL_C000_DFFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    ;
    ldd     <playfieldTopLeftY
    addd    #143
    subd    @verticalDifferenceFromLastFrame
    andb    #$1F
    tfr     b,a
    clrb
    ldx     #$A000
    leax    d,x
    clra
    pshs    a
    ldw     #80*7
    tfm     s,x+
    puls    a
    ;
    puls    a
    ldb     #LOGICAL_C000_DFFF
    jsr     MapPhysicalBlockToLogicalBlock
    puls    a
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc
@verticalDifferenceFromLastFrame    fdb $0000

PrintBcdValues
; ```plainttext
; > x = logical address of first byte of text score area
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ; Print Timer
    leax    80*1+17*2,x
    lda     bcdTimeRemaining,pcr
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #$3030
    sta     ,x++
    stb     ,x
    ;
    ; Print Gems
    leax    80*2+-9*2,x
    lda     bcdGems,pcr
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #$3030
    sta     ,x++
    stb     ,x
    ;
    ; Print Score
    leax    10*2,x
    leay    bcdScore,pcr
    lda     ,y+
    anda    #$0F
    adda    #$30
    sta     ,x++
    lde     #3
@loop
    lda     ,y+
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #$3030
    sta     ,x++
    stb     ,x++
    dece
    bne     @loop
    ;
    ; Print Keys
    leax    7*2,x
    lda     bcdKeys,pcr
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #$3030
    sta     ,x++
    stb     ,x
    ;
    pulsw
    puls    a,b,x,y,pc

DrawScoreBar
    pshs    x,b,a
    pshsw
    ;
    leax    80*5+8*2,x
    lda     health
    lsra
    lsra
    clrb
@loop
    cmpr    a,b
    bge     @black
@green
    ldw     #$2032
    bra     @setBlock
@black
    ldw     #$2000
@setBlock
    stw     ,x++
    incb
    cmpb    #25
    blt     @loop
    ;
    pulsw
    puls    a,b,x,pc

AddToBcdScore
; ```plaintext
; > d = BCD number to add to score (0-9999)
; ```
;***********************
    pshs    x,b,a
    ;
    leax    bcdScore+3,pcr
    lda     ,x
    adda    1,s
    daa
    sta     ,x
    lda     ,-x
    adca    ,s
    daa
    sta     ,x
    lda     ,-x
    adca    #0
    daa
    sta     ,x
    lda     ,-x
    adca    #0
    daa
    sta     ,x
    ;
    puls    a,b,x,pc

SubtractFromBcdScore
; ```plaintext
; > a = BCD number to subtract from score (1-99; Cannot subtract 0)
; ```
;***********************
    pshs    x,a
    ;
    lda     #$99
    suba    ,s
    inca
    daa
    pshs    a
    ;
    leax    bcdScore+3,pcr
    lda     ,x
    adda    ,s+
    daa
    sta     ,x
    lda     ,-x
    adca    #$99
    daa
    sta     ,x
    lda     ,-x
    adca    #$99
    daa
    sta     ,x
    lda     ,-x
    adca    #$99
    daa
    sta     ,x
    ;
    puls    a,x,pc

AddToSingleByteBcd
; ```plaintext
; > x = Address of BCD number to add to
; > a = BCD number to add (0-99)
; ```
;***********************
    pshs    a
    ;
    lda     ,x
    adda    ,s
    daa
    sta     ,x
    ;
    puls    a,pc

SubtractFromSingleByteBcd
; ```plaintext
; > x = Address of BCD number to subtract from
; > a = BCD number to subtract (1-99; Cannot subtract 0)
; ```
;***********************
    pshs    a
    ;
    lda     #$99
    suba    ,s
    inca
    daa
    pshs    a
    ;
    lda     ,x
    adda    ,s+
    daa
    sta     ,x
    ;
    puls    a,pc

SwapVideoBuffers
    pshs    y,x,b,a
    ;
    clra
    pshs    a
@nextSprite
    ldx     #spriteAddresses
    lsla
    ldx     a,x
    ldd     SpriteBase.onScreenBackgroundAddress,x
    ldy     SpriteBase.offScreenBackgroundAddress,x
    std     SpriteBase.offScreenBackgroundAddress,x
    sty     SpriteBase.onScreenBackgroundAddress,x
    inc     ,s
    lda     ,s
    cmpa    #spriteCount
    blt     @nextSprite
    puls    a
    ;
    ldx     onScreenBuffer,pcr
    ldy     offScreenBuffer,pcr
    stx     offScreenBuffer,pcr
    sty     onScreenBuffer,pcr
    ;
    lda     ScreenBuffer.videoBank,y
    sta     GIME_VideoBankSelect_FF9B
    ldd     ScreenBuffer.verticalOffset,y
    std     GIME_VerticalOffset1_FF9D
    ;         ┌────────── Bit  7   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%10000001
    sta     GIME_VideoMode_FF98
    ;         ┌────────── Bit  7   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%00111110
    sta     GIME_VideoResolution_FF99
    ldb     ScreenBuffer.horizontalOffset,y
    orb     #%10000000;             ; Keep HVEN (bit 7) of GIME Horizontal Offset set to keep us in 256-byte wide mode.
    stb     GIME_HorizontalOffset_FF9F
    ;
    puls    a,b,x,y,pc

DisplayLevelLoadingScreen
;***********************
    pshs    x,a
    ;
    lbsr    RestorePalette
    lbsr    Set80x24TextMode
    leax    @loadingLevel,pcr
    lbsr    PrintString
    lda     currentLevel
    lbsr    PrintHexByte
    ;
    puls    a,x,pc
@loadingLevel                   fcn "Loading level $"

GetControlsPressed
;***********************
    lbsr    ClearGameKeys
    ldx     #keyColumnScans
    ldb     #1
@checkShiftLeft
    lda     KEYSCAN_COLUMN_Shifts,x
    anda    #KEYSCAN_ROW_Shifts
    beq     @checkShiftRight
    lda     KEYSCAN_COLUMN_LeftArrow,x
    anda    #KEYSCAN_ROW_LeftArrow
    beq     @checkShiftRight
    lda     KEYSCAN_COLUMN_Shifts+8,x
    anda    #KEYSCAN_ROW_Shifts
    beq     @shiftLeftNotDownLastFrame
    lda     KEYSCAN_COLUMN_LeftArrow+8,x
    anda    #KEYSCAN_ROW_LeftArrow
    beq     @shiftLeftNotDownLastFrame
    bra     @checkShiftRight
@shiftLeftNotDownLastFrame
    stb     shouldCycleToPreviousLevel
    lbra     @doneCheckingKeys
@checkShiftRight
    lda     KEYSCAN_COLUMN_Shifts,x
    anda    #KEYSCAN_ROW_Shifts
    beq     @checkShiftR
    lda     KEYSCAN_COLUMN_RightArrow,x
    anda    #KEYSCAN_ROW_RightArrow
    beq     @checkShiftR
    lda     KEYSCAN_COLUMN_Shifts+8,x
    anda    #KEYSCAN_ROW_Shifts
    beq     @shiftRightNotDownLastFrame
    lda     KEYSCAN_COLUMN_RightArrow+8,x
    anda    #KEYSCAN_ROW_RightArrow
    beq     @shiftRightNotDownLastFrame
    bra     @checkShiftR
@shiftRightNotDownLastFrame
    stb     shouldCycleToNextLevel
    lbra    @doneCheckingKeys
@checkShiftR
    lda     KEYSCAN_COLUMN_Shifts,x
    anda    #KEYSCAN_ROW_Shifts
    beq     @checkP
    lda     KEYSCAN_COLUMN_R,x
    anda    #KEYSCAN_ROW_R
    beq     @checkP
    lda     KEYSCAN_COLUMN_Shifts+8,x
    anda    #KEYSCAN_ROW_Shifts
    beq     @shiftRNotDownLastFrame
    lda     KEYSCAN_COLUMN_R+8,x
    anda    #KEYSCAN_ROW_R
    beq     @shiftRNotDownLastFrame
    bra     @checkP
@shiftRNotDownLastFrame
    stb     shouldReloadLevel
    lbra     @doneCheckingKeys
@checkP
    lda     KEYSCAN_COLUMN_P,x
    anda    #KEYSCAN_ROW_P
    beq     @checkF1
    lda     KEYSCAN_COLUMN_P+8,x    ; Check if P was down last game frame.
    anda    #KEYSCAN_ROW_P
    bne     @checkF1                ; If it was, don't trigger it again.  Don't want to cycle pause/unpause rapidly because P was held down for a millisecond.
    stb     shouldPauseUnpause
@checkF1
    lda     KEYSCAN_COLUMN_F1,x
    anda    #KEYSCAN_ROW_F1
    beq     @checkF2
    lda     KEYSCAN_COLUMN_F1+8,x   ; Check if F1 was down last game frame.
    anda    #KEYSCAN_ROW_F1
    bne     @checkF2           ; If it was, don't trigger it again.  Don't want to cycle toggle no fall cheat rapidly because F1 was held down for a millisecond.
    stb     shouldToggleNoFallCheat
@checkF2
    lda     KEYSCAN_COLUMN_F2,x
    anda    #KEYSCAN_ROW_F2
    beq     @checkUpArrow
    lda     KEYSCAN_COLUMN_F2+8,x   ; Check if F2 was down last game frame.
    anda    #KEYSCAN_ROW_F2
    bne     @checkUpArrow           ; If it was, don't trigger it again.  Don't want to cycle toggle no damage cheat rapidly because F2 was held down for a millisecond.
    stb     shouldToggleNoDamageCheat
@checkUpArrow
    lda     KEYSCAN_COLUMN_UpArrow,x
    anda    #KEYSCAN_ROW_UpArrow
    beq     @checkDownArrow
    oim     Controls.Up;controls
@checkDownArrow
    lda     KEYSCAN_COLUMN_DownArrow,x
    anda    #KEYSCAN_ROW_DownArrow
    beq     @checkLeftArrow
    oim     Controls.Down;controls
@checkLeftArrow
    lda     KEYSCAN_COLUMN_LeftArrow,x
    anda    #KEYSCAN_ROW_LeftArrow
    beq     @checkRightArrow
    oim     Controls.Left;controls
@checkRightArrow
    lda     KEYSCAN_COLUMN_RightArrow,x
    anda    #KEYSCAN_ROW_RightArrow
    beq     @checkQ
    oim     Controls.Right;controls
@checkQ
    lda     KEYSCAN_COLUMN_Q,x
    anda    #KEYSCAN_ROW_Q
    beq     @checkSpace
    stb     shouldQuit
@checkSpace
    lda     KEYSCAN_COLUMN_Space,x
    anda    #KEYSCAN_ROW_Space
    beq     @checkLT
    oim     Controls.Fire;controls
* @checkW
*     lda     KEYSCAN_COLUMN_W,x
*     anda    #KEYSCAN_ROW_W
*     beq     @checkLT
*     stb     shouldCycleToNextWaveTable
@checkLT
    lda     KEYSCAN_COLUMN_CommaLessThan,x
    anda    #KEYSCAN_ROW_CommaLessThan
    beq     @checkGT
    lda     KEYSCAN_COLUMN_CommaLessThan+8,x    ; Check if ,< was down last game frame.
    anda    #KEYSCAN_ROW_CommaLessThan
    bne     @checkGT                            ; If it was, don't trigger it again.
    stb     shouldDebugLT
@checkGT
    lda     KEYSCAN_COLUMN_DotGreaterThan,x
    anda    #KEYSCAN_ROW_DotGreaterThan
    beq     @doneCheckingKeys
    lda     KEYSCAN_COLUMN_DotGreaterThan+8,x   ; Check if .> was down last game frame.
    anda    #KEYSCAN_ROW_DotGreaterThan
    bne     @doneCheckingKeys                   ; If it was, don't trigger it again.
    stb     shouldDebugGT
@doneCheckingKeys
    ldd     ,x
    std     8,x
    ldd     2,x
    std     10,x
    ldd     4,x
    std     12,x
    ldd     6,x
    std     14,x
    rts

HandlePauseAccelerateJumpOpenDoor
    tst     shouldPauseUnpause
    beq     @checkJumpOpenDoor
    jsr     PauseGame
@checkJumpOpenDoor
    tim     Controls.Fire;controls
    beq     @doneJump
    jsr     JumpAndOpenDoor
@doneJump
    ldb     controls
    andb    #%1111      ; Mask off Fire bit (bit 4)
    lslb
    lslb
    ldx     #heroAccelerationAndDirectionInfo
    abx
    ;
    lda     hero+SpriteBase.speedX
    adda    HeroAccelerationAndDirection.accelerationX,x
    sta     hero+SpriteBase.speedX
    lda     hero+SpriteBase.speedY
    adda    HeroAccelerationAndDirection.accelerationY,x
    sta     hero+SpriteBase.speedY
    lda     HeroAccelerationAndDirection.looking,x
    cmpa    #-1
    beq     @doneSettingDirectionHeroIsLooking
    sta     directionHeroIsLooking
@doneSettingDirectionHeroIsLooking
    jsr     CapHeroSpeedAt8
    lda     HeroAccelerationAndDirection.heroIndex,x
    jsr     SetHeroCompiledSpriteAndOverrideIfHammerTimerActive
@done
    rts

CapHeroSpeedAt8
    lda     hero+SpriteBase.speedX
    cmpa    #8
    ble     @checkLowerCapX
    lda     #8
    sta     hero+SpriteBase.speedX
@checkLowerCapX
    cmpa    #-8
    bge     @checkUpperCapY
    lda     #-8
    sta     hero+SpriteBase.speedX
@checkUpperCapY
    lda     hero+SpriteBase.speedY
    cmpa    #8
    ble     @checkLowerCapY
    lda     #8
    sta     hero+SpriteBase.speedY
@checkLowerCapY
    cmpa    #-8
    bge     @done
    lda     #-8
    sta     hero+SpriteBase.speedY
@done
    rts

SetHeroCompiledSpriteAndOverrideIfHammerTimerActive
;***********************
; ```plaintext
; > a = Index into heroFrames
; ```
    ldb     hammerTimer
    beq     @setHeroCompiledSpriteIfIndexNotNegative
    cmpb    #Hero.EyesClose.FrameCount
    bhs     @notOpeningEyes
    lda     #Hero.EyesClose.Index
    addr    b,a                                                 ; Open the Hero's eyes (Hero first index + 3, 2, 1, 0) for the last Hero.EyesClose.FrameCount ticks of hammerTimer
    bra     @setHeroCompiledSpriteIfIndexNotNegative
@notOpeningEyes
    lda     #Hero.EyesClose.Index+Hero.EyesClose.FrameCount-1   ; Index of Hero.EyesClosed frame
    cmpb    #Hammer.TimerTicks-Hero.EyesClose.FrameCount+1      ; Hero.EyesClose.FrameCount ticks after HammerTimer starts
    blo     @setHeroCompiledSpriteIfIndexNotNegative
    lda     #Hammer.TimerTicks
    subr    b,a
    adda    #Hero.EyesClose.Index                               ; Close the Hero's eyes (Hero last index - 0, 1, 2, 3) for the last Hero.EyesClose.FrameCount ticks of HammerTimer
@setHeroCompiledSpriteIfIndexNotNegative
    tsta
    bmi     @done
    ldb     #HERO_NORMAL_DATA_PHYSICAL_BLOCK
    cmpa    #Hero.EyesClose.Index
    blt     @gotCompiledSpritePhysicalBlock
    ldb     #HERO_ACTION_DATA_PHYSICAL_BLOCK
@gotCompiledSpritePhysicalBlock
    stb     hero+SpriteBase.compiledPhysicalBlock
    lsla
    ldx     #heroFrames
    ldd     a,x
    std     hero+SpriteBase.compiled
@done
    rts

JumpAndOpenDoor
    tst     bcdKeys
    beq     @makeHeroJumpUnlessOnMagnet
    lda     directionHeroIsLooking
    tfr     a,b
    addr    a,b
    addr    a,b                         ; b = offset into mapOffsetAndDirections for 3-byte MapOffsets structure for direction hero is looking
    ldx     #mapOffsetAndDirections
    abx
    pshs    x
    ldb     MapOffsets.mapOffset,x      ; b = offset in currentMap from the cell Hero is in to the cell the Hero is looking toward
    ldx     addressOfMapCellHeroIsOn
    lda     b,x                         ; a = Cell data of cell in currentMap that Hero is looking toward
    ldx     #mapDataToHeroActionIndexLookup
    lda     a,x                         ; a = Action index for cell data Hero is looking toward
    cmpa    #$0C                        ; $0C = Action index for doors
    beq     @openDoor
    puls    x
    bra     @makeHeroJumpUnlessOnMagnet
@openDoor
    ldw     addressOfMapCellHeroIsOn
    sex
    addr    d,w
    stw     addressOfMapCellHeroIsOn
    ldd     mapXHeroIsOn                ; a,b = Map X, Map Y Hero is on
    ldx     ,s
    adda    MapOffsets.xOffset,x        ; a = Map X + xOffset
    addb    MapOffsets.yOffset,x        ; b = MapY + yOffset
    std     mapXHeroIsOn
    ldx     #bcdKeys
    lda     #$01
    lbsr    SubtractFromSingleByteBcd
    lbsr    UpdateMapCellAndQueueTileRedrawing
    ldx     ,s++
    clra
    ldb     MapOffsets.mapOffset,x
    subr    d,w
    stw     addressOfMapCellHeroIsOn
    ldd     mapXHeroIsOn                ; a,b = Map X, Map Y Hero is on
    subd    MapOffsets.xOffset,x        ; a,b = Map X + xOffset, MapY + yOffset
    std     mapXHeroIsOn
    lda     #tunes.jumpOpenDoor.index
    lbsr    StartTuneIfNoTuneAlreadyPlaying
    ;
@makeHeroJumpUnlessOnMagnet
    tst     heroIsOnMagnet
    bne     @done
    tst     currentJumpIndex
    bne     @done
    lda     #25
    sta     currentJumpIndex
    lda     #tunes.jump.index
    lbsr    StartTuneIfNoTuneAlreadyPlaying
@done
    rts

MoveHeroAccordingToSpeed
    ;
    lda     hero+SpriteBase.speedX
    ldx     hero+SpriteBase.x
    leax    a,x
    cmpx    #8                      ; Keep Hero X 8 pixels from left side of Playfield
    bge     @doneLowerCapHeroX
    ldx     #8
    clr     hero+SpriteBase.speedX
@doneLowerCapHeroX
    cmpx    #1024-40-1              ; Keep Hero X 40 pixels from right side of Playfield
    ble     @doneUpperCapHeroX
    ldx     #1024-40-1
    clr     hero+SpriteBase.speedX
@doneUpperCapHeroX
    stx     hero+SpriteBase.x
    ;
    lda     hero+SpriteBase.speedY
    ldx     hero+SpriteBase.y
    leax    a,x
    cmpx    #8                      ; Keep Hero Y 8 pixels from top of Playfield
    bge     @doneLowerCapHeroY
    ldx     #8
    clr     hero+SpriteBase.speedY
@doneLowerCapHeroY
    cmpx    #512-40-1               ; Keep Hero Y 40 pixels from bottom of Playfield
    ble     @doneUpperCapHeroY
    ldx     #512-40-1
    clr     hero+SpriteBase.speedY
@doneUpperCapHeroY
    stx     hero+SpriteBase.y
    rts

UpdatePlayfieldTopLeftBasedOnHeroLocation
    ;
    ldx     hero+SpriteBase.x
    leax    -(32*5),x               ; Keep Playfield left X 5 tiles (32 * 5) pixels to the left of Hero's x
    cmpx    #0
    bge     @doneLowerCapPlayfieldX
    ldx     #0                      ; Don't let Playfield left X go below 0
@doneLowerCapPlayfieldX
    cmpx    #1024-320               ; Keep Playfield left X 320 pixels to the left of right side of the Playfield (1024)
    ble     @doneUpperCapPlayfieldX
    ldx     #1024-320               ; Don't let Playfield left X go above 1024-320
@doneUpperCapPlayfieldX
    stx     <playfieldTopLeftX      ; Update Playfield left X
    ;
    ldx     hero+SpriteBase.y
    leax    -(32*2),x               ; Keep Playfield top Y 2 tiles (32 * 2) pixels above Hero's y
    cmpx    #0
    bge     @doneLowerCapPlayfieldY
    ldx     #0                      ; Don't let Playfield top Y go below 0
@doneLowerCapPlayfieldY
    cmpx    #512-144-1              ; Keep Playfield top Y 144 pixels above bottom of Playfield
    ble     @doneUpperCapPlayfieldY
    ldx     #512-144-1              ; Don't let Playfield top Y go above 512-144
@doneUpperCapPlayfieldY
    ldy     <playfieldTopLeftY
    sty     <previousFramePlayfieldTopLeftY     ; Save Playfield top Y from last frame
    stx     <playfieldTopLeftY      ; Update Playfield top Y
    rts

PauseGame
    lda     #tunes.pause.index
    lbsr    #StartTuneIfNoTuneAlreadyPlaying
    clr     timerForTimeRemainingDecrementActive
    clr     shouldPauseUnpause
@pauseLoop
    clr     <vsyncCount
@timingDelay
    lbsr    WaitForVsync
    lda     <vsyncCount
    cmpa    #GAME_FRAME_VSYNCS
    blt     @timingDelay
    lbsr    ProgressTuneIfPlaying
    lbsr    GetControlsPressed
    tst     shouldToggleNoFallCheat
    bne     @toggleNoFallCheat
    tst     shouldToggleNoDamageCheat
    bne     @toggleNoDamageCheat
    tst     shouldPauseUnpause
    bne     @unpause
    bra     @pauseLoop
@toggleNoFallCheat
    eim     #1;cheat.HeroNoFall
    bra     @unpause
@toggleNoDamageCheat
    eim     #1;cheat.HeroNoDamage
@unpause
    lda     #tunes.pause.index
    lbsr    #StartTuneIfNoTuneAlreadyPlaying
    inc     timerForTimeRemainingDecrementActive
@done
    rts

BounceHeroIfTouchingWallsOrBumpers
    tst     wingTimer
    bne     @done
    ;
    lbsr    GetHeroActionIndexesForFourQuadrantsOfHero
    lda     #$0D        ; $0D = HeroHitBumperItem
    lbsr    GetFlagsForMatchingHeroActionIndexes
    beq     @heroNotTouchingBumpers
    cmpb    #$0F        ; Hero hit Bumpers in all 4 quadrants.  Kill the Hero.
    beq     @killHeroIfOnGround
    lda     #tunes.hitBumper.index
    lbsr    StartTuneIfNoTuneAlreadyPlaying
    ldx     #routinesToBounceHeroFullSpeed
    abx
    abx
    jsr     [,x]
    rts
@heroNotTouchingBumpers
    lda     heroActionIndexesForFourQuadrants       ; Action index for top left quadrant
    cmpa    #$0C        ; $0C = HeroHitWall for doors
    bne     @doneCheckTopLeftForDoor
    clr     heroActionIndexesForFourQuadrants       ; Change top left quadrant to HeroHitWall for walls
@doneCheckTopLeftForDoor
    lda     heroActionIndexesForFourQuadrants+1     ; Action index for top right quadrant
    cmpa    #$0C        ; $0C = HeroHitWall for doors
    bne     @doneCheckTopRightForDoor
    clr     heroActionIndexesForFourQuadrants+1       ; Change top right quadrant to HeroHitWall for walls
@doneCheckTopRightForDoor
    lda     heroActionIndexesForFourQuadrants+2     ; Action index for bottom left quadrant
    cmpa    #$0C        ; $0C = HeroHitWall for doors
    bne     @doneCheckBottomLeftForDoor
    clr     heroActionIndexesForFourQuadrants+2       ; Change bottom left quadrant to HeroHitWall for walls
@doneCheckBottomLeftForDoor
    lda     heroActionIndexesForFourQuadrants+3     ; Action index for bottom right quadrant
    cmpa    #$0C        ; $0C = HeroHitWall for doors
    bne     @doneCheckBottomRightForDoor
    clr     heroActionIndexesForFourQuadrants+3       ; Change bottom right quadrant to HeroHitWall for walls
@doneCheckBottomRightForDoor
    clra
    lbsr    GetFlagsForMatchingHeroActionIndexes
    beq     @done       ; Hero is not touching any doors/walls
    cmpb    #$0F        ; Hero hit doors/walls in all 4 quadrants.  Kill the hero.
    beq     @killHeroIfOnGround
    jmp     @bounceHeroIfMoving
@killHeroIfOnGround
    tst     currentJumpHeight+1
    bne     @done
    clr     currentJumpIndex
    jmp     StartHeroDeath
@bounceHeroIfMoving
    lslb
    ldx     #routinesToBounceHeroIfMoving
    jsr     [b,x]
@done
    rts

StartHeroDeath
    clr     wingTimer
    clr     sparkleTimer
    lda     #tunes.death.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    jsr     CloseHerosEyesAndFadeOutWithSparkles
    ;jsr     HandleHeroDeath
    rts

routinesToBounceHeroIfMoving  ;                                                                  ↘ ↙ ↗↖
                              ;                                                                  0 0 0 0 
                                fdb BounceHeroNoDirection                              ; %0000 = Bounced from no quadrant
                                fdb BounceHeroDownAndOrRightIfMovingUpAndOrLeft        ; %0001 = Bounced from ↖ quadrant
                                fdb BounceHeroDownAndOrLeftIfMovingUpAndOrRight        ; %0010 = Bounced from ↗ quadrant
                                fdb BounceHeroDownIfMovingUp                           ; %0011 = Bounced from ↖ ↗ quadrants
                                fdb BounceHeroUpAndOrRightIfMovingDownAndOrLeft        ; %0100 = Bounced from ↙ quadrant
                                fdb BounceHeroRightIfMovingLeft                        ; %0101 = Bounced from ↖ ↙ quadrants
                                fdb BounceHeroNoDirection                              ; %0110 = Bounced from ↙ ↗ quadrants
                                fdb BounceHeroDownAndOrRightIfMovingUpAndOrLeft        ; %0111 = Bounced from ↙ ↗ ↖ quadrants
                                fdb BounceHeroUpAndOrLeftIfMovingDownAndOrRight        ; %1000 = Bounced from ↘ quadrant
                                fdb BounceHeroNoDirection                              ; %1001 = Bounced from ↘ ↖ quadrants
                                fdb BounceHeroLeftIfMovingRight                        ; %1010 = Bounced from ↘ ↗ quadrants
                                fdb BounceHeroDownAndOrLeftIfMovingUpAndOrRight        ; %1011 = Bounced from ↘ ↗ ↖ quadrants
                                fdb BounceHeroUpIfMovingDown                           ; %1100 = Bounced from ↘ ↙ quadrants
                                fdb BounceHeroUpAndOrRightIfMovingDownAndOrLeft        ; %1101 = Bounced from ↘ ↙ ↖ quadrants
                                fdb BounceHeroUpAndOrLeftIfMovingDownAndOrRight        ; %1110 = Bounced from ↘ ↙ ↗ quadrants
                                fdb BounceHeroNoDirection                              ; %1111 = Bounced from ↘ ↙ ↗ ↖ quadrants

BounceHeroRightIfMovingLeft
    tst     hero+SpriteBase.speedX
    bpl     @done
    inc     hero+SpriteBase.speedX
    neg     hero+SpriteBase.speedX
    ldd     hero+SpriteBase.x
    addb    hero+SpriteBase.speedX
    adca    #0
    addb    hero+SpriteBase.speedX
    adca    #0
    std     hero+SpriteBase.x
    * pshs    a
    * lda     #'d
    * jsr     PrintChar80x24
    * puls    a
    * jsr     PrintHexWordWithSpace
    lda     hero+SpriteBase.speedX
    cmpa    #1
    ble     @done
    lda     #tunes.land.index
    lbsr    StartTuneIfNoTuneAlreadyPlaying
@done
    rts

BounceHeroLeftIfMovingRight
    tst     hero+SpriteBase.speedX
    bmi     @done
    beq     @done
    dec     hero+SpriteBase.speedX
    neg     hero+SpriteBase.speedX
    ldx     hero+SpriteBase.x
    lda     hero+SpriteBase.speedX
    leax    a,x
    leax    a,x
    stx     hero+SpriteBase.x
    * pshs    b,a
    * lda     #'e
    * jsr     PrintChar80x24
    * tfr     x,d
    * jsr     PrintHexWordWithSpace
    * puls    a,b
    cmpa    #-1
    bge     @done
    lda     #tunes.land.index
    lbsr    StartTuneIfNoTuneAlreadyPlaying
@done
    rts

BounceHeroDownIfMovingUp
    tst     hero+SpriteBase.speedY
    bpl     @done
    inc     hero+SpriteBase.speedY
    neg     hero+SpriteBase.speedY
    ldd     hero+SpriteBase.y
    addb    hero+SpriteBase.speedY
    adca    #0
    addb    hero+SpriteBase.speedY
    adca    #0
    std     hero+SpriteBase.y
    lda     hero+SpriteBase.speedY
    cmpa    #1
    ble     @done
    lda     #tunes.land.index
    lbsr    StartTuneIfNoTuneAlreadyPlaying
@done
    rts

BounceHeroUpIfMovingDown
    tst     hero+SpriteBase.speedY
    bmi     @done
    beq     @done
    dec     hero+SpriteBase.speedY
    neg     hero+SpriteBase.speedY
    ldx     hero+SpriteBase.y
    lda     hero+SpriteBase.speedY
    leax    a,x
    leax    a,x
    stx     hero+SpriteBase.y
    cmpa    #-1
    bge     @done
    lda     #tunes.land.index
    lbsr    StartTuneIfNoTuneAlreadyPlaying
@done
    rts

BounceHeroDownAndOrRightIfMovingUpAndOrLeft
    lbsr    BounceHeroDownIfMovingUp
    lbsr    BounceHeroRightIfMovingLeft
    rts

BounceHeroDownAndOrLeftIfMovingUpAndOrRight
    lbsr    BounceHeroDownIfMovingUp
    lbsr    BounceHeroLeftIfMovingRight
    rts

BounceHeroUpAndOrRightIfMovingDownAndOrLeft
    lbsr    BounceHeroUpIfMovingDown ;
    lbsr    BounceHeroRightIfMovingLeft ;
    rts

BounceHeroUpAndOrLeftIfMovingDownAndOrRight
    lbsr    BounceHeroUpIfMovingDown
    lbsr    BounceHeroLeftIfMovingRight
    rts

BounceHeroNoDirection
    rts

BounceHeroRightFullSpeed
    lda     #8
    sta     hero+SpriteBase.speedX
    ldx     hero+SpriteBase.x
    leax    2,x
    stx     hero+SpriteBase.x
    * lda     #'f
    * jsr     PrintChar80x24
    * tfr     x,d
    * jsr     PrintHexWordWithSpace
    rts

BounceHeroLeftFullSpeed
    lda     #-8
    sta     hero+SpriteBase.speedX
    ldx     hero+SpriteBase.x
    leax    -2,x
    stx     hero+SpriteBase.x
    * lda     #'g
    * jsr     PrintChar80x24
    * tfr     x,d
    * jsr     PrintHexWordWithSpace
    rts

BounceHeroDownFullSpeed
    lda     #8
    sta     hero+SpriteBase.speedY
    ldx     hero+SpriteBase.y
    leax    2,x
    stx     hero+SpriteBase.y
    rts

BounceHeroUpFullSpeed
    lda     #-8
    sta     hero+SpriteBase.speedY
    ldx     hero+SpriteBase.y
    leax    -2,x
    stx     hero+SpriteBase.y
    rts

BounceHeroDownAndRightFullSpeed
    bsr     BounceHeroDownFullSpeed
    bsr     BounceHeroRightFullSpeed
    rts

BounceHeroDownAndLeftFullSpeed
    bsr     BounceHeroDownFullSpeed
    bsr     BounceHeroLeftFullSpeed
    rts

BounceHeroUpAndRightFullSpeed
    bsr     BounceHeroUpFullSpeed
    bsr     BounceHeroRightFullSpeed
    rts

BounceHeroUpAndLeftFullSpeed
    bsr     BounceHeroUpFullSpeed
    bsr     BounceHeroLeftFullSpeed
    rts

ClearGameKeys
;***********************
    clr     controls
    clr     shouldQuit
    clr     shouldCycleToNextLevel
    clr     shouldCycleToPreviousLevel
    clr     shouldReloadLevel
    * clr     shouldCycleToNextWaveTable
    clr     shouldDebugLT
    clr     shouldDebugGT
    clr     shouldPauseUnpause
    clr     shouldToggleNoFallCheat
    clr     shouldToggleNoDamageCheat
    rts

SetEgaPalette:
;***********************
; ```plaintext
; > a = level
; ```
    ; Store the current pallete values for later restoring
    ldy     #ColorPaletteFirstRegister_FFB0
    ldx     #paletteStore
    ldw     #16
    tfm     y+,x+
    ;
    ; Set the pallete
    ldx     #palettes
    clrb
    lsrd
    lsrd
    lsrd
    lsrd                        ; d = level * 16
    leax    d,x                 ; index into palettes for level's palette
    leay    -16,y
            * ; DEBUG START
            * pshs    y,x,b,a
            * pshsw
            * ldx     #debugData+DebugData.data
            * ldb     debugDataIndex
            * ldy     6,s     ; Log y (#ColorPaletteFirstRegister_FFB0)
            * sty     b,x
            * incb
            * incb
            * ldy     4,s
            * sty     b,x     ; Log x (#palettes)
            * incb
            * incb
            * abx
            * ldw     #16
            * tfm     y+,x+
            * addb    #16
            * stb     debugDataIndex
            * pulsw
            * puls    a,b,x,y
            * ; DEBUG END
    ldw     #16
    tfm     x+,y+
    ;
    rts

SavePalette:
;***********************
    pshs    y,x
    pshsw
    ;
    ldy     #ColorPaletteFirstRegister_FFB0
    ldx     #paletteStore
    ldw     #15
    tfm     y+,x+
    ;
    pulsw
    puls    x,y,pc

RestorePalette:
;***********************
    pshs    y,x
    pshsw
    ;
    ldy     #ColorPaletteFirstRegister_FFB0
    ldx     #paletteStore
    ldw     #15
    tfm     x+,y+
    ;
    pulsw
    puls    x,y,pc

StartTuneIfNoTuneAlreadyPlaying
    ; ```plaintext
    ; > a = Index of tune to play
    ; ```
    tst     tunes.tonesRemaining
    bne     @done
StartTunePlaying
    pshs    x,b,a
    ;
    ldx     #tunes.lengths
    ldb     a,x
    stb     tunes.tonesRemaining
    lsla
    ldx     #tunes.addresses
    ldx     a,x                             ; x = address of first tone in tune
    pshs    a
    stx     tunes.toneInTuneAddress
    lda     Tone.duration,x
    sta     tunes.durationRemaining
    ldd     Tone.frequencyValue,x
    std     freqValue+1
    ldx     #tunes.waveTables
    puls    a
    lsla
    ldx     a,x
    stx     ldWaveTbl+1
    ;
    puls    a,b,x
@done
    rts

ProgressTuneIfPlaying
    tst     tunes.tonesRemaining
    beq     @done
    dec     tunes.durationRemaining
    bne     @done
    ;
    ; Increment toneInTuneOffset to the next tone in the tune
    ldx     tunes.toneInTuneAddress
    leax    sizeof{Tone},x
    stx     tunes.toneInTuneAddress
    ; Decrement tonesRemaining
    dec     tunes.tonesRemaining
    beq     @done
    ; Set next tone's duration
    lda     Tone.duration,x
    sta     tunes.durationRemaining
    ; Set next tone's frequency
    ldd     Tone.frequencyValue,x
    std     freqValue+1
    ;
@done
    rts

FIRQHandler
    std     @restore+1
    ;
    andcc   #%11101111                          ; unmask IRQ so we don't delay vsync.  Vsync needs to restart the timer precisely on time.
    ldd     <firqCount
    incd
    std     <firqCount
    cmpd    #70
    bne     @handleSound
    tst     <splitScreen
    beq     @handleSound
    brn     *+2     ; 3 cycles
    brn     *+2     ; 3 cycles
    brn     *+2     ; 3 cycles
GimeCopy86
    ;
    ;         Video Mode
    ;         ┌────────── Bit  7   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%00000011
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00000101
    sta     GIME_VideoResolution_FF99
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    lda     #%00000011
    sta     GIME_VideoBankSelect_FF9B
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen Enable
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     GIME_HorizontalOffset_FF9F
GimeCopy86End
    ;
@handleSound
            tst     tunes.tonesRemaining
            beq     @done
            ldd     freqValue+1
            beq     @done
@wavePtr    ldd     #$0000                      ; Continuously updated by next line after freqValue
freqValue   addd    #$0000                      ; Filled in by StartTuneIfNoTuneAlreadyPlaying and ProgressTuneIfPlaying
            std     @wavePtr+1,pcr
            sta     ldWaveTbl+2,pcr
ldWaveTbl   lda     >squareWaveTable
            sta     PIA1SideADataRegister_FF20
    ;
@done
    lda     GIME_FastInterruptReqEnable_FF93    ; Acknowledge the FIRQ interrupt
@restore    ldd     #$0000
    rti

waitingForVsync     fcb     $00

IRQHandler
;***********************
highSpeedTimerCount
    ldd     #$0000      ; Filled in by SetHsyncCountForGimeVersion
    stb     GIME_TimerLSB_FF95
    sta     GIME_TimerMSB_FF94
    ldd     <firqCount
    std     debugData+DebugData.data
    clr     <firqCount
    clr     <firqCount+1
    ;
    tst     <splitScreen
    beq     @continue
    ;
    ;         Video Mode
    ;         ┌────────── Bit  7   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%10000001
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00111110
    sta     GIME_VideoResolution_FF99
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    ldx     onScreenBuffer
    lda     ScreenBuffer.videoBank,x
    sta     GIME_VideoBankSelect_FF9B
    ;
    lda     ScreenBuffer.horizontalOffset,x
    ora     #%10000000;     ; Keep HVEN (bit 7) of GIME Horizontal Offset set to keep us in 256-byte wide mode.
    sta     GIME_HorizontalOffset_FF9F
    ;
@continue
    ldx     #keyColumnScans
    lda     #%11111111
    sta     PIA0SideBDataRegister_FF02  ; Disable all keyboard column strobes
    ;
    ldb     #%00000001      ; Start off reading column 0
@loop_keyscan
    comb
    stb     PIA0SideBDataRegister_FF02  ; Enable the next column
    lda     PIA0SideADataRegister_FF00  ; Read the column
    ora     #%10000000
    coma                    ; Invert bits so 1=key down, 0=key up
    sta     ,x+             ; Store in next keyColumnScans element
    ;
    comb
    lslb                    ; Next column. Shift bits in b to the left
    bne     @loop_keyscan
    ;
    inc     <vsyncCount
    clr     waitingForVsync
    ;
@done
    lda     GIME_InterruptReqEnable_FF92
    rti

WaitForKeyPressNoRom:
;***********************
    pshs    b,a
@waitForNoKey
    bsr     IsKeyDownNoRom
    bne     @waitForNoKey
@waitForKey
    bsr     IsKeyDownNoRom
    beq     @waitForKey
@waitForNoKeyAgain
    bsr     IsKeyDownNoRom
    bne     @waitForNoKeyAgain
    ;
    puls    a,b,pc

IsKeyDownRom:
; ```plaintext
; < a = 1 if any key is down, 0 otherwise
; ```
    ; Make ROM call to see if key is pressed
    jsr     [$A000]
    cmpa    #0
    bhi     @keyPressed
    clra                            ; Return 0 for no key down
    bra     @done
@keyPressed
    lda     #1                      ; Return 1 for key down
@done
    rts

IsKeyDownNoRom:
; ```plaintext
; < a = 1 if any key is down, 0 otherwise
; ```
;***********************
    pshs    x,b,a
    ;
    ldx     #keyColumnScans
    ldd     ,x
    ord     2,x
    ord     4,x
    ord     6,x
    ;
    puls    a,b,x,pc

WaitForVsync:
;***********************
    inc     waitingForVsync,pcr
@wait
    tst     waitingForVsync,pcr
    bne     @wait
    rts

SetFastSpeed:
;***********************
    clr     SetClockSpeedR1_FFD9
    rts

SetSlowSpeed:
;***********************
    clr     ClearClockSpeedR1_FFD8
    rts

ResetRgbPalette:
;***********************
    pshs    y,x,a
    ; Fix an error in SECB where it only set 15 of the 16 palette registers to the RGB colors
    lda     #16
    sta     $E649
    ; Call the SECB routine to set the paleete to the RGB colors
    jsr     $E5FA
    puls    a,x,y,pc

originalVectors    rmb 18

SetupInterruptVectors:
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #$FEEE
    leay    originalVectors,pcr
    ldw     #18
    tfm     x+,y+
    ;
    lda     #$3B             ; rti
    pshs    a
    ldx     #$FEEE
    ldw     #18
    tfm     s,x+
    puls    a
    ;
    * tst     GIME_InterruptReqEnable_FF92
    * tst     GIME_FastInterruptReqEnable_FF93
    ldx     #$FEF4
    lda     #$7E            ; jmp
    sta     ,x+
    leay     FIRQHandler,pcr
    sty     ,x++
    lda     #$7E            ; jmp
    sta     ,x+
    leay    IRQHandler,pcr
    sty     ,x
    ;
    ldd     highSpeedTimerCount+1
    stb     GIME_TimerLSB_FF95
    sta     GIME_TimerMSB_FF94
    ;
    ;         Fast Interrupt Request Enable Registers - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer FIRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync FIRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync FIRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data FIRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard FIRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge FIRQ, 0 = disable)
    lda     #%00100000
    sta     GIME_FastInterruptReqEnable_FF93    ; Enable Timer FIRQ
    ;
    pulsw
    puls    a,b,x,y,pc

RestoreInterruptVectors:
;***********************
    pshs    y,x
    pshsw
    ;
    ;         Fast Interrupt Request Enable Registers - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer FIRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync FIRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync FIRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data FIRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard FIRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge FIRQ, 0 = disable)
    lda     #%00000000
    sta     GIME_FastInterruptReqEnable_FF93    ; Disable Timer FIRQ
    leax    originalVectors,pcr
    ldy     #$FEEE
    ldw     #18
    tfm     x+,y+
    ;
    pulsw
    puls    x,y,pc

InitializeLevel
; ```plaintext
; > currentLevel = Level #
; ```
    pshs    u,y,x,b,a
    pshsw
    ;
    jsr     ClearScoreMemoryForDebug
    jsr     DrawPowerUpTilesForLevel
    jsr     LoadCurrentMap
    jsr     InitializeStateForLevel
    jsr     InitializeHero
    jsr     InitializeHeroForLevel
    jsr     InitializeBaddiesForLevel
    ;
    pulsw
    puls    a,b,x,y,u,pc

PrerenderLevel256ByteVerticalSlice
; ```plaintext
; e > Map X coordinate to start at (0-31)
; a > Start Physical block to render to
; b > Non 0 to indicate map data is actual tile indexes
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    stb     @skipMapTileLookup,pcr
    ;
    sta     @block,pcr
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ldx     #$A000
    clrf            ; Map Y to start at
    lda     #16
    sta     @tileCount,pcr
    lda     #'.
    lbsr    PrintChar80x24
@nextTile
    ; Calculate address of map cell and get tile #
    tfr     f,a
    ldb     #32
    mul
    leay    currentMap,pcr
    leay    d,y
    lda     e,y     ; a = Cell #
    tst     @skipMapTileLookup,pcr
    bne     @skip
    leay    mapToTileLookup,pcr
    lda     a,y     ; a = Tile #
@skip
    ;
    lbsr    DrawTileIn256ByteWideBuffer
    leax    16,x
    ince
    dec     @tileCount,pcr
    bne     @nextTile
    sube    #16
    lda     #16
    sta     @tileCount,pcr
    incf
    cmpf    #16                     ; 16 rows
    bhs     @doneTiles
    inc     @block,pcr
    lda     @block,pcr
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ldx     #$A000
    bra     @nextTile
@doneTiles
    ;
    puls    a
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc
@tileCount          rmb 1
@block              rmb 1
@skipMapTileLookup  rmb 1

DrawTilesFromMapToOffScreenBufferIfNeeded
; ```plaintext
; > redrawTile1/2.mapX = Map X (0-31)
; > redrawTile1/2.mapY = Map Y (0-15)
;***********************
    tst     redrawTile2+RedrawTile.pending
    beq     @checkRedrawTile1
    ldy     #redrawTile2
    bsr     DrawTileFromMapToOffScreenBuffer
@checkRedrawTile1
    tst     redrawTile1+RedrawTile.pending
    beq     @done
    ldy     #redrawTile1
    bsr     DrawTileFromMapToOffScreenBuffer
    ldw     #sizeof{RedrawTile}
    ldx     #redrawTile1
    ldy     #redrawTile2
    tfm     x+,y+
    clr     redrawTile1+RedrawTile.pending
@done
    rts

DrawTileFromMapToOffScreenBuffer
; ```plaintext
; > y = Address of RedrawTile to draw
;***********************
    pshs    x,b,a
    pshsw
    ; Each slice is 512x512 (256 bytes x 512 rows), 16 blocks, 128K.
    ; There are 4 slices needed to cover the 1024 x 512 pixel play area.
    ; Each slice overlaps the next slice by 320 pixels (160 bytes) so transition from one slice to the next is seamless.
    ; There are 2 buffers of 4 slices (blocks $40-7F and $80-BF) each for double buffering, using up 1 meg.
    ;
    ;       |%% Slice 1 %%%%%%%%%%%%%=======================================| Blocks $40-4F/ $80-8F
    ;       |                       |== Slice 2 =============================oooooooooooooooooooooooo| Blocks $50-5F/ $90-9F
    ;       |                       |                       |== Slice 3 =====ooooooooooooooooooooooooo@@@@@@@@@@@@@@@@@@@@@@@| Blocks $60-6F/ $A0-AF
    ;       |                       |                       |               |oo Slice 4 oooooooooooooo@@@@@@@@@@@@@@@@@@@@@@@@---------------|  Blocks $70-7F / $B0-BF
    ; MapX     0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19   20  21  22  23  24  25  26  27  28  29  30  31
    ; LevelX 000 032 064 096 128 160 192 224 256 288 320 352 384 416 448 480 512 544 576 608 0640 672 704 736 768 800 832 864 896 928 960 992
    ;
    lda     [RedrawTile.addressOfMapCell,y]
    ldx     #mapToTileLookup
    ldf     a,x         ; f = Tile #
    lda     RedrawTile.mapY,y
    ldb     RedrawTile.mapX,y
@checkSlice1
    cmpb    #5
    bgt     @checkSlice1And2
    ; We're on slice 1
    clre
    ldx     #0
    lbsr    DrawTileFromMapToOffScreenSlice
    lbra    @done
@checkSlice1And2
    cmpb    #11
    bgt     @checkSlice1And2And3
    ; We're on slices 1 and 2
    clre
    ldx     #0
    lbsr    DrawTileFromMapToOffScreenSlice
    lde     #16
    ldx     #192
    lbsr    DrawTileFromMapToOffScreenSlice
    lbra    @done
@checkSlice1And2And3
    cmpb    #15
    bgt     @checkSlice2And3And4
    ; We're on slices 1 and 2 and 3
    clre
    ldx     #0
    lbsr    DrawTileFromMapToOffScreenSlice
    lde     #16
    ldx     #192
    lbsr    DrawTileFromMapToOffScreenSlice
    lde     #32
    ldx     #384
    lbsr    DrawTileFromMapToOffScreenSlice
    lbra    @done
@checkSlice2And3And4
    cmpb    #21
    bgt     @checkSlice3And4
    ; We're on slices 2 and 3 and 4
    lde     #16
    ldx     #192
    lbsr    DrawTileFromMapToOffScreenSlice
    lde     #32
    ldx     #384
    lbsr    DrawTileFromMapToOffScreenSlice
    lde     #48
    ldx     #512
    lbsr    DrawTileFromMapToOffScreenSlice
    bra     @done
@checkSlice3And4
    cmpb    #27
    bgt     @slice4
    ; We're on slices 3 and 4
    lde     #32
    ldx     #384
    lbsr    DrawTileFromMapToOffScreenSlice
    lde     #48
    ldx     #512
    lbsr    DrawTileFromMapToOffScreenSlice
    bra     @done
@slice4
    ; We're on slice 4
    lde     #48
    ldx     #512
    lbsr    DrawTileFromMapToOffScreenSlice
    ;
@done
    ;
    pulsw
    puls    a,b,x,pc

DrawTileFromMapToOffScreenSlice
; ```plaintext
; a > Map Y (0-15)
; b > Map X (0-31)
; e > Slice # (0-3)
; f > Tile #
; x > left Level X in slice
; ```
    pshs    y,b,a
    ;
    tfr     e,a
    adda    ,s          ; a = (slice # * 16) + Map Y
    ldy     offScreenBuffer
    adda    ScreenBuffer.firstPhysicalBlockOfBuffer,y   ; a = Physical block to map to $A000 to draw tile in
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock              ; Map Logical A000-BFFF to physical block containing tile to draw
    pshs    a
    clrb
    lda     2,s         ; a = Map X
    lsrd
    lsrd
    lsrd
    subr    x,d         
    lsrd                ; d = Level X (Map X * 32) - (left Level X in slice) / 2 = offset from A000 to start drawing tile
    addd    #$A000
    tfr     d,x         ; x = Address to draw tile to
    tfr     f,a         ; a = Tile # to draw
    ;
    lbsr    DrawTileIn256ByteWideBuffer
    ;
    puls    a
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    puls    a,b,y,pc

DrawTileIn256ByteWideBuffer
; ```plaintext
; > a = Tile #
; > x = Logical address to start drawing
; - Uses $6000-$7FFF to map tile data in
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldb     mmu+LOGICAL_6000_7FFF,pcr
    pshs    b
    ; Calculate the physical block the tile is in by taking the high nibble (4 bits)
    ; of the tile # and adding the first block # of the tile data.
    ; ex: Tile # $6B is in physical block $10 (first block of tile data) + 6, or $16
    pshs    a
    lsra
    lsra
    lsra
    lsra
    adda    #TILE_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    ; Calculate the offset into the tile #'s block where tile # starts by taking the
    ; low nibble of the tile # and multiplying by 512 (number of bytes in each tile).
    ; ex: Tile # $6B is at offset $B * 512, or $1600
    anda    #$0F
    lsla
    clrb                ; d now contains the low nibble of the tile # * 512
    ;
    ldy     #$6000
    leay    d,y
    ldb     #32         ; Draw 32 rows of the tile
@nextRow
    ldw     #16         ; Draw 16 bytes (32 pixels) in this row
    tfm     y+,x+
    leax    256-16,x    ; Adjust x (where we're drawing) down one row and back to the beginning column
    decb
    bne     @nextRow    ; Keep drawing until all 32 rows have been drawn
    ;
    puls    a
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc

PrenderAllTilesForDebug
    pshs    x,b,a
    pshsw
;
    clra
    pshs    a
    leax    currentMap,pcr
    ldw     #512
    tfm     s,x+
    puls    a
    leax    currentMap,pcr
    lda     #0
    ldb     #16
@continue
    sta     ,x+
    inca
    cmpa    #$78
    beq     @done
    decb
    bne     @continue
    ldb     #16
    leax    16,x
    bra     @continue
@done
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    clre
    ldb     #1
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #4
    lbsr    PrerenderLevel256ByteVerticalSlice
    ;
    clre
    adda    #16
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #6
    lbsr    PrerenderLevel256ByteVerticalSlice
    adda    #16
    adde    #4
    lbsr    PrerenderLevel256ByteVerticalSlice
;
    pulsw
    puls    a,b,x,pc

SetHsyncCountForGimeVersion
;***********************
    pshs    u,y,x,b,a
    pshsw
    ;
    sta     $FFD9
    ldu     #$0112
    ldb     1,u
@WaitForStartOfNextTick
    cmpb    1,u
    beq     @WaitForStartOfNextTick
    ldd     #$FFC4
    std     ,u
    ldx     #$FF93
    clr     -2,x
    lda     #$20
    sta     ,x
    ldd     #1
    stb     2,x
    sta     1,x
    ldy     #1
    ldb     ,x
@WaitTimerHighByteToCycleBackToZero
    lda     #$20
    bita    ,x
    beq     @SkipCount
    leay    1,y
@SkipCount
    tst     ,u
    bne     @WaitTimerHighByteToCycleBackToZero
    sty     debugData+DebugData.gimeCheckCount
    sta     $FFD8
    ldd     #685 ;#342 ;685
    cmpy    #7085
    ble     @WeGotUsAn86Gime
    ldd     #689 ;#344 ;689
    bra     @adjustForGime
@WeGotUsAn86Gime
    ldx     #GimeCopy86
    leay    -4,x
    ldw     #GimeCopy86End-GimeCopy86
    tfm     x+,y+
    ldx     #$2100
    stx     GimeCopy86End-4
    stx     GimeCopy86End-2
@adjustForGime
    std     highSpeedTimerCount+1,pcr
    std     debugData+DebugData.highSpeedTimerCountStart
    std     debugData+DebugData.highSpeedTimerCountFinal
    stb     $FFD8
    ;
    pulsw
    puls    a,b,x,y,u,pc

GetHerosCurrentMapCellData
; ```plaintext
; < mapXHeroIsOn                  = X coordinate (0-31) of cell in currentMap Hero is currently on
; < mapYHeroIsOn                  = Y coordinate (0-15) of cell in currentMap Hero is currently on
; < addressOfMapCellHeroIsOn      = address of cell in currentMap Hero is currently on
; < actionIndexForMapCellHeroIsOn = Index into heroActionLookup for action routine for map cell Hero is currently on
; < a                             = Index into heroActionLookup for action routine for map cell Hero is currently on
; ```
;***********************
    pshs    x,b
    ldd     hero+SpriteBase.x
    addd    #16                 ; Center of sprite horizontally
    lsrd
    lsrd
    lsrd
    lsrd
    lsrd                        ; d = X coordinate (0-31) in currentMap
    stb     mapXHeroIsOn
    ;
    ldd     hero+SpriteBase.y
    addd    #16                 ; Center of sprite vertically
    tfr     d,x
    lsrd
    lsrd
    lsrd
    lsrd
    lsrd                        ; d = Y coordinate (0-15) in currentMap
    stb     mapYHeroIsOn
    ;
    tfr     x,d                 ; d = Hero Y coordinate + 16 (center of sprite vertically)
    andd    #$FFE0              ; d / 32 = Map Y, * 32 = offset into currentMap for row.  d / 32 * 32 = d and #$FFE0
    addb    mapXHeroIsOn,pcr
    adca    #0                  ; d = offset into currentMap of cell Hero's center is on
    addd    #currentMap
    std     addressOfMapCellHeroIsOn
    ;
    lda     [addressOfMapCellHeroIsOn]
    ldx     #mapDataToHeroActionIndexLookup
    lda     a,x
    lsla
    sta     actionIndexForMapCellHeroIsOn
    ;
    puls    b,x,pc

heroActionIndexesForFourQuadrants   ;    ┌─────────────────── Top left quadrant
;                                        │   ┌─────────────── Top right quadrant
;                                        │   │   ┌─────────── Bottom left quadrant
;                                        │   │   │   ┌─────── Bottom right quadrant
                                    fcb $00,$00,$00,$00

GetHeroActionIndexesForFourQuadrantsOfHero
    ldx     hero+SpriteBase.x
    leax    10,x
    ldy     hero+SpriteBase.y
    leay    10,y
    lbsr    GetHeroActionIndexForLevelXY
    sta     heroActionIndexesForFourQuadrants
    leax    12,x
    lbsr    GetHeroActionIndexForLevelXY
    sta     heroActionIndexesForFourQuadrants+1
    leay    12,y
    lbsr    GetHeroActionIndexForLevelXY
    sta     heroActionIndexesForFourQuadrants+3
    leax    -12,x
    lbsr    GetHeroActionIndexForLevelXY
    sta     heroActionIndexesForFourQuadrants+2
    rts

GetFlagsForMatchingHeroActionIndexes
; ```plaintext
; > a                                 = Action Index to match
; > heroActionIndexesForFourQuadrants = Action indexes for the four quadrants of Hero
; < b = Bits 0-3 set for quadrants matching queried Action Index
; -     ┌────────── Bit  3 - Bottom right quadrant matches Action Index
; -     │┌───────── Bits 2 - Bottom left quadrant matches Action Index
; -     ││┌──────── Bits 1 - Top right quadrant matches Action Index
; -     │││┌─────── Bits 0 - Top left quadrant matches Action Index
; -     0000 ────── Could have multiple bits set
; ```
    pshs    a
    clrb
    lda     heroActionIndexesForFourQuadrants
    cmpa    ,s
    bne     @doneTopLeftQuadrant
    orb     #%0001
@doneTopLeftQuadrant
    lda     heroActionIndexesForFourQuadrants+1
    cmpa    ,s
    bne     @doneTopRightQuadrant
    orb     #%0010
@doneTopRightQuadrant
    lda     heroActionIndexesForFourQuadrants+2
    cmpa    ,s
    bne     @doneBottomLeftQuadrant
    orb     #%0100
@doneBottomLeftQuadrant
    lda     heroActionIndexesForFourQuadrants+3
    cmpa    ,s
    bne     @doneBottomRightQuadrant
    orb     #%1000
@doneBottomRightQuadrant
    ;
    tstb
    puls    a,pc

GetHeroActionIndexForLevelXY
; ```plaintext
; > x = X coordinate in level
; > y = Y coordinate in level
; < a = Index into heroActionLookup for action routine for map cell Hero is currently on
; ```
;***********************
    pshs    x,b
    tfr     x,d                 ; d = Level X coordinate
    lsrd
    lsrd
    lsrd
    lsrd
    lsrd                        ; b = Level X / 32 = map X
    stb     @mapX,pcr
    ;
    tfr     y,d                 ; d = Level Y coordinate
    andd    #$FFE0              ; d / 32 = Map Y, * 32 = offset into currentMap for row.  d / 32 * 32 = d and #$FFE0
    addb    @mapX,pcr
    adca    #0                  ; d = Offset into currentMap of cell that Level X,Y coordinate (passed in) is on
    addd    #currentMap         ; d = Address of currentMap cell that Level X,Y coordinate (passed in) is on
    ;
    tfr     d,x
    lda     ,x
    ldx     #mapDataToHeroActionIndexLookup
    lda     a,x
    ;
    puls    b,x,pc
@mapX       fcb $00

UpdateMapCellAndQueueTileRedrawing
; ```plaintext
; > a = New map cell data
; ```
;***********************
    pshs    b,a
    ;
    sta     [addressOfMapCellHeroIsOn]
    lda     #1
    sta     redrawTile1+RedrawTile.pending
    ldd     addressOfMapCellHeroIsOn
    std     redrawTile1+RedrawTile.addressOfMapCell
    ldd     mapXHeroIsOn                ; Loads both mapX and mapY
    std     redrawTile1+RedrawTile.mapX  ; Stores both mapX and mapy
    ;
    puls    a,b,pc


    org     $8000-sizeof{Hero}-sizeof{SpriteBase}-sizeof{Baddie}*5
hero                        rmb sizeof{Hero}
heroShadow                  rmb sizeof{SpriteBase}
baddies                     rmb sizeof{Baddie}*5
sprites.end                 equ *-1


    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
    fcb     PHYSICAL_068000_069FFF
    org     $4000

ApplyFriction               equ     ApplyFrictionTemp+$4000
ApplyFrictionTemp
    lda     preventFrictionFromSlowingHero
    clr     preventFrictionFromSlowingHero
    tsta
    bne     @zeroSpeedInDirectionIfOne
    tst     hero+SpriteBase.speedX
    beq     @doneApplyingHorizontalFriction
    blt     @applyPositiveHorizontalFriction
    dec     hero+SpriteBase.speedX
    bra     @doneApplyingHorizontalFriction
@applyPositiveHorizontalFriction
    inc     hero+SpriteBase.speedX
@doneApplyingHorizontalFriction
    tst     hero+SpriteBase.speedY
    beq     @doneApplyingVerticalFriction
    blt     @applyPositiveVerticalFriction
    dec     hero+SpriteBase.speedY
    bra     @doneApplyingVerticalFriction
@applyPositiveVerticalFriction
    inc     hero+SpriteBase.speedY
@doneApplyingVerticalFriction
    bra     @done
@zeroSpeedInDirectionIfOne
@checkXSpeed
    lda     hero+SpriteBase.speedX
    cmpa    #1
    beq     @clearXSpeed
    cmpa    #-1
    bne     @checkYSpeed
@clearXSpeed
    clr     hero+SpriteBase.speedX
@checkYSpeed
    lda     hero+SpriteBase.speedY
    cmpa    #1
    beq     @clearYSpeed
    cmpa    #-1
    bne     @done
@clearYSpeed
    clr     hero+SpriteBase.speedY
@done
    rts

ActionHeroHitNoAction       equ     ActionHeroHitNoActionTemp+$4000
ActionHeroHitNoActionTemp
    rts

ActionHeroHitCrazyBumper    equ     ActionHeroHitCrazyBumperTemp+$4000
ActionHeroHitCrazyBumperTemp
    tst     currentJumpHeight+1
    bne     @done
    ;
    pshs    x,b,a
    ;
    lda     constantlyCyclingWord+1
    anda    #%111
    lsla
    ldx     #fullSpeedDirections
    ldd     a,x                         ; a = speed X, b = speed Y
    std     hero+SpriteBase.speedX       ; Stores SpriteBase.speedX and SpriteBase.speedY both
    lda     #25
    sta     currentJumpIndex
    lda     #tunes.hitCrazyBummper.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ;
    puls    a,b,x
@done
    rts

ActionHeroHitDamagingFloor  equ     ActionHeroHitDamagingFloorTemp+$4000
ActionHeroHitDamagingFloorTemp
    rts

ActionHeroHitFloorFall      equ     ActionHeroHitFloorFallTemp+$4000
ActionHeroHitFloorFallTemp
    pshs    a
    ;
    tst     currentJumpHeight+1
    bne     @done
    tst     sparkleTimer
    bne     @done
@fall
    lda     #tunes.hitFloorFall.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    clra
    jsr     UpdateMapCellAndQueueTileRedrawing
    ;
@done
    puls    a,pc

ActionHeroHitIceFloor       equ     ActionHeroHitIceFloorTemp+$4000
ActionHeroHitIceFloorTemp
    inc     preventFrictionFromSlowingHero
    rts

ActionHeroHitNoFloorFall    equ     ActionHeroHitNoFloorFallTemp+$4000
ActionHeroHitNoFloorFallTemp
    pshs    a
    ;
    tst     currentJumpHeight+1
    bne     @done
    tst     sparkleTimer
    beq     @checkFall
    lda     #1
    jsr     UpdateMapCellAndQueueTileRedrawing
    bra     @done
@checkFall
    tst     cheat.HeroNoFall
    bne     @done
    jsr     GetHeroActionIndexesForFourQuadrantsOfHero
    lda     #$0B        ; $0B = HeroHitNoFloorFall
    jsr     GetFlagsForMatchingHeroActionIndexes
    cmpb    #$0F        ; All quadrants match HeroHitNoFloorFall
    bne     @slidingTowardNoFloorTile
@falling
    lda     #tunes.falling.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ldx     #fallAnimation
    jsr     PlayHeroAnimation
    lda     #1
    jsr     HandleHeroDeath
    bra     @done
@slidingTowardNoFloorTile
    ldx     #heroSpeedAndControlFilterWhenAboutToFall
    lslb
    lslb
    abx
    lda     AboutToFallSpeedAndControlFilter.controlFilter,x
    sta     controlFilter
    lda     AboutToFallSpeedAndControlFilter.speedX,x
    beq     @doneSpeedX
    sta     hero+SpriteBase.speedX
@doneSpeedX
    lda     AboutToFallSpeedAndControlFilter.speedY,x
    beq     @doneSpeedY
    sta     hero+SpriteBase.speedY
@doneSpeedY
    lda     #HERO_ACTION_DATA_PHYSICAL_BLOCK
    sta     hero+SpriteBase.compiledPhysicalBlock
    ldd     #Hero.Fall.Frame0
    std     hero+SpriteBase.compiled
    ; TODO: Grab AboutToFallSpeedAndControlFilter.speedX/.speedY,x
    ;     : Add to hero+SpriteBase.speedX/.speedY
    ;     : New variable, controlFilter, defaulted/initialized to $1F (enable all 5 controls), and'ed with controls when we get the controls
    ;     : Set controlFilter to AboutToFallSpeedAndControlFilter.controlFilter,x
    ;     : Code rest
    ;
@done
    puls    a,pc

ActionHeroHitPushDown       equ     ActionHeroHitPushDownTemp+$4000
ActionHeroHitPushDownTemp
    inc     preventFrictionFromSlowingHero
    inc     hero+SpriteBase.speedY
    inc     hero+SpriteBase.speedY
    jsr     CapHeroSpeedAt8
    rts

ActionHeroHitPushLeft       equ     ActionHeroHitPushLeftTemp+$4000
ActionHeroHitPushLeftTemp
    inc     preventFrictionFromSlowingHero
    dec     hero+SpriteBase.speedX
    dec     hero+SpriteBase.speedX
    jsr     CapHeroSpeedAt8
    rts

ActionHeroHitPushLeftDown   equ     ActionHeroHitPushLeftDownTemp+$4000
ActionHeroHitPushLeftDownTemp
    inc     preventFrictionFromSlowingHero
    dec     hero+SpriteBase.speedX
    dec     hero+SpriteBase.speedX
    inc     hero+SpriteBase.speedY
    inc     hero+SpriteBase.speedY
    jsr     CapHeroSpeedAt8
    rts

ActionHeroHitPushLeftUp     equ     ActionHeroHitPushLeftUpTemp+$4000
ActionHeroHitPushLeftUpTemp
    inc     preventFrictionFromSlowingHero
    dec     hero+SpriteBase.speedX
    dec     hero+SpriteBase.speedX
    dec     hero+SpriteBase.speedY
    dec     hero+SpriteBase.speedY
    jsr     CapHeroSpeedAt8
    rts

ActionHeroHitPushRight      equ     ActionHeroHitPushRightTemp+$4000
ActionHeroHitPushRightTemp
    inc     preventFrictionFromSlowingHero
    inc     hero+SpriteBase.speedX
    inc     hero+SpriteBase.speedX
    jsr     CapHeroSpeedAt8
    rts

ActionHeroHitPushRightDown  equ     ActionHeroHitPushRightDownTemp+$4000
ActionHeroHitPushRightDownTemp
    inc     preventFrictionFromSlowingHero
    inc     hero+SpriteBase.speedX
    inc     hero+SpriteBase.speedX
    inc     hero+SpriteBase.speedY
    inc     hero+SpriteBase.speedY
    jsr     CapHeroSpeedAt8
    rts

ActionHeroHitPushRightUp    equ     ActionHeroHitPushRightUpTemp+$4000
ActionHeroHitPushRightUpTemp
    inc     preventFrictionFromSlowingHero
    inc     hero+SpriteBase.speedX
    inc     hero+SpriteBase.speedX
    dec     hero+SpriteBase.speedY
    dec     hero+SpriteBase.speedY
    jsr     CapHeroSpeedAt8
    rts

ActionHeroHitPushUp         equ     ActionHeroHitPushUpTemp+$4000
ActionHeroHitPushUpTemp
    inc     preventFrictionFromSlowingHero
    dec     hero+SpriteBase.speedY
    dec     hero+SpriteBase.speedY
    jsr     CapHeroSpeedAt8
    rts

ActionHeroHitWall           equ     ActionHeroHitWallTemp+$4000
ActionHeroHitWallTemp
    rts

ActionHeroHitMagnet         equ     ActionHeroHitMagnetTemp+$4000
ActionHeroHitMagnetTemp
    inc     heroIsOnMagnet
    rts

ActionHeroHitGemItem        equ     ActionHeroHitGemItemTemp+$4000
ActionHeroHitGemItemTemp
    pshs    x,b,a
    ;
    lda     #tunes.hitGemItem.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ;
    lda     #1
    jsr     UpdateMapCellAndQueueTileRedrawing
    ;
    lda     #1
    ldx     #bcdGems
    jsr     SubtractFromSingleByteBcd
    ;
    ldd     #$1000
    jsr     AddToBcdScore
    ;
    puls    a,b,x,pc

ActionHeroHitPaintItem      equ     ActionHeroHitPaintItemTemp+$4000
ActionHeroHitPaintItemTemp
    pshs    a
    ;
    tst     currentJumpHeight+1
    bne     @done
    lda     #1
    jsr     UpdateMapCellAndQueueTileRedrawing
    lda     #$46
    sta     sparkleTimer
    lda     #tunes.hitPaintItem.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ;
@done
    puls    a,pc

ActionHeroHitWingItem       equ     ActionHeroHitWingItemTemp+$4000
ActionHeroHitWingItemTemp
    pshs    a
    ;
    tst     currentJumpHeight+1
    bne     @done
    tst     wingTimer
    bne     @done
    lda     #32
    sta     wingTimer
    clr     wingFrameIndex
    lda     #$19
    sta     currentJumpIndex
    lda     #tunes.hitWingItem.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ;
@done
    puls    a,pc

ActionHeroHitKeyItem        equ     ActionHeroHitKeyItemTemp+$4000
ActionHeroHitKeyItemTemp
    pshs    x,a
    ;
    lda     #tunes.hitKeyItem.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ;
    lda     #1
    jsr     UpdateMapCellAndQueueTileRedrawing
    ;
    lda     #1
    ldx     #bcdKeys
    jsr     AddToSingleByteBcd
    ;
@done
    puls    a,x,pc

ActionHeroHitClockItem      equ     ActionHeroHitClockItemTemp+$4000
ActionHeroHitClockItemTemp
    pshs    x,a
    ;
    lda     #tunes.hitClockItem.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ;
    lda     #1
    jsr     UpdateMapCellAndQueueTileRedrawing
    ;
    ldx     #bcdTimeRemaining
    lda     #5
    jsr     AddToSingleByteBcd
    lda     bcdTimeRemaining
    cmpa    #$99
    blo     @done
    lda     #$99
    sta     bcdTimeRemaining
    ;
@done
    puls    a,x,pc

ActionHeroHitSodaItem       equ     ActionHeroHitSodaItemTemp+$4000
ActionHeroHitSodaItemTemp
    tst     currentJumpHeight+1
    bne     @done
    sta     @restoreA+1,pcr
    ;
    lda     #tunes.hitSodaItem.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ;
    lda     #1
    jsr     UpdateMapCellAndQueueTileRedrawing
    ;
    lda     #100
    sta     health
    ;
@restoreA
    lda     #$00
@done
    rts

ActionHeroHitHammerItem     equ     ActionHeroHitHammerItemTemp+$4000
ActionHeroHitHammerItemTemp
    tst     currentJumpHeight+1
    bne     @done
    sta     @restoreA+1,pcr
    ;
    lda     #tunes.hitHammerItem.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    ;
    lda     #$BE
    sta     hammerTimer
    ;
    lda     #1
    jsr     UpdateMapCellAndQueueTileRedrawing
    ;
@restoreA
    lda     #$00
@done
    rts

TurnAndMoveAllBaddies       equ     TurnAndMoveAllBaddiesTemp+$4000
TurnAndMoveAllBaddiesTemp
    lda     #5
    pshs    a
    ldx     #baddies+sizeof{Baddie}*4
@baddieLoop
    ;lda     ,s
    ;jsr     PrintHexByteWithSpace
    tst     Baddie.sprite.isActive,x
    beq     @nextBaddie
    bsr     TurnBaddieAndMoveForward
@nextBaddie
    ;
    leax    -sizeof{Baddie},x
    dec     ,s
    bne     @baddieLoop 
    puls    a
    ;
    rts

TurnBaddieAndMoveForward
    lda     Baddie.looking,x
    ldb     #sizeof{BaddieDirectionInfo}
    mul
    ldy     #baddiesDirectionData
    leay    d,y                     ; y = baddiesDirectionData entry for direction baddie is looking
    lda     Baddie.sprite.x+1,x
    anda    #$1F                    ; a = baddie's Sprite.x mod 32 (how far into the tile it's left side is)
    cmpa    #4
    lbge    @moveBaddieAndDone
    lda     Baddie.sprite.y+1,x
    anda    #$1F                    ; a = baddie's Sprite.y mod 32 (how far into the tile it's top side is)
    cmpa    #4
    lbge    @moveBaddieAndDone
    ;
    ldd     Baddie.sprite.x,x
    lsrd
    lsrd    
    lsrd
    lsrd
    lsrd                            ; d = baddie's x / 32 = baddie's top left corner mapX
    pshs    d
    ldd     Baddie.sprite.y,x
    andd    #$FFE0                  ; Sprite.y / 32 = Map Y, * 32 = offset into currentMap for row.  Sprite.y / 32 * 32 = Sprite.y and #$FFE0
    addd    ,s
    addd    #currentMap             ; d = Address of map cell in currentMap for baddie's top left corner
    std     ,s
    pshs    x
@checkLeftForSolid
    ldb     BaddieDirectionInfo.mapOffsetDirections+BaddieMapOffsets.left,y
    sex                              ; d = offset to map cell in currentMap to baddie's relative left
    addd    2,s                      ; d = Address of map cell in currentMap to baddie's relative left
    tfr     d,x
    lda     ,x                       ; a = map cell data of cell to baddie's relative left
    ldx     #mapDataToBaddiesSolidFlagLookup
    lda     a,x                      ; a = 1 if map cell to baddie's relative left is solid, otherwise 0.
    sta     @isLeftSolid,pcr
@checkRightForSolid
    ldb     BaddieDirectionInfo.mapOffsetDirections+BaddieMapOffsets.right,y
    sex                              ; d = offset to map cell in currentMap to baddie's relative right
    addd    2,s                      ; d = Address of map cell in currentMap to baddie's relative right
    tfr     d,x
    lda     ,x                       ; a = map cell data of cell to baddie's relative right
    ldx     #mapDataToBaddiesSolidFlagLookup
    lda     a,x                      ; a = 1 if map cell to baddie's relative right is solid, otherwise 0.
    sta     @isRightSolid,pcr
@checkFrontForSolid
    ldb     BaddieDirectionInfo.mapOffsetDirections+BaddieMapOffsets.front,y
    sex                              ; d = offset to map cell in currentMap to baddie's relative front
    addd    2,s                      ; d = Address of map cell in currentMap to baddie's relative front
    tfr     d,x
    lda     ,x                       ; a = map cell data of cell to baddie's relative front
    ldx     #mapDataToBaddiesSolidFlagLookup
    lda     a,x                      ; a = 1 if map cell to baddie's relative front is solid, otherwise 0.
    sta     @isFrontSolid,pcr
@checkBackForSolid
    ldb     BaddieDirectionInfo.mapOffsetDirections+BaddieMapOffsets.back,y
    sex                              ; d = offset to map cell in currentMap to baddie's relative back
    addd    2,s                      ; d = Address of map cell in currentMap to baddie's relative back
    tfr     d,x
    lda     ,x                       ; a = map cell data of cell to baddie's relative back
    ldx     #mapDataToBaddiesSolidFlagLookup
    lda     a,x                      ; a = 1 if map cell to baddie's relative back is solid, otherwise 0.
    sta     @isBackSolid,pcr
    ;
    puls    x
    puls    d
@checkAllSidesBlocked
    ldd     @isLeftSolid,pcr    ; a = @isLeftSolid, b = @isRightSolid
    anda    @isFrontSolid,pcr      ; a = @isLeftSolid & @isFrontSolid
    andb    @isBackSolid,pcr    ; b = @isRightSolid & @isBackSolid
    andr    b,a                 ; a = @isLeftSolid & @isFrontSolid & @isRightSolid & @isBackSolid
    bne     @done
@checkLeftRightFrontBlocked
    lda     @isLeftSolid,pcr
    anda    @isRightSolid,pcr
    anda    @isFrontSolid,pcr
    beq     @checkLeftRightFront
    lda     BaddieDirectionInfo.turnAroundDirection,y
    bra     @setNewDirection
@checkLeftRightFront
    tst     @isLeftSolid,pcr
    bne     @checkRightFront
    lda     BaddieDirectionInfo.leftDirection,y
    bra     @setNewDirection
@checkRightFront
    tst     @isFrontSolid,pcr
    beq     @moveBaddieAndDone
    lda     BaddieDirectionInfo.rightDirection,y
@setNewDirection
    sta     Baddie.looking,x
@moveBaddieAndDone
    lda     Baddie.looking,x
    ldb     #sizeof{BaddieDirectionInfo}
    mul
    ldy     #baddiesDirectionData
    leay    d,y                             ; y = baddiesDirectionData entry for direction baddie is now looking
    ldb     BaddieDirectionInfo.speedX,y
    sex
    ldw     Baddie.sprite.x,x
    addr    d,w
    stw     Baddie.sprite.x,x
    ldb     BaddieDirectionInfo.speedY,y
    sex
    ldw     Baddie.sprite.y,x
    addr    d,w
    stw     Baddie.sprite.y,x
@done
    rts
@isLeftSolid                fcb 0
@isRightSolid               fcb 0
@isFrontSolid               fcb 0
@isBackSolid                fcb 0

CheckHeroTouchedByBaddie    equ     CheckHeroTouchedByBaddieTemp+$4000
CheckHeroTouchedByBaddieTemp
    tst     currentJumpHeight+1
    lbne    @done
    ldx     hero+SpriteBase.x
    leax    4,x
    pshs    x                   ; 6,s = hero's left X + 4
    leax    24,x
    pshs    x                   ; 4,s = hero's right X - 4
    ldx     hero+SpriteBase.y
    leax    4,x
    pshs    x                   ; 2,s = hero's top Y + 4
    leax    24,x
    pshs    x                   ; ,s = hero's bottom Y - 4
    ;
    ldb     #5
    ldx     #baddies-sizeof{Baddie}
@baddieLoop
    leax    sizeof{Baddie},x
    tst     Baddie.sprite.isActive,x
    beq     @nextBaddie
    ldy     Baddie.sprite.x,x
    cmpy    4,s                 ; Hero's right
    bgt     @nextBaddie
    leay    24,y
    cmpy    6,s                 ; Hero's left
    blt     @nextBaddie
    ldy     Baddie.sprite.y,x
    cmpy    ,s                  ; Hero's bottom
    bgt     @nextBaddie
    leay    24,y
    cmpy    2,s                 ; Hero's top
    blt     @nextBaddie
    bsr     BounceAndDamageHeroIfHammerOrCheatNotActive.Local
    ;
@nextBaddie
    decb
    bne     @baddieLoop
    leas    8,s
    ;
@done
    rts

BounceAndDamageHeroIfHammerOrCheatNotActive.Local
    tst     hammerTimer
    bne     @done
    tst     cheat.HeroNoDamage
    bne     @done
    lda     #tunes.bounceDamage.index
    jsr     StartTuneIfNoTuneAlreadyPlaying
    neg     hero+SpriteBase.speedX
    neg     hero+SpriteBase.speedY
    lda     health
    suba    #$09
    sta     health
    bpl     @heroStillAlive
    clr     health
    clr     currentJumpIndex
    clr     currentJumpHeight
    clr     currentJumpHeight+1
    jmp     StartHeroDeath
@heroStillAlive
    ;
@done
    rts

ClearScoreMemoryForDebug    equ     ClearScoreMemoryForDebugTemp+$4000
ClearScoreMemoryForDebugTemp
    lda     #$C0
    ldb     #$03
    pshs    a
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    pshs    a
    clr     ,s
@loop
    lda     2,s
    jsr     MapPhysicalBlockToLogicalBlock
    ldx     #$6000
    ldw     #$2000
    tfm     s,x+
    inc     2,s
    bne     @loop
    puls    a
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    puls    a
    rts

DrawPowerUpTilesForLevel    equ     DrawPowerUpTilesForLevelTemp+$4000
DrawPowerUpTilesForLevelTemp
    lda     #6
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock  ; Map the Tile Data block containing the destination tiles
    pshs    a
    lda     #POWERUPS_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_C000_DFFF
    jsr     MapPhysicalBlockToLogicalBlock  ; Map the Tile Data block containing the destination tiles
    pshs    a
    lda     currentLevel
    ldx     #levelToFloorTileLookup
    lda     a,x                             ; a = Tile # for floor tile for this map
    sta     @restoreA+1,pcr
    ; Calculate the physical block the tile is in by taking the high nibble (4 bits)
    ; of the tile #. ex: Tile # $6B is in physical block $06
    lsra
    lsra
    lsra
    lsra
    ldb     #LOGICAL_6000_7FFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
@restoreA
    lda     #$00    ; value set above
    ; Calculate the offset into the tile #'s block where tile # starts by taking the
    ; low nibble of the tile # and multiplying by 512 (number of bytes in each tile).
    ; ex: Tile # $6B is at offset $B * 512, or $1600
    anda    #$0F
    lsla
    clrb                                    ; d now contains the low nibble of the tile # * 512
    ldx     #$6000
    leax    d,x                             ; x = address of 512 bytes for the floor tile to copy
    pshs    x
    ldy     #$A000+(512*4)                  ; y = address of first (of 8) 512 bytes to copy the floor tile to
    lda     #8
@nextTile
    ldw     #512
    tfr     y,u
    tfm     x+,y+
    cmpa    #1
    beq     @skipPowerUp                    ; Only draw the 7 powerups on the first 7 floor tiles.  Leave the last floor tile just floor.
    pshs    y,x,a
    nega
    adda    #8                              ; a = powerup index (0-6)
    lsla                                    ; a*2 = index into powerUpCompiledSprites to get the compiled sprite address
    ldx     #powerUpCompiledSprites
    jsr     [a,x]                           ; Call compiled sprite to draw the powerup on the floor tile we just copied
    puls    a,x,y
@skipPowerUp
    ldx     ,s
    deca
    bne     @nextTile
    puls    x
    ;
    puls    a
    ldb     #LOGICAL_6000_7FFF
    jsr     MapPhysicalBlockToLogicalBlock
    puls    a
    ldb     #LOGICAL_C000_DFFF
    jsr     MapPhysicalBlockToLogicalBlock
    puls    a
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    rts

LoadCurrentMap              equ     LoadCurrentMapTemp+$4000
LoadCurrentMapTemp
    ; Copy the current 512 byte map from the loaded map data (2 blocks at #MAP_DATA_PHYSICAL_BLOCK) to currentMap.
    lda     #MAP_DATA_PHYSICAL_BLOCK
    ldb     currentLevel
    cmpb    #16
    blt     @doneAdjustBlock
    inca
@doneAdjustBlock
    ldb     #LOGICAL_6000_7FFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     currentLevel
    anda    #$0F
    clrb
    lsla                                    ; d = offset into block where 512 byte map data is for currentLevel
    ldx     #$6000
    leax    d,x
    ldy     #currentMap
    ldw     #512
    tfm     x+,y+
    puls    a
    ldb     #LOGICAL_6000_7FFF
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    rts

InitializeState             equ     InitializeStateTemp+$4000
InitializeStateTemp
    lda     #$99
    sta     bcdTimeRemaining
    lda     #4
    sta     currentLevel
    ldq     #$00000000
    stq     bcdScore
    sta     bcdGems
    sta     bcdKeys
    lda     #$6E
    sta     timerForTimeRemainingDecrement
    ;
    clra
    pshs    a
    ldx     #levelCompletedFlags
    ldw     #32
    tfm     s,x+
    ;
    ldx     #redrawTile1
    ldw     #sizeof{RedrawTile}*2
    tfm     s,x+
    puls    a
    ;
    clr     controlFilter
    clr     <vsyncCount
    clr     <firqCount
    clr     <firqCount+1
    clr     <previousFramePlayfieldTopLeftY
    clr     <previousFramePlayfieldTopLeftY+1
    clr     heroIsOnMagnet
    clr     heroIsDead
    clr     wingTimer
    clr     hammerTimer
    clr     sparkleTimer
    clr     directionHeroIsLooking
    clr     preventFrictionFromSlowingHero
    clr     tunes.tonesRemaining
    clr     tunes.durationRemaining
    ;
    rts

InitializeStateForLevel     equ     InitializeStateForLevelTemp+$4000
InitializeStateForLevelTemp
    ldx     #levelBcdGemCounts
    ldb     currentLevel
    lda     b,x
    sta     bcdGems
    clra
    sta     bcdKeys
    lda     #$6E
    sta     timerForTimeRemainingDecrement
    ;
    clra
    pshs    a
    ldx     #redrawTile1
    ldw     #sizeof{RedrawTile}*2
    tfm     s,x+
    puls    a
    ;
    clr     <vsyncCount
    clr     <firqCount
    clr     <firqCount+1
    clr     <previousFramePlayfieldTopLeftY
    clr     <previousFramePlayfieldTopLeftY+1
    clr     controlFilter
    clr     heroIsOnMagnet
    clr     heroIsDead
    clr     wingTimer
    clr     hammerTimer
    clr     sparkleTimer
    clr     directionHeroIsLooking
    clr     preventFrictionFromSlowingHero
    clr     tunes.tonesRemaining
    clr     tunes.durationRemaining
    ;
    rts

InitializeHeroForLevel                  equ     InitializeHeroForLevelTemp+$4000
InitializeHeroForLevelTemp
    ldy     #hero
    ldx     #heroStartingMapCoordinates
    ldb     currentLevel
    lslb
    abx
    lda     ,x+
    clrb
    lsrd
    lsrd
    lsrd                                        ; d = starting map x * 32 = starting playfield x
    std     Hero.sprite.x,y
    std     SpriteBase.x+sizeof{Hero},y
    lda     ,x
    clrb
    lsrd
    lsrd
    lsrd                                        ; d = starting map y * 32 = starting playfield y
    std     Hero.sprite.y,y
    std     SpriteBase.y+sizeof{Hero},y
    ;
    rts

InitializeHero                          equ     InitializeHeroTemp+$4000
InitializeHeroTemp
    clr     currentJumpIndex
    clr     currentJumpHeight
    clr     currentJumpHeight+1
    ldy     #hero
    ldb     #1
    stb     Hero.sprite.isActive,y                              ; Hero is active
    stb     SpriteBase.isActive+sizeof{Hero},y                  ; Hero Shadow is active
    ;
    clr     Hero.sprite.speedY,y                                ; Hero's speed Y = 0
    clr     SpriteBase.speedY+sizeof{Hero},y                    ; Hero Shadow's speed Y = 0
    clr     Hero.sprite.speedX,y                                ; Hero's speed X = 0
    clr     SpriteBase.speedX+sizeof{Hero},y                    ; Hero Shadow's speed X = 0
    leax    Hero.sprite.background1,y
    stx     Hero.sprite.onScreenBackgroundAddress,y             ; Hero's on screen background address = Hero's background1
    clr     SpriteBackground.isCaptured,x                       ; Hero's on screen background is not captured
    leax    SpriteBase.background1+sizeof{Hero},y
    stx     SpriteBase.onScreenBackgroundAddress+sizeof{Hero},y ; Hero Shadow's on screen background address = Shadow's background1
    clr     SpriteBackground.isCaptured,x                       ; Hero Shadow's on screen background is not captured
    leax    Hero.sprite.background2,y
    stx     Hero.sprite.offScreenBackgroundAddress,y            ; Hero's off screen background address = Hero's background2
    clr     SpriteBackground.isCaptured,x                       ; Hero's off screen background is not captured
    leax    SpriteBase.background2+sizeof{Hero},y
    stx     SpriteBase.offScreenBackgroundAddress+sizeof{Hero},y    ; Hero Shadow's off screen background address = Shadow's background2
    clr     SpriteBackground.isCaptured,x                           ; Hero Shadow's off screen background is not captured
    ldx     #Hero.Idle
    stx     Hero.sprite.compiled,y                              ; Hero's compiled sprite = Hero.Idle
    ldx     #Hero.Shadow
    stx     SpriteBase.compiled+sizeof{Hero},y                  ; Hero Shadow's compiled sprite = Hero.Shadow
    lda     #HERO_NORMAL_DATA_PHYSICAL_BLOCK
    sta     Hero.sprite.compiledPhysicalBlock,y                 ; Hero's compiled physical block = HERO_NORMAL_DATA_PHYSICAL_BLOCK
    sta     SpriteBase.compiledPhysicalBlock+sizeof{Hero},y     ; Hero Shadow's compiled physical block = HERO_NORMAL_DATA_PHYSICAL_BLOCK
    ;
    rts

InitializeBaddiesForLevel               equ     InitializeBaddiesForLevelTemp+$4000
InitializeBaddiesForLevelTemp
    ldx     #baddiesLevelStartingData
    ldb     currentLevel
    lda     #15
    mul
    leax    d,x
    ldy     #baddies
    lda     #spriteCount-2
    pshs    a
@baddieLoop
    tst     BaddieStartingInfo.mapX,x
    bpl     @initializeBaddie
    clr     Baddie.sprite.isActive,y
    bra     @nextBaddie
@initializeBaddie
    ldb     #1
    stb     Baddie.sprite.isActive,y
    lda     BaddieStartingInfo.mapX,x
    clrb
    lsrd
    lsrd
    lsrd
    std     Baddie.sprite.x,y
    lda     BaddieStartingInfo.mapY,x
    clrb
    lsrd
    lsrd
    lsrd
    std     Baddie.sprite.y,y
    lda     BaddieStartingInfo.looking,x
    sta     Baddie.looking,y
    clr     Baddie.sprite.speedX,y
    clr     Baddie.sprite.speedY,y
    tfr     y,w
    addw    #SpriteBase.background1
    stw     Baddie.sprite.onScreenBackgroundAddress,y
    clr     SpriteBackground.isCaptured,w
    addw    #sizeof{SpriteBackground}
    stw     Baddie.sprite.offScreenBackgroundAddress,y
    clr     SpriteBackground.isCaptured,w
    ldw     #Baddie.Vertical1
    stw     Baddie.sprite.compiled,y
    lda     #BADDIES_DATA_PHYSICAL_BLOCK
    sta     Baddie.sprite.compiledPhysicalBlock,y
    ;
@nextBaddie
    leax    sizeof{BaddieStartingInfo},x
    leay    sizeof{Baddie},y
    dec     ,s
    lbne     @baddieLoop
    puls    a,pc

DisableAllBaddies                       equ     DisableAllBaddiesTemp+$4000
DisableAllBaddiesTemp
    ldy     #baddies
    lda     #spriteCount-2
@baddieLoop
    clr     Baddie.sprite.isActive,y
    leay    sizeof{Baddie},y
    deca
    bne     @baddieLoop
    rts

UpdateNextWingFrameIfWingsActive        equ     UpdateNextWingFrameIfWingsActiveTemp+$4000
UpdateNextWingFrameIfWingsActiveTemp
    tst     wingTimer
    beq     @done
    lda     wingFrameIndex
    adda    wingFrameDirection
    sta     wingFrameIndex
    beq     @reverseDirection
    cmpa    #Hero.Wings.FrameCount-1
    beq     @reverseDirection
    bra     @done
@reverseDirection
    neg     wingFrameDirection
@done
    rts

UpdateNextSparkleFrameIfSparklesActive  equ     UpdateNextSparkleFrameIfSparklesActiveTemp+$4000
UpdateNextSparkleFrameIfSparklesActiveTemp
    tst     sparkleTimer
    beq     @done
    dec     sparkleFrameIndex
    bpl     @done
    lda     #Hero.Sparkles.FrameCount-1
    sta     sparkleFrameIndex
@done
    rts

GoToLevelSelectionScreen                equ     GoToLevelSelectionScreenTemp+$4000
GoToLevelSelectionScreenTemp
    jsr     UpdateStateForNonPlayScreen
    jsr     UpdateScroll
    jsr     UpdateScore
    jsr     DrawLevelSelectScreen
    jsr     Set320x200x16_GraphicsMode
    jsr     Enable256ByteWideMode
    jsr     EnableSplitScreen
    lda     #tunes.levelSelectAndNewLevel.index
    jsr     StartTunePlaying
    jsr     FadeHeroInWithSparklesAndOpenEyes
    jsr     LevelSelectGameLoop
    ;
    rts

UpdateStateForNonPlayScreen             equ     UpdateStateForNonPlayScreenTemp+$4000
UpdateStateForNonPlayScreenTemp
    jsr     ResetJumpHeightAndTimers
    jsr     InitializeHero
    jsr     DisableAllBaddies
    ldd     #144
    std     hero+SpriteBase.x
    ldd     #40
    std     hero+SpriteBase.y
    jsr     ResetScreenBuffers
    jsr     SetEgaPalette
    ;
    rts

ResetJumpHeightAndTimers                equ     ResetJumpHeightAndTimersTemp+$4000
ResetJumpHeightAndTimersTemp
    clr     currentJumpIndex
    clr     currentJumpHeight
    clr     currentJumpHeight+1
    clr     hammerTimer
    clr     wingTimer
    clr     wingFrameIndex
    clr     wingFrameDirection
    inc     wingFrameDirection
    clr     sparkleTimer
    clr     sparkleFrameIndex
    rts

ResetScreenBuffers                      equ     ResetScreenBuffersTemp+$4000
ResetScreenBuffersTemp
    clrd
    std     playfieldTopLeftX
    std     playfieldTopLeftY
    std     previousFramePlayfieldTopLeftY
    std     screenBuffers+ScreenBuffer.verticalOffset
    std     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.verticalOffset
    sta     screenBuffers+ScreenBuffer.horizontalOffset
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.horizontalOffset
    std     screenBuffers+ScreenBuffer.leftPlayfieldXInSlice
    std     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.leftPlayfieldXInSlice
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    sta     screenBuffers+ScreenBuffer.firstPhysicalBlockOfBuffer
    sta     screenBuffers+ScreenBuffer.firstPhysicalBlockOfVerticalStripe
    lda     #BUFFER2_FIRST_PHYSICAL_BLOCK
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.firstPhysicalBlockOfBuffer
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.firstPhysicalBlockOfVerticalStripe
    lda     #BUFFER1_VIDEO_BANK
    sta     screenBuffers+ScreenBuffer.videoBank
    lda     #BUFFER2_VIDEO_BANK
    sta     screenBuffers+sizeof{ScreenBuffer}+ScreenBuffer.videoBank
    rts

ClearFirstVideoBufferForNonPlayScreen   equ     ClearFirstVideoBufferForNonPlayScreenTemp+$4000
ClearFirstVideoBufferForNonPlayScreenTemp
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    pshs    a
    ;
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock
    pshs    a
    ;
    pshs    a
    clr     ,s
@loop
    lda     2,s
    jsr     MapPhysicalBlockToLogicalBlock
    ldx     #$A000
    ldw     #$2000
    tfm     s,x+
    inc     2,s
    lda     2,s
    cmpa    #BUFFER1_FIRST_PHYSICAL_BLOCK+5
    blo     @loop
    puls    a
    ;
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    puls    a
    rts

CopyFirstVideoBufferToSecondForNonPlayScreen    equ     CopyFirstVideoBufferToSecondForNonPlayScreenTemp+$4000
CopyFirstVideoBufferToSecondForNonPlayScreenTemp
    lda     mmu+LOGICAL_A000_BFFF
    pshs    a
    lda     mmu+LOGICAL_C000_DFFF
    pshs    a
    ;
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    pshs    a
    ;
@loop
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock
    lda     ,s
    adda    #$40
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    ldx     #$A000
    ldy     #$C000
    ldw     #$2000
    tfm     x+,y+
    inc     ,s
    lda     ,s
    cmpa    #BUFFER1_FIRST_PHYSICAL_BLOCK+5
    blo     @loop
    ;
    puls    a
    ;
    puls    a
    ldb     #LOGICAL_C000_DFFF
    jsr     MapPhysicalBlockToLogicalBlock
    puls    a
    decb
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    rts

DrawLevelSelectScreen                   equ     DrawLevelSelectScreenTemp+$4000
DrawLevelSelectScreenTemp
    jsr     ClearFirstVideoBufferForNonPlayScreen
    jsr     DrawStarField
    lda     mmu+LOGICAL_A000_BFFF
    pshs    a
    lda     mmu+LOGICAL_C000_DFFF
    pshs    a
    ;
    ; Center Platform
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK+1
    ldb     #52
    ldx     #$A000+$1040
    jsr     Draw2ConsecutiveTilesInFirstVideoBuffer
    ;
    ; Left Platform
    lda     #3
    jsr     DrawLevelNameNearPlatform
    jsr     GetLevelCompleteOrNewLevelPlatformTileIndex
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK+1
    leax    -32,x
    jsr     Draw2ConsecutiveTilesInFirstVideoBuffer
    ;
    ; Right Platform
    lda     #2
    jsr     DrawLevelNameNearPlatform
    jsr     GetLevelCompleteOrNewLevelPlatformTileIndex
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK+1
    leax    +64,x
    jsr     Draw2ConsecutiveTilesInFirstVideoBuffer
    ;
    ; Top Platform
    lda     #0
    jsr     DrawLevelNameNearPlatform
    jsr     GetLevelCompleteOrNewLevelPlatformTileIndex
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK
    leax    -48,x
    jsr     Draw2ConsecutiveTilesInFirstVideoBuffer
    ;
    ; Bottom Platform
    lda     #1
    jsr     DrawLevelNameNearPlatform
    jsr     GetLevelCompleteOrNewLevelPlatformTileIndex
    lda     #BUFFER1_FIRST_PHYSICAL_BLOCK+2
    leax    32,x
    jsr     Draw2ConsecutiveTilesInFirstVideoBuffer
    ;
    puls    a
    ldb     #LOGICAL_C000_DFFF
    jsr     MapPhysicalBlockToLogicalBlock
    puls    a
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    jsr     CopyFirstVideoBufferToSecondForNonPlayScreen
    rts

Draw2ConsecutiveTilesInFirstVideoBuffer equ     Draw2ConsecutiveTilesInFirstVideoBufferTemp+$4000
Draw2ConsecutiveTilesInFirstVideoBufferTemp
; ```plaintext
; > a = First physical block to draw tiles in (this and next block will be mapped to $A000-$BFFF and $C000-$DFFF and not restored)
; > b = 1st Tile # to draw
; > x = Logical address to start drawing ($A000-$C000)
; ```
;***********************
    pshs    x,b,a
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock
    lda     ,s
    inca
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    lda     1,s
    jsr     DrawTileIn256ByteWideBuffer
    inca
    leax    16,x
    jsr     DrawTileIn256ByteWideBuffer
    puls    a,b,x,pc

GetLevelForPlatform                     equ     GetLevelForPlatformTemp+$4000
GetLevelForPlatformTemp
; ```plaintext
; > currentLevel = Current level
; > a = Platform # (0 = Top, 1 = Bottom, 2 = Right, 3 = Left)
; < b = level for that platform
; ```
;***********************
    pshs    a
    ldb     currentLevel
    cmpa    #0          ; 0 = Top platform
    bne     @checkForBottomPlatform
    subb    #4          ; Top platform gets 4 levels lower than current level (wrapping to highest levels)
    bpl     @done
    addb    #32
    bra     @done
@checkForBottomPlatform
    cmpa    #1          ; 1 = Bottom platform
    bne     @checkForRightPlatform
    addb    #4          ; Bottom platform gets 4 levels higher than current level (wrapping to lowest levesl)
    cmpb    #31
    ble     @done
    subb    #32
    bra     @done
@checkForRightPlatform
    cmpa    #2          ; 2 = Right Platform
    bne     @leftPlatform
    tfr     b,a
    inca
    andd    #$3FC
    orr     a,b         ; If current level MOD 4 < 3, right platform gets 1 level higher than the current level
    ;                   ; If current level MOD 4 == 3, right platform gets 3 levels lower than current level
    bra     @done
@leftPlatform
    tfr     b,a
    deca
    andd    #$3FC
    orr     a,b         ; If current level MOD 4 > 0, left platform gets 1 level lower than the current level
    ;                   ; If current level MOD 4 == 0, left platform gets 3 levels higher than current level
@done
    puls    a
    rts

DrawLevelNameNearPlatform               equ     DrawLevelNameNearPlatformTemp+$4000
DrawLevelNameNearPlatformTemp
; ```plaintext
; > a = Platform # (0 = Top, 1 = Bottom, 2 = Right, 3 = Left)
; ```
;***********************
    pshs    x,b,a                   ; 4,s (a) = platform # (0 = top, 1 = bottom, 2 = right, 3 = left)
    ldb     mmu+LOGICAL_6000_7FFF
    pshs    b
    ldb     mmu+LOGICAL_A000_BFFF
    pshs    b
    ldb     mmu+LOGICAL_C000_DFFF
    pshs    b
    ;
    jsr     GetLevelForPlatform
    pshs    b                       ; ,s (b) = level for that platform
    ;
    ; Map in the font compiled sprites
    lda     #CHARS_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_6000_7FFF
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    ; Map in the first block in the first video buffer where the level name should be drawn
    ldb     4,s                      ; Platform # (0 = top, 1 = bottom, 2 = right, 3 = left)
    lslb
    addb    4,s                      ; 3 * platform #
    ldx     #platformLevelNameLocations
    lda     b,x                     ; a = First block in first video buffer where the level name should be drawn
    incb
    ldu     b,x                     ; 16-bit Offset into that block
    leau    $A000,u                 ; u = the logical address where the level name should be drawn
    ldb     #LOGICAL_A000_BFFF
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    ; Map in the second block in the first video buffer where the level name should be drawn
    inca
    incb
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    ; Draw level name
    ldx     #levelNames
    ldb     ,s
    lslb                             ; b,x = offset into levelNames for the level name to draw
    ldx     b,x
@characterLoop
    lda     ,x+
    beq     @doneDrawing
    jsr     DrawFontCharacter
    leau    4,u
    bra     @characterLoop
@doneDrawing
    ;
    puls    b                        ; Discard level for that platform pushed earlier
    ldb     #LOGICAL_C000_DFFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    ldb     #LOGICAL_A000_BFFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    ldb     #LOGICAL_6000_7FFF
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    ;
    puls    a,b,x
    rts

DrawFontCharacter                       equ     DrawFontCharacterTemp+$4000
DrawFontCharacterTemp
; ```plaintext
; > a = Character to draw
; > u = Logical address to draw character
; > Font sprites mapped into LOGICAL_6000_7FFF
; > Physical block and next block mapped into logical bock and next block pointed to by u
; ```
;***********************
    pshs    u,x,a
    cmpa    #'0
    blo     @done
    cmpa    #'9
    bhi     @checkForUpperCase
    suba    #'0
    bra     @drawCharacter
@checkForUpperCase
    cmpa    #'A
    blo     @done
    cmpa    #'Z
    bhi     @done
    suba    #('A-10)
@drawCharacter
    ldx     #fontSprites
    lsla
    jsr     [a,x]
@done
    puls    a,x,u,pc

DrawStarField                           equ     DrawStarFieldTemp+$4000
DrawStarFieldTemp
    lda     mmu+LOGICAL_A000_BFFF
    pshs    a
    ldb     #LOGICAL_A000_BFFF
    ldx     #starLocations
@loop
    lda     ,x+
    beq     @doneDrawing
    jsr     MapPhysicalBlockToLogicalBlock
    ldy     ,x++
    leay    $A000,y
    lda     #$F0
    sta     ,y
    bra     @loop
@doneDrawing
    puls    a
    jsr     MapPhysicalBlockToLogicalBlock
    rts

LevelSelectGameLoop                     equ     LevelSelectGameLoopTemp+$4000
LevelSelectGameLoopTemp
@loop
    jsr     GetControlsPressed
    jsr     SetHeroLookDirectionForLevelSelectScreen
    tim     Controls.Fire;controls
    beq     @fireNotPressed
    ldx     #directionControlsToPlatformLookupForLevelSelectionScreen
    lda     controls
    anda    #%01111     ; Strip off the fire button bit
    lda     a,x         ; a = platform # being looked at (-1=none,0=top, 1=bottom, 2=right, 3=left)
    bpl     @firePressedWhileLookingAtPlatform
@fireNotPressed
    jsr     DrawHeroOffscreenWaitForGameFrameSwapBuffersAndProgressTune
    bra     @loop
    ;
@firePressedWhileLookingAtPlatform
    jsr     GetLevelForPlatform
    stb     currentLevel
    ldx     #speedsToMoveToPlatformForLevelSelectionScreen
    tfr     a,b     ; a = Platform # being looked at (0=top, 1=bottom, 2=right, 3=left)
    lslb
    lslb
    abx
    ldd     ,x
    ldy     2,x
    pshs    y,b,a     ; ,s = speed x to move to platform
                      ; 2,s= speed y to move to platform
    lda     #32
    sta     @moveFrameIndex,pcr ; Move to platform in 32 frames
@moveLoop
    ldx     hero+SpriteBase.x
    ldd     ,s
    leax    d,x
    stx     hero+SpriteBase.x
    ldx     hero+SpriteBase.y
    ldd     2,s
    leax    d,x
    stx     hero+SpriteBase.y
    jsr     DrawHeroOffscreenWaitForGameFrameSwapBuffersAndProgressTune
    dec     @moveFrameIndex,pcr
    bne     @moveLoop
    puls    a,b,y
    lda     #tunes.levelSelectedAndGameOver.index
    jsr     StartTunePlaying
    jsr     CloseHerosEyesAndFadeOutWithSparkles
    ;
    rts
@moveFrameIndex     fcb     0

SetHeroLookDirectionForLevelSelectScreen equ     SetHeroLookDirectionForLevelSelectScreenTemp+$4000
SetHeroLookDirectionForLevelSelectScreenTemp
    ldx     #directionControlsToHeroIndexLookupForLevelSelectionScreen
    lda     controls
    anda    #%01111     ; Strip off the fire button bit
    lda     a,x
    jsr     SetHeroCompiledSpriteAndOverrideIfHammerTimerActive
    rts

PlayHeroAnimation                       equ     PlayHeroAnimationTemp+$4000
PlayHeroAnimationTemp
; ```plaintext
; > x = Address of HeroAnimtion to play
; ```
;***********************
    lda     HeroAnimation.spritesPhysicalBlock,x
    sta     hero+SpriteBase.compiledPhysicalBlock
    ldy     HeroAnimation.spritesLookup,x
    lda     HeroAnimation.firstFrameIndex,x
    sta     @frameIndex,pcr
    cmpa    HeroAnimation.lastFrameIndex,x
    bmi     @increaseFrames
    lda     #$6A        ; dec @label,pcr
    bra     @continue
@increaseFrames
    lda     #$6C        ; inc @label,pcr
@continue
    sta     @nextFrame,pcr
    jsr     WaitForVsync
    clr     <vsyncCount
    pshs    y,x
@animationLoop
    lda     @frameIndex,pcr
    lsla
    ldd     a,y             ; d = compiled sprite address
    std     hero+SpriteBase.compiled
    jsr     UpdatePlayfieldTopLeftBasedOnHeroLocation
    jsr     TurnAndMoveAllBaddies
    jsr     UpdateScroll
    jsr     UpdateScore
    jsr     UpdateSprites
    jsr     DrawTilesFromMapToOffScreenBufferIfNeeded
    jsr     SaveOffScreenSpriteBackgrounds
    jsr     UpdateNextSparkleFrameIfSparklesActive
    jsr     DrawOffScreenSpritesWithoutHeroShadow ; DrawOffScreenHeroWithAdornmentsWithoutShadow
    ;
    jsr     WaitForNextGameFrame
    jsr     SwapVideoBuffers
    jsr     RestoreOffScreenSpriteBackgrounds
    jsr     ProgressTuneIfPlaying
    ;
    ldx     ,s
    ldy     2,s
    lda     @frameIndex,pcr
    cmpa    HeroAnimation.lastFrameIndex,x
    beq     @done
@nextFrame
    dec     @frameIndex,pcr
    lbra    @animationLoop
    ;
@done
    puls    x,y
    rts
@frameIndex     fcb     0

FadeHeroInWithSparklesAndOpenEyes       equ     FadeHeroInWithSparklesAndOpenEyesTemp+$4000
FadeHeroInWithSparklesAndOpenEyesTemp
    lda     #$20
    sta     sparkleTimer
    ldx     #fadeInAnimation
    jsr     PlayHeroAnimation
    ldx     #openEyesAnimation
    jsr     PlayHeroAnimation
    clr     sparkleTimer
    rts

CloseHerosEyesAndFadeOutWithSparkles       equ     CloseHerosEyesAndFadeOutWithSparklesTemp+$4000
CloseHerosEyesAndFadeOutWithSparklesTemp
    ldx     #closeEyesAnimation
    jsr     PlayHeroAnimation
    lda     #$20
    sta     sparkleTimer
    ldx     #fadeOutAnimation
    jsr     PlayHeroAnimation
    clr     sparkleTimer
    rts

WaitForNextGameFrame                    equ     WaitForNextGameFrameTemp+$4000
WaitForNextGameFrameTemp
    jsr     WaitForVsync
    lda     <vsyncCount
    cmpa    #GAME_FRAME_VSYNCS
    blt     WaitForNextGameFrameTemp
    clr     <vsyncCount
    rts

DrawHeroOffscreenWaitForGameFrameSwapBuffersAndProgressTune equ     DrawHeroOffscreenWaitForGameFrameSwapBuffersAndProgressTuneTemp+$4000
DrawHeroOffscreenWaitForGameFrameSwapBuffersAndProgressTuneTemp
    jsr     UpdatePlayfieldTopLeftBasedOnHeroLocation
    jsr     UpdateScore
    jsr     UpdateSprites
    jsr     DrawTilesFromMapToOffScreenBufferIfNeeded
    jsr     SaveOffScreenSpriteBackgrounds
    jsr     DrawOffScreenHeroAndShadowWithAdornments
    ;
    jsr     WaitForNextGameFrame
    jsr     SwapVideoBuffers
    jsr     RestoreOffScreenSpriteBackgrounds
    jsr     ProgressTuneIfPlaying
    rts

MoveAndDrawBaddiesDrawHeroWithShadowProgressTune    equ MoveAndDrawBaddiesDrawHeroWithShadowProgressTuneTemp+$4000
MoveAndDrawBaddiesDrawHeroWithShadowProgressTuneTemp
    jsr     UpdatePlayfieldTopLeftBasedOnHeroLocation
    jsr     TurnAndMoveAllBaddies
    jsr     UpdateScore
    jsr     UpdateSprites
    jsr     DrawTilesFromMapToOffScreenBufferIfNeeded
    jsr     SaveOffScreenSpriteBackgrounds
    jsr     DrawOffScreenSpritesWithoutHeroShadow
    ;
    jsr     WaitForNextGameFrame
    jsr     SwapVideoBuffers
    jsr     RestoreOffScreenSpriteBackgrounds
    jsr     ProgressTuneIfPlaying
    rts

WinLevel                                equ     WinLevelTemp+$4000
WinLevelTemp
    jsr     DrawHeroOffscreenWaitForGameFrameSwapBuffersAndProgressTune    
    jsr     DrawHeroOffscreenWaitForGameFrameSwapBuffersAndProgressTune    
    clr     timerForTimeRemainingDecrementActive
    clr     currentJumpIndex
    lda     bcdTimeRemaining
    clrb
    jsr     AddToBcdScore
    ldx     #bcdTimeRemaining
    lda     #$20
    jsr     AddToSingleByteBcd
    lda     bcdTimeRemaining
    cmpa    #$20
    bhs     @doneCappingTimeRemaining
    lda     #$99
@doneCappingTimeRemaining
    sta     bcdTimeRemaining
    jsr     DrawHeroOffscreenWaitForGameFrameSwapBuffersAndProgressTune    
    lda     #tunes.openTitleWinLevel.index
    jsr     StartTunePlaying
    jsr     CloseHerosEyesAndFadeOutWithSparkles
    jsr     FlagLevelAsComplete
@done
    rts

FlagLevelAsComplete                     equ     FlagLevelAsCompleteTemp+$4000
FlagLevelAsCompleteTemp
    ldx     #levelCompletedFlags
    ldb     currentLevel
    lda     #1
    sta     b,x
    rts

GetLevelCompleteOrNewLevelPlatformTileIndex equ     GetLevelCompleteOrNewLevelPlatformTileIndexTemp+$4000
GetLevelCompleteOrNewLevelPlatformTileIndexTemp
; ```plaintext
; > currentLevel = Current level
; > a = Platform # (0 = Top, 1 = Bottom, 2 = Right, 3 = Left)
; < b = Tile index for first of two consecutive tiles to draw for the platform
; ```
;***********************
    pshs    x
    jsr     GetLevelForPlatform
    ldx     #levelCompletedFlags
    tst     b,x
    bne     @levelNotComplete
    ldb     #$3A
    bra     @done
@levelNotComplete
    ldb     #$30
    ;
@done
    puls    x
    rts

HandleHeroDeath                         equ     HandleHeroDeathTemp+$4000
HandleHeroDeathTemp
    clr     timerForTimeRemainingDecrementActive
    jsr     ResetJumpHeightAndTimers
    lda     bcdTimeRemaining
    lda     #$10
    ldx     #bcdTimeRemaining
    jsr     SubtractFromSingleByteBcd
    bcs     @doneUnderflowCheck
    clra
    sta     bcdTimeRemaining
@doneUnderflowCheck
    jsr     UpdateScore
    lda     #15*3
    pshs    a
    clr     <vsyncCount
@loop
    jsr     MoveAndDrawBaddiesDrawHeroWithShadowProgressTune
    dec     ,s
    bne     @loop
    puls    a,pc

    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
                fcb     PHYSICAL_074000_075FFF

    END     start
