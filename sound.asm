    org     $4000

irqCount                fcb     0
firqCount               fcb     0
firqsPerIrq             fcb     0
originalVectors         rmb     18

tunes.falling.waveTable equ     sineWave
tunes.falling.length    equ     8
tunes.falling           fcb $75,$03,3  ; 2386 Hz * 256 / 5220 * 256 = $7503
                        fcb $70,$80,3  ; 2294 Hz * 256 / 5220 * 256 = $7080
                        fcb $6C,$55,3  ; 2209 Hz * 256 / 5220 * 256 = $6C55
                        fcb $68,$75,3  ; 2130 Hz * 256 / 5220 * 256 = $6875
                        fcb $64,$E1,3  ; 2057 Hz * 256 / 5220 * 256 = $64E1
                        fcb $61,$7E,3  ; 1988 Hz * 256 / 5220 * 256 = $617E
                        fcb $5E,$5B,3  ; 1924 Hz * 256 / 5220 * 256 = $5E5B
                        fcb $5B,$6A,3  ; 1864 Hz * 256 / 5220 * 256 = $5B6A

tunes.jump.waveTable    equ     squareWave
tunes.jump.length       equ     4
tunes.jump              fcb $08,$56,3  ;  170 Hz * 256 / 5220 * 256 = $0856
                        fcb $09,$B6,3  ;  198 Hz * 256 / 5220 * 256 = $09B6
                        fcb $08,$FA,3  ;  183 Hz * 256 / 5220 * 256 = $08FA
                        fcb $0A,$98,3  ;  216 Hz * 256 / 5220 * 256 = $0A98

tunes.test.waveTable    equ     squareWave
tunes.test.length       equ     1
;tunes.test              fcb $0A,$CA,30  ;  220 Hz * 256 / 5210 * 256 = $0ACA
tunes.test              fcb $75,$03,30 ; 2386 Hz * 256 / 5210 * 256 = $7503

Start
    clr     $FFD9                   ; Set Fast Speed
    orcc    #%01010000
    jsr     SetupInterruptSources
    jsr     InitializeSound
    andcc   #%10101111
    ;
    jsr     TestSound
    ;    
*     ldd     #tunes.test.waveTable
*     std     loadWaveTable+1
*     lda     #tunes.test.length
*     pshs    a
*     ldx     #tunes.test
*     clr     irqCount
* @w1 tst     irqCount
*     beq     @w1
* @loop
*     clr     irqCount
*     ldd     ,x++
*     std     freqValue+1
* @w2 lda     irqCount
*     cmpa    ,x
*     ble     @w2
*     ;
*     leax    1,x
*     dec     ,s
*     bne     @loop
*     puls    a
    ;
    orcc    #%01010000
    jsr     UninitializeSound
    jsr     RestoreInterruptSources
    andcc   #%10101111
    clr     $FFD8                   ; Set Slow Speed
    rts

TestSound
    ldx     #squareWave
    stx     loadWaveTable+1
    ldx     #$0100
    clr     irqCount
@w1 tst     irqCount
    beq     @w1
@loop
    clr     irqCount
    stx     freqValue+1
@w2 lda     irqCount
    cmpa    #5
    ble     @w2
    leax    256,x
    tfr     x,d
    tsta
    bpl     @loop
    rts

FIRQHandler
    std     @restoreD+1
    andcc   #%11101111              ; unmask IRQ so we don't delay vsync.  Vsync needs to restart the timer precisely on time.
    inc     firqCount
@wavePtr
    ldd     #$0000                  ; Continuously update by next line after freqValue
freqValue
    addd    #$0000
    std     @wavePtr+1
    sta     loadWaveTable+2
loadWaveTable
    lda     >squareWave
    sta     $FF20
@restoreD
    ldd     #0000
    tst     $FF93                   ; Acknowledge the FIRQ interrupt
    rti

IRQHandler
    lda     firqCount
    sta     firqsPerIrq
    ;clr     firqCount
    inc     irqCount
    ;
    tst     $FF92                   ; Acknowledge the IRQ interrupt
    rti

SetupInterruptSources
;***********************
    lda     $FF01
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable PIA HSYNC IRQ
    sta     $FF01
    ;
    lda     $FF03
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable PIA VSYNC IRQ
    sta     $FF03
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable PIA Cassette Data FIRQ
    sta     $FF21
    ;
    lda     $FF23
    ;        PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable PIA Cartridge FIRQ
    sta     $FF23
    ;
    ;         Initialization Register 0 - INIT0 
    ;         ┌────────── Bit  7   - CoCo Bit (0 = CoCo 3 Mode, 1 = CoCo 1/2 Compatible)
    ;         │┌───────── Bit  6   - M/P (1 = MMU enabled, 0 = MMU disabled)
    ;         ││┌──────── Bit  5   - IEN (1 = GIME IRQ output enabled to CPU, 0 = disabled)
    ;         │││┌─────── Bit  4   - FEN (1 = GIME FIRQ output enabled to CPU, 0 = disabled)
    ;         ││││┌────── Bit  3   - MC3 (1 = Vector RAM at FEXX enabled, 0 = disabled)
    ;         │││││┌───── Bit  2   - MC2 (1 = Standard SCS (DISK) (0=expand 1=normal))
    ;         ││││││┌┬─── Bits 1-0 - MC1-0 (10 = ROM Map 32k Internal, 0x = 16K Internal/16K External, 11 = 32K External - Except Interrupt Vectors)
    lda     #%01111110
    sta     $FF90               ; CoCo 3 Mode; Enable MMU, IRQ, FIRQ; Vector RAM at $FFEx; ROM 32k internal
    ;
    ;         Initialization Register 1 - INIT1
    ;         ┌────────── Bit  7   - Unused
    ;         │┌───────── Bit  6   - Memory type (1=256K, 0=64K chips)
    ;         ││┌──────── Bit  5   - Timer input clock source (1=279.365 nsec, 0=63.695 usec)
    ;         │││┌┬┬┬──── Bits 4-1 - Unused
    ;         │││││││┌─── Bits 0   - MMU Task Register select (0=enable $FFA0-$FFA7, 1=enable $FFA8-$FFAF)
    lda     #%00100000
    sta     $FF91               ; Timer 63.695 usec (*10^-6, low speed); MMU $FFA0-$FFA7 task selected
    ;
    ;         Interrupt Request Enable Register - IRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00001000
    sta     $FF92        ; Enable IRQS: VSYNC; Disable IRQs: Timer, HSYNC, RS232, Keyboard, Cartridge
    ;
    ;         Fast Interrupt Request Enable Register - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00000000
    sta     $FF93    ; Disable FIRQS: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    ;
    lbsr    SetupInterruptVectors
    ;
    rts

SetupInterruptVectors:
;***********************
    pshs    y,x,b,a
    ;
    ldx     #$FEEE
    ldy     #originalVectors
    lda     #9
    pshs    a
@saveVectors
    ldd     ,x
    std     ,y++
    ldd     #$3838          ; rti rti
    std     ,x++
    dec     ,s
    bne     @saveVectors
    puls    a
    ;
    ldx     #$FEF4
    lda     #$7E            ; jmp
    sta     ,x+
    ldy     #FIRQHandler
    sty     ,x++
    sta     ,x+
    ldy     #IRQHandler
    sty     ,x++
    ;
    ldd     #685
    stb     $FF95
    sta     $FF94
    ;
    ;         Fast Interrupt Request Enable Registers - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer FIRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync FIRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync FIRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data FIRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard FIRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge FIRQ, 0 = disable)
    lda     #%00100000
    sta     $FF93    ; Enable Timer FIRQ
    ;
    puls    a,b,x,y,pc

RestoreInterruptSources
;***********************
    lda     $FF03
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    ora     #%00000001          ; Enable VSYNC IRQ
    sta     $FF03
    ;
    lda     $FF01
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable HSYNC IRQ
    sta     $FF01
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable the Cassette Data FIRQ
    sta     $FF21
    ;
    lda     $FF23
    ;        PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable the Cartridge FIRQ
    sta     $FF23
    ;
    ;         Initialization Register 0 - INIT0 
    ;         ┌────────── Bit  7   - CoCo Bit (0 = CoCo 3 Mode, 1 = CoCo 1/2 Compatible)
    ;         │┌───────── Bit  6   - M/P (1 = MMU enabled, 0 = MMU disabled)
    ;         ││┌──────── Bit  5   - IEN (1 = GIME IRQ output enabled to CPU, 0 = disabled)
    ;         │││┌─────── Bit  4   - FEN (1 = GIME FIRQ output enabled to CPU, 0 = disabled)
    ;         ││││┌────── Bit  3   - MC3 (1 = Vector RAM at FEXX enabled, 0 = disabled)
    ;         │││││┌───── Bit  2   - MC2 (1 = Standard SCS (DISK) (0=expand 1=normal))
    ;         ││││││┌┬─── Bits 1-0 - MC1-0 (10 = ROM Map 32k Internal, 0x = 16K Internal/16K External, 11 = 32K External - Except Interrupt Vectors)
    lda     #%01001110
    sta     $FF90
    ;
    ;         Fast/Interrupt Request Enable Registers - IRQENR/FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00000000
    sta     $FF92               ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     $FF93               ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    ;
    lda     $FF92
    lda     $FF93
    lbsr    RestoreInterruptVectors
    ;
    rts

RestoreInterruptVectors:
;***********************
    pshs    y,x
    ;
    ;         Fast Interrupt Request Enable Registers - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer FIRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync FIRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync FIRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data FIRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard FIRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge FIRQ, 0 = disable)
    lda     #%00000000
    sta     $FF93    ; Disable Timer FIRQ
    ;
    ldx     #originalVectors
    ldy     #$FEEE
    lda     #9
    pshs    a
@restoreVectors
    ldd     ,x++
    std     ,y++
    dec    ,s
    bne    @restoreVectors
    puls    a
    ;
    puls    x,y,pc

InitializeSound
    lda     $FF01
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11110111
    sta     $FF01               ; MSB & LSB of MUX set to 0 = Select DAC (Digital to Analog Converter) as sound source
    ;
    lda     $FF03
    ;         PIA 0 Side B Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11110111          ; MSB & LSB set to 0 = Select DAC (Digital to Analog Converter) as sound source
    sta     $FF03
    ;
    lda     $FF23
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    ora     #%00001000          ; Enable Sound
    sta     $FF23
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    anda    #%11111011          ; Make $FF20 configure direction for DAC, RS-323C, and Cassette
    sta     $FF21
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%11111100          ; Set DAC to Output; Set RS-232C and Cassette to Input (so writes to those two bits do nothing)
    sta     $FF20
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    ora     #%00000100          ; Make $FF20 function as input/output again
    sta     $FF21
    ;
    rts

UninitializeSound
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    anda    #%11111011          ; Make $FF20 configure direction for DAC, RS-323C, and Cassette
    sta     $FF21
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%11111110
    sta     $FF20               ; DAC/RS-232C to Output, Cassette to Input
    ;
    lda     $FF23
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    anda    #%11110111          ; Disable Sound
    sta     $FF23
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    ora     #%00000100          ; Make $FF20 function as input/output again
    sta     $FF21
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%00000010
    sta     $FF20                                   ; Set RS-232C idle
    ;
    rts

filler              rmb     256-(*%256)
squareWave
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

sineWave
                    fcb     $7C,$80,$84,$84,$88,$8C,$90,$90,$94,$98,$9C,$9C,$A0,$A4,$A8,$A8
                    fcb     $AC,$B0,$B4,$B4,$B8,$BC,$BC,$C0,$C4,$C4,$C8,$C8,$CC,$D0,$D0,$D4
                    fcb     $D4,$D8,$D8,$DC,$DC,$E0,$E0,$E4,$E4,$E8,$E8,$E8,$EC,$EC,$F0,$F0
                    fcb     $F0,$F0,$F4,$F4,$F4,$F4,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8
                    fcb     $F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F4,$F4,$F4,$F4,$F0
                    fcb     $F0,$F0,$EC,$EC,$EC,$E8,$E8,$E4,$E4,$E4,$E0,$E0,$DC,$DC,$D8,$D8
                    fcb     $D4,$D4,$D0,$CC,$CC,$C8,$C8,$C4,$C0,$C0,$BC,$B8,$B8,$B4,$B0,$AC
                    fcb     $AC,$A8,$A4,$A4,$A0,$9C,$98,$98,$94,$90,$8C,$88,$88,$84,$80,$7C
                    fcb     $7C,$78,$74,$70,$70,$6C,$68,$64,$60,$60,$5C,$58,$54,$54,$50,$4C
                    fcb     $4C,$48,$44,$40,$40,$3C,$38,$38,$34,$30,$30,$2C,$2C,$28,$24,$24
                    fcb     $20,$20,$1C,$1C,$18,$18,$14,$14,$14,$10,$10,$0C,$0C,$0C,$08,$08
                    fcb     $08,$04,$04,$04,$04,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$04,$04,$04,$04,$08,$08
                    fcb     $08,$08,$0C,$0C,$10,$10,$10,$14,$14,$18,$18,$1C,$1C,$20,$20,$24
                    fcb     $24,$28,$28,$2C,$30,$30,$34,$34,$38,$3C,$3C,$40,$44,$44,$48,$4C
                    fcb     $50,$50,$54,$58,$5C,$5C,$60,$64,$68,$68,$6C,$70,$74,$74,$78,$7C

sawtoothWave
                    fcb     $00,$00,$00,$00,$04,$04,$04,$04,$08,$08,$08,$08,$0C,$0C,$0C,$0C
                    fcb     $10,$10,$10,$10,$14,$14,$14,$14,$18,$18,$18,$18,$1C,$1C,$1C,$1C
                    fcb     $20,$20,$20,$20,$24,$24,$24,$24,$28,$28,$28,$28,$2C,$2C,$2C,$2C
                    fcb     $30,$30,$30,$30,$34,$34,$34,$34,$38,$38,$38,$38,$3C,$3C,$3C,$3C
                    fcb     $40,$40,$40,$40,$44,$44,$44,$44,$48,$48,$48,$48,$4C,$4C,$4C,$4C
                    fcb     $50,$50,$50,$50,$54,$54,$54,$54,$58,$58,$58,$58,$5C,$5C,$5C,$5C
                    fcb     $60,$60,$60,$60,$64,$64,$64,$64,$68,$68,$68,$68,$6C,$6C,$6C,$6C
                    fcb     $70,$70,$70,$70,$74,$74,$74,$74,$78,$78,$78,$78,$7C,$7C,$7C,$7C
                    fcb     $80,$80,$80,$80,$84,$84,$84,$84,$88,$88,$88,$88,$8C,$8C,$8C,$8C
                    fcb     $90,$90,$90,$90,$94,$94,$94,$94,$98,$98,$98,$98,$9C,$9C,$9C,$9C
                    fcb     $A0,$A0,$A0,$A0,$A4,$A4,$A4,$A4,$A8,$A8,$A8,$A8,$AC,$AC,$AC,$AC
                    fcb     $B0,$B0,$B0,$B0,$B4,$B4,$B4,$B4,$B8,$B8,$B8,$B8,$BC,$BC,$BC,$BC
                    fcb     $C0,$C0,$C0,$C0,$C4,$C4,$C4,$C4,$C8,$C8,$C8,$C8,$CC,$CC,$CC,$CC
                    fcb     $D0,$D0,$D0,$D0,$D4,$D4,$D4,$D4,$D8,$D8,$D8,$D8,$DC,$DC,$DC,$DC
                    fcb     $E0,$E0,$E0,$E0,$E4,$E4,$E4,$E4,$E8,$E8,$E8,$E8,$EC,$EC,$EC,$EC
                    fcb     $F0,$F0,$F0,$F0,$F4,$F4,$F4,$F4,$F8,$F8,$F8,$F8,$FC,$FC,$FC,$FC

midStepWave
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC
                    fcb     $7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C
                    fcb     $7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C
                    fcb     $7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C
                    fcb     $7C,$7C,$7C
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
                    fcb     $00,$00,$00,$00
                    fcb     $7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C
                    fcb     $7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C
                    fcb     $7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C,$7C
                    fcb     $7C,$7C,$7C
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC,$FC
                    fcb     $FC,$FC,$FC

    END     Start
