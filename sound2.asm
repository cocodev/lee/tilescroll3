    org     $4000

irqCount                fcb     0
firqCount               fcb     0
firqsPerIrq             fcb     0
originalVectors         rmb     18

Start
    clr     $FFD9                   ; Set Fast Speed
    orcc    #%01010000
    jsr     Set80ColumnMode
    jsr     SetupInterruptSources
    jsr     InitializeSound
    andcc   #%10101111
    ;
    jsr     TestSound
    ;    
    orcc    #%01010000
    jsr     UninitializeSound
    jsr     RestoreInterruptSources
    andcc   #%10101111
    clr     $FFD8                   ; Set Slow Speed
    rts

TestSound
    lda     tunes.fall.length
    pshs    a
    ldx     #tunes.fall
    clr     irqCount
@w1 tst     irqCount
    beq     @w1
@loop
    clr     irqCount
    lda     ,x+
    sta     freqCount+1
@w2 lda     irqCount
    cmpa    ,x
    ble     @w2
    ;
    leax    1,x
    dec     ,s
    bne     @loop
    puls    a
    rts

@count      fcb     1
FIRQHandler
    std     @restoreD+1
    andcc   #%11101111              ; unmask IRQ so we don't delay VSYNC.  In real game, VSYNC needs to restart the timer precisely on time.
    inc     firqCount
    dec     @count
    bne     @continue
    lda     @continue+1
    coma
    anda    #%11111100
    sta     @continue+1
freqCount
    lda     #00
    sta     @count
@continue
    lda     #00
    sta     $FF20
@restoreD
    ldd     #0000
    tst     $FF93                   ; Acknowledge the FIRQ interrupt
    rti

IRQHandler
    lda     firqCount
    sta     firqsPerIrq
    ;clr     firqCount
    inc     irqCount
    ;
    tst     $FF92                   ; Acknowledge the IRQ interrupt
    rti

Set80ColumnMode
    ;
    ;         Video Mode
    ;         ┌────────── Bit  7   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%00000011
    sta     $FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (Graphics: 000=16,001=20,010=32,011=40,100=64,101=80,110=128,111=160 bytes per row, Text: 0x0=32,0x1=40,1x0=64,1x1=80)
    ;         ││││││┌┬─── Bits 1-0 - CRES (Graphics: 00=2, 01=4, 10=16, 11=undefined colors, Text: x0=No color attributes, x1=Color attributes)
    lda     #%00010101
    sta     $FF99
    ;
    ;         Video Bank Select
    ;         ┌┬┬┬┬┬───── Bits 7-2 - Unused
    ;         ││││││┌┬─── Bits 1-0 - VBANK (512K bank to use for video memory, 0-3)
    lda     #%00000011
    sta     $FF9B
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen Enable
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     $FF9F
    ;
    rts    


SetupInterruptSources
;***********************
    lda     $FF01
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable PIA HSYNC IRQ
    sta     $FF01
    ;
    lda     $FF03
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable PIA VSYNC IRQ
    sta     $FF03
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable PIA Cassette Data FIRQ
    sta     $FF21
    ;
    lda     $FF23
    ;        PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable PIA Cartridge FIRQ
    sta     $FF23
    ;
    ;         Initialization Register 0 - INIT0 
    ;         ┌────────── Bit  7   - CoCo Bit (0 = CoCo 3 Mode, 1 = CoCo 1/2 Compatible)
    ;         │┌───────── Bit  6   - M/P (1 = MMU enabled, 0 = MMU disabled)
    ;         ││┌──────── Bit  5   - IEN (1 = GIME IRQ output enabled to CPU, 0 = disabled)
    ;         │││┌─────── Bit  4   - FEN (1 = GIME FIRQ output enabled to CPU, 0 = disabled)
    ;         ││││┌────── Bit  3   - MC3 (1 = Vector RAM at FEXX enabled, 0 = disabled)
    ;         │││││┌───── Bit  2   - MC2 (1 = Standard SCS (DISK) (0=expand 1=normal))
    ;         ││││││┌┬─── Bits 1-0 - MC1-0 (10 = ROM Map 32k Internal, 0x = 16K Internal/16K External, 11 = 32K External - Except Interrupt Vectors)
    lda     #%01111110
    sta     $FF90               ; CoCo 3 Mode; Enable MMU, IRQ, FIRQ; Vector RAM at $FFEx; ROM 32k internal
    ;
    ;         Initialization Register 1 - INIT1
    ;         ┌────────── Bit  7   - Unused
    ;         │┌───────── Bit  6   - Memory type (1=256K, 0=64K chips)
    ;         ││┌──────── Bit  5   - Timer input clock source (1=279.365 nsec, 0=63.695 usec)
    ;         │││┌┬┬┬──── Bits 4-1 - Unused
    ;         │││││││┌─── Bits 0   - MMU Task Register select (0=enable $FFA0-$FFA7, 1=enable $FFA8-$FFAF)
    lda     #%00100000
    sta     $FF91               ; Timer 63.695 usec (*10^-6, low speed); MMU $FFA0-$FFA7 task selected
    ;
    ;         Interrupt Request Enable Register - IRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00001000
    sta     $FF92        ; Enable IRQS: VSYNC; Disable IRQs: Timer, HSYNC, RS232, Keyboard, Cartridge
    ;
    ;         Fast Interrupt Request Enable Register - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00000000
    sta     $FF93    ; Disable FIRQS: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    ;
    lbsr    SetupInterruptVectors
    ;
    rts

SetupInterruptVectors:
;***********************
    pshs    y,x,b,a
    ;
    ldx     #$FEEE
    ldy     #originalVectors
    lda     #9
    pshs    a
@saveVectors
    ldd     ,x
    std     ,y++
    ldd     #$3838          ; rti rti
    std     ,x++
    dec     ,s
    bne     @saveVectors
    puls    a
    ;
    ldx     #$FEF4
    lda     #$7E            ; jmp
    sta     ,x+
    ldy     #FIRQHandler
    sty     ,x++
    sta     ,x+
    ldy     #IRQHandler
    sty     ,x++
    ;
    ldd     #685
    stb     $FF95
    sta     $FF94
    ;
    ;         Fast Interrupt Request Enable Registers - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer FIRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync FIRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync FIRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data FIRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard FIRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge FIRQ, 0 = disable)
    lda     #%00100000
    sta     $FF93    ; Enable Timer FIRQ
    ;
    puls    a,b,x,y,pc

RestoreInterruptSources
;***********************
    lda     $FF03
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    ora     #%00000001          ; Enable VSYNC IRQ
    sta     $FF03
    ;
    lda     $FF01
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable HSYNC IRQ
    sta     $FF01
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable the Cassette Data FIRQ
    sta     $FF21
    ;
    lda     $FF23
    ;        PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    anda    #%11111110          ; Disable the Cartridge FIRQ
    sta     $FF23
    ;
    ;         Initialization Register 0 - INIT0 
    ;         ┌────────── Bit  7   - CoCo Bit (0 = CoCo 3 Mode, 1 = CoCo 1/2 Compatible)
    ;         │┌───────── Bit  6   - M/P (1 = MMU enabled, 0 = MMU disabled)
    ;         ││┌──────── Bit  5   - IEN (1 = GIME IRQ output enabled to CPU, 0 = disabled)
    ;         │││┌─────── Bit  4   - FEN (1 = GIME FIRQ output enabled to CPU, 0 = disabled)
    ;         ││││┌────── Bit  3   - MC3 (1 = Vector RAM at FEXX enabled, 0 = disabled)
    ;         │││││┌───── Bit  2   - MC2 (1 = Standard SCS (DISK) (0=expand 1=normal))
    ;         ││││││┌┬─── Bits 1-0 - MC1-0 (10 = ROM Map 32k Internal, 0x = 16K Internal/16K External, 11 = 32K External - Except Interrupt Vectors)
    lda     #%01001110
    sta     $FF90
    ;
    ;         Fast/Interrupt Request Enable Registers - IRQENR/FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer IRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync IRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync IRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data IRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard IRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge IRQ, 0 = disable)
    lda     #%00000000
    sta     $FF92               ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     $FF93               ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    ;
    lda     $FF92
    lda     $FF93
    lbsr    RestoreInterruptVectors
    ;
    rts

RestoreInterruptVectors:
;***********************
    pshs    y,x
    ;
    ;         Fast Interrupt Request Enable Registers - FIRQENR
    ;         ┌┬───────── Bit  7-6 - Unused
    ;         ││┌──────── Bit  5   - TMR (1 = Enable timer FIRQ, 0 = disable)
    ;         │││┌─────── Bit  4   - HBORD (1 = Enable Horizontal border Sync FIRQ, 0 = disable)
    ;         ││││┌────── Bit  3   - VBORD (1 = Enable Vertical border Sync FIRQ, 0 = disable)
    ;         │││││┌───── Bit  2   - EI2 (1 = Enable RS232 Serial data FIRQ, 0 = disable)
    ;         ││││││┌──── Bit  1   - EI1 (1 = Enable Keyboard FIRQ, 0 = disable)
    ;         │││││││┌─── Bits 0   - EI0 (1 = Enable Cartridge FIRQ, 0 = disable)
    lda     #%00000000
    sta     $FF93    ; Disable Timer FIRQ
    ;
    ldx     #originalVectors
    ldy     #$FEEE
    lda     #9
    pshs    a
@restoreVectors
    ldd     ,x++
    std     ,y++
    dec    ,s
    bne    @restoreVectors
    puls    a
    ;
    puls    x,y,pc

InitializeSound
    lda     $FF01
    ;         PIA 0 Side A Control Register
    ;         ┌────────── Bit  7   - HSYNC Flag
    ;         │┌───────── Bit  6   - Unused
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line LSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF00 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - HSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11110111
    sta     $FF01               ; MSB & LSB of MUX set to 0 = Select DAC (Digital to Analog Converter) as sound source
    ;
    lda     $FF03
    ;         PIA 0 Side B Control Register
    ;         ┌────────── Bit  7   - VSYNC Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Select Line MSB of MUX
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF02 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - IRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - VSYNC IRQ (0=disabled, 1=enabled)
    anda    #%11110111          ; MSB & LSB set to 0 = Select DAC (Digital to Analog Converter) as sound source
    sta     $FF03
    ;
    lda     $FF23
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    ora     #%00001000          ; Enable Sound
    sta     $FF23
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    anda    #%11111011          ; Make $FF20 configure direction for DAC, RS-323C, and Cassette
    sta     $FF21
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%11111100          ; Set DAC to Output; Set RS-232C and Cassette to Input (so writes to those two bits do nothing)
    sta     $FF20
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    ora     #%00000100          ; Make $FF20 function as input/output again
    sta     $FF21
    ;
    rts

UninitializeSound
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    anda    #%11111011          ; Make $FF20 configure direction for DAC, RS-323C, and Cassette
    sta     $FF21
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%11111110
    sta     $FF20               ; DAC/RS-232C to Output, Cassette to Input
    ;
    lda     $FF23
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CART FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Sound Enable
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF22 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CART FIRQ (0=disabled, 1=enabled)
    anda    #%11110111          ; Disable Sound
    sta     $FF23
    ;
    lda     $FF21
    ;         PIA 1 Side B Control Register
    ;         ┌────────── Bit  7   - CD FIRQ Flag
    ;         │┌───────── Bit  6   - N/A
    ;         ││┌┬─────── Bits 5-4 - 11
    ;         ││││┌────── Bit  3   - Cassette Motor Control (0=off, 1=on)
    ;         │││││┌───── Bit  2   - Data Direction Toggle (0=$FF20 sets direction, 1=normal)
    ;         ││││││┌──── Bit  1   - FIRQ Polarity (0=Flag set on falling edge, 1=set on rising edge)
    ;         │││││││┌─── Bit  0   - CD FIRQ (RS-232C) (0=disabled, 1=enabled)
    ora     #%00000100          ; Make $FF20 function as input/output again
    sta     $FF21
    ;
    ;         PIA 1 Side A Control Data Register
    ;         ┌┬┬┬┬┬───── 6-Bit DAC
    ;         ││││││┌──── Bit  1   - RS-232C Data Output
    ;         │││││││┌─── Bit  0   - Cassette Data Input
    lda     #%00000010
    sta     $FF20                                   ; Set RS-232C idle
    ;
    rts

tunes.fall.length       fcb 1
tunes.fall              fcb 1,60
                        fcb 1,60    ; 5220 / 2 = 2610
                        fcb 2,16    ; 5220 / 4 = 1305
                        fcb 3,16    ; 5220 / 6 = 870
                        fcb 4,16    ; 5220 / 8 = 652
                        fcb 5,16    ; 5220 / 10 = 522
                        fcb 6,16    ; 5220 / 12 = 435
                        fcb 7,16    ; 5220 / 14 = 374
                        fcb 8,16    ; 5220 / 16 = 326
                        fcb 9,16    ; 5220 / 18 = 290
                        fcb 10,16   ; 5220 / 20 = 261
                        fcb 11,16   ; 5220 / 22 = 237
                        fcb 12,16   ; 5220 / 24 = 217
                        fcb 13,16   ; 5220 / 26 = 201
                        fcb 14,16   ; 5220 / 28 = 187
                        fcb 15,16   ; 5220 / 30 = 174
                        fcb 16,16   ; 5220 / 32 = 163
                        fcb 17,16   ; 5220 / 34 = 153
                        fcb 18,16   ; 5220 / 36 = 145
                        fcb 19,16   ; 5220 / 38 = 137
                        fcb 20,16   ; 5220 / 40 = 130
                        fcb 21,16   ; 5220 / 42 = 124
                        fcb 22,16   ; 5220 / 44 = 118
                        fcb 23,16   ; 5220 / 46 = 113
                        fcb 24,16   ; 5220 / 48 = 108
                        fcb 25,16   ; 5220 / 50 = 104
                        fcb 26,16   ; 5220 / 52 = 100
                        fcb 27,16   ; 5220 / 54 = 97
                        fcb 28,16   ; 5220 / 56 = 94
                        fcb 29,16   ; 5220 / 58 = 91
                        fcb 30,16   ; 5220 / 60 = 87
                        fcb 31,16   ; 5220 / 62 = 84
                        fcb 32,16   ; 5220 / 64 = 81
                        fcb 33,16   ; 5220 / 66 = 79
                        fcb 34,16   ; 5220 / 68 = 76
                        fcb 35,16   ; 5220 / 70 = 74
                        fcb 36,16   ; 5220 / 72 = 72
                        fcb 37,16   ; 5220 / 74 = 70
                        fcb 38,16   ; 5220 / 76 = 68
                        fcb 39,16   ; 5220 / 78 = 66
                        fcb 40,16   ; 5220 / 80 = 65
                        fcb 41,16   ; 5220 / 82 = 63
                        fcb 42,16   ; 5220 / 84 = 62
                        fcb 43,16   ; 5220 / 86 = 60
                        fcb 44,16   ; 5220 / 88 = 59
                        fcb 45,16   ; 5220 / 90 = 58
                        fcb 46,16   ; 5220 / 92 = 57
                        fcb 47,16   ; 5220 / 94 = 56
                        fcb 48,16   ; 5220 / 96 = 55
                        fcb 49,16   ; 5220 / 98 = 53
                        fcb 50,16   ; 5220 / 100 = 52
                        fcb 51,16   ; 5220 / 102 = 51
                        fcb 52,16   ; 5220 / 104 = 50
                        fcb 53,16   ; 5220 / 106 = 49
                        fcb 54,16   ; 5220 / 108 = 48
                        fcb 55,16   ; 5220 / 110 = 47
                        fcb 56,16   ; 5220 / 112 = 46
                        fcb 57,16   ; 5220 / 114 = 45
                        fcb 58,16   ; 5220 / 116 = 44
                        fcb 59,16   ; 5220 / 118 = 43
                        fcb 60,16   ; 5220 / 120 = 43
                        

    END     Start
