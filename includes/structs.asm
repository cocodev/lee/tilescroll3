DebugData           STRUCT
gimeCheckCount                  rmb 2       ; $2000-$2001
highSpeedTimerCountStart        rmb 2       ; $2002-$2003
highSpeedTimerCountFinal        rmb 2       ; $2004-$2005
lastFrameVsyncCount             rmb 1       ; $2006
lastFrameFIRQCount              rmb 2       ; $2007-$2008
data                            rmb 256     ; $2009-$2108
                    ENDSTRUCT

ScreenBuffer        STRUCT
verticalOffset                      rmb 2
horizontalOffset                    rmb 1
firstPhysicalBlockOfBuffer          rmb 1
firstPhysicalBlockOfVerticalStripe  rmb 1
leftPlayfieldXInSlice               rmb 2
videoBank                           rmb 1
                    ENDSTRUCT

SpriteBackground    STRUCT
isCaptured                      rmb 1
firstOfTwoPhysicalBlocks        rmb 1
byteOffset                      rmb 2
buffer                          rmb 16*32
                    ENDSTRUCT


SpriteBase          STRUCT
isActive                        rmb 1
x                               rmb 2
y                               rmb 2
speedX                          rmb 1
speedY                          rmb 1
background1                     SpriteBackground
background2                     SpriteBackground
onScreenBackgroundAddress       rmb 2
offScreenBackgroundAddress      rmb 2
compiled                        rmb 2
compiledPhysicalBlock           rmb 1
                    ENDSTRUCT

Hero                STRUCT
sprite                          SpriteBase
                    ENDSTRUCT


Baddie              STRUCT
sprite                          SpriteBase
looking                         rmb 1
                    ENDSTRUCT

HeroAnimation       STRUCT
spritesPhysicalBlock            rmb 1
spritesLookup                   rmb 2
firstFrameIndex                 rmb 1
lastFrameIndex                  rmb 1
                    ENDSTRUCT

Tone                STRUCT
duration                        rmb 1
frequencyValue                  rmb 2
                    ENDSTRUCT

RedrawTile          STRUCT
pending                         rmb 1
mapX                            rmb 1
mapY                            rmb 1
addressOfMapCell                rmb 2
                    ENDSTRUCT

HeroAccelerationAndDirection STRUCT
accelerationX                   rmb 1
accelerationY                   rmb 1
heroIndex                       rmb 1
looking                         rmb 1   ; For Baddies (sprites 1-5): 0=left, 1=Right, 2=Up, 3=Down (not used for hero, sprite 0)
                    ENDSTRUCT

ByteDirection        STRUCT
x                               rmb 1
y                               rmb 1
                    ENDSTRUCT

MapOffsets          STRUCT
mapOffset                       rmb 1
xOffset                         rmb 1
yOffset                         rmb 1
                    ENDSTRUCT

BaddieStartingInfo  STRUCT
mapX                            rmb 1
mapY                            rmb 1
looking                         rmb 1   ; 0 = left, 1 = right, 2 = up, 3 = down
                    ENDSTRUCT

BaddieMapOffsets    STRUCT
left                            rmb 1
right                           rmb 1
front                           rmb 1
back                            rmb 1
                    ENDSTRUCT

BaddieDirectionInfo STRUCT
leftDirection                   rmb 1   ; 0 = left, 1 = right, 2 = up, 3 = down
rightDirection                  rmb 1   ; 0 = left, 1 = right, 2 = up, 3 = down
turnAroundDirection             rmb 1   ; 0 = left, 1 = right, 2 = up, 3 = down
speedX                          rmb 1
speedY                          rmb 1
mapOffsetDirections             BaddieMapOffsets
                    ENDSTRUCT

MapCoordinates      STRUCT
mapX                            rmb 1
mapY                            rmb 1
                    ENDSTRUCT

AboutToFallSpeedAndControlFilter    STRUCT
speedX                          rmb 1
speedY                          rmb 1
controlFilter                   rmb 1
filler                          rmb 1
                    ENDSTRUCT