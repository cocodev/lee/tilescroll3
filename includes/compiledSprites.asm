Compiled.Sprites.Start
    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
                fcb     PHYSICAL_014000_015FFF
    
    org     $4000
Hero.00
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$0fa6
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$6af0
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$0ff2
                ldd     #$0060
                pshu    y,x,d
                ldy     #$0600
                ldx     #$2ff0
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff0a
                ldx     #$f00f
                ldd     #$0006
                pshu    y,x,d
                ldy     #$f06f
                ldx     #$a0ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff6a
                ldx     #$000f
                ldd     #$0ff0
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a6ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa2
                ldx     #$000f
                ldd     #$0ff0
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a2ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f2a2
                ldx     #$00ff
                ldd     #$f22f
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$2a2f
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2fff
                ldd     #$6226
                pshu    y,x,d
                ldy     #$fff2
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.01
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$afa6
                ldd     #$0062
                pshu    y,x,d
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$fff2
                ldd     #$00ff
                pshu    y,x,d
                ldy     #$006f
                ldx     #$06f0
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff0a
                ldx     #$ffff
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0066
                ldx     #$a000
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff6a
                ldx     #$ffff
                ldd     #$600f
                pshu    y,x,d
                ldy     #$06ff
                ldx     #$a6f0
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa2
                ldx     #$ffff
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a2ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f2a2
                ldx     #$ffff
                ldd     #$f22f
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$2a2f
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2fff
                ldd     #$6226
                pshu    y,x,d
                ldy     #$fff2
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.02
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$0fa6
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$6af0
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$0ff2
                ldd     #$06f0
                pshu    y,x,d
                ldy     #$6f00
                ldx     #$2ff0
                ldb     #$a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff0a
                ldx     #$000f
                ldd     #$0660
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a0ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff6a
                ldx     #$00ff
                ldd     #$6ff6
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$a6ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa2
                ldx     #$ffff
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a2ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f2a2
                ldx     #$ffff
                ldd     #$f22f
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$2a2f
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2fff
                ldd     #$6226
                pshu    y,x,d
                ldy     #$fff2
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.03
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$6aff
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$6f00
                ldd     #$f000
                pshu    y,x,d
                ldy     #$ff06
                ldx     #$2fff
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$000a
                ldx     #$6600
                ldd     #$0000
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a0ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0a6a
                ldx     #$ff60
                ldd     #$f006
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a6ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa2
                ldx     #$ffff
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a2ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f2a2
                ldx     #$ffff
                ldd     #$f22f
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$2a2f
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2fff
                ldd     #$6226
                pshu    y,x,d
                ldy     #$fff2
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.04
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0000
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$6aff
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$0260
                ldx     #$0000
                ldd     #$6000
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$2fff
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f00a
                ldx     #$0006
                ldd     #$06f0
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a0ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$000a
                ldx     #$fa00
                ldd     #$0000
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a6ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$00a2
                ldx     #$fa00
                ldd     #$0000
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a2ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$06a2
                ldx     #$ffa0
                ldd     #$f00f
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$2a2f
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2ffa
                ldd     #$6226
                pshu    y,x,d
                ldy     #$fff2
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.05
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$afa6
                ldd     #$0002
                pshu    y,x,d
                ldy     #$2000
                ldx     #$6afa
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$fff2
                ldd     #$0aff
                pshu    y,x,d
                ldy     #$ffa0
                ldx     #$2fff
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0f0a
                ldx     #$fff0
                ldd     #$f00f
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a0ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$006a
                ldx     #$ff00
                ldd     #$0000
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a6ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$00a2
                ldx     #$6600
                ldd     #$0000
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$a2ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f0a2
                ldx     #$0006
                ldd     #$06f0
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$2a2f
                pshu    y,x,a

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2000
                ldd     #$6226
                pshu    y,x,d
                ldy     #$fff2
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.06
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$afa6
                ldd     #$0062
                pshu    y,x,d
                ldy     #$2600
                ldx     #$6afa
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$fff2
                ldd     #$2aff
                pshu    y,x,d
                ldy     #$ffa2
                ldx     #$2fff
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff0a
                ldx     #$00ff
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$a0ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff6a
                ldx     #$000f
                ldd     #$0ff0
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a6ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa2
                ldx     #$000f
                ldd     #$0660
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a2ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f2a2
                ldx     #$6f0f
                ldd     #$0000
                pshu    y,x,d
                ldy     #$f06f
                ldx     #$2a2f
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$000f
                ldd     #$6226
                pshu    y,x,d
                ldy     #$f000
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.07
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$afa6
                ldd     #$0062
                pshu    y,x,d
                ldy     #$2600
                ldx     #$6afa
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$fff2
                ldd     #$2aff
                pshu    y,x,d
                ldy     #$ffa2
                ldx     #$2fff
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff0a
                ldx     #$ffff
                ldd     #$f00f
                pshu    y,x,d
                ldy     #$0fff
                ldx     #$a0f0
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff6a
                ldx     #$ffff
                ldd     #$0000
                pshu    y,x,d
                ldy     #$00ff
                ldx     #$a600
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa2
                ldx     #$6fff
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0066
                ldx     #$a200
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f2a2
                ldx     #$6fff
                ldd     #$06f0
                pshu    y,x,d
                ldy     #$f000
                ldx     #$2a26
                pshu    y,x,a

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2fff
                ldd     #$0220
                pshu    y,x,d
                ldy     #$0000
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$0002
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.08
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$afa6
                ldd     #$0062
                pshu    y,x,d
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$fff2
                ldd     #$60ff
                pshu    y,x,d
                ldy     #$0000
                ldx     #$0060
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff0a
                ldx     #$ffff
                ldd     #$06f0
                pshu    y,x,d
                ldy     #$f006
                ldx     #$a006
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff6a
                ldx     #$ffff
                ldd     #$0000
                pshu    y,x,d
                ldy     #$00ff
                ldx     #$a600
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa2
                ldx     #$ffff
                ldd     #$0000
                pshu    y,x,d
                ldy     #$00ff
                ldx     #$a200
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f2a2
                ldx     #$ffff
                ldd     #$f00f
                pshu    y,x,d
                ldy     #$0fff
                ldx     #$2a60
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2fff
                ldd     #$6226
                pshu    y,x,d
                ldy     #$fff2
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.SH
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$0000
                ldx     #$0000
                lda     #$00
                pshu    y,x,d

                leau    261,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    264,u           ;row07
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    261,u           ;row08
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    264,u           ;row09
                lda     #$00
                pshu    y,x,d
                pshu    y,x

                leau    266,u           ;row10
                pshu    y,x,d
                pshu    y,x

                leau    265,u           ;row11
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                pshu    y,d

                leau    266,u           ;row12
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                pshu    y,d

                leau    268,u           ;row13
                lda     #$00
                pshu    y,x,d
                lda     -6,u
                anda    #$f0
                pshu    y,x,d

                leau    268,u           ;row14
                lda     #$00
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row15
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row16
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row17
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row18
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row19
                pshu    y,x,d
                lda     -6,u
                anda    #$f0
                pshu    y,x,d

                leau    266,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                pshu    y,d

                leau    266,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                pshu    y,b

                leau    266,u           ;row22
                pshu    y,x,d
                pshu    y,x

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    263,u           ;row24
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    260,u           ;row25
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    263,u           ;row26
                lda     -6,u
                anda    #$f0
                pshu    y,x,d

                leau    259,u           ;row27
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                rts

Hero.SH2
                leau    1031,u          ;row04
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldb     -1,u
                andb    #$f0
                stb     -1,u

                leau    258,u           ;row05
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u

                leau    256,u           ;row06
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldb     -5,u
                andb    #$f0
                stb     -5,u

                leau    257,u           ;row07
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u

                leau    256,u           ;row08
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldb     -7,u
                andb    #$f0
                stb     -7,u

                leau    257,u           ;row09
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u
                ldd     -8,u
                anda    #$0f
                andb    #$0f
                std     -8,u

                leau    256,u           ;row10
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldd     -8,u
                anda    #$f0
                andb    #$f0
                std     -8,u

                leau    257,u           ;row11
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u
                ldd     -8,u
                anda    #$0f
                andb    #$0f
                std     -8,u
                ldb     -9,u
                andb    #$0f
                stb     -9,u

                leau    255,u           ;row12
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldd     -8,u
                anda    #$f0
                andb    #$f0
                std     -8,u
                ldb     -9,u
                andb    #$f0
                stb     -9,u

                leau    257,u           ;row13
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u
                ldd     -8,u
                anda    #$0f
                andb    #$0f
                std     -8,u
                ldb     -9,u
                andb    #$0f
                stb     -9,u

                leau    256,u           ;row14
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldd     -8,u
                anda    #$f0
                andb    #$f0
                std     -8,u
                ldd     -10,u
                anda    #$f0
                andb    #$f0
                std     -10,u

                leau    256,u           ;row15
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u
                ldd     -8,u
                anda    #$0f
                andb    #$0f
                std     -8,u
                ldd     -10,u
                anda    #$0f
                andb    #$0f
                std     -10,u

                leau    256,u           ;row16
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldd     -8,u
                anda    #$f0
                andb    #$f0
                std     -8,u
                ldd     -10,u
                anda    #$f0
                andb    #$f0
                std     -10,u

                leau    256,u           ;row17
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u
                ldd     -8,u
                anda    #$0f
                andb    #$0f
                std     -8,u
                ldd     -10,u
                anda    #$0f
                andb    #$0f
                std     -10,u

                leau    256,u           ;row18
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldd     -8,u
                anda    #$f0
                andb    #$f0
                std     -8,u
                ldd     -10,u
                anda    #$f0
                andb    #$f0
                std     -10,u

                leau    256,u           ;row19
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u
                ldd     -8,u
                anda    #$0f
                andb    #$0f
                std     -8,u
                ldb     -9,u
                andb    #$0f
                stb     -9,u

                leau    255,u           ;row20
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldd     -8,u
                anda    #$f0
                andb    #$f0
                std     -8,u
                ldb     -9,u
                andb    #$f0
                stb     -9,u

                leau    257,u           ;row21
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u
                ldd     -8,u
                anda    #$0f
                andb    #$0f
                std     -8,u
                ldb     -9,u
                andb    #$0f
                stb     -9,u

                leau    255,u           ;row22
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldd     -8,u
                anda    #$f0
                andb    #$f0
                std     -8,u

                leau    256,u           ;row23
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                std     -6,u
                ldb     -7,u
                andb    #$0f
                stb     -7,u

                leau    255,u           ;row24
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u

                leau    256,u           ;row25
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                std     -2,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldb     -5,u
                andb    #$0f
                stb     -5,u

                leau    255,u           ;row26
                ldd     ,u
                anda    #$f0
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$f0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                std     -4,u

                leau    255,u           ;row27
                ldd     ,u
                anda    #$0f
                andb    #$0f
                std     ,u
                ldb     -1,u
                andb    #$0f
                stb     -1,u

                rts
Hero.Normal.End

    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
                fcb     PHYSICAL_016000_017FFF
    
    org     $4000
Hero.10
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0006
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$6000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$0fa6
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$6af0
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$0ff2
                ldd     #$0060
                pshu    y,x,d
                ldy     #$0600
                ldx     #$2ff0
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff0a
                ldx     #$f00f
                ldd     #$0006
                pshu    y,x,d
                ldy     #$f06f
                ldx     #$a0ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff6a
                ldx     #$000f
                ldd     #$0ff0
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a6ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa2
                ldx     #$000f
                ldd     #$0ff0
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a2ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f2a2
                ldx     #$00ff
                ldd     #$f22f
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$2a2f
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0222
                ldx     #$2fff
                ldd     #$6226
                pshu    y,x,d
                ldy     #$fff2
                ldx     #$2a20
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2606
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$6062
                ldx     #$22a2
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.11
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0002
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$2000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$0006
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$6000
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$6260
                ldx     #$0fa2
                ldd     #$0060
                pshu    y,x,d
                ldy     #$0600
                ldx     #$2af0
                ldd     #$06a6
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$fa0a
                ldx     #$f00f
                ldd     #$0006
                pshu    y,x,d
                ldy     #$f06f
                ldx     #$a0af
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff6a
                ldx     #$000f
                ldd     #$0000
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a6ff
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a0a2
                ldx     #$000f
                ldd     #$0220
                pshu    y,x,d
                ldy     #$f000
                ldx     #$a20a
                pshu    y,x,a

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$02a2
                ldx     #$0000
                ldd     #$2aa2
                pshu    y,x,d
                ldy     #$0000
                ldx     #$2a20
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$aa22
                ldx     #$2222
                ldd     #$a22a
                pshu    y,x,d
                ldy     #$2222
                ldx     #$2aa2
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$2aaa
                ldd     #$aaa2
                pshu    y,x,d
                ldy     #$aaa2
                ldx     #$22aa
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.12
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0002
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$2000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$0022
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$2a20
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$2260
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldd     #$06a2
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2602
                ldx     #$a00a
                ldd     #$0006
                pshu    y,x,d
                ldy     #$a06a
                ldx     #$2062
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2062
                ldx     #$000f
                ldd     #$0220
                pshu    y,x,d
                ldy     #$f000
                ldx     #$2602
                pshu    y,x,a

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$02a2
                ldx     #$0000
                ldd     #$2222
                pshu    y,x,d
                ldy     #$0000
                ldx     #$a220
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$22a2
                ldx     #$2aa2
                ldd     #$2aa2
                pshu    y,x,d
                ldy     #$aa22
                ldx     #$2a22
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$a2aa
                ldd     #$a22a
                pshu    y,x,d
                ldy     #$222a
                ldx     #$2aaa
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$aaaa
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$aaaa
                ldx     #$22aa
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a2aa
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$aaaa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.13
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$6600
                ldx     #$aaa2
                ldd     #$0062
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$faa2
                ldx     #$aaff
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$2220
                ldx     #$ffaa
                ldd     #$afff
                pshu    y,x,d
                ldx     #$022a
                stx     -2,u

                leau    261,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a22a
                ldx     #$afaa
                ldb     #$aa
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$2a
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$0060
                ldx     #$a200
                ldd     #$a6aa
                pshu    y,x,d
                ldx     #$002a
                ldd     #$0600
                pshu    x,d

                leau    265,u           ;row11
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0062
                ldx     #$6a20
                ldd     #$0220
                pshu    y,x,d
                ldx     #$2600
                lda     -4,u
                anda    #$f0
                ldb     #$62
                pshu    x,d

                leau    266,u           ;row12
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0002
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$2000
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                pshu    x,d

                leau    268,u           ;row13
                ldy     #$2260
                ldx     #$0220
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$0a20
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    268,u           ;row14
                ldy     #$2260
                ldx     #$222a
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$aaa2
                ldd     #$0622
                pshu    y,x,d

                leau    267,u           ;row15
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6602
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$2066
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row16
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2202
                ldx     #$2222
                ldd     #$0000
                pshu    y,x,d
                ldy     #$2222
                ldx     #$2022
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row17
                lda     #$20
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a2a2
                ldx     #$aaaa
                ldd     #$0220
                pshu    y,x,d
                ldy     #$aaaa
                ldx     #$a22a
                pshu    y,x,a

                leau    267,u           ;row18
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$22a2
                ldx     #$0222
                ldd     #$2aa2
                pshu    y,x,d
                ldy     #$2a20
                ldx     #$2aa2
                ldb     #$06
                pshu    y,x,b

                leau    267,u           ;row19
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$aa22
                ldx     #$2aaa
                ldd     #$a22a
                pshu    y,x,d
                ldy     #$aaaa
                ldb     -5,u
                andb    #$f0
                orb     #$06
                pshu    y,x,b

                leau    267,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2a26
                ldx     #$aaaa
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$aaaa
                ldx     #$22aa
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    267,u           ;row21
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a226
                ldx     #$a2aa
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$aaaa
                lda     #$62
                pshu    x,d

                leau    267,u           ;row22
                ldy     #$6000
                ldx     #$2222
                lda     #$fa
                pshu    y,x,d
                ldy     #$afff
                ldx     #$2aaa
                ldb     #$02
                pshu    y,x,b

                leau    267,u           ;row23
                ldy     #$0000
                ldx     #$2666
                ldd     #$aaaa
                pshu    y,x,d
                ldy     #$affa
                ldx     #$222a
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    265,u           ;row24
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    263,u           ;row25
                ldy     #$0000
                ldx     #$2226
                ldb     #$a2
                pshu    y,x,d
                ldx     #$622a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    263,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6000
                ldx     #$2222
                lda     -6,u
                anda    #$f0
                ldb     #$06
                pshu    y,x,d

                leau    263,u           ;row27
                ldy     #$0000
                ldx     #$0000
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row28
                lda     -4,u
                anda    #$f0
                pshu    y,d

                rts

Hero.14
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$f600
                ldx     #$faaf
                ldd     #$006f
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$a6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a000
                ldx     #$000a
                lda     -6,u
                anda    #$f0
                ldb     #$6a
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$00a0
                ldx     #$a606
                ldd     #$6062
                pshu    y,x,d
                ldx     #$0a00
                stx     -2,u

                leau    261,u           ;row08
                lda     #$0a
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$2620
                ldx     #$2afa
                ldd     #$0262
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$a0
                std     -2,u

                leau    264,u           ;row09
                ldy     #$00a0
                ldx     #$6ff6
                ldd     #$afaa
                pshu    y,x,d
                ldd     #$0a00
                pshu    x,d

                leau    266,u           ;row10
                ldy     #$6026
                ldx     #$ffff
                ldd     #$6fa6
                pshu    y,x,d
                ldd     #$6202
                pshu    x,d

                leau    265,u           ;row11
                lda     #$02
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffa6
                ldx     #$afff
                ldd     #$fffa
                pshu    y,x,d
                ldx     #$aaff
                lda     -4,u
                anda    #$f0
                ldb     #$20
                pshu    x,d

                leau    266,u           ;row12
                lda     #$62
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$fff2
                ldx     #$0f66
                ldd     #$66fa
                pshu    y,x,d
                ldx     #$2fff
                lda     -4,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    x,d

                leau    266,u           ;row13
                lda     #$20
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$6ffa
                ldx     #$0000
                ldd     #$0006
                pshu    y,x,d
                ldx     #$aff6
                lda     -4,u
                anda    #$f0
                ora     #$06
                ldb     #$02
                pshu    x,d

                leau    268,u           ;row14
                ldy     #$2620
                ldx     #$0ffa
                ldd     #$0660
                pshu    y,x,d
                ldy     #$6600
                ldx     #$aff0
                ldd     #$0262
                pshu    y,x,d

                leau    268,u           ;row15
                ldy     #$6220
                ldx     #$0fff
                ldd     #$06f0
                pshu    y,x,d
                ldy     #$6f00
                ldx     #$fff0
                ldd     #$0226
                pshu    y,x,d

                leau    268,u           ;row16
                ldy     #$6a20
                ldx     #$0fff
                ldd     #$f000
                pshu    y,x,d
                ldy     #$000f
                ldx     #$fff0
                ldd     #$02a6
                pshu    y,x,d

                leau    268,u           ;row17
                ldy     #$a220
                ldx     #$6fff
                ldd     #$f600
                pshu    y,x,d
                ldy     #$006f
                ldx     #$fff6
                ldd     #$02a2
                pshu    y,x,d

                leau    268,u           ;row18
                ldy     #$a260
                ldd     #$6f66
                pshu    y,x,d
                ldy     #$66f6
                ldx     #$6fff
                ldd     #$062a
                pshu    y,x,d

                leau    266,u           ;row19
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$ff62
                ldx     #$226f
                ldd     #$f662
                pshu    y,x,d
                ldx     #$20ff
                lda     -4,u
                anda    #$f0
                ora     #$06
                ldb     #$2a
                pshu    x,d

                leau    266,u           ;row20
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$062a
                ldx     #$a226
                ldd     #$62aa
                pshu    y,x,d
                ldx     #$a260
                lda     -4,u
                anda    #$f0
                ldb     #$22
                pshu    x,d

                leau    267,u           ;row21
                ldy     #$a226
                ldx     #$a222
                ldd     #$ffaa
                pshu    y,x,d
                ldx     #$22aa
                lda     #$62
                pshu    x,d

                leau    266,u           ;row22
                ldy     #$2260
                ldx     #$aa22
                ldd     #$fffa
                pshu    y,x,d
                ldx     #$aaaf
                ldd     #$022a
                pshu    x,d

                leau    264,u           ;row23
                lda     #$66
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$aa26
                ldx     #$faaa
                ldd     #$2aaf
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$22
                std     -2,u

                leau    263,u           ;row24
                ldy     #$2260
                ldx     #$aaa2
                ldd     #$aaaa
                pshu    y,x,d
                ldx     #$062a
                stx     -2,u

                leau    260,u           ;row25
                lda     #$26
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$a222
                ldx     #$2aaa
                lda     -6,u
                anda    #$f0
                ldb     #$62
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$2222
                lda     -4,u
                anda    #$f0
                ldb     #$06
                pshu    x,d

                leau    258,u           ;row27
                ldx     #$0000
                stx     ,u

                rts

Hero.15
                leau    1546,u          ;row06
                ldx     #$aff6
                lda     -4,u
                anda    #$f0
                ora     #$06
                ldb     #$ff
                pshu    x,d

                leau    261,u           ;row07
                ldy     #$00a6
                ldx     #$00a0
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$a0
                pshu    y,x,d

                leau    261,u           ;row08
                lda     #$00
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u
                ldy     #$2606
                ldx     #$0606
                ldb     #$a0
                pshu    y,x,b

                leau    263,u           ;row09
                ldy     #$200a
                ldx     #$aa26
                ldd     #$2622
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ora     #$0a
                ldb     #$00
                std     -2,u

                leau    261,u           ;row10
                lda     #$00
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u
                ldy     #$6ff6
                ldx     #$6afa
                ldd     #$06ff
                pshu    y,x,d
                ldb     #$a0
                stb     -1,u

                leau    264,u           ;row11
                ldy     #$6026
                ldx     #$ffff
                ldd     #$f6f6
                pshu    y,x,d
                ldx     #$2fff
                ldb     #$20
                pshu    x,b

                leau    265,u           ;row12
                ldy     #$a602
                ldx     #$ffff
                ldd     #$ffaf
                pshu    y,x,d
                ldx     #$afff
                ldb     #$2a
                pshu    x,b

                leau    265,u           ;row13
                ldy     #$fa22
                ldx     #$00ff
                ldd     #$0f6f
                pshu    y,x,d
                ldx     #$fff0
                lda     -4,u
                anda    #$f0
                ora     #$06
                ldb     #$2a
                pshu    x,d

                leau    266,u           ;row14
                ldy     #$fa26
                ldx     #$600f
                ldd     #$6006
                pshu    y,x,d
                ldx     #$ff06
                lda     -4,u
                anda    #$f0
                ora     #$02
                ldb     #$2a
                pshu    x,d

                leau    266,u           ;row15
                ldy     #$ff62
                ldx     #$f00f
                ldd     #$f006
                pshu    y,x,d
                ldx     #$ff06
                lda     -4,u
                anda    #$f0
                ora     #$02
                ldb     #$6f
                pshu    x,d

                leau    266,u           ;row16
                ldy     #$ff6a
                ldx     #$000f
                ldd     #$00f0
                pshu    y,x,d
                ldx     #$ff00
                lda     -4,u
                anda    #$f0
                ora     #$02
                ldb     #$6f
                pshu    x,d

                leau    266,u           ;row17
                ldy     #$ffa2
                ldx     #$006f
                ldd     #$06f6
                pshu    y,x,d
                ldx     #$ff60
                lda     -4,u
                anda    #$f0
                ora     #$02
                ldb     #$2f
                pshu    x,d

                leau    266,u           ;row18
                ldy     #$f6a2
                ldx     #$66ff
                ldd     #$6f6f
                pshu    y,x,d
                ldx     #$fff6
                lda     -4,u
                anda    #$f0
                ora     #$06
                ldb     #$a6
                pshu    x,d

                leau    266,u           ;row19
                ldy     #$6222
                ldx     #$6fff
                ldd     #$6622
                pshu    y,x,d
                ldx     #$0fff
                lda     -4,u
                anda    #$f0
                ora     #$06
                ldb     #$a2
                pshu    x,d

                leau    266,u           ;row20
                ldy     #$a226
                ldx     #$a222
                ldd     #$affa
                pshu    y,x,d
                ldx     #$a22a
                ldb     #$2a
                pshu    x,b

                leau    263,u           ;row21
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$aa22
                ldx     #$fffa
                ldd     #$aaaa
                pshu    y,x,d
                ldb     #$22
                stb     -1,u

                leau    263,u           ;row22
                ldy     #$2666
                ldx     #$aaaa
                ldb     #$ff
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ora     #$02
                ldb     #$22
                std     -2,u

                leau    260,u           ;row23
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$aaa2
                ldb     #$62
                pshu    y,x,b

                leau    262,u           ;row24
                ldy     #$2226
                ldx     #$aaa2
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                pshu    y,x,d

                leau    259,u           ;row25
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     #$62
                stb     -1,u

                rts

Hero.16
                leau    1802,u          ;row07
                ldx     #$aff6
                ldb     #$ff
                pshu    x,b

                leau    258,u           ;row08
                lda     #$00
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldx     #$00a0
                ldb     #$0a
                pshu    x,b

                leau    260,u           ;row09
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u
                ldy     #$aa26
                ldx     #$0222
                ldb     #$a0
                pshu    y,x,b

                leau    263,u           ;row10
                ldy     #$600a
                ldx     #$fa6f
                ldd     #$6f6a
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ora     #$02
                ldb     #$00
                std     -2,u

                leau    262,u           ;row11
                ldy     #$f602
                ldx     #$f6ff
                ldd     #$fff6
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ora     #$06
                ldb     #$02
                std     -2,u

                leau    261,u           ;row12
                lda     #$60
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldy     #$fffa
                ldx     #$ffaf
                ldd     #$aaff
                pshu    y,x,d
                ldb     -1,u
                andb    #$f0
                orb     #$02
                stb     -1,u

                leau    262,u           ;row13
                lda     #$a2
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$00ff
                ldx     #$0f6f
                ldd     #$aff6
                pshu    y,x,d
                ldb     #$60
                stb     -1,u

                leau    262,u           ;row14
                lda     #$a2
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldy     #$60ff
                ldx     #$6006
                ldd     #$aff0
                pshu    y,x,d
                ldb     #$26
                stb     -1,u

                leau    262,u           ;row15
                lda     #$f6
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldy     #$f0ff
                ldx     #$f006
                ldd     #$fff0
                pshu    y,x,d
                ldb     #$22
                stb     -1,u

                leau    262,u           ;row16
                lda     #$fa
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldy     #$00ff
                ldx     #$06f6
                ldd     #$fff6
                pshu    y,x,d
                ldb     #$2a
                stb     -1,u

                leau    262,u           ;row17
                lda     #$6a
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$ffff
                ldx     #$ff6f
                ldd     #$6fff
                pshu    y,x,d
                ldb     #$62
                stb     -1,u

                leau    262,u           ;row18
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$6ff6
                ldx     #$6622
                ldd     #$20ff
                pshu    y,x,d
                ldb     #$62
                stb     -1,u

                leau    263,u           ;row19
                ldy     #$2a22
                ldx     #$faa2
                ldd     #$22af
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ora     #$06
                ldb     #$aa
                std     -2,u

                leau    262,u           ;row20
                ldy     #$2226
                ldx     #$faaa
                ldd     #$aaff
                pshu    y,x,d
                ldb     #$2a
                stb     -1,u

                leau    260,u           ;row21
                lda     #$66
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$aaaa
                ldx     #$2aff
                ldb     #$22
                pshu    y,x,b

                leau    262,u           ;row22
                ldy     #$a226
                ldx     #$aaaa
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$2a
                pshu    y,x,d

                leau    259,u           ;row23
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     #$62
                stb     -1,u

                rts

Hero.17
                leau    2313,u          ;row09
                lda     #$2a
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldx     #$22a2
                ldb     -3,u
                andb    #$f0
                orb     #$0a
                pshu    x,b

                leau    261,u           ;row10
                ldy     #$0000
                ldx     #$6626
                ldb     #$a0
                pshu    y,x,b

                leau    260,u           ;row11
                lda     #$60
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$226f
                ldx     #$0ff2
                ldb     -5,u
                andb    #$f0
                orb     #$0a
                pshu    y,x,b

                leau    261,u           ;row12
                lda     #$f6
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldy     #$2fff
                ldx     #$afff
                ldb     #$20
                pshu    y,x,b

                leau    263,u           ;row13
                ldy     #$f226
                ldx     #$2f6f
                ldd     #$ff6f
                pshu    y,x,d
                ldb     #$22
                stb     -1,u

                leau    262,u           ;row14
                ldy     #$fa62
                ldx     #$0660
                ldd     #$f060
                pshu    y,x,d
                ldb     #$62
                stb     -1,u

                leau    262,u           ;row15
                ldy     #$ff22
                ldx     #$06f0
                ldb     #$60
                pshu    y,x,d
                ldb     #$26
                stb     -1,u

                leau    262,u           ;row16
                ldx     #$f606
                ldd     #$f606
                pshu    y,x,d
                ldb     #$22
                stb     -1,u

                leau    262,u           ;row17
                ldy     #$f626
                ldx     #$ffff
                ldd     #$ffff
                pshu    y,x,d
                ldb     #$22
                stb     -1,u

                leau    260,u           ;row18
                lda     #$6a
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$a220
                ldx     #$206a
                ldb     #$22
                pshu    y,x,b

                leau    261,u           ;row19
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldy     #$fa22
                ldx     #$a2af
                ldb     -5,u
                andb    #$f0
                orb     #$02
                pshu    y,x,b

                leau    262,u           ;row20
                ldy     #$2266
                ldx     #$2faa
                pshu    y,x,a

                leau    259,u           ;row21
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldx     #$2222
                ldb     -3,u
                andb    #$f0
                orb     #$02
                pshu    x,b

                leau    258,u           ;row22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$02
                stb     -1,u

                rts

Hero.18
                leau    2312,u          ;row09
                lda     #$2f
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$0f
                stb     -1,u

                leau    258,u           ;row10
                ldx     #$2660
                lda     -4,u
                anda    #$f0
                ora     #$02
                ldb     #$00
                pshu    x,d

                leau    259,u           ;row11
                lda     #$f6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$f6fa
                ldb     #$20
                pshu    x,b

                leau    261,u           ;row12
                ldy     #$ff62
                ldx     #$ffaf
                ldb     #$0a
                pshu    y,x,b

                leau    261,u           ;row13
                ldy     #$0f20
                ldx     #$6060
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$2f
                pshu    y,x,d

                leau    262,u           ;row14
                ldy     #$0ff2
                ldx     #$0f06
                lda     -6,u
                anda    #$f0
                ora     #$02
                ldb     #$6f
                pshu    y,x,d

                leau    262,u           ;row15
                ldx     #$60f6
                lda     -6,u
                anda    #$f0
                ora     #$02
                ldb     #$2f
                pshu    y,x,d

                leau    262,u           ;row16
                ldy     #$ff22
                ldx     #$f622
                lda     -6,u
                anda    #$f0
                ora     #$06
                ldb     #$a0
                pshu    y,x,d

                leau    262,u           ;row17
                ldy     #$2226
                ldx     #$2af2
                ldb     #$2a
                pshu    y,x,b

                leau    259,u           ;row18
                lda     #$26
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldx     #$2f22
                ldb     -3,u
                andb    #$f0
                orb     #$02
                pshu    x,b

                leau    260,u           ;row19
                ldb     #$22
                pshu    y,b

                rts

Hero.19
                leau    2823,u          ;row11
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$26
                std     ,u

                leau    257,u           ;row12
                lda     #$f6
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                lda     -2,u
                anda    #$f0
                ora     #$02
                ldb     #$22
                std     -2,u

                leau    258,u           ;row13
                ldx     #$2f62
                ldd     #$20ff
                pshu    x,d

                leau    260,u           ;row14
                ldx     #$00f2
                ldd     #$22f0
                pshu    x,d

                leau    260,u           ;row15
                ldx     #$f0f6
                pshu    x,d

                leau    260,u           ;row16
                ldx     #$2f62
                ldd     #$62f6
                pshu    x,d

                leau    260,u           ;row17
                ldx     #$f226
                lda     -4,u
                anda    #$f0
                ora     #$02
                ldb     #$2f
                pshu    x,d

                leau    258,u           ;row18
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                sta     -1,u

                rts

Hero.20
                leau    3079,u          ;row12
                lda     ,u
                anda    #$f0
                ora     #$02
                ldb     #$22
                std     ,u

                leau    257,u           ;row13
                lda     #$2f
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                sta     -1,u

                leau    256,u           ;row14
                lda     #$00
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     #$26
                stb     -1,u

                leau    256,u           ;row15
                lda     #$6f
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldb     #$26
                stb     -1,u

                leau    256,u           ;row16
                lda     #$f2
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldb     #$22
                stb     -1,u

                leau    256,u           ;row17
                ldb     ,u
                andb    #$0f
                orb     #$20
                stb     ,u

                rts

Hero.21
                rts

Hero.22
                leau    269,u           ;row01
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u
                ldb     -4,u
                andb    #$f0
                orb     #$0f
                stb     -4,u

                leau    255,u           ;row02
                lda     ,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     ,u
                ldb     -10,u
                andb    #$f0
                orb     #$0f
                stb     -10,u

                leau    256,u           ;row03
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$ff
                std     ,u
                ldb     -1,u
                andb    #$0f
                orb     #$f0
                stb     -1,u
                ldb     -8,u
                andb    #$f0
                orb     #$0f
                stb     -8,u

                leau    256,u           ;row04
                lda     ,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     ,u
                ldb     -11,u
                andb    #$0f
                orb     #$f0
                stb     -11,u

                leau    257,u           ;row05
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u
                lda     -13,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     -13,u

                leau    243,u           ;row06
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$ff
                std     ,u

                leau    256,u           ;row07
                lda     ,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     ,u

                leau    270,u           ;row08
                ldb     ,u
                andb    #$0f
                orb     #$70
                stb     ,u
                ldb     -13,u
                andb    #$0f
                orb     #$f0
                stb     -13,u

                leau    1522,u          ;row14
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u

                leau    270,u           ;row15
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u
                ldb     -14,u
                andb    #$f0
                orb     #$0f
                stb     -14,u

                leau    1025,u          ;row19
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                leau    1022,u          ;row23
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u
                ldb     -13,u
                andb    #$f0
                orb     #$0f
                stb     -13,u

                leau    257,u           ;row24
                ldb     ,u
                andb    #$f0
                orb     #$07
                stb     ,u

                leau    766,u           ;row27
                ldd     ,u
                anda    #$f0
                ora     #$0f
                andb    #$0f
                orb     #$f0
                std     ,u
                ldb     -7,u
                andb    #$0f
                orb     #$f0
                stb     -7,u
                ldb     -9,u
                andb    #$0f
                orb     #$f0
                stb     -9,u

                leau    256,u           ;row28
                ldd     ,u
                anda    #$f0
                ora     #$0f
                andb    #$0f
                orb     #$f0
                std     ,u
                ldb     -2,u
                andb    #$0f
                orb     #$f0
                stb     -2,u
                lda     -10,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     -10,u

                leau    246,u           ;row29
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$ff
                std     ,u

                leau    261,u           ;row30
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u
                lda     -5,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     -5,u

                leau    252,u           ;row31
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                rts

Hero.23
                leau    265,u           ;row01
                ldb     ,u
                andb    #$f0
                orb     #$07
                stb     ,u
                ldb     -3,u
                andb    #$f0
                orb     #$0f
                stb     -3,u

                leau    260,u           ;row02
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u
                ldb     -11,u
                andb    #$f0
                orb     #$07
                stb     -11,u

                leau    255,u           ;row03
                lda     ,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     ,u
                ldb     -1,u
                andb    #$0f
                orb     #$70
                stb     -1,u
                ldb     -8,u
                andb    #$f0
                orb     #$07
                stb     -8,u

                leau    257,u           ;row04
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                leau    244,u           ;row05
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                leau    255,u           ;row06
                lda     ,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     ,u

                leau    257,u           ;row07
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                leau    269,u           ;row08
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                leau    1522,u          ;row14
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u

                leau    270,u           ;row15
                ldb     ,u
                andb    #$f0
                orb     #$07
                stb     ,u
                lda     #$7f
                ldb     -13,u
                andb    #$0f
                orb     #$70
                std     -14,u

                leau    242,u           ;row16
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u

                leau    1039,u          ;row20
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                leau    497,u           ;row22
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u

                leau    269,u           ;row23
                ldd     ,u
                anda    #$0f
                ora     #$70
                andb    #$f0
                orb     #$0f
                std     ,u
                lda     #$7f
                ldb     -12,u
                andb    #$0f
                orb     #$70
                std     -13,u

                leau    257,u           ;row24
                lda     #$7f
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u
                ldb     -14,u
                andb    #$f0
                orb     #$0f
                stb     -14,u

                leau    256,u           ;row25
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u

                leau    510,u           ;row27
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u
                ldb     -7,u
                andb    #$0f
                orb     #$70
                stb     -7,u

                leau    254,u           ;row28
                ldb     ,u
                andb    #$0f
                orb     #$70
                stb     ,u
                ldb     -7,u
                andb    #$0f
                orb     #$f0
                stb     -7,u

                leau    253,u           ;row29
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u
                lda     -5,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     -5,u

                leau    255,u           ;row30
                lda     ,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     ,u
                ldb     -3,u
                andb    #$0f
                orb     #$f0
                stb     -3,u

                leau    257,u           ;row31
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                rts

Hero.24
                leau    6,u             ;row00
                ldb     ,u
                andb    #$f0
                orb     #$07
                stb     ,u

                leau    256,u           ;row01
                lda     #$7f
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u

                leau    256,u           ;row02
                ldb     ,u
                andb    #$f0
                orb     #$07
                stb     ,u

                leau    263,u           ;row03
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                leau    756,u           ;row06
                ldb     ,u
                andb    #$0f
                orb     #$f0
                stb     ,u

                leau    269,u           ;row07
                ldb     #$ff
                stb     ,u

                leau    256,u           ;row08
                stb     ,u

                leau    1522,u          ;row14
                lda     #$7f
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u

                leau    270,u           ;row15
                ldb     ,u
                andb    #$f0
                orb     #$07
                stb     ,u
                lda     #$ff
                ldb     -13,u
                andb    #$0f
                orb     #$f0
                std     -14,u

                leau    242,u           ;row16
                lda     #$7f
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u

                leau    1039,u          ;row20
                ldb     ,u
                andb    #$0f
                orb     #$70
                stb     ,u

                leau    241,u           ;row21
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u

                leau    270,u           ;row22
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u
                lda     #$7f
                ldb     -13,u
                andb    #$0f
                orb     #$70
                std     -14,u

                leau    256,u           ;row23
                lda     #$7f
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u
                ldb     -1,u
                andb    #$0f
                orb     #$70
                stb     -1,u
                lda     #$ff
                ldb     -13,u
                andb    #$0f
                orb     #$f0
                std     -14,u

                leau    256,u           ;row24
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$f0
                std     ,u
                lda     #$7f
                ldb     -13,u
                andb    #$0f
                orb     #$70
                std     -14,u

                leau    256,u           ;row25
                lda     #$7f
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u
                ldb     -14,u
                andb    #$f0
                orb     #$0f
                stb     -14,u

                leau    256,u           ;row26
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u

                leau    254,u           ;row27
                ldb     ,u
                andb    #$f0
                orb     #$07
                stb     ,u
                ldb     -7,u
                andb    #$0f
                orb     #$70
                stb     -7,u

                leau    506,u           ;row29
                lda     ,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     ,u
                ldb     -3,u
                andb    #$0f
                orb     #$70
                stb     -3,u

                leau    256,u           ;row30
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$ff
                std     ,u

                leau    256,u           ;row31
                lda     ,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                std     ,u

                rts

Hero.25
                leau    270,u           ;row01
                lda     ,u
                anda    #$f0
                ora     #$08
                ldb     #$88
                std     ,u
                lda     #$88
                ldb     -13,u
                andb    #$0f
                orb     #$80
                std     -14,u

                leau    258,u           ;row02
                ldx     #$8ff8
                ldb     -3,u
                andb    #$f0
                orb     #$08
                pshu    x,b
                lda     #$f8
                ldb     -11,u
                andb    #$0f
                orb     #$80
                std     -12,u
                ldb     #$8f
                stb     -13,u

                leau    247,u           ;row03
                lda     #$f8
                ldb     11,u
                andb    #$0f
                orb     #$80
                std     10,u
                ldx     #$888f
                stx     8,u
                ldx     #$f888
                lda     -4,u
                anda    #$f0
                ora     #$08
                ldb     #$8f
                pshu    x,d

                leau    270,u           ;row04
                lda     #$88
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                ldx     #$fff8
                ldb     -3,u
                andb    #$f0
                orb     #$08
                pshu    x,b
                leau    -8,u
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                ldx     #$888f
                ldb     -3,u
                andb    #$f0
                orb     #$08
                pshu    x,b

                leau    271,u           ;row05
                ldx     #$88f8
                ldd     #$8ff8
                pshu    x,d
                leau    -6,u
                ldx     #$8ff8
                ldb     #$88
                pshu    x,d

                leau    268,u           ;row06
                lda     #$f8
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                lda     -2,u
                anda    #$f0
                ora     #$08
                ldb     #$f8
                std     -2,u
                lda     #$8f
                ldb     -9,u
                andb    #$0f
                orb     #$80
                std     -10,u
                lda     -12,u
                anda    #$f0
                ora     #$08
                ldb     #$8f
                std     -12,u

                leau    255,u           ;row07
                lda     #$88
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$08
                stb     -1,u
                ldb     -8,u
                andb    #$0f
                orb     #$80
                std     -9,u
                sta     -10,u

                rts

Hero.26
                leau    784,u           ;row03
                ldx     #$8888
                ldb     -3,u
                andb    #$f0
                orb     #$08
                pshu    x,b
                lda     #$88
                ldb     -11,u
                andb    #$0f
                orb     #$80
                std     -12,u
                sta     -13,u

                leau    259,u           ;row04
                ldy     #$fff8
                ldx     #$888f
                ldb     -5,u
                andb    #$f0
                orb     #$08
                pshu    y,x,b
                leau    -8,u
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                ldb     #$8f
                pshu    y,b

                leau    270,u           ;row05
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                ldb     #$8f
                pshu    y,b
                leau    -6,u
                ldb     -5,u
                andb    #$f0
                orb     #$08
                pshu    y,x,b

                leau    271,u           ;row06
                ldx     #$88f8
                ldb     #$f8
                pshu    x,d
                leau    -6,u
                ldx     #$8f88
                ldd     #$8f88
                pshu    x,d

                leau    267,u           ;row07
                lda     #$88
                ldb     1,u
                andb    #$0f
                orb     #$f0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$08
                stb     -1,u
                ldb     -8,u
                andb    #$0f
                orb     #$80
                std     -9,u
                ldb     #$8f
                stb     -10,u

                rts

Hero.27
                leau    1296,u          ;row05
                ldy     #$8888
                ldx     #$8888
                ldb     #$88
                pshu    y,x,b
                leau    -6,u
                pshu    y,x,b

                leau    272,u           ;row06
                ldy     #$fff8
                ldx     #$ffff
                ldb     #$8f
                pshu    y,x,b
                leau    -6,u
                pshu    y,x,b

                leau    270,u           ;row07
                lda     #$88
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                ldx     #$8888
                ldb     -3,u
                andb    #$f0
                orb     #$08
                pshu    x,b
                leau    -8,u
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                ldb     -3,u
                andb    #$f0
                orb     #$08
                pshu    x,b

                leau    268,u           ;row08
                ldx     #$88f8
                stx     ,u
                ldx     #$8f88
                stx     -10,u

                leau    256,u           ;row09
                ldd     ,u
                anda    #$f0
                ora     #$08
                andb    #$0f
                orb     #$80
                std     ,u
                ldd     -10,u
                anda    #$f0
                ora     #$08
                andb    #$0f
                orb     #$80
                std     -10,u

                rts

Hero.28
                leau    1291,u          ;row05
                lda     #$88
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                lda     -8,u
                anda    #$f0
                ora     #$08
                ldb     #$88
                std     -8,u

                leau    259,u           ;row06
                ldx     #$f888
                ldb     #$8f
                pshu    x,b
                leau    -6,u
                ldx     #$8ff8
                ldb     #$88
                pshu    x,b

                leau    270,u           ;row07
                ldy     #$8888
                ldx     #$8fff
                ldb     -5,u
                andb    #$f0
                orb     #$08
                pshu    y,x,b
                leau    -8,u
                lda     #$f8
                ldb     1,u
                andb    #$0f
                orb     #$80
                std     ,u
                ldx     #$88ff
                ldb     #$88
                pshu    x,b

                leau    272,u           ;row08
                ldx     #$fff8
                lda     #$88
                pshu    x,d
                leau    -8,u
                ldx     #$8fff
                pshu    y,x

                rts
Hero.Action.End

    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
                fcb     PHYSICAL_018000_019FFF
    
    org     $4000
Hero.30
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u

                leau    257,u           ;row05
                lda     #$66
                ldb     1,u
                andb    #$f0
                std     ,u
                ldx     #$aaa2
                lda     #$00
                ldb     -3,u
                andb    #$0f
                orb     #$60
                pshu    x,d

                leau    260,u           ;row06
                lda     #$a2
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldx     #$fffa
                lda     #$62
                ldb     -3,u
                andb    #$f0
                orb     #$0a
                pshu    x,d
                ldb     -1,u
                andb    #$f0
                stb     -1,u

                leau    261,u           ;row07
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$20
                std     ,u
                ldd     -2,u
                anda    #$f0
                ora     #$0f
                andb    #$f0
                orb     #$0a
                std     -2,u
                lda     #$af
                ldb     -3,u
                andb    #$f0
                orb     #$0f
                std     -4,u
                ldx     #$022a
                stx     -6,u

                leau    257,u           ;row08
                lda     #$a2
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     #$a2
                ldb     -1,u
                andb    #$0f
                orb     #$20
                std     -2,u
                ldd     -4,u
                anda    #$0f
                ora     #$a0
                andb    #$f0
                orb     #$0a
                std     -4,u
                ldb     -5,u
                andb    #$f0
                orb     #$0a
                stb     -5,u
                lda     -8,u
                anda    #$f0
                ldb     #$2a
                std     -8,u

                leau    258,u           ;row09
                ldy     #$0000
                ldx     #$2aa2
                ldd     #$aaaa
                pshu    y,x,d
                lda     #$2a
                ldb     -1,u
                andb    #$0f
                orb     #$a0
                std     -2,u
                ldd     -4,u
                anda    #$f0
                andb    #$0f
                std     -4,u

                leau    262,u           ;row10
                ldx     #$0060
                ldb     #$00
                pshu    x,b
                leau    -1,u
                ldx     #$a6aa
                lda     #$00
                ldb     -3,u
                andb    #$f0
                orb     #$0a
                pshu    x,d
                lda     #$06
                ldb     -1,u
                andb    #$f0
                std     -2,u

                leau    259,u           ;row11
                lda     #$26
                ldb     5,u
                andb    #$0f
                std     4,u
                ldx     #$2000
                stx     1,u
                ldx     #$0220
                ldb     -3,u
                andb    #$0f
                pshu    x,d
                lda     -2,u
                anda    #$f0
                ldb     #$62
                std     -2,u

                leau    264,u           ;row12
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                std     ,u
                ldy     #$0002
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u
                lda     -4,u
                anda    #$f0
                ldb     #$6a
                std     -4,u

                leau    258,u           ;row13
                lda     #$22
                ldb     5,u
                andb    #$0f
                orb     #$60
                std     4,u
                lda     #$02
                ldb     3,u
                andb    #$0f
                orb     #$20
                std     2,u
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     #$0a
                ldb     -3,u
                andb    #$f0
                pshu    x,d
                lda     -2,u
                anda    #$f0
                ora     #$06
                ldb     #$22
                std     -2,u

                leau    266,u           ;row14
                ldx     #$2260
                ldd     -4,u
                anda    #$0f
                ora     #$20
                andb    #$f0
                orb     #$0a
                pshu    x,d
                ldy     #$0000
                ldx     #$0000
                ldd     #$aaa2
                pshu    y,x,d
                ldd     -2,u
                anda    #$0f
                andb    #$0f
                orb     #$20
                std     -2,u

                leau    266,u           ;row15
                ldy     #$0220
                ldx     #$0066
                ldd     #$0000
                pshu    y,x,d
                ldx     #$0000
                stx     -2,u
                leau    -3,u
                ldx     #$2066
                ldb     #$02
                pshu    x,b

                leau    266,u           ;row16
                lda     ,u
                anda    #$f0
                ora     #$02
                ldb     #$20
                std     ,u
                ldy     #$2222
                ldx     #$0022
                lda     #$22
                ldb     -5,u
                andb    #$f0
                pshu    y,x,d
                ldx     #$0220
                pshu    y,x

                leau    268,u           ;row17
                ldy     #$a220
                ldx     #$aaa2
                ldd     #$20aa
                pshu    y,x,d
                ldy     #$aa02
                ldx     #$2aaa
                lda     -6,u
                anda    #$0f
                ldb     #$a2
                pshu    y,x,d

                leau    268,u           ;row18
                ldy     #$a260
                ldx     #$2222
                ldd     #$a202
                pshu    y,x,d
                ldy     #$202a
                ldx     #$a22a
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                orb     #$20
                pshu    y,x,d

                leau    260,u           ;row19
                lda     #$22
                ldb     7,u
                andb    #$0f
                orb     #$60
                std     6,u
                lda     #$aa
                ldb     5,u
                andb    #$f0
                orb     #$0a
                std     4,u
                lda     #$2a
                ldb     3,u
                andb    #$f0
                orb     #$0a
                std     2,u
                lda     #$aa
                ldb     1,u
                andb    #$f0
                orb     #$02
                std     ,u
                ldx     #$aaaa
                ldd     -4,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                orb     #$20
                pshu    x,d

                leau    266,u           ;row20
                ldd     ,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$0f
                ora     #$a0
                ldb     #$2a
                std     -2,u
                lda     #$aa
                ldb     -3,u
                andb    #$f0
                orb     #$0a
                std     -4,u
                lda     #$aa
                ldb     -5,u
                andb    #$0f
                orb     #$a0
                std     -6,u
                lda     #$aa
                ldb     -7,u
                andb    #$f0
                orb     #$0a
                std     -8,u
                lda     -10,u
                anda    #$f0
                ldb     #$22
                std     -10,u

                leau    251,u           ;row21
                lda     #$a2
                ldb     5,u
                andb    #$0f
                orb     #$20
                std     4,u
                lda     2,u
                anda    #$0f
                ora     #$a0
                ldb     #$aa
                std     2,u
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u
                lda     #$62
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                pshu    x,d

                leau    266,u           ;row22
                ldy     #$2260
                ldx     #$aa22
                ldb     #$fa
                pshu    y,x,b
                leau    -1,u
                ldx     #$aaaf
                ldd     #$022a
                pshu    x,d

                leau    264,u           ;row23
                lda     #$66
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$aa26
                ldd     -4,u
                anda    #$0f
                ora     #$f0
                andb    #$f0
                orb     #$0a
                pshu    x,d
                lda     -2,u
                anda    #$f0
                ora     #$0a
                ldb     #$af
                std     -2,u
                ldb     -3,u
                andb    #$f0
                orb     #$02
                stb     -3,u

                leau    259,u           ;row24
                lda     ,u
                anda    #$0f
                ora     #$20
                ldb     #$60
                std     ,u
                lda     -2,u
                anda    #$f0
                ora     #$0a
                ldb     #$a2
                std     -2,u
                lda     #$aa
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                std     -4,u
                ldx     #$062a
                stx     -6,u

                leau    254,u           ;row25
                ldd     2,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                std     2,u
                ldd     ,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$20
                std     ,u
                ldx     #$2aaa
                ldb     #$62
                pshu    x,b

                leau    260,u           ;row26
                ldd     ,u
                anda    #$0f
                ora     #$60
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ora     #$02
                ldb     #$22
                std     -2,u
                ldb     -3,u
                andb    #$f0
                orb     #$06
                stb     -3,u

                leau    254,u           ;row27
                ldx     #$0000
                stx     ,u


                rts

Hero.31
                leau    1032,u          ;row04
                ldb     #$00
                stb     ,u

                leau    257,u           ;row05
                lda     #$66
                ldb     1,u
                andb    #$f0
                std     ,u
                ldx     #$aaa2
                ldb     -3,u
                andb    #$0f
                orb     #$60
                pshu    x,b

                leau    259,u           ;row06
                lda     #$a2
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                lda     -2,u
                anda    #$0f
                ora     #$f0
                ldb     #$fa
                std     -2,u
                ldd     -4,u
                anda    #$0f
                ora     #$60
                andb    #$f0
                orb     #$0a
                std     -4,u
                ldb     -5,u
                andb    #$f0
                stb     -5,u

                leau    257,u           ;row07
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$20
                std     ,u
                ldd     -4,u
                anda    #$0f
                ora     #$a0
                andb    #$f0
                orb     #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                orb     #$20
                std     -6,u

                leau    256,u           ;row08
                lda     ,u
                anda    #$0f
                ora     #$20
                ldb     #$a2
                std     ,u
                ldd     -2,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$02
                std     -2,u
                ldb     #$af
                stb     -3,u
                ldd     -7,u
                anda    #$f0
                andb    #$0f
                orb     #$20
                std     -7,u

                leau    256,u           ;row09
                lda     ,u
                anda    #$0f
                ora     #$a0
                ldb     #$00
                std     ,u
                ldx     #$aa2a
                lda     #$a2
                ldb     -3,u
                andb    #$f0
                orb     #$0a
                pshu    x,d
                ldx     #$002a
                ldb     -3,u
                andb    #$f0
                pshu    x,b

                leau    266,u           ;row10
                ldx     #$0060
                ldb     #$00
                pshu    x,b
                lda     -3,u
                anda    #$f0
                ora     #$06
                ldb     #$aa
                std     -3,u
                ldd     -5,u
                anda    #$0f
                andb    #$f0
                orb     #$0a
                std     -5,u
                lda     #$06
                ldb     -6,u
                andb    #$f0
                std     -7,u

                leau    254,u           ;row11
                lda     3,u
                anda    #$f0
                ora     #$02
                ldb     #$26
                std     3,u
                lda     1,u
                anda    #$f0
                ldb     #$00
                std     1,u
                ldx     #$0220
                lda     #$26
                ldb     -3,u
                andb    #$0f
                pshu    x,d
                ldb     #$62
                stb     -1,u

                leau    263,u           ;row12
                lda     #$02
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldx     #$0000
                lda     -4,u
                anda    #$0f
                ldb     #$00
                pshu    x,d
                ldd     -2,u
                anda    #$f0
                andb    #$0f
                std     -2,u
                lda     -5,u
                anda    #$f0
                ldb     #$6a
                std     -5,u

                leau    255,u           ;row13
                lda     #$22
                ldb     7,u
                andb    #$0f
                orb     #$60
                std     6,u
                ldd     4,u
                anda    #$0f
                andb    #$0f
                orb     #$20
                std     4,u
                ldd     2,u
                anda    #$0f
                andb    #$0f
                std     2,u
                lda     ,u
                anda    #$f0
                ldb     #$00
                std     ,u
                ldx     #$0a20
                ldd     -4,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                orb     #$20
                pshu    x,d

                leau    268,u           ;row14
                ldx     #$2260
                ldd     -4,u
                anda    #$f0
                ora     #$02
                andb    #$f0
                orb     #$0a
                pshu    x,d
                ldy     #$0000
                ldx     #$0000
                ldd     -6,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                pshu    y,x,d
                ldb     -2,u
                andb    #$0f
                stb     -2,u

                leau    264,u           ;row15
                lda     #$02
                ldb     1,u
                andb    #$f0
                std     ,u
                ldy     #$0066
                ldd     #$0000
                pshu    y,x,d
                leau    -1,u
                ldx     #$2066
                ldb     #$02
                pshu    x,b

                leau    266,u           ;row16
                lda     ,u
                anda    #$f0
                ora     #$02
                ldb     #$20
                std     ,u
                ldy     #$2222
                ldx     #$0022
                ldd     -6,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                pshu    y,x,d
                ldx     #$0220
                pshu    y,x

                leau    261,u           ;row17
                lda     4,u
                anda    #$0f
                ora     #$a0
                ldb     #$a2
                std     4,u
                lda     2,u
                anda    #$0f
                ora     #$a0
                ldb     #$aa
                std     2,u
                lda     #$02
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldy     #$aaaa
                ldx     #$a22a
                pshu    y,x,a

                leau    268,u           ;row18
                ldx     #$a260
                ldd     #$2222
                pshu    x,d
                leau    -1,u
                ldy     #$2aa2
                ldx     #$2a20
                lda     -6,u
                anda    #$0f
                ora     #$20
                ldb     #$a2
                pshu    y,x,d

                leau    260,u           ;row19
                ldd     4,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$02
                std     4,u
                lda     2,u
                anda    #$f0
                ora     #$0a
                ldb     #$aa
                std     2,u
                lda     ,u
                anda    #$f0
                ora     #$02
                ldb     #$2a
                std     ,u
                ldx     #$aaaa
                lda     -4,u
                anda    #$0f
                ora     #$20
                ldb     #$aa
                pshu    x,d
                ldb     -1,u
                andb    #$f0
                orb     #$06
                stb     -1,u

                leau    265,u           ;row20
                ldd     ,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$0f
                ora     #$a0
                ldb     #$2a
                std     -2,u
                lda     #$aa
                ldb     -3,u
                andb    #$f0
                orb     #$0a
                std     -4,u
                lda     #$aa
                ldb     -5,u
                andb    #$0f
                orb     #$a0
                std     -6,u
                lda     #$aa
                ldb     -7,u
                andb    #$f0
                orb     #$0a
                std     -8,u
                ldb     #$22
                stb     -9,u

                leau    251,u           ;row21
                lda     #$a2
                ldb     5,u
                andb    #$0f
                orb     #$20
                std     4,u
                ldd     2,u
                anda    #$0f
                ora     #$a0
                andb    #$0f
                orb     #$a0
                std     2,u
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u
                ldd     -4,u
                anda    #$f0
                ora     #$02
                andb    #$f0
                orb     #$0a
                pshu    x,d

                leau    262,u           ;row22
                ldb     3,u
                andb    #$0f
                orb     #$60
                stb     3,u
                lda     #$aa
                ldb     1,u
                andb    #$f0
                orb     #$02
                std     ,u
                ldx     #$fffa
                lda     #$aa
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                pshu    x,d
                ldx     #$022a
                stx     -2,u

                leau    262,u           ;row23
                lda     #$66
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$aa26
                ldd     -4,u
                anda    #$0f
                ora     #$f0
                andb    #$f0
                orb     #$0a
                pshu    x,d
                lda     -2,u
                anda    #$f0
                ora     #$0a
                ldb     #$af
                std     -2,u
                ldb     #$22
                stb     -3,u

                leau    259,u           ;row24
                lda     ,u
                anda    #$0f
                ora     #$20
                ldb     #$60
                std     ,u
                lda     -2,u
                anda    #$0f
                ora     #$a0
                ldb     #$a2
                std     -2,u
                lda     #$aa
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                std     -4,u
                ldx     #$062a
                stx     -6,u

                leau    257,u           ;row25
                ldb     ,u
                andb    #$0f
                stb     ,u
                ldd     -3,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$20
                std     -3,u
                ldx     #$622a
                stx     -6,u

                leau    253,u           ;row26
                ldd     ,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     #$06
                stb     -2,u

                leau    255,u           ;row27
                lda     #$00
                ldb     1,u
                andb    #$f0
                std     ,u


                rts

Hero.32
                leau    1032,u          ;row04
                ldb     #$00
                stb     ,u

                leau    257,u           ;row05
                lda     #$66
                ldb     1,u
                andb    #$f0
                std     ,u
                ldx     #$aaa2
                ldb     -3,u
                andb    #$0f
                orb     #$60
                pshu    x,b

                leau    259,u           ;row06
                ldd     ,u
                anda    #$0f
                ora     #$a0
                andb    #$0f
                orb     #$20
                std     ,u
                lda     -2,u
                anda    #$0f
                ora     #$f0
                ldb     #$fa
                std     -2,u
                ldd     -4,u
                anda    #$0f
                ora     #$60
                andb    #$f0
                orb     #$0a
                std     -4,u
                ldb     -5,u
                andb    #$f0
                stb     -5,u

                leau    257,u           ;row07
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$20
                std     ,u
                ldd     -4,u
                anda    #$0f
                ora     #$a0
                andb    #$f0
                orb     #$0f
                std     -4,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                orb     #$20
                std     -6,u

                leau    257,u           ;row08
                ldb     #$a2
                stb     ,u
                ldd     -3,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$02
                std     -3,u
                ldb     -4,u
                andb    #$0f
                orb     #$a0
                stb     -4,u
                ldd     -8,u
                anda    #$f0
                andb    #$0f
                orb     #$20
                std     -8,u

                leau    255,u           ;row09
                lda     ,u
                anda    #$0f
                ora     #$a0
                ldb     #$00
                std     ,u
                ldb     #$2a
                stb     -1,u
                ldd     -4,u
                anda    #$0f
                ora     #$a0
                andb    #$f0
                orb     #$0a
                std     -4,u
                lda     -6,u
                anda    #$0f
                ldb     #$2a
                std     -6,u

                leau    258,u           ;row10
                ldb     #$60
                stb     ,u
                ldb     -2,u
                andb    #$0f
                stb     -2,u
                ldd     -5,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                orb     #$a0
                std     -5,u
                ldd     -8,u
                anda    #$f0
                andb    #$0f
                std     -8,u
                ldb     #$06
                stb     -9,u

                leau    256,u           ;row11
                ldb     #$26
                stb     ,u
                lda     -3,u
                anda    #$f0
                ldb     #$00
                std     -3,u
                lda     #$02
                ldb     -5,u
                andb    #$0f
                orb     #$20
                std     -6,u
                lda     #$26
                ldb     -7,u
                andb    #$0f
                std     -8,u
                ldb     -9,u
                andb    #$f0
                orb     #$02
                stb     -9,u

                leau    256,u           ;row12
                ldx     #$0002
                ldd     #$0000
                pshu    x,d
                ldb     -3,u
                andb    #$f0
                stb     -3,u
                ldb     -6,u
                andb    #$f0
                stb     -6,u

                leau    260,u           ;row13
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$60
                std     ,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                std     -6,u
                ldd     -8,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                std     -8,u
                ldd     -10,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                orb     #$20
                std     -10,u

                leau    252,u           ;row14
                lda     #$22
                ldb     5,u
                andb    #$f0
                std     4,u
                ldb     3,u
                andb    #$f0
                orb     #$0a
                stb     3,u
                lda     #$00
                ldb     1,u
                andb    #$f0
                std     ,u
                ldx     #$0000
                ldd     -4,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                pshu    x,d

                leau    262,u           ;row15
                lda     #$02
                ldb     3,u
                andb    #$f0
                std     2,u
                lda     ,u
                anda    #$f0
                ldb     #$66
                std     ,u
                ldd     #$0000
                pshu    x,d
                lda     #$20
                ldb     -2,u
                andb    #$f0
                orb     #$06
                std     -3,u

                leau    262,u           ;row16
                ldd     ,u
                anda    #$f0
                ora     #$02
                andb    #$f0
                std     ,u
                lda     #$22
                ldb     -2,u
                andb    #$f0
                orb     #$02
                std     -3,u
                ldb     -4,u
                andb    #$f0
                stb     -4,u
                ldd     -8,u
                anda    #$f0
                ora     #$02
                andb    #$f0
                orb     #$02
                std     -8,u
                ldb     #$20
                stb     -9,u

                leau    255,u           ;row17
                lda     ,u
                anda    #$0f
                ora     #$a0
                ldb     #$a2
                std     ,u
                ldd     -2,u
                anda    #$0f
                ora     #$a0
                andb    #$f0
                orb     #$0a
                std     -2,u
                lda     #$aa
                ldb     -4,u
                andb    #$0f
                std     -5,u
                lda     -7,u
                anda    #$0f
                ora     #$20
                ldb     #$aa
                std     -7,u
                lda     -9,u
                anda    #$0f
                ldb     #$a2
                std     -9,u

                leau    254,u           ;row18
                ldd     3,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$60
                std     3,u
                ldx     #$2222
                stx     1,u
                ldx     #$2aa2
                lda     #$2a
                ldb     -3,u
                andb    #$0f
                orb     #$20
                pshu    x,d
                ldb     #$a2
                stb     -1,u

                leau    262,u           ;row19
                ldd     ,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$02
                std     ,u
                ldd     -2,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$0a
                std     -2,u
                lda     -6,u
                anda    #$f0
                ora     #$0a
                ldb     #$aa
                std     -6,u
                ldd     -8,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$a0
                std     -8,u
                ldb     -9,u
                andb    #$f0
                orb     #$06
                stb     -9,u

                leau    258,u           ;row20
                ldb     ,u
                andb    #$0f
                stb     ,u
                lda     -3,u
                anda    #$0f
                ora     #$a0
                ldb     #$2a
                std     -3,u
                ldb     #$aa
                stb     -5,u
                ldd     -8,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                std     -8,u
                ldx     #$22aa
                stx     -10,u

                leau    254,u           ;row21
                lda     #$a2
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u
                ldd     -2,u
                anda    #$0f
                ora     #$a0
                andb    #$0f
                orb     #$a0
                std     -2,u
                lda     #$ff
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                std     -4,u
                lda     #$aa
                ldb     -5,u
                andb    #$f0
                orb     #$0a
                std     -6,u
                ldb     -8,u
                andb    #$f0
                orb     #$02
                stb     -8,u

                leau    254,u           ;row22
                lda     #$aa
                ldb     1,u
                andb    #$f0
                orb     #$02
                std     ,u
                ldb     -1,u
                andb    #$0f
                orb     #$f0
                stb     -1,u
                ldd     -4,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                std     -4,u
                lda     -6,u
                anda    #$f0
                ora     #$02
                ldb     #$2a
                std     -6,u

                leau    258,u           ;row23
                ldd     ,u
                anda    #$0f
                ora     #$60
                andb    #$0f
                std     ,u
                ldx     #$aa26
                ldd     -4,u
                anda    #$0f
                ora     #$f0
                andb    #$f0
                orb     #$0a
                pshu    x,d
                lda     -2,u
                anda    #$f0
                ora     #$0a
                ldb     #$af
                std     -2,u
                ldb     -3,u
                andb    #$f0
                orb     #$02
                stb     -3,u

                leau    259,u           ;row24
                lda     ,u
                anda    #$0f
                ora     #$20
                ldb     #$60
                std     ,u
                ldb     #$a2
                stb     -1,u
                lda     #$aa
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                std     -4,u
                ldd     -6,u
                anda    #$f0
                ora     #$06
                andb    #$f0
                orb     #$0a
                std     -6,u

                leau    257,u           ;row25
                ldb     ,u
                andb    #$0f
                stb     ,u
                ldd     -3,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$20
                std     -3,u
                ldx     #$622a
                stx     -6,u

                leau    251,u           ;row26
                ldb     ,u
                andb    #$f0
                orb     #$06
                stb     ,u

                leau    257,u           ;row27
                lda     #$00
                ldb     1,u
                andb    #$f0
                std     ,u


                rts

Hero.33
                leau    1032,u          ;row04
                ldb     #$00
                stb     ,u

                leau    256,u           ;row05
                ldd     ,u
                anda    #$0f
                ora     #$a0
                andb    #$0f
                orb     #$60
                std     ,u
                lda     -2,u
                anda    #$0f
                ora     #$60
                ldb     #$aa
                std     -2,u

                leau    257,u           ;row06
                ldd     ,u
                anda    #$0f
                ora     #$a0
                andb    #$0f
                orb     #$20
                std     ,u
                ldd     -2,u
                anda    #$0f
                ora     #$f0
                andb    #$f0
                orb     #$0a
                std     -2,u
                ldd     -5,u
                anda    #$f0
                andb    #$0f
                orb     #$60
                std     -5,u

                leau    257,u           ;row07
                ldb     ,u
                andb    #$0f
                orb     #$20
                stb     ,u
                ldb     -3,u
                andb    #$f0
                orb     #$0f
                stb     -3,u
                ldb     -5,u
                andb    #$0f
                orb     #$20
                stb     -5,u

                leau    257,u           ;row08
                ldb     ,u
                andb    #$f0
                orb     #$02
                stb     ,u
                ldd     -3,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$02
                std     -3,u
                ldb     -4,u
                andb    #$0f
                orb     #$a0
                stb     -4,u
                ldd     -8,u
                anda    #$f0
                andb    #$0f
                orb     #$20
                std     -8,u

                leau    255,u           ;row09
                ldd     ,u
                anda    #$0f
                ora     #$a0
                andb    #$0f
                std     ,u
                ldb     #$2a
                stb     -1,u
                ldd     -6,u
                anda    #$0f
                andb    #$0f
                orb     #$20
                std     -6,u

                leau    258,u           ;row10
                ldb     #$60
                stb     ,u
                ldb     -2,u
                andb    #$0f
                stb     -2,u
                ldd     -5,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                orb     #$a0
                std     -5,u
                ldd     -8,u
                anda    #$f0
                andb    #$0f
                std     -8,u
                ldb     #$06
                stb     -9,u

                leau    256,u           ;row11
                ldb     ,u
                andb    #$0f
                orb     #$20
                stb     ,u
                ldd     -3,u
                anda    #$f0
                andb    #$0f
                std     -3,u
                lda     #$02
                ldb     -5,u
                andb    #$0f
                orb     #$20
                std     -6,u
                lda     #$26
                ldb     -7,u
                andb    #$0f
                std     -8,u
                ldb     -9,u
                andb    #$f0
                orb     #$02
                stb     -9,u

                leau    256,u           ;row12
                ldx     #$0002
                ldd     #$0000
                pshu    x,d
                ldb     -3,u
                andb    #$f0
                stb     -3,u

                leau    260,u           ;row13
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$60
                std     ,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                std     -6,u
                ldd     -8,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                std     -8,u
                ldb     -10,u
                andb    #$f0
                orb     #$06
                stb     -10,u

                leau    256,u           ;row14
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$f0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$0a
                stb     -1,u
                ldd     -4,u
                anda    #$0f
                andb    #$f0
                std     -4,u
                lda     #$00
                ldb     -5,u
                andb    #$f0
                std     -6,u
                ldd     -8,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                std     -8,u

                leau    254,u           ;row15
                lda     #$02
                ldb     3,u
                andb    #$f0
                std     2,u
                ldd     ,u
                anda    #$f0
                andb    #$0f
                orb     #$60
                std     ,u
                ldx     #$0000
                ldd     #$0000
                pshu    x,d
                lda     #$20
                ldb     -2,u
                andb    #$f0
                orb     #$06
                std     -3,u

                leau    262,u           ;row16
                ldd     ,u
                anda    #$f0
                ora     #$02
                andb    #$f0
                std     ,u
                ldd     -3,u
                anda    #$f0
                ora     #$02
                andb    #$f0
                orb     #$02
                std     -3,u
                ldb     -4,u
                andb    #$f0
                stb     -4,u
                lda     #$20
                ldb     -8,u
                andb    #$f0
                orb     #$02
                std     -9,u

                leau    255,u           ;row17
                lda     ,u
                anda    #$0f
                ora     #$a0
                ldb     #$a2
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$0a
                stb     -1,u
                ldd     -6,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$0a
                std     -6,u
                lda     #$a2
                ldb     -7,u
                andb    #$0f
                orb     #$20
                std     -8,u
                ldb     -9,u
                andb    #$0f
                stb     -9,u

                leau    257,u           ;row18
                ldd     ,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$60
                std     ,u
                lda     #$22
                ldb     -1,u
                andb    #$0f
                orb     #$20
                std     -2,u
                lda     -5,u
                anda    #$f0
                ora     #$0a
                ldb     #$a2
                std     -5,u
                lda     #$2a
                ldb     -6,u
                andb    #$0f
                orb     #$20
                std     -7,u
                ldb     -8,u
                andb    #$f0
                orb     #$02
                stb     -8,u

                leau    255,u           ;row19
                ldb     ,u
                andb    #$f0
                orb     #$0a
                stb     ,u
                lda     -6,u
                anda    #$f0
                ora     #$0a
                ldb     #$aa
                std     -6,u
                ldb     -7,u
                andb    #$0f
                orb     #$a0
                stb     -7,u
                ldb     -9,u
                andb    #$f0
                orb     #$06
                stb     -9,u

                leau    258,u           ;row20
                ldb     ,u
                andb    #$0f
                stb     ,u
                lda     -3,u
                anda    #$0f
                ora     #$a0
                ldb     #$2a
                std     -3,u
                ldb     #$aa
                stb     -5,u
                ldd     -9,u
                anda    #$0f
                ora     #$a0
                andb    #$f0
                orb     #$0a
                std     -9,u
                ldb     #$22
                stb     -10,u

                leau    254,u           ;row21
                ldd     ,u
                anda    #$0f
                ora     #$a0
                andb    #$0f
                orb     #$20
                std     ,u
                lda     #$ff
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                std     -4,u
                ldb     #$aa
                stb     -6,u

                leau    254,u           ;row22
                lda     #$aa
                ldb     1,u
                andb    #$f0
                orb     #$02
                std     ,u
                ldb     -1,u
                andb    #$0f
                orb     #$f0
                stb     -1,u
                ldd     -5,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$0a
                std     -5,u
                ldb     -6,u
                andb    #$f0
                orb     #$02
                stb     -6,u

                leau    258,u           ;row23
                ldd     ,u
                anda    #$0f
                ora     #$60
                andb    #$0f
                std     ,u
                lda     #$aa
                ldb     -1,u
                andb    #$f0
                orb     #$06
                std     -2,u
                ldd     -4,u
                anda    #$0f
                ora     #$f0
                andb    #$f0
                orb     #$0a
                std     -4,u
                lda     -6,u
                anda    #$f0
                ora     #$0a
                ldb     #$af
                std     -6,u
                ldb     -7,u
                andb    #$f0
                orb     #$02
                stb     -7,u

                leau    254,u           ;row24
                ldd     ,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$20
                std     ,u
                lda     #$aa
                ldb     -2,u
                andb    #$0f
                orb     #$a0
                std     -3,u
                ldd     -5,u
                anda    #$f0
                ora     #$06
                andb    #$f0
                orb     #$0a
                std     -5,u

                leau    257,u           ;row25
                ldb     1,u
                andb    #$0f
                stb     1,u
                ldx     #$622a
                stx     -5,u

                leau    252,u           ;row26
                ldb     ,u
                andb    #$f0
                orb     #$06
                stb     ,u

                leau    257,u           ;row27
                ldb     ,u
                andb    #$0f
                stb     ,u


                rts

Hero.34
                leau    1032,u          ;row04
                ldb     ,u
                andb    #$f0
                stb     ,u

                leau    257,u           ;row05
                ldb     ,u
                andb    #$0f
                orb     #$60
                stb     ,u
                ldb     -2,u
                andb    #$f0
                orb     #$0a
                stb     -2,u

                leau    257,u           ;row06
                ldb     ,u
                andb    #$0f
                orb     #$20
                stb     ,u
                ldd     -3,u
                anda    #$0f
                ora     #$f0
                andb    #$f0
                orb     #$0a
                std     -3,u

                leau    253,u           ;row07
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u
                ldb     -2,u
                andb    #$0f
                orb     #$20
                stb     -2,u

                leau    260,u           ;row08
                ldb     ,u
                andb    #$f0
                orb     #$02
                stb     ,u
                ldb     -4,u
                andb    #$0f
                orb     #$a0
                stb     -4,u
                ldd     -8,u
                anda    #$f0
                andb    #$0f
                orb     #$20
                std     -8,u

                leau    254,u           ;row09
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$a0
                std     ,u
                ldb     -5,u
                andb    #$0f
                stb     -5,u

                leau    259,u           ;row10
                ldb     ,u
                andb    #$f0
                stb     ,u
                ldb     -2,u
                andb    #$0f
                stb     -2,u
                ldd     -5,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                orb     #$a0
                std     -5,u
                ldd     -8,u
                anda    #$f0
                andb    #$0f
                std     -8,u
                ldb     -9,u
                andb    #$f0
                orb     #$06
                stb     -9,u

                leau    253,u           ;row11
                ldd     ,u
                anda    #$f0
                andb    #$0f
                std     ,u
                ldb     -6,u
                andb    #$f0
                orb     #$02
                stb     -6,u

                leau    257,u           ;row12
                ldd     ,u
                anda    #$f0
                andb    #$0f
                std     ,u
                ldb     #$00
                stb     -2,u
                ldb     -5,u
                andb    #$f0
                stb     -5,u

                leau    258,u           ;row13
                ldd     ,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$60
                std     ,u
                ldd     -4,u
                anda    #$0f
                andb    #$0f
                std     -4,u
                ldd     -6,u
                anda    #$f0
                andb    #$f0
                std     -6,u
                ldb     -7,u
                andb    #$f0
                stb     -7,u
                ldb     -10,u
                andb    #$f0
                orb     #$06
                stb     -10,u

                leau    257,u           ;row14
                ldb     ,u
                andb    #$f0
                stb     ,u
                ldb     -2,u
                andb    #$f0
                orb     #$0a
                stb     -2,u
                ldd     -7,u
                anda    #$0f
                andb    #$f0
                std     -7,u
                ldd     -9,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                std     -9,u

                leau    255,u           ;row15
                ldd     ,u
                anda    #$0f
                andb    #$f0
                std     ,u
                ldd     -2,u
                anda    #$f0
                andb    #$0f
                orb     #$60
                std     -2,u
                ldd     -9,u
                anda    #$0f
                ora     #$20
                andb    #$f0
                orb     #$06
                std     -9,u

                leau    256,u           ;row16
                ldb     ,u
                andb    #$f0
                orb     #$02
                stb     ,u
                ldd     -4,u
                anda    #$f0
                andb    #$f0
                orb     #$02
                std     -4,u
                ldb     -9,u
                andb    #$0f
                orb     #$20
                stb     -9,u

                leau    255,u           ;row17
                ldd     ,u
                anda    #$0f
                ora     #$a0
                andb    #$0f
                orb     #$a0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$0a
                stb     -1,u
                ldd     -6,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$0a
                std     -6,u
                lda     #$a2
                ldb     -7,u
                andb    #$0f
                orb     #$20
                std     -8,u
                ldb     -9,u
                andb    #$0f
                stb     -9,u

                leau    258,u           ;row18
                ldb     ,u
                andb    #$0f
                orb     #$60
                stb     ,u
                ldd     -3,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$20
                std     -3,u
                ldd     -7,u
                anda    #$0f
                ora     #$20
                andb    #$f0
                orb     #$0a
                std     -7,u
                ldb     -8,u
                andb    #$0f
                orb     #$20
                stb     -8,u

                leau    254,u           ;row19
                ldb     ,u
                andb    #$f0
                orb     #$0a
                stb     ,u
                lda     -6,u
                anda    #$f0
                ora     #$0a
                ldb     #$aa
                std     -6,u
                ldb     -7,u
                andb    #$0f
                orb     #$a0
                stb     -7,u
                ldb     -9,u
                andb    #$f0
                orb     #$06
                stb     -9,u

                leau    258,u           ;row20
                ldb     ,u
                andb    #$0f
                stb     ,u
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                stb     -3,u
                ldb     #$aa
                stb     -5,u
                ldd     -9,u
                anda    #$0f
                ora     #$a0
                andb    #$f0
                orb     #$0a
                std     -9,u
                ldb     #$22
                stb     -10,u

                leau    254,u           ;row21
                ldb     ,u
                andb    #$0f
                orb     #$a0
                stb     ,u
                ldb     #$ff
                stb     -4,u
                ldb     -6,u
                andb    #$f0
                orb     #$0a
                stb     -6,u

                leau    254,u           ;row22
                ldb     ,u
                andb    #$0f
                orb     #$a0
                stb     ,u
                ldd     -5,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$0a
                std     -5,u
                ldb     -6,u
                andb    #$f0
                orb     #$02
                stb     -6,u

                leau    257,u           ;row23
                ldb     ,u
                andb    #$f0
                orb     #$06
                stb     ,u
                ldb     -2,u
                andb    #$f0
                orb     #$0a
                stb     -2,u
                ldb     -4,u
                andb    #$f0
                orb     #$0f
                stb     -4,u
                ldb     -6,u
                andb    #$f0
                orb     #$02
                stb     -6,u

                leau    255,u           ;row24
                ldd     ,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$20
                std     ,u
                lda     #$aa
                ldb     -2,u
                andb    #$0f
                orb     #$a0
                std     -3,u
                ldb     -4,u
                andb    #$f0
                orb     #$0a
                stb     -4,u

                leau    258,u           ;row25
                ldb     ,u
                andb    #$0f
                stb     ,u
                lda     -6,u
                anda    #$0f
                ora     #$60
                ldb     #$2a
                std     -6,u

                leau    508,u           ;row27
                ldb     ,u
                andb    #$0f
                stb     ,u


                rts

Hero.35
                leau    1032,u          ;row04
                ldb     ,u
                andb    #$f0
                stb     ,u

                leau    767,u           ;row07
                ldb     ,u
                andb    #$f0
                orb     #$0f
                stb     ,u
                ldb     -2,u
                andb    #$0f
                orb     #$20
                stb     -2,u

                leau    252,u           ;row08
                ldb     ,u
                andb    #$f0
                stb     ,u

                leau    263,u           ;row09
                ldb     ,u
                andb    #$0f
                orb     #$a0
                stb     ,u
                ldb     -6,u
                andb    #$0f
                stb     -6,u

                leau    258,u           ;row10
                ldb     ,u
                andb    #$f0
                stb     ,u
                ldb     -2,u
                andb    #$0f
                stb     -2,u
                ldb     -9,u
                andb    #$f0
                orb     #$06
                stb     -9,u

                leau    253,u           ;row11
                ldb     ,u
                andb    #$f0
                stb     ,u
                ldb     -6,u
                andb    #$f0
                orb     #$02
                stb     -6,u

                leau    257,u           ;row12
                ldb     ,u
                andb    #$f0
                stb     ,u
                ldb     -2,u
                andb    #$0f
                stb     -2,u
                ldb     -5,u
                andb    #$f0
                stb     -5,u

                leau    248,u           ;row13
                ldb     ,u
                andb    #$f0
                orb     #$06
                stb     ,u

                leau    265,u           ;row14
                ldb     ,u
                andb    #$f0
                orb     #$0a
                stb     ,u
                ldb     -6,u
                andb    #$0f
                orb     #$a0
                stb     -6,u

                leau    256,u           ;row15
                ldd     ,u
                anda    #$0f
                ora     #$60
                andb    #$0f
                std     ,u
                ldb     -1,u
                andb    #$f0
                stb     -1,u
                ldb     -8,u
                andb    #$0f
                orb     #$20
                stb     -8,u

                leau    257,u           ;row16
                ldb     ,u
                andb    #$f0
                orb     #$02
                stb     ,u
                ldb     -3,u
                andb    #$f0
                orb     #$02
                stb     -3,u
                ldb     -9,u
                andb    #$0f
                orb     #$20
                stb     -9,u

                leau    250,u           ;row17
                ldb     ,u
                andb    #$f0
                orb     #$0a
                stb     ,u
                lda     #$a2
                ldb     -2,u
                andb    #$0f
                orb     #$20
                std     -3,u
                ldb     -4,u
                andb    #$0f
                stb     -4,u

                leau    263,u           ;row18
                ldb     ,u
                andb    #$0f
                orb     #$60
                stb     ,u
                ldd     -8,u
                anda    #$0f
                ora     #$20
                andb    #$0f
                orb     #$20
                std     -8,u

                leau    254,u           ;row19
                ldb     ,u
                andb    #$f0
                orb     #$0a
                stb     ,u
                ldd     -7,u
                anda    #$0f
                ora     #$a0
                andb    #$f0
                orb     #$0a
                std     -7,u

                leau    258,u           ;row20
                ldb     ,u
                andb    #$0f
                stb     ,u
                ldb     -3,u
                andb    #$0f
                orb     #$a0
                stb     -3,u
                ldb     -8,u
                andb    #$f0
                orb     #$0a
                stb     -8,u

                leau    254,u           ;row21
                ldb     ,u
                andb    #$0f
                orb     #$a0
                stb     ,u
                ldb     -4,u
                andb    #$f0
                orb     #$0f
                stb     -4,u

                leau    254,u           ;row22
                ldb     ,u
                andb    #$0f
                orb     #$a0
                stb     ,u
                ldd     -5,u
                anda    #$f0
                ora     #$0a
                andb    #$f0
                orb     #$0a
                std     -5,u
                ldb     -6,u
                andb    #$f0
                orb     #$02
                stb     -6,u

                leau    255,u           ;row23
                ldb     ,u
                andb    #$f0
                orb     #$0a
                stb     ,u
                ldb     -4,u
                andb    #$f0
                orb     #$02
                stb     -4,u

                leau    258,u           ;row24
                ldb     ,u
                andb    #$0f
                orb     #$20
                stb     ,u
                ldd     -4,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                std     -4,u

                leau    257,u           ;row25
                ldb     ,u
                andb    #$0f
                stb     ,u
                ldd     -6,u
                anda    #$0f
                ora     #$60
                andb    #$0f
                orb     #$20
                std     -6,u

                rts

Hero.36
                rts
Hero.Fade.End

    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
                fcb     PHYSICAL_01A000_01BFFF
    
    org     $4000
PowerUps.00
                leau    56,u           ;row03
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    19,u           ;row04
                ldy     #$c000
                ldx     #$0ccc
                lda     -6,u
                anda    #$f0
                pshu    y,x,d

                leau    23,u           ;row05
                ldy     #$4c00
                ldx     #$eeee
                ldd     #$44c0
                pshu    y,x,d
                ldx     #$000c
                stx     -2,u

                leau    21,u           ;row06
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$44ee
                ldx     #$f404
                ldd     #$ffff
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$c0
                std     -2,u

                leau    24,u           ;row07
                ldy     #$e4c0
                ldx     #$4444
                lda     #$ff
                pshu    y,x,d
                ldx     #$ffff
                ldd     #$0c0f
                pshu    x,d

                leau    25,u           ;row08
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$e44e
                ldx     #$40c4
                ldd     #$c4ff
                pshu    y,x,d
                ldx     #$fff4
                lda     -4,u
                anda    #$f0
                ldb     #$c0
                pshu    x,d

                leau    26,u           ;row09
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ee44
                ldx     #$40c4
                ldd     #$44ff
                pshu    y,x,d
                ldx     #$ff44
                lda     -4,u
                anda    #$f0
                ldb     #$0f
                pshu    x,d

                leau    28,u           ;row10
                ldy     #$4ec0
                ldx     #$eee4
                ldd     #$0cce
                pshu    y,x,d
                ldy     #$4fff
                ldx     #$f444
                ldd     #$00ff
                pshu    y,x,d

                leau    28,u           ;row11
                ldy     #$4e40
                ldx     #$eeee
                ldd     #$cc4e
                pshu    y,x,d
                ldy     #$fff4
                ldx     #$4444
                ldd     #$0cff
                pshu    y,x,d

                leau    27,u           ;row12
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ee44
                ldx     #$eeee
                ldd     #$40c4
                pshu    y,x,d
                ldy     #$4fff
                ldx     #$ffc4
                lda     -6,u
                anda    #$f0
                ldb     #$04
                pshu    y,x,d

                leau    28,u           ;row13
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$eee4
                ldx     #$4eee
                ldd     #$f40c
                pshu    y,x,d
                ldy     #$fff4
                ldx     #$ffff
                lda     -6,u
                anda    #$f0
                ldb     #$04
                pshu    y,x,d

                leau    28,u           ;row14
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$eee4
                ldx     #$c4ee
                ldd     #$ff40
                pshu    y,x,d
                ldy     #$ff4f
                ldx     #$ffff
                lda     -6,u
                anda    #$f0
                ldb     #$0c
                pshu    y,x,d

                leau    28,u           ;row15
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$eee4
                ldx     #$0c4e
                ldd     #$fff4
                pshu    y,x,d
                ldy     #$f40c
                ldx     #$4fff
                lda     -6,u
                anda    #$f0
                ldb     #$40
                pshu    y,x,d

                leau    28,u           ;row16
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$eee4
                ldx     #$4004
                ldd     #$4fff
                pshu    y,x,d
                ldy     #$ccc0
                ldx     #$4000
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row17
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4ee4
                ldx     #$ff40
                ldd     #$04ff
                pshu    y,x,d
                ldy     #$c4ee
                ldx     #$4e4c
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row18
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$c4e4
                ldx     #$ff40
                ldd     #$e04f
                pshu    y,x,d
                ldy     #$eeee
                ldx     #$4eee
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row19
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0c44
                ldx     #$f444
                ldd     #$ee0f
                pshu    y,x,d
                ldy     #$eeee
                ldx     #$44ee
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    29,u           ;row20
                ldy     #$4e40
                ldx     #$ff40
                ldd     #$044f
                pshu    y,x,d
                ldy     #$eeee
                ldx     #$eeee
                ldb     #$e4
                pshu    y,x,d

                leau    28,u           ;row21
                ldy     #$0e40
                ldx     #$fff4
                ldd     #$00c4
                pshu    y,x,d
                ldy     #$eeee
                ldx     #$4eee
                ldd     #$0ce4
                pshu    y,x,d

                leau    26,u           ;row22
                lda     #$40
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4fff
                ldx     #$4fff
                ldd     #$eee0
                pshu    y,x,d
                ldx     #$44ee
                lda     -4,u
                anda    #$f0
                ldb     #$4e
                pshu    x,d

                leau    26,u           ;row23
                lda     #$f4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f4ff
                ldx     #$ffff
                ldd     #$ee04
                pshu    y,x,d
                ldx     #$e44e
                lda     -4,u
                anda    #$f0
                ldb     #$c4
                pshu    x,d

                leau    27,u           ;row24
                ldy     #$4ff0
                ldx     #$ffff
                ldd     #$04fc
                pshu    y,x,d
                ldx     #$444e
                ldd     #$0c4e
                pshu    x,d

                leau    24,u           ;row25
                lda     #$04
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffff
                ldx     #$0c0c
                ldd     #$ee44
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$c4
                std     -2,u

                leau    23,u           ;row26
                ldy     #$ff00
                ldx     #$04cc
                ldd     #$eee0
                pshu    y,x,d
                ldx     #$00c4
                stx     -2,u

                leau    20,u           ;row27
                lda     #$fc
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$000f
                ldx     #$cc44
                ldb     #$00
                pshu    y,x,b

                leau    22,u           ;row28
                ldy     #$c4c0
                ldx     #$0000
                pshu    y,x,b

                leau    19,u           ;row29
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u

                rts

PowerUps.01
                leau    9,u             ;row00
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    22,u           ;row01
                ldy     #$4440
                ldx     #$4444
                ldd     #$0444
                pshu    y,x,d

                leau    22,u           ;row02
                ldy     #$c440
                ldx     #$ffff
                ldb     #$c4
                pshu    y,x,d

                leau    22,u           ;row03
                pshu    y,x,d

                leau    22,u           ;row04
                ldy     #$4440
                ldx     #$44c4
                ldb     #$44
                pshu    y,x,d

                leau    23,u           ;row05
                ldy     #$4400
                ldx     #$ff4c
                ldd     #$44ff
                pshu    y,x,d
                ldx     #$000c
                stx     -2,u

                leau    21,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$cffc
                ldx     #$4444
                ldd     #$cffc
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$44
                std     -2,u

                leau    24,u           ;row07
                ldy     #$fc40
                ldx     #$c444
                ldd     #$c44c
                pshu    y,x,d
                ldx     #$444c
                ldd     #$04cf
                pshu    x,d

                leau    25,u           ;row08
                lda     #$c4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$c44f
                ldx     #$fff4
                ldd     #$4fff
                pshu    y,x,d
                ldx     #$f44c
                lda     -4,u
                anda    #$f0
                ldb     #$4c
                pshu    x,d

                leau    26,u           ;row09
                lda     #$f4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$fc4c
                ldx     #$4fff
                ldd     #$fff4
                pshu    y,x,d
                ldx     #$c4cf
                lda     -4,u
                anda    #$f0
                ldb     #$4f
                pshu    x,d

                leau    28,u           ;row10
                ldy     #$4c40
                ldx     #$ffc4
                ldd     #$fff4
                pshu    y,x,d
                ldy     #$4ff4
                ldx     #$4cff
                ldd     #$0444
                pshu    y,x,d

                leau    28,u           ;row11
                ldy     #$4f40
                ldx     #$fffc
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$fff4
                ldx     #$cfff
                ldd     #$04f4
                pshu    y,x,d

                leau    28,u           ;row12
                ldy     #$4fc0
                ldx     #$f4f4
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$fff4
                ldx     #$4f4f
                ldd     #$4cf4
                pshu    y,x,d
                ldb     -1,u
                andb    #$f0
                stb     -1,u

                leau    27,u           ;row13
                lda     #$44
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffc4
                ldx     #$ffff
                ldd     #$f4ff
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$4cff
                lda     -6,u
                anda    #$f0
                ldb     #$44
                pshu    y,x,d

                leau    28,u           ;row14
                lda     #$f4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffcc
                ldx     #$ffff
                ldb     #$ff
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$ccff
                lda     -6,u
                anda    #$f0
                ldb     #$4f
                pshu    y,x,d

                leau    28,u           ;row15
                lda     #$f4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4f44
                ldx     #$ffff
                ldb     #$4f
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$44f4
                lda     -6,u
                anda    #$f0
                ldb     #$4f
                pshu    y,x,d

                leau    28,u           ;row16
                lda     #$f4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4f44
                ldx     #$ffff
                ldb     #$4f
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$44f4
                lda     -6,u
                anda    #$f0
                ldb     #$4f
                pshu    y,x,d

                leau    28,u           ;row17
                lda     #$f4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffc4
                ldx     #$ffff
                ldd     #$ff44
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$ccff
                lda     -6,u
                anda    #$f0
                ldb     #$4f
                pshu    y,x,d

                leau    28,u           ;row18
                lda     #$44
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ffc4
                ldx     #$ffff
                ldd     #$fff4
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$4cff
                lda     -6,u
                anda    #$f0
                ldb     #$44
                pshu    y,x,d

                leau    28,u           ;row19
                lda     #$c4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f44f
                ldx     #$4ff4
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$4fff
                ldx     #$f44f
                lda     -6,u
                anda    #$f0
                ldb     #$4c
                pshu    y,x,d

                leau    29,u           ;row20
                ldy     #$4f40
                ldx     #$fffc
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$cfff
                ldd     #$04f4
                pshu    y,x,d

                leau    28,u           ;row21
                ldy     #$4440
                ldx     #$ffc4
                lda     #$ff
                pshu    y,x,d
                ldy     #$4fff
                ldx     #$4cff
                ldd     #$0444
                pshu    y,x,d

                leau    26,u           ;row22
                lda     #$fc
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$fc4c
                ldx     #$4fff
                ldd     #$fff4
                pshu    y,x,d
                ldx     #$c4cf
                lda     -4,u
                anda    #$f0
                ldb     #$cf
                pshu    x,d

                leau    26,u           ;row23
                lda     #$c4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$c44f
                ldx     #$fff4
                ldd     #$4fff
                pshu    y,x,d
                ldx     #$f44c
                lda     -4,u
                anda    #$f0
                ldb     #$4c
                pshu    x,d

                leau    27,u           ;row24
                ldy     #$fc40
                ldx     #$c444
                lda     #$c4
                pshu    y,x,d
                ldx     #$444c
                ldd     #$04cf
                pshu    x,d

                leau    24,u           ;row25
                lda     #$44
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$cffc
                ldx     #$4444
                ldd     #$cffc
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$44
                std     -2,u

                leau    23,u           ;row26
                ldy     #$4400
                ldx     #$ff4c
                ldd     #$c4ff
                pshu    y,x,d
                ldx     #$0044
                stx     -2,u

                leau    21,u           ;row27
                ldx     #$4444
                ldd     #$0044
                pshu    y,x,d

                leau    21,u           ;row28
                ldx     #$0000
                ldb     #$00
                pshu    x,d

                rts

PowerUps.02
                leau    4,u             ;row00
                ldb     #$00
                stb     ,u

                leau    24,u           ;row01
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     #$ff
                ldb     -7,u
                andb    #$0f
                std     -8,u
                ldb     -9,u
                andb    #$f0
                stb     -9,u

                leau    18,u           ;row02
                ldx     #$4f40
                ldb     -3,u
                andb    #$f0
                pshu    x,b
                ldd     -5,u
                anda    #$f0
                andb    #$0f
                std     -5,u
                lda     #$ff
                ldb     -6,u
                andb    #$0f
                std     -7,u
                ldb     -8,u
                andb    #$f0
                stb     -8,u

                leau    19,u           ;row03
                ldx     #$fff0
                ldb     #$04
                pshu    x,b
                leau    -1,u
                ldx     #$0000
                ldd     #$0ff0
                pshu    x,d
                ldb     #$00
                stb     -2,u

                leau    23,u           ;row04
                lda     #$f4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff00
                ldx     #$cc00
                ldb     #$04
                pshu    y,x,d
                ldx     #$00ff
                ldd     #$0040
                pshu    x,d

                leau    26,u           ;row05
                lda     #$4f
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f000
                ldx     #$e04f
                ldd     #$f40e
                pshu    y,x,d
                ldy     #$c0ff
                ldx     #$ff00
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    27,u           ;row06
                lda     #$0f
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0c00
                ldx     #$0ff4
                ldd     #$4044
                pshu    y,x,d
                ldy     #$ee04
                ldx     #$ff04
                ldb     -5,u
                andb    #$f0
                pshu    y,x,b

                leau    28,u           ;row07
                ldy     #$c000
                ldx     #$04e4
                ldd     #$e0ff
                pshu    y,x,d
                ldy     #$400e
                ldx     #$4e44
                ldb     #$00
                pshu    y,x,b

                leau    25,u           ;row08
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0000
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                ldb     #$c4
                pshu    y,d

                leau    26,u           ;row09
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$fff0
                ldx     #$ffff
                ldd     #$ffff
                pshu    y,x,d
                ldx     #$0fff
                lda     -4,u
                anda    #$f0
                ldb     #$4e
                pshu    x,d

                leau    28,u           ;row10
                ldy     #$4ec0
                ldx     #$4440
                ldd     #$ff44
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$044f
                ldd     #$0ce4
                pshu    y,x,d

                leau    28,u           ;row11
                ldy     #$4e40
                ldx     #$4440
                ldd     #$fff4
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$04ff
                ldd     #$04e4
                pshu    y,x,d

                leau    27,u           ;row12
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4044
                ldx     #$c4f4
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$4404
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row13
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$40e4
                ldx     #$f4f4
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$4e04
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row14
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$40e4
                ldx     #$44f4
                ldd     #$4ff4
                pshu    y,x,d
                ldy     #$ff44
                ldx     #$4e04
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row15
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$40e4
                ldx     #$cc44
                ldd     #$c44c
                pshu    y,x,d
                ldy     #$f4cc
                ldx     #$4e04
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row16
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$40e4
                ldx     #$fcc4
                ldd     #$cccf
                pshu    y,x,d
                ldy     #$4cff
                ldx     #$4e04
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row17
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$40e4
                ldx     #$ccc4
                ldd     #$cccc
                pshu    y,x,d
                ldy     #$4ccf
                ldx     #$4e04
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row18
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$40e4
                ldx     #$ccc4
                ldd     #$cccc
                pshu    y,x,d
                ldy     #$4ccc
                ldx     #$4e04
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row19
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4044
                ldx     #$cc44
                ldd     #$cccc
                pshu    y,x,d
                ldy     #$f4cc
                ldx     #$4404
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    29,u           ;row20
                ldy     #$4e40
                ldx     #$f440
                ldd     #$cc44
                pshu    y,x,d
                ldy     #$44cc
                ldx     #$04cf
                ldd     #$04e4
                pshu    y,x,d

                leau    28,u           ;row21
                ldy     #$4e40
                ldx     #$f440
                ldd     #$c4f4
                pshu    y,x,d
                ldy     #$ff4c
                ldx     #$04cf
                ldd     #$0ce4
                pshu    y,x,d

                leau    28,u           ;row22
                ldy     #$e400
                ldx     #$f440
                ldd     #$4ff4
                pshu    y,x,d
                ldy     #$ccf4
                ldx     #$04ff
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    26,u           ;row23
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f440
                ldx     #$fff4
                ldd     #$ccff
                pshu    y,x,d
                ldx     #$04fc
                lda     -4,u
                anda    #$f0
                ldb     #$c4
                pshu    x,d

                leau    27,u           ;row24
                ldy     #$40c0
                ldx     #$f4f4
                ldd     #$ffff
                pshu    y,x,d
                ldx     #$cccf
                ldd     #$0c04
                pshu    x,d

                leau    24,u           ;row25
                lda     #$40
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f4f4
                ldx     #$ffff
                ldd     #$ccff
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$04
                std     -2,u

                leau    23,u           ;row26
                ldy     #$f440
                ldx     #$fff4
                ldd     #$ffff
                pshu    y,x,d
                ldx     #$04cf
                stx     -2,u

                leau    22,u           ;row27
                ldy     #$fff0
                ldx     #$ffff
                pshu    y,x,d
                ldx     #$0fff
                stx     -2,u

                leau    22,u           ;row28
                ldy     #$0000
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                sty     -2,u

                rts

PowerUps.03
                leau    58,u           ;row03
                ldx     #$0000
                ldd     #$0000
                pshu    x,d

                leau    21,u           ;row04
                ldy     #$cc00
                ldx     #$4444
                ldb     #$cc
                pshu    y,x,d
                ldb     -5,u
                andb    #$f0
                stb     -5,u

                leau    23,u           ;row05
                ldy     #$4c00
                ldx     #$eeee
                ldd     #$eeee
                pshu    y,x,d
                ldx     #$00c4
                stx     -2,u
                lda     #$00
                ldb     -4,u
                andb    #$0f
                std     -5,u
                ldb     #$04
                stb     -6,u

                leau    21,u           ;row06
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$44ee
                ldx     #$4444
                ldd     #$ee44
                pshu    y,x,d
                ldy     #$0004
                ldx     #$ff40
                ldb     #$04
                pshu    y,x,b

                leau    29,u           ;row07
                ldy     #$e4c0
                ldx     #$e444
                ldb     #$ee
                pshu    y,x,d
                ldy     #$044e
                ldx     #$ff40
                ldd     #$4fff
                pshu    y,x,d
                ldb     -1,u
                andb    #$f0
                stb     -1,u

                leau    27,u           ;row08
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$e44e
                ldx     #$eeee
                ldd     #$00ee
                pshu    y,x,d
                ldy     #$ff40
                ldx     #$4fff
                pshu    y,x,a

                leau    27,u           ;row09
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ee44
                ldx     #$eeee
                ldd     #$f400
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$004f
                lda     -6,u
                anda    #$f0
                ldb     #$44
                pshu    y,x,d

                leau    30,u           ;row10
                ldy     #$4ec0
                ldx     #$eee4
                ldd     #$00ee
                pshu    y,x,d
                ldy     #$fff4
                ldx     #$04ff
                ldd     #$ff00
                pshu    y,x,d
                ldb     #$04
                stb     -1,u

                leau    28,u           ;row11
                ldy     #$4e40
                ldx     #$eeee
                ldd     #$f400
                pshu    y,x,d
                ldy     #$ffff
                ldx     #$4444
                ldd     #$44ff
                pshu    y,x,d
                ldb     #$00
                stb     -1,u

                leau    27,u           ;row12
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ee44
                ldx     #$f40e
                ldd     #$44ff
                pshu    y,x,d
                ldy     #$4400
                ldx     #$444f
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    28,u           ;row13
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$eee4
                ldx     #$4ff0
                ldd     #$fff4
                pshu    y,x,d
                ldy     #$40ff
                ldx     #$0004
                lda     -6,u
                anda    #$f0
                ldb     #$c0
                pshu    y,x,d

                leau    28,u           ;row14
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0ee4
                ldx     #$44ff
                ldd     #$ffff
                pshu    y,x,d
                ldy     #$f04f
                ldx     #$4fff
                lda     -6,u
                anda    #$f0
                ldb     #$40
                pshu    y,x,d

                leau    28,u           ;row15
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f0e4
                ldx     #$444f
                ldd     #$ff4f
                pshu    y,x,d
                ldy     #$ff04
                ldx     #$0444
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row16
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f0e4
                ldx     #$f444
                ldd     #$04f4
                pshu    y,x,d
                ldy     #$44f0
                ldx     #$400f
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row17
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f0e4
                ldx     #$4f44
                ldd     #$4004
                pshu    y,x,d
                ldy     #$0044
                ldx     #$4e00
                lda     -6,u
                anda    #$f0
                ldb     #$4e
                pshu    y,x,d

                leau    28,u           ;row18
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$40e4
                ldx     #$ff44
                ldd     #$404f
                pshu    y,x,d
                ldy     #$44ff
                ldx     #$4e04
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row19
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0e44
                ldx     #$f444
                ldd     #$f0f4
                pshu    y,x,d
                ldy     #$0444
                ldx     #$44e0
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    29,u           ;row20
                ldy     #$4e40
                ldx     #$40ee
                ldd     #$0444
                pshu    y,x,d
                ldy     #$0044
                ldx     #$eee0
                ldb     #$e4
                pshu    y,x,d

                leau    28,u           ;row21
                ldy     #$4e40
                ldx     #$00e4
                ldd     #$4440
                pshu    y,x,d
                ldy     #$e000
                ldx     #$4eee
                ldd     #$0ce4
                pshu    y,x,d

                leau    26,u           ;row22
                lda     #$e4
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4044
                ldx     #$000f
                ldd     #$e04f
                pshu    y,x,d
                ldx     #$44ee
                lda     -4,u
                anda    #$f0
                ldb     #$4e
                pshu    x,d

                leau    26,u           ;row23
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$404e
                ldx     #$ffff
                ldd     #$e04f
                pshu    y,x,d
                ldx     #$e44e
                lda     -4,u
                anda    #$f0
                ldb     #$c4
                pshu    x,d

                leau    27,u           ;row24
                ldy     #$e4c0
                ldx     #$f400
                ldd     #$04ff
                pshu    y,x,d
                ldx     #$444e
                ldd     #$0c4e
                pshu    x,d

                leau    24,u           ;row25
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$400e
                ldx     #$0044
                ldd     #$ee44
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$c4
                std     -2,u

                leau    23,u           ;row26
                ldy     #$4c00
                ldx     #$0000
                ldd     #$eee0
                pshu    y,x,d
                ldx     #$00c4
                stx     -2,u

                leau    21,u           ;row27
                ldy     #$cc00
                ldx     #$4444
                ldd     #$00cc
                pshu    y,x,d

                leau    21,u           ;row28
                ldx     #$0000
                ldb     #$00
                pshu    x,d

                rts

PowerUps.04
                leau    58,u           ;row03
                ldx     #$0000
                ldd     #$0000
                pshu    x,d

                leau    21,u           ;row04
                ldy     #$6600
                ldx     #$cccc
                ldb     #$66
                pshu    y,x,d

                leau    23,u           ;row05
                ldy     #$c600
                ldx     #$eeee
                ldd     #$eeee
                pshu    y,x,d
                ldx     #$006c
                stx     -2,u

                leau    21,u           ;row06
                lda     #$c6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4444
                ldx     #$4444
                ldd     #$4444
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$6c
                std     -2,u

                leau    24,u           ;row07
                ldy     #$4460
                ldx     #$fff4
                ldd     #$ff6f
                pshu    y,x,d
                ldx     #$4fef
                ldd     #$0644
                pshu    x,d

                leau    25,u           ;row08
                lda     #$44
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$fefe
                ldx     #$efff
                ldd     #$fff0
                pshu    y,x,d
                ldx     #$eefe
                lda     -4,u
                anda    #$f0
                ldb     #$44
                pshu    x,d

                leau    28,u           ;row09
                ldy     #$fe40
                ldx     #$efee
                ldd     #$00ff
                pshu    y,x,d
                ldy     #$ff06
                ldx     #$efef
                ldd     #$04fe
                pshu    y,x,d

                leau    28,u           ;row10
                ldy     #$ee40
                ldx     #$feef
                ldd     #$440e
                pshu    y,x,d
                ldy     #$f066
                ldx     #$feff
                ldd     #$04ee
                pshu    y,x,d

                leau    28,u           ;row11
                ldy     #$e640
                ldx     #$eefe
                ldd     #$4460
                pshu    y,x,d
                ldy     #$0e66
                ldx     #$efff
                ldd     #$04ef
                pshu    y,x,d

                leau    27,u           ;row12
                lda     #$46
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ee6e
                ldx     #$0eef
                ldd     #$6644
                pshu    y,x,d
                ldy     #$f06e
                ldx     #$feff
                lda     -6,u
                anda    #$f0
                ldb     #$64
                pshu    y,x,d

                leau    28,u           ;row13
                lda     #$46
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$e6ee
                ldx     #$eefe
                ldd     #$6640
                pshu    y,x,d
                ldy     #$0e6e
                ldx     #$efff
                lda     -6,u
                anda    #$f0
                ldb     #$64
                pshu    y,x,d

                leau    28,u           ;row14
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6eef
                ldx     #$0fee
                ldd     #$6644
                pshu    y,x,d
                ldy     #$ee6e
                ldx     #$eff0
                lda     -6,u
                anda    #$f0
                ldb     #$c4
                pshu    y,x,d

                leau    28,u           ;row15
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$eefe
                ldx     #$40e6
                ldd     #$6444
                pshu    y,x,d
                ldy     #$0e6e
                ldx     #$efe0
                lda     -6,u
                anda    #$f0
                ldb     #$c4
                pshu    y,x,d

                leau    28,u           ;row16
                lda     #$4c
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$efe4
                ldx     #$460e
                ldd     #$0044
                pshu    y,x,d
                ldy     #$f06e
                ldx     #$4eff
                lda     -6,u
                anda    #$f0
                ldb     #$c4
                pshu    y,x,d

                leau    28,u           ;row17
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$fee4
                ldx     #$4660
                ldd     #$ee04
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$4eff
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row18
                lda     #$e6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ee64
                ldx     #$4660
                ldd     #$ef60
                pshu    y,x,d
                ldy     #$fefe
                ldx     #$4eff
                lda     -6,u
                anda    #$f0
                ldb     #$6e
                pshu    y,x,d

                leau    28,u           ;row19
                lda     #$e6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$e644
                ldx     #$000e
                ldd     #$fe66
                pshu    y,x,d
                ldy     #$efee
                ldx     #$44ef
                lda     -6,u
                anda    #$f0
                ldb     #$6e
                pshu    y,x,d

                leau    29,u           ;row20
                ldy     #$4ec0
                ldx     #$ee6e
                ldd     #$6eef
                pshu    y,x,d
                ldy     #$efee
                ldx     #$eefe
                ldd     #$0ce4
                pshu    y,x,d

                leau    28,u           ;row21
                ldy     #$4ec0
                ldx     #$e6e4
                ldd     #$6efe
                pshu    y,x,d
                ldy     #$fee6
                ldx     #$4eee
                ldd     #$06e4
                pshu    y,x,d

                leau    26,u           ;row22
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$6e44
                ldx     #$6fee
                ldd     #$ee6e
                pshu    y,x,d
                ldx     #$44ef
                lda     -4,u
                anda    #$f0
                ldb     #$ce
                pshu    x,d

                leau    26,u           ;row23
                lda     #$c6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$e44e
                ldx     #$eee6
                ldd     #$e6ee
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                ldb     #$6c
                pshu    y,d

                leau    27,u           ;row24
                ldy     #$ec60
                ldx     #$6e44
                ldd     #$ef6e
                pshu    y,x,d
                ldx     #$446e
                ldd     #$06ce
                pshu    x,d

                leau    24,u           ;row25
                lda     #$c6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$e44e
                ldx     #$fe66
                ldd     #$e44e
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$6c
                std     -2,u

                leau    23,u           ;row26
                ldy     #$c600
                ldx     #$644e
                ldd     #$e44e
                pshu    y,x,d
                ldx     #$006c
                stx     -2,u

                leau    21,u           ;row27
                ldy     #$6600
                ldx     #$c44c
                ldd     #$0066
                pshu    y,x,d

                leau    21,u           ;row28
                ldx     #$0000
                ldb     #$00
                pshu    x,d

                rts

PowerUps.05
                leau    107,u          ;row06
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0000
                ldx     #$0000
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    24,u           ;row07
                ldy     #$9990
                ldx     #$9999
                ldd     #$9999
                pshu    y,x,d
                lda     #$09
                pshu    x,d

                leau    25,u           ;row08
                lda     #$f9
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$fffb
                ldx     #$f99b
                ldd     #$9f9b
                pshu    y,x,d
                ldx     #$9bf9
                lda     -4,u
                anda    #$f0
                ldb     #$9f
                pshu    x,d

                leau    28,u           ;row09
                ldy     #$9f90
                ldx     #$bfff
                ldd     #$bf99
                pshu    y,x,d
                ldy     #$99ff
                ldx     #$f999
                ldd     #$0995
                pshu    y,x,d

                leau    27,u           ;row10
                lda     #$b9
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$bf59
                ldx     #$f99f
                ldd     #$bf99
                pshu    y,x,d
                ldy     #$99bf
                ldx     #$5d59
                lda     -6,u
                anda    #$f0
                pshu    y,x,d

                leau    30,u           ;row11
                ldy     #$9f90
                ldx     #$9bfb
                lda     #$99
                pshu    y,x,d
                ldy     #$9999
                ldx     #$9999
                lda     #$95
                pshu    y,x,d
                ldx     #$0999
                stx     -2,u

                leau    28,u           ;row12
                ldy     #$f990
                ldx     #$bf9b
                lda     #$99
                pshu    y,x,d
                ldy     #$bb99
                ldx     #$99bb
                lda     #$ff
                pshu    y,x,d
                ldx     #$099b
                stx     -2,u

                leau    26,u           ;row13
                lda     #$b9
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f99f
                ldx     #$999b
                ldd     #$9bff
                pshu    y,x,d
                ldy     #$9bf9
                ldx     #$bfff
                lda     -6,u
                anda    #$f0
                ldb     #$99
                pshu    y,x,d

                leau    29,u           ;row14
                ldy     #$9f90
                ldx     #$9bf9
                lda     #$f9
                pshu    y,x,d
                ldy     #$fd9f
                ldx     #$bf9b
                ldd     #$099f
                pshu    y,x,d

                leau    26,u           ;row15
                lda     #$f9
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$fb9f
                ldx     #$bbf9
                ldd     #$bf9b
                pshu    y,x,d
                ldx     #$fbf9
                lda     -4,u
                anda    #$f0
                ldb     #$99
                pshu    x,d

                leau    27,u           ;row16
                ldy     #$9f90
                ldx     #$99bf
                ldd     #$9bff
                pshu    y,x,d
                ldx     #$f9bb
                ldd     #$09bb
                pshu    x,d

                leau    24,u           ;row17
                lda     #$f9
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$9fb9
                ldx     #$fbff
                ldd     #$bf9b
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$9f
                std     -2,u

                leau    23,u           ;row18
                ldy     #$9990
                ldx     #$f99b
                ldd     #$9b9b
                pshu    y,x,d
                ldx     #$09bf
                stx     -2,u

                leau    20,u           ;row19
                lda     #$99
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f9ff
                ldx     #$f9bf
                lda     -6,u
                anda    #$f0
                ldb     #$9b
                pshu    y,x,d

                leau    23,u           ;row20
                ldy     #$b990
                ldx     #$fbf9
                ldd     #$09b9
                pshu    y,x,d

                leau    20,u           ;row21
                lda     #$f9
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$9fbf
                lda     -4,u
                anda    #$f0
                ldb     #$9b
                pshu    x,d

                leau    21,u           ;row22
                ldx     #$9f90
                ldd     #$099f
                pshu    x,d

                leau    18,u           ;row23
                lda     #$f9
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$99
                std     -2,u

                leau    15,u           ;row24
                ldx     #$0990
                stx     ,u

                leau    16,u           ;row25
                ldd     ,u
                anda    #$f0
                andb    #$0f
                std     ,u

                rts

PowerUps.06
                leau    58,u           ;row03
                ldx     #$0000
                ldd     #$0000
                pshu    x,d

                leau    21,u           ;row04
                ldy     #$6600
                ldx     #$cccc
                ldb     #$66
                pshu    y,x,d

                leau    23,u           ;row05
                ldy     #$c600
                ldx     #$eeee
                ldd     #$eeee
                pshu    y,x,d
                ldx     #$006c
                stx     -2,u

                leau    21,u           ;row06
                lda     #$06
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$4cee
                ldx     #$4444
                ldd     #$eec4
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$6c
                std     -2,u

                leau    24,u           ;row07
                ldy     #$4060
                ldx     #$e440
                ldd     #$eeee
                pshu    y,x,d
                ldx     #$c44e
                ldd     #$06ce
                pshu    x,d

                leau    25,u           ;row08
                lda     #$c6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0460
                ldx     #$eeee
                ldd     #$eeee
                pshu    y,x,d
                ldx     #$e44e
                lda     -4,u
                anda    #$f0
                ldb     #$6c
                pshu    x,d

                leau    26,u           ;row09
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$c640
                ldx     #$eee0
                ldd     #$eeee
                pshu    y,x,d
                ldx     #$44ee
                lda     -4,u
                anda    #$f0
                ldb     #$ce
                pshu    x,d

                leau    28,u           ;row10
                ldy     #$ce60
                ldx     #$6404
                ldd     #$eee0
                pshu    y,x,d
                ldy     #$eeee
                ldx     #$4eee
                ldd     #$06ec
                pshu    y,x,d

                leau    28,u           ;row11
                ldy     #$4ec0
                ldx     #$440e
                ldd     #$ee06
                pshu    y,x,d
                ldy     #$eeee
                ldx     #$eeee
                ldd     #$0ce4
                pshu    y,x,d

                leau    27,u           ;row12
                lda     #$e6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ee4c
                ldx     #$0440
                ldd     #$eeee
                pshu    y,x,d
                ldy     #$eeee
                ldx     #$c4ee
                lda     -6,u
                anda    #$f0
                ldb     #$6e
                pshu    y,x,d

                leau    28,u           ;row13
                lda     #$e6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0ee4
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$4ee0
                lda     -6,u
                anda    #$f0
                ldb     #$6e
                pshu    y,x,d

                leau    28,u           ;row14
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$c0e4
                ldx     #$cccc
                ldd     #$cecc
                pshu    y,x,d
                ldy     #$fccc
                ldx     #$4e0c
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row15
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$c0e4
                ldx     #$c66c
                ldd     #$cfcc
                pshu    y,x,d
                ldy     #$c600
                ldx     #$4e0c
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row16
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$c0e4
                ldx     #$6606
                ldd     #$6ccc
                pshu    y,x,d
                ldy     #$00e0
                ldx     #$4e0c
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row17
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0ee4
                ldx     #$60e0
                ldd     #$0ccc
                pshu    y,x,d
                ldy     #$6efe
                ldx     #$4ee0
                lda     -6,u
                anda    #$f0
                ldb     #$ce
                pshu    y,x,d

                leau    28,u           ;row18
                lda     #$e6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0ee4
                ldx     #$0ee6
                ldd     #$f0c6
                pshu    y,x,d
                ldy     #$6efe
                ldx     #$4ee0
                lda     -6,u
                anda    #$f0
                ldb     #$6e
                pshu    y,x,d

                leau    28,u           ;row19
                lda     #$e6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0e4c
                ldx     #$0fe6
                ldd     #$f0c6
                pshu    y,x,d
                ldy     #$6efe
                ldx     #$c4e0
                lda     -6,u
                anda    #$f0
                ldb     #$6e
                pshu    y,x,d

                leau    29,u           ;row20
                ldy     #$4ec0
                ldx     #$e60e
                ldd     #$c60f
                pshu    y,x,d
                ldy     #$fef0
                ldx     #$e06e
                ldd     #$0ce4
                pshu    y,x,d

                leau    28,u           ;row21
                ldy     #$cec0
                ldx     #$e604
                ldd     #$c60f
                pshu    y,x,d
                ldy     #$fef0
                ldx     #$406e
                ldd     #$06ec
                pshu    y,x,d

                leau    26,u           ;row22
                lda     #$ec
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$e604
                ldx     #$cc0f
                ldd     #$fef0
                pshu    y,x,d
                ldx     #$406e
                lda     -4,u
                anda    #$f0
                ldb     #$ce
                pshu    x,d

                leau    26,u           ;row23
                lda     #$c6
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$e60e
                ldx     #$cc0f
                ldd     #$fef0
                pshu    y,x,d
                ldx     #$e06e
                lda     -4,u
                anda    #$f0
                ldb     #$6c
                pshu    x,d

                leau    27,u           ;row24
                ldy     #$0c60
                ldx     #$60fe
                ldd     #$0cfc
                pshu    y,x,d
                ldx     #$eff0
                ldd     #$06c0
                pshu    x,d

                leau    24,u           ;row25
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$c60e
                ldx     #$cccc
                ldd     #$e0cc
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$60
                std     -2,u

                leau    23,u           ;row26
                ldy     #$0000
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                sty     -2,u

                leau    21,u           ;row27
                ldy     #$6600
                ldx     #$cccc
                ldb     #$66
                pshu    y,x,d

                leau    21,u           ;row28
                ldx     #$0000
                ldb     #$00
                pshu    x,d

                rts
PowerUps.End

    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
                fcb     PHYSICAL_01C000_01DFFF
    
    org     $4000
Baddies.00
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$0000
                ldx     #$0000
                lda     #$00
                pshu    y,x,d

                leau    261,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    264,u           ;row07
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    261,u           ;row08
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    264,u           ;row09
                lda     #$00
                pshu    y,x,d
                ldx     #$7770
                pshu    x,d

                leau    266,u           ;row10
                ldx     #$0000
                pshu    y,x,d
                ldx     #$ff77
                ldb     #$07
                pshu    x,d

                leau    265,u           ;row11
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$f770
                ldd     #$ff7f
                pshu    y,x,d
                ldx     #$07ff
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    266,u           ;row12
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$7ff7
                ldd     #$fff7
                pshu    y,x,d
                ldx     #$077f
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    266,u           ;row13
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7000
                ldx     #$00ff
                ldd     #$ff00
                pshu    y,x,d
                ldx     #$007f
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    268,u           ;row14
                ldy     #$0000
                ldx     #$f000
                ldd     #$f07f
                pshu    y,x,d
                ldy     #$f70f
                ldx     #$000f
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row15
                ldy     #$0000
                ldx     #$f700
                ldd     #$f07f
                pshu    y,x,d
                ldy     #$f70f
                ldx     #$007f
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row16
                ldy     #$0000
                ldx     #$f700
                ldb     #$ff
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$007f
                ldb     #$00
                pshu    y,x,d

                leau    268,u           ;row17
                ldy     #$0000
                ldx     #$f700
                ldd     #$f07f
                pshu    y,x,d
                ldy     #$f70f
                ldx     #$007f
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row18
                ldy     #$0000
                ldx     #$f000
                ldd     #$f07f
                pshu    y,x,d
                ldy     #$f70f
                ldx     #$000f
                ldd     #$0000
                pshu    y,x,d

                leau    266,u           ;row19
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7000
                ldx     #$00ff
                ldd     #$ff00
                pshu    y,x,d
                ldx     #$0007
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0000
                ldx     #$7ff7
                ldd     #$7ff7
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    y,d

                leau    267,u           ;row21
                ldx     #$7000
                ldd     #$7ff7
                pshu    y,x,d
                ldx     #$0007
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row22
                ldx     #$0000
                pshu    y,x,d
                pshu    y,x

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    263,u           ;row24
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    260,u           ;row25
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    y,d

                leau    258,u           ;row27
                sty     ,u

                rts

Baddies.01
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$0000
                ldx     #$0000
                lda     #$00
                pshu    y,x,d

                leau    261,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    264,u           ;row07
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    261,u           ;row08
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    264,u           ;row09
                lda     #$00
                pshu    y,x,d
                ldx     #$7770
                pshu    x,d

                leau    266,u           ;row10
                ldx     #$0000
                pshu    y,x,d
                ldx     #$ff77
                ldb     #$07
                pshu    x,d

                leau    265,u           ;row11
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$7700
                pshu    y,x,d
                ldx     #$07ff
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row12
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$7700
                pshu    y,x,d
                ldx     #$0777
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row13
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$7000
                pshu    y,x,d
                ldx     #$0077
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    268,u           ;row14
                ldx     #$0000
                lda     #$00
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row15
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row16
                ldd     #$ff70
                pshu    y,x,d
                ldy     #$07ff
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row17
                ldy     #$0000
                ldd     #$7fff
                pshu    y,x,d
                ldy     #$fff7
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row18
                ldy     #$0000
                ldx     #$f000
                ldb     #$ff
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$000f
                ldb     #$00
                pshu    y,x,d

                leau    266,u           ;row19
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f000
                ldx     #$f07f
                ldd     #$f70f
                pshu    y,x,d
                ldx     #$000f
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    266,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f700
                ldx     #$f07f
                ldd     #$f70f
                pshu    y,x,d
                ldx     #$007f
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    267,u           ;row21
                ldy     #$0000
                ldx     #$fff7
                lda     #$00
                pshu    y,x,d
                ldx     #$7fff
                pshu    x,d

                leau    266,u           ;row22
                ldx     #$7ff7
                ldd     #$0ff0
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7ff0
                ldx     #$0ff0
                ldd     #$0ff7
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    263,u           ;row24
                ldy     #$7000
                ldx     #$00ff
                lda     #$ff
                pshu    y,x,d
                ldx     #$0007
                stx     -2,u

                leau    260,u           ;row25
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7f77
                ldx     #$77f7
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$7777
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    258,u           ;row27
                ldx     #$0000
                stx     ,u

                rts

Baddies.02
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u
                lda     -2,u
                anda    #$f0
                ora     #$07
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$7770
                ldx     #$0770
                ldd     #$0777
                pshu    y,x,d

                leau    261,u           ;row06
                lda     #$70
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$00ff
                ldx     #$ff00
                lda     -6,u
                anda    #$f0
                ldb     #$07
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$0000
                ldx     #$7ff7
                ldd     #$7ff7
                pshu    y,x,d
                sty     -2,u

                leau    261,u           ;row08
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7000
                ldb     #$07
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$0000
                lda     #$00
                pshu    y,x,d
                ldx     #$7770
                pshu    x,d

                leau    266,u           ;row10
                ldx     #$0000
                pshu    y,x,d
                ldx     #$ff77
                ldb     #$07
                pshu    x,d

                leau    265,u           ;row11
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$7700
                pshu    y,x,d
                ldx     #$07ff
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row12
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$7700
                pshu    y,x,d
                ldx     #$0777
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row13
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$7000
                pshu    y,x,d
                ldx     #$0077
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    268,u           ;row14
                ldx     #$0000
                lda     #$00
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row15
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row16
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row17
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row18
                pshu    y,x,d
                pshu    y,x,d

                leau    266,u           ;row19
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                pshu    y,d

                leau    266,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                pshu    y,d

                leau    267,u           ;row21
                lda     #$00
                pshu    y,x,d
                pshu    y,x

                leau    266,u           ;row22
                pshu    y,x,d
                pshu    y,x

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7000
                ldx     #$7ff7
                ldb     #$07
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    263,u           ;row24
                ldy     #$0000
                ldd     #$7ff7
                pshu    y,x,d
                sty     -2,u

                leau    260,u           ;row25
                lda     #$70
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$00ff
                ldx     #$ff00
                lda     -6,u
                anda    #$f0
                ldb     #$07
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$77
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u
                ldx     #$0770
                lda     -4,u
                anda    #$f0
                ora     #$07
                ldb     #$77
                pshu    x,d

                leau    258,u           ;row27
                ldx     #$0000
                stx     ,u

                rts

Baddies.03
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$0000
                ldx     #$7777
                lda     #$00
                pshu    y,x,d

                leau    261,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7f77
                ldx     #$77f7
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    264,u           ;row07
                ldy     #$7000
                ldx     #$00ff
                lda     #$ff
                pshu    y,x,d
                ldx     #$0007
                stx     -2,u

                leau    261,u           ;row08
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7ff0
                ldx     #$0ff0
                ldd     #$0ff7
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    264,u           ;row09
                ldy     #$0000
                ldx     #$7ff7
                ldd     #$0ff0
                pshu    y,x,d
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row10
                ldx     #$fff7
                pshu    y,x,d
                ldx     #$ffff
                ldb     #$07
                pshu    x,d

                leau    265,u           ;row11
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f700
                ldx     #$f07f
                ldd     #$f70f
                pshu    y,x,d
                ldx     #$07ff
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    266,u           ;row12
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$f000
                ldx     #$f07f
                ldd     #$f70f
                pshu    y,x,d
                ldx     #$07ff
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    266,u           ;row13
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$00ff
                ldd     #$ff00
                pshu    y,x,d
                ldx     #$007f
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    268,u           ;row14
                ldy     #$0000
                ldx     #$0000
                ldd     #$7fff
                pshu    y,x,d
                ldy     #$fff7
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row15
                ldy     #$0000
                ldd     #$ff70
                pshu    y,x,d
                ldy     #$07ff
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row16
                ldy     #$0000
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row17
                pshu    y,x,d
                pshu    y,x,d

                leau    268,u           ;row18
                pshu    y,x,d
                pshu    y,x,d

                leau    266,u           ;row19
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                pshu    y,d

                leau    266,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                pshu    y,d

                leau    267,u           ;row21
                lda     #$00
                pshu    y,x,d
                pshu    y,x

                leau    266,u           ;row22
                pshu    y,x,d
                pshu    y,x

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    263,u           ;row24
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    260,u           ;row25
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    y,d

                leau    258,u           ;row27
                sty     ,u

                rts

Baddies.04
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$0000
                ldx     #$0000
                lda     #$00
                pshu    y,x,d

                leau    261,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    264,u           ;row07
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    261,u           ;row08
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    264,u           ;row09
                lda     #$00
                pshu    y,x,d
                ldx     #$7770
                pshu    x,d

                leau    266,u           ;row10
                ldx     #$0000
                pshu    y,x,d
                ldx     #$ff77
                ldb     #$07
                pshu    x,d

                leau    265,u           ;row11
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$f770
                ldd     #$ff7f
                pshu    y,x,d
                ldx     #$07ff
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    266,u           ;row12
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$7ff7
                ldd     #$fff7
                pshu    y,x,d
                ldx     #$077f
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    x,d

                leau    266,u           ;row13
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7000
                ldx     #$00ff
                ldd     #$ff00
                pshu    y,x,d
                ldx     #$007f
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    268,u           ;row14
                ldy     #$0000
                ldx     #$f000
                ldd     #$f07f
                pshu    y,x,d
                ldy     #$f70f
                ldx     #$000f
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row15
                ldy     #$0000
                ldx     #$f700
                ldd     #$f07f
                pshu    y,x,d
                ldy     #$f70f
                ldx     #$007f
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row16
                ldy     #$0000
                ldx     #$f700
                ldb     #$ff
                pshu    y,x,d
                ldy     #$ff00
                ldx     #$007f
                ldb     #$00
                pshu    y,x,d

                leau    268,u           ;row17
                ldy     #$0000
                ldx     #$f700
                ldd     #$f07f
                pshu    y,x,d
                ldy     #$f70f
                ldx     #$007f
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row18
                ldy     #$0000
                ldx     #$f000
                ldd     #$f07f
                pshu    y,x,d
                ldy     #$f70f
                ldx     #$000f
                ldd     #$0000
                pshu    y,x,d

                leau    266,u           ;row19
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$7000
                ldx     #$00ff
                ldd     #$ff00
                pshu    y,x,d
                ldx     #$0007
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0000
                ldx     #$7ff7
                ldd     #$7ff7
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    y,d

                leau    267,u           ;row21
                ldx     #$7000
                ldd     #$7ff7
                pshu    y,x,d
                ldx     #$0007
                ldd     #$0000
                pshu    x,d

                leau    266,u           ;row22
                ldx     #$0000
                pshu    y,x,d
                pshu    y,x

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    263,u           ;row24
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    260,u           ;row25
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    y,d

                leau    258,u           ;row27
                sty     ,u

                rts

Baddies.05
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$0000
                ldx     #$0000
                lda     #$00
                pshu    y,x,d

                leau    261,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    264,u           ;row07
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    261,u           ;row08
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    264,u           ;row09
                lda     #$00
                pshu    y,x,d
                ldx     #$7770
                pshu    x,d

                leau    266,u           ;row10
                ldx     #$0000
                pshu    y,x,d
                ldx     #$ff77
                ldb     #$07
                pshu    x,d

                leau    265,u           ;row11
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$f700
                pshu    y,x,d
                ldx     #$0fff
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row12
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$ff70
                pshu    y,x,d
                ldx     #$ffff
                lda     -4,u
                anda    #$f0
                ldb     #$07
                pshu    x,d

                leau    266,u           ;row13
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$0ff7
                pshu    y,x,d
                ldx     #$f777
                lda     -4,u
                anda    #$f0
                ldb     #$07
                pshu    x,d

                leau    268,u           ;row14
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldy     #$07ff
                ldx     #$70ff
                ldb     #$7f
                pshu    y,x,d

                leau    268,u           ;row15
                ldy     #$0000
                ldx     #$0000
                ldd     #$7000
                pshu    y,x,d
                ldy     #$07ff
                ldx     #$70ff
                ldd     #$007f
                pshu    y,x,d

                leau    268,u           ;row16
                ldy     #$0000
                ldx     #$0000
                ldd     #$7000
                pshu    y,x,d
                ldy     #$0fff
                ldx     #$f000
                ldd     #$007f
                pshu    y,x,d

                leau    268,u           ;row17
                ldy     #$0000
                ldx     #$0000
                ldd     #$7000
                pshu    y,x,d
                ldy     #$07ff
                ldx     #$70ff
                ldd     #$007f
                pshu    y,x,d

                leau    268,u           ;row18
                ldy     #$0000
                ldx     #$0000
                ldb     #$00
                pshu    y,x,d
                ldy     #$07ff
                ldx     #$70ff
                ldb     #$7f
                pshu    y,x,d

                leau    266,u           ;row19
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0000
                ldx     #$0000
                ldd     #$0ff7
                pshu    y,x,d
                ldx     #$f000
                lda     -4,u
                anda    #$f0
                ldb     #$07
                pshu    x,d

                leau    266,u           ;row20
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$ff70
                pshu    y,x,d
                ldx     #$ff77
                lda     -4,u
                anda    #$f0
                ldb     #$07
                pshu    x,d

                leau    267,u           ;row21
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$ff77
                ldb     #$77
                pshu    x,d

                leau    266,u           ;row22
                ldx     #$0000
                ldb     #$00
                pshu    y,x,d
                pshu    y,x

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    263,u           ;row24
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    260,u           ;row25
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    y,d

                leau    258,u           ;row27
                sty     ,u

                rts

Baddies.06
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$0000
                ldx     #$0000
                lda     #$00
                pshu    y,x,d

                leau    261,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    264,u           ;row07
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    261,u           ;row08
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    264,u           ;row09
                lda     #$00
                pshu    y,x,d
                ldx     #$7770
                pshu    x,d

                leau    266,u           ;row10
                ldx     #$0000
                pshu    y,x,d
                ldx     #$ff77
                ldb     #$07
                pshu    x,d

                leau    265,u           ;row11
                lda     #$07
                ldb     1,u
                andb    #$0f
                std     ,u
                ldx     #$0000
                ldd     #$7700
                pshu    y,x,d
                ldx     #$07ff
                lda     -4,u
                anda    #$f0
                ldb     #$70
                pshu    x,d

                leau    266,u           ;row12
                lda     #$7f
                ldb     1,u
                andb    #$0f
                orb     #$70
                std     ,u
                ldx     #$0000
                ldd     #$7700
                pshu    y,x,d
                ldx     #$0777
                lda     -4,u
                anda    #$f0
                ora     #$07
                ldb     #$f7
                pshu    x,d

                leau    266,u           ;row13
                lda     #$f0
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0007
                ldx     #$0000
                ldd     #$7000
                pshu    y,x,d
                ldx     #$7077
                lda     -4,u
                anda    #$f0
                ldb     #$0f
                pshu    x,d

                leau    268,u           ;row14
                ldy     #$7070
                ldx     #$000f
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$f000
                ldd     #$0707
                pshu    y,x,d

                leau    268,u           ;row15
                ldy     #$7070
                ldx     #$007f
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$f700
                ldd     #$0707
                pshu    y,x,d

                leau    268,u           ;row16
                ldy     #$f000
                ldx     #$007f
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$f700
                ldb     #$0f
                pshu    y,x,d

                leau    268,u           ;row17
                ldy     #$7070
                ldx     #$007f
                ldb     #$00
                pshu    y,x,d
                ldy     #$0000
                ldx     #$f700
                ldd     #$0707
                pshu    y,x,d

                leau    268,u           ;row18
                ldy     #$7070
                ldx     #$000f
                ldd     #$0000
                pshu    y,x,d
                ldy     #$0000
                ldx     #$f000
                ldd     #$0707
                pshu    y,x,d

                leau    266,u           ;row19
                lda     #$f0
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0007
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                ldx     #$7000
                lda     -4,u
                anda    #$f0
                ldb     #$0f
                pshu    x,d

                leau    266,u           ;row20
                lda     #$77
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$0000
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d
                lda     -4,u
                anda    #$f0
                ldb     #$77
                pshu    y,d

                leau    267,u           ;row21
                ldy     #$0007
                ldd     #$0000
                pshu    y,x,d
                lda     #$70
                pshu    x,d

                leau    266,u           ;row22
                ldy     #$0000
                lda     #$00
                pshu    y,x,d
                pshu    y,x

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    263,u           ;row24
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    260,u           ;row25
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    y,d

                leau    258,u           ;row27
                sty     ,u

                rts

Baddies.07
                leau    1032,u          ;row04
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -2,u
                anda    #$f0
                ldb     #$00
                std     -2,u

                leau    259,u           ;row05
                ldy     #$0000
                ldx     #$0000
                lda     #$00
                pshu    y,x,d

                leau    261,u           ;row06
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    264,u           ;row07
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    261,u           ;row08
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    264,u           ;row09
                lda     #$00
                pshu    y,x,d
                ldx     #$7770
                pshu    x,d

                leau    266,u           ;row10
                ldx     #$0000
                pshu    y,x,d
                ldx     #$ff77
                ldb     #$07
                pshu    x,d

                leau    265,u           ;row11
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$ff77
                ldx     #$0077
                ldd     #$7700
                pshu    y,x,d
                ldx     #$07ff
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row12
                lda     #$70
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$77ff
                ldd     #$7700
                pshu    y,x,d
                ldx     #$0777
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row13
                lda     #$70
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$000f
                ldx     #$7ff0
                ldb     #$00
                pshu    y,x,d
                ldx     #$0077
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    268,u           ;row14
                ldy     #$f700
                ldx     #$ff07
                ldd     #$ff70
                pshu    y,x,d
                ldy     #$0000
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row15
                ldy     #$f700
                ldx     #$ff07
                ldd     #$ff70
                pshu    y,x,d
                ldy     #$0007
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row16
                ldy     #$f700
                ldx     #$000f
                ldd     #$fff0
                pshu    y,x,d
                ldy     #$0007
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row17
                ldy     #$f700
                ldx     #$ff07
                ldd     #$ff70
                pshu    y,x,d
                ldy     #$0007
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d

                leau    268,u           ;row18
                ldy     #$f700
                ldx     #$ff07
                ldd     #$ff70
                pshu    y,x,d
                ldy     #$0000
                ldx     #$0000
                ldd     #$0000
                pshu    y,x,d

                leau    266,u           ;row19
                lda     #$70
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$000f
                ldx     #$7ff0
                ldd     #$0000
                pshu    y,x,d
                ldx     #$0000
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    266,u           ;row20
                lda     #$70
                ldb     1,u
                andb    #$0f
                std     ,u
                ldy     #$77ff
                ldx     #$07ff
                ldd     #$0000
                pshu    y,x,d
                ldx     #$0000
                lda     -4,u
                anda    #$f0
                pshu    x,d

                leau    267,u           ;row21
                ldy     #$7700
                ldx     #$77ff
                lda     #$00
                pshu    y,x,d
                ldx     #$0000
                pshu    x,d

                leau    266,u           ;row22
                ldy     #$0000
                pshu    y,x,d
                pshu    y,x

                leau    264,u           ;row23
                ldb     1,u
                andb    #$0f
                std     ,u
                ldb     #$00
                pshu    y,x,d
                lda     -2,u
                anda    #$f0
                std     -2,u

                leau    263,u           ;row24
                lda     #$00
                pshu    y,x,d
                sty     -2,u

                leau    260,u           ;row25
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -6,u
                anda    #$f0
                ldb     #$00
                pshu    y,x,d

                leau    261,u           ;row26
                lda     #$00
                ldb     1,u
                andb    #$0f
                std     ,u
                lda     -4,u
                anda    #$f0
                ldb     #$00
                pshu    y,d

                leau    258,u           ;row27
                sty     ,u

                rts
Baddies.End

    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
                fcb     PHYSICAL_01E000_01FFFF
    
    org     $4000
Chars.00
                leau    3,u             ;row00
                ldx     #$ffa6
                ldb     #$6a
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffa
                ldb     #$af
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$22ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$66aa
                ldb     #$aa
                pshu    x,b

                leau    258,u           ;row04
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row05
                ldb     #$aa
                stb     ,u
                stb     -2,u

                leau    257,u           ;row06
                ldx     #$22aa
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$ffaa
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$aaa2
                ldb     #$2a
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$2226
                ldb     #$62
                pshu    x,b

                leau    257,u           ;row10
                lda     #$66
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$06
                stb     -1,u

                rts

Chars.01
                leau    1,u             ;row00
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$02
                stb     -1,u

                leau    256,u           ;row01
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$f0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$0f
                stb     -1,u

                leau    256,u           ;row02
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$f0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$0f
                stb     -1,u

                leau    256,u           ;row03
                lda     #$aa
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$02
                stb     -1,u

                leau    256,u           ;row04
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$f0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$06
                stb     -1,u

                leau    256,u           ;row05
                lda     #$aa
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u

                leau    256,u           ;row06
                lda     #$aa
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u

                leau    256,u           ;row07
                lda     #$aa
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u

                leau    256,u           ;row08
                lda     #$aa
                ldb     1,u
                andb    #$0f
                orb     #$a0
                std     ,u

                leau    256,u           ;row09
                lda     #$22
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u

                leau    256,u           ;row10
                lda     #$66
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u

                rts

Chars.02
                leau    3,u             ;row00
                ldx     #$ffa6
                ldb     #$6a
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffa
                ldb     #$af
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$22ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$62aa
                ldb     #$66
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$2f
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$aaa6
                ldb     #$aa
                pshu    x,b

                leau    257,u           ;row06
                lda     #$26
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     #$aa
                stb     -1,u

                leau    258,u           ;row07
                ldx     #$aaaa
                pshu    x,b

                leau    259,u           ;row08
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$2222
                ldb     #$22
                pshu    x,b

                leau    259,u           ;row10
                ldx     #$6666
                ldb     #$66
                pshu    x,b

                rts

Chars.03
                leau    3,u             ;row00
                ldx     #$ffa6
                ldb     #$6a
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffa
                ldb     #$af
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$22ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$2aaa
                ldb     #$66
                pshu    x,b

                leau    257,u           ;row04
                ldx     #$aff2
                stx     ,u

                leau    256,u           ;row05
                ldx     #$2aaa
                stx     ,u

                leau    258,u           ;row06
                ldx     #$66aa
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$aaaa
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$aaa2
                ldb     #$2a
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$2226
                ldb     #$62
                pshu    x,b

                leau    257,u           ;row10
                lda     #$66
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$06
                stb     -1,u

                rts

Chars.04
                ldb     #$ff
                stb     ,u

                leau    256,u           ;row01
                stb     ,u

                leau    257,u           ;row02
                ldd     ,u
                anda    #$f0
                ora     #$0f
                andb    #$0f
                orb     #$f0
                std     ,u
                ldb     #$ff
                stb     -1,u

                leau    256,u           ;row03
                ldd     ,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                std     ,u
                ldb     #$aa
                stb     -1,u

                leau    256,u           ;row04
                ldd     ,u
                anda    #$f0
                ora     #$0f
                andb    #$0f
                orb     #$f0
                std     ,u
                ldb     #$ff
                stb     -1,u

                leau    258,u           ;row05
                ldx     #$aaaa
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row06
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$6aa6
                ldb     #$66
                pshu    x,b

                leau    257,u           ;row08
                ldd     ,u
                anda    #$f0
                ora     #$0a
                andb    #$0f
                orb     #$a0
                std     ,u

                leau    256,u           ;row09
                ldd     ,u
                anda    #$f0
                ora     #$02
                andb    #$0f
                orb     #$20
                std     ,u

                leau    256,u           ;row10
                ldd     ,u
                anda    #$f0
                ora     #$06
                andb    #$0f
                orb     #$60
                std     ,u

                rts

Chars.05
                leau    3,u             ;row00
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$6666
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$aaa2
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$22aa
                ldb     #$22
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$66aa
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$aaaa
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$aaa2
                ldb     #$2a
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$2226
                ldb     #$62
                pshu    x,b

                leau    257,u           ;row10
                lda     #$66
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$06
                stb     -1,u

                rts

Chars.06
                leau    3,u             ;row00
                ldx     #$ffa6
                ldb     #$6a
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffa
                ldb     #$af
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$66ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$aa26
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$fff2
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$22aa
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$66aa
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$aaaa
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$aaa2
                ldb     #$2a
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$2226
                ldb     #$62
                pshu    x,b

                leau    257,u           ;row10
                lda     #$66
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$06
                stb     -1,u

                rts

Chars.07
                leau    3,u             ;row00
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$66aa
                ldb     #$66
                pshu    x,b

                leau    257,u           ;row03
                lda     ,u
                anda    #$f0
                ora     #$0a
                ldb     #$a2
                std     ,u

                leau    256,u           ;row04
                ldd     ,u
                anda    #$f0
                ora     #$0f
                andb    #$0f
                orb     #$f0
                std     ,u

                leau    256,u           ;row05
                lda     #$aa
                ldb     1,u
                andb    #$0f
                orb     #$20
                std     ,u

                leau    256,u           ;row06
                sta     ,u

                leau    256,u           ;row07
                sta     ,u

                leau    256,u           ;row08
                sta     ,u

                leau    256,u           ;row09
                ldb     #$22
                stb     ,u

                leau    256,u           ;row10
                ldb     #$66
                stb     ,u

                rts

Chars.08
                leau    3,u             ;row00
                ldx     #$ffa6
                ldb     #$6a
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffa
                ldb     #$af
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$66ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$aaaa
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$fff2
                ldb     #$2f
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$22aa
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$66aa
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$aaaa
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$aaa2
                ldb     #$2a
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$2226
                ldb     #$62
                pshu    x,b

                leau    257,u           ;row10
                lda     #$66
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$06
                stb     -1,u

                rts

Chars.09
                leau    3,u             ;row00
                ldx     #$ffa6
                ldb     #$6a
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffa
                ldb     #$af
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$66ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$aaaa
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$2f
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$22aa
                ldb     #$62
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$66aa
                ldb     #$aa
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$aaaa
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$aaa2
                ldb     #$2a
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$2226
                ldb     #$62
                pshu    x,b

                leau    257,u           ;row10
                lda     #$66
                ldb     1,u
                andb    #$0f
                orb     #$60
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$06
                stb     -1,u

                rts

Chars.10
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$1b
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                ldb     #$bf
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$99ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$11bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$bbbb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$99bb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$11bb
                pshu    x,b

                leau    258,u           ;row08
                ldb     #$99
                stb     ,u
                stb     -2,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u
                stb     -2,u

                rts

Chars.11
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$19ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bbb1
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$99bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$11bb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$bbb9
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9991
                ldb     #$99
                pshu    x,b

                leau    257,u           ;row09
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                sta     -1,u

                rts

Chars.12
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$1b
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                ldb     #$bf
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$99ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$1199
                ldb     #$bb
                pshu    x,b

                leau    258,u           ;row04
                ldb     #$11
                stb     ,u
                ldb     #$ff
                stb     -2,u

                leau    256,u           ;row05
                ldb     #$bb
                stb     ,u
                stb     -2,u

                leau    257,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$bbb9
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9991
                ldb     #$19
                pshu    x,b

                leau    257,u           ;row09
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$01
                stb     -1,u

                rts

Chars.13
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$99ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$11bb
                ldb     #$bb
                pshu    x,b

                leau    258,u           ;row04
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row05
                ldb     #$bb
                stb     ,u
                stb     -2,u

                leau    257,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$bbb9
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9991
                ldb     #$99
                pshu    x,b

                leau    257,u           ;row09
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                sta     -1,u

                rts

Chars.14
                leau    3,u             ;row00
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$1999
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bb11
                ldb     #$bb
                pshu    x,b

                leau    256,u           ;row04
                ldx     #$ffff
                stx     ,u

                leau    256,u           ;row05
                ldx     #$bb19
                stx     ,u

                leau    259,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9999
                ldb     #$99
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$1111
                ldb     #$11
                pshu    x,b

                rts

Chars.15
                leau    3,u             ;row00
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$1999
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bb11
                ldb     #$bb
                pshu    x,b

                leau    256,u           ;row04
                ldx     #$ffff
                stx     ,u

                leau    256,u           ;row05
                ldx     #$bb99
                stx     ,u

                leau    256,u           ;row06
                ldx     #$bb11
                stx     ,u

                leau    256,u           ;row07
                stb     ,u

                leau    256,u           ;row08
                ldb     #$99
                stb     ,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u

                rts

Chars.16
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$1b
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                ldb     #$bf
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$99ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$1199
                ldb     #$bb
                pshu    x,b

                leau    257,u           ;row04
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$ff
                std     ,u
                stb     -1,u

                leau    256,u           ;row05
                lda     ,u
                anda    #$f0
                ora     #$01
                ldb     #$bb
                std     ,u
                stb     -1,u

                leau    258,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$bbb9
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9991
                ldb     #$19
                pshu    x,b

                leau    257,u           ;row09
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$01
                stb     -1,u

                rts

Chars.17
                leau    2,u             ;row00
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row01
                stb     ,u
                stb     -2,u

                leau    256,u           ;row02
                stb     ,u
                stb     -2,u

                leau    257,u           ;row03
                ldx     #$bbbb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$99bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$11bb
                pshu    x,b

                leau    258,u           ;row07
                stb     ,u
                stb     -2,u

                leau    256,u           ;row08
                ldb     #$99
                stb     ,u
                stb     -2,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u
                stb     -2,u

                rts

Chars.18
                leau    3,u             ;row00
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$ff99
                ldb     #$99
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bb11
                ldb     #$11
                pshu    x,b

                leau    257,u           ;row04
                ldb     #$ff
                stb     ,u

                leau    256,u           ;row05
                ldb     #$bb
                stb     ,u

                leau    258,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9999
                ldb     #$99
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$1111
                ldb     #$11
                pshu    x,b

                rts

Chars.19
                leau    2,u             ;row00
                ldb     #$ff
                stb     ,u

                leau    256,u           ;row01
                stb     ,u

                leau    256,u           ;row02
                stb     ,u

                leau    256,u           ;row03
                ldb     #$bb
                stb     ,u

                leau    256,u           ;row04
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row05
                ldb     #$bb
                stb     ,u
                stb     -2,u

                leau    257,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$bbb9
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9991
                ldb     #$19
                pshu    x,b

                leau    257,u           ;row09
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$01
                stb     -1,u

                rts

Chars.20
                leau    2,u             ;row00
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    255,u           ;row01
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$ff
                std     ,u
                stb     -1,u

                leau    258,u           ;row02
                ldx     #$fff9
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bb91
                ldb     #$bb
                pshu    x,b

                leau    257,u           ;row04
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$f0
                std     ,u
                sta     -1,u

                leau    256,u           ;row05
                lda     #$9b
                ldb     1,u
                andb    #$0f
                orb     #$b0
                std     ,u
                ldb     #$bb
                stb     -1,u

                leau    258,u           ;row06
                ldx     #$19bb
                pshu    x,b

                leau    257,u           ;row07
                lda     ,u
                anda    #$f0
                ora     #$01
                ldb     #$bb
                std     ,u
                stb     -1,u

                leau    257,u           ;row08
                ldb     #$99
                stb     ,u
                stb     -2,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u
                stb     -2,u

                rts

Chars.21
                ldb     #$ff
                stb     ,u

                leau    256,u           ;row01
                stb     ,u

                leau    256,u           ;row02
                stb     ,u

                leau    256,u           ;row03
                ldb     #$bb
                stb     ,u

                leau    256,u           ;row04
                ldb     #$ff
                stb     ,u

                leau    256,u           ;row05
                ldb     #$bb
                stb     ,u

                leau    259,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9999
                ldb     #$99
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$1111
                ldb     #$11
                pshu    x,b

                rts

Chars.22
                leau    3,u             ;row00
                ldx     #$11bf
                ldb     #$fb
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row02
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bbbb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$99bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$11bb
                pshu    x,b

                leau    258,u           ;row07
                stb     ,u
                stb     -2,u

                leau    256,u           ;row08
                ldb     #$99
                stb     ,u
                stb     -2,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u
                stb     -2,u

                rts

Chars.23
                leau    1,u             ;row00
                lda     ,u
                anda    #$0f
                ora     #$10
                ldb     #$ff
                std     ,u
                stb     -1,u

                leau    258,u           ;row01
                ldx     #$f1ff
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$ffff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bbbb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$9bbb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$19bb
                pshu    x,b

                leau    257,u           ;row07
                lda     ,u
                anda    #$f0
                ora     #$01
                ldb     #$bb
                std     ,u
                stb     -1,u

                leau    257,u           ;row08
                ldb     #$99
                stb     ,u
                stb     -2,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u
                stb     -2,u

                rts

Chars.24
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$1b
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                ldb     #$bf
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$99ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$11bb
                ldb     #$bb
                pshu    x,b

                leau    258,u           ;row04
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row05
                ldb     #$bb
                stb     ,u
                stb     -2,u

                leau    257,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$bbb9
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9991
                ldb     #$19
                pshu    x,b

                leau    257,u           ;row09
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$01
                stb     -1,u

                rts

Chars.25
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$99ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$11bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$bbb9
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$9991
                pshu    x,b

                leau    257,u           ;row07
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     #$bb
                stb     -1,u

                leau    255,u           ;row08
                ldb     #$99
                stb     ,u

                leau    256,u           ;row09
                sta     ,u

                rts

Chars.26
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$1b
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                ldb     #$bf
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$99ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$11bb
                ldb     #$bb
                pshu    x,b

                leau    257,u           ;row04
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$ff
                std     ,u
                stb     -1,u

                leau    256,u           ;row05
                lda     ,u
                anda    #$f0
                ora     #$0b
                ldb     #$bb
                std     ,u
                stb     -1,u

                leau    258,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9999
                ldb     #$19
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$1111
                ldb     -3,u
                andb    #$f0
                orb     #$01
                pshu    x,b

                rts

Chars.27
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$99ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$11bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$fff9
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$bbb1
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$99bb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$11bb
                pshu    x,b

                leau    258,u           ;row08
                ldb     #$99
                stb     ,u
                stb     -2,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u
                stb     -2,u

                rts

Chars.28
                leau    3,u             ;row00
                ldx     #$ffb1
                ldb     #$1b
                pshu    x,b

                leau    259,u           ;row01
                ldx     #$fffb
                ldb     #$bf
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$11ff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bbb1
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$fffb
                ldb     #$1b
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$11bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$bbb9
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9991
                ldb     #$19
                pshu    x,b

                leau    257,u           ;row09
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$01
                stb     -1,u

                rts

Chars.29
                leau    3,u             ;row00
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                pshu    x,b

                leau    259,u           ;row02
                ldx     #$ff99
                ldb     #$99
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bb11
                ldb     #$11
                pshu    x,b

                leau    257,u           ;row04
                ldb     #$ff
                stb     ,u

                leau    256,u           ;row05
                ldb     #$bb
                stb     ,u

                leau    256,u           ;row06
                stb     ,u

                leau    256,u           ;row07
                stb     ,u

                leau    256,u           ;row08
                ldb     #$99
                stb     ,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u

                rts

Chars.30
                leau    2,u             ;row00
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row01
                stb     ,u
                stb     -2,u

                leau    256,u           ;row02
                stb     ,u
                stb     -2,u

                leau    256,u           ;row03
                ldb     #$bb
                stb     ,u
                stb     -2,u

                leau    256,u           ;row04
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row05
                ldb     #$bb
                stb     ,u
                stb     -2,u

                leau    257,u           ;row06
                ldx     #$bbbb
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$bbb9
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9991
                ldb     #$19
                pshu    x,b

                leau    257,u           ;row09
                lda     #$11
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$01
                stb     -1,u

                rts

Chars.31
                leau    2,u             ;row00
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row01
                stb     ,u
                stb     -2,u

                leau    256,u           ;row02
                stb     ,u
                stb     -2,u

                leau    257,u           ;row03
                ldx     #$11bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$bbb1
                ldb     #$1b
                pshu    x,b

                leau    257,u           ;row06
                lda     #$bb
                ldb     1,u
                andb    #$0f
                orb     #$b0
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$0b
                stb     -1,u

                leau    256,u           ;row07
                lda     #$bb
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$01
                stb     -1,u

                leau    256,u           ;row08
                ldb     #$99
                stb     ,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u

                rts

Chars.32
                leau    2,u             ;row00
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row01
                stb     ,u
                stb     -2,u

                leau    257,u           ;row02
                ldx     #$11ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$99bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$bbbb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                pshu    x,b

                leau    259,u           ;row07
                ldx     #$99bb
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$1199
                ldb     #$99
                pshu    x,b

                leau    258,u           ;row09
                ldb     #$11
                stb     ,u
                stb     -2,u

                rts

Chars.33
                leau    2,u             ;row00
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row01
                stb     ,u
                stb     -2,u

                leau    257,u           ;row02
                ldx     #$11ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bbb9
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$fff1
                ldb     #$1f
                pshu    x,b

                leau    259,u           ;row05
                ldx     #$99bb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row06
                ldx     #$11bb
                pshu    x,b

                leau    258,u           ;row07
                stb     ,u
                stb     -2,u

                leau    256,u           ;row08
                ldb     #$99
                stb     ,u
                stb     -2,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u
                stb     -2,u

                rts

Chars.34
                leau    2,u             ;row00
                ldb     #$ff
                stb     ,u
                stb     -2,u

                leau    256,u           ;row01
                stb     ,u
                stb     -2,u

                leau    257,u           ;row02
                ldx     #$11ff
                pshu    x,b

                leau    259,u           ;row03
                ldx     #$bbb9
                ldb     #$9b
                pshu    x,b

                leau    259,u           ;row04
                ldx     #$fff1
                ldb     #$1f
                pshu    x,b

                leau    257,u           ;row05
                lda     #$bb
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$01
                stb     -1,u

                leau    256,u           ;row06
                sta     ,u

                leau    256,u           ;row07
                sta     ,u

                leau    256,u           ;row08
                ldb     #$99
                stb     ,u

                leau    256,u           ;row09
                ldb     #$11
                stb     ,u

                rts

Chars.35
                leau    3,u             ;row00
                ldx     #$ffff
                ldb     #$ff
                pshu    x,b

                leau    259,u           ;row01
                pshu    x,b

                leau    257,u           ;row02
                lda     ,u
                anda    #$f0
                ora     #$0f
                ldb     #$ff
                std     ,u

                leau    256,u           ;row03
                ldx     #$bbb1
                stx     ,u

                leau    256,u           ;row04
                lda     #$ff
                ldb     1,u
                andb    #$0f
                orb     #$10
                std     ,u
                ldb     -1,u
                andb    #$f0
                orb     #$0f
                stb     -1,u

                leau    255,u           ;row05
                stx     ,u

                leau    259,u           ;row06
                ldx     #$bbbb
                ldb     #$bb
                pshu    x,b

                leau    259,u           ;row07
                pshu    x,b

                leau    259,u           ;row08
                ldx     #$9999
                ldb     #$99
                pshu    x,b

                leau    259,u           ;row09
                ldx     #$1111
                ldb     #$11
                pshu    x,b

                rts
Chars.End

    org     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_4000_5FFF
                fcb     PHYSICAL_074000_075FFF
Compiled.Sprites.End
