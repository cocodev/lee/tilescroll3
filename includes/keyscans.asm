KEYSCAN_COLUMN_At               equ 0
KEYSCAN_COLUMN_H                equ 0
KEYSCAN_COLUMN_P                equ 0
KEYSCAN_COLUMN_X                equ 0
KEYSCAN_COLUMN_0                equ 0
KEYSCAN_COLUMN_8OpenParen       equ 0
KEYSCAN_COLUMN_Enter            equ 0

KEYSCAN_COLUMN_A                equ 1
KEYSCAN_COLUMN_I                equ 1
KEYSCAN_COLUMN_Q                equ 1
KEYSCAN_COLUMN_Y                equ 1
KEYSCAN_COLUMN_1Exlamation      equ 1
KEYSCAN_COLUMN_9CloseParen      equ 1
KEYSCAN_COLUMN_Clear            equ 1

KEYSCAN_COLUMN_B                equ 2
KEYSCAN_COLUMN_J                equ 2
KEYSCAN_COLUMN_R                equ 2
KEYSCAN_COLUMN_Z                equ 2
KEYSCAN_COLUMN_2DoubleQuote     equ 2
KEYSCAN_COLUMN_ColonAsterisk    equ 2
KEYSCAN_COLUMN_Escape           equ 2

KEYSCAN_COLUMN_C                equ 3
KEYSCAN_COLUMN_K                equ 3
KEYSCAN_COLUMN_S                equ 3
KEYSCAN_COLUMN_UpArrow          equ 3
KEYSCAN_COLUMN_3Hash            equ 3
KEYSCAN_COLUMN_SemiColon        equ 3
KEYSCAN_COLUMN_Alt              equ 3

KEYSCAN_COLUMN_D                equ 4
KEYSCAN_COLUMN_L                equ 4
KEYSCAN_COLUMN_T                equ 4
KEYSCAN_COLUMN_DownArrow        equ 4
KEYSCAN_COLUMN_4                equ 4
KEYSCAN_COLUMN_CommaLessThan    equ 4
KEYSCAN_COLUMN_Control          equ 4

KEYSCAN_COLUMN_E                equ 5
KEYSCAN_COLUMN_M                equ 5
KEYSCAN_COLUMN_U                equ 5
KEYSCAN_COLUMN_LeftArrow        equ 5
KEYSCAN_COLUMN_5Percent         equ 5
KEYSCAN_COLUMN_Minus            equ 5
KEYSCAN_COLUMN_F1               equ 5

KEYSCAN_COLUMN_F                equ 6
KEYSCAN_COLUMN_N                equ 6
KEYSCAN_COLUMN_V                equ 6
KEYSCAN_COLUMN_RightArrow       equ 6
KEYSCAN_COLUMN_6Ampersand       equ 6
KEYSCAN_COLUMN_DotGreaterThan   equ 6
KEYSCAN_COLUMN_F2               equ 6

KEYSCAN_COLUMN_G                equ 7
KEYSCAN_COLUMN_O                equ 7
KEYSCAN_COLUMN_W                equ 7
KEYSCAN_COLUMN_Space            equ 7
KEYSCAN_COLUMN_7Apostrophy      equ 7
KEYSCAN_COLUMN_Slash            equ 7
KEYSCAN_COLUMN_Shifts           equ 7

KEYSCAN_ROW_At                  equ %00000001
KEYSCAN_ROW_A                   equ %00000001
KEYSCAN_ROW_B                   equ %00000001
KEYSCAN_ROW_C                   equ %00000001
KEYSCAN_ROW_D                   equ %00000001
KEYSCAN_ROW_E                   equ %00000001
KEYSCAN_ROW_F                   equ %00000001
KEYSCAN_ROW_G                   equ %00000001

KEYSCAN_ROW_H                   equ %00000010
KEYSCAN_ROW_I                   equ %00000010
KEYSCAN_ROW_J                   equ %00000010
KEYSCAN_ROW_K                   equ %00000010
KEYSCAN_ROW_L                   equ %00000010
KEYSCAN_ROW_M                   equ %00000010
KEYSCAN_ROW_N                   equ %00000010
KEYSCAN_ROW_O                   equ %00000010

KEYSCAN_ROW_P                   equ %00000100
KEYSCAN_ROW_Q                   equ %00000100
KEYSCAN_ROW_R                   equ %00000100
KEYSCAN_ROW_S                   equ %00000100
KEYSCAN_ROW_T                   equ %00000100
KEYSCAN_ROW_U                   equ %00000100
KEYSCAN_ROW_V                   equ %00000100
KEYSCAN_ROW_W                   equ %00000100

KEYSCAN_ROW_X                   equ %00001000
KEYSCAN_ROW_Y                   equ %00001000
KEYSCAN_ROW_Z                   equ %00001000
KEYSCAN_ROW_UpArrow             equ %00001000
KEYSCAN_ROW_DownArrow           equ %00001000
KEYSCAN_ROW_LeftArrow           equ %00001000
KEYSCAN_ROW_RightArrow          equ %00001000
KEYSCAN_ROW_Space               equ %00001000

KEYSCAN_ROW_0                   equ %00010000
KEYSCAN_ROW_1Exclamation        equ %00010000
KEYSCAN_ROW_2DoubleQuote        equ %00010000
KEYSCAN_ROW_3Hash               equ %00010000
KEYSCAN_ROW_4DollarSign         equ %00010000
KEYSCAN_ROW_5Percent            equ %00010000
KEYSCAN_ROW_6Ampersand          equ %00010000
KEYSCAN_ROW_7Apostrophy         equ %00010000

KEYSCAN_ROW_8OpenParen          equ %00100000
KEYSCAN_ROW_9CloseParen         equ %00100000
KEYSCAN_ROW_ColonAsterisk       equ %00100000
KEYSCAN_ROW_SemiColon           equ %00100000
KEYSCAN_ROW_CommaLessThan       equ %00100000
KEYSCAN_ROW_Minus               equ %00100000
KEYSCAN_ROW_DotGreaterThan      equ %00100000
KEYSCAN_ROW_Slash               equ %00100000

KEYSCAN_ROW_Enter               equ %01000000
KEYSCAN_ROW_Clear               equ %01000000
KEYSCAN_ROW_Escape              equ %01000000
KEYSCAN_ROW_Alt                 equ %01000000
KEYSCAN_ROW_Control             equ %01000000
KEYSCAN_ROW_F1                  equ %01000000
KEYSCAN_ROW_F2                  equ %01000000
KEYSCAN_ROW_Shifts              equ %01000000
