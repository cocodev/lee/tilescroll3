palettes                       ;       0       1       2       3       4       5       6       7       8       9       10      11      12      13      14      15
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 0
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%011000,%111000,%000111,%001011,%010111,%011011,%001011,%011011,%110110,%111111 ; Level 1
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 2
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 3
                                fcb %000000,%001000,%010000,%011000,%010000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%010111,%011011,%110110,%111111 ; Level 4
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 5
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%011000,%111000,%000111,%001011,%010111,%011011,%001011,%011011,%110110,%111111 ; Level 6
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 7
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 8
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 9
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%011000,%111000,%000111,%001011,%010111,%011011,%001011,%011011,%110110,%111111 ; Level 10
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 11
                                fcb %000000,%001000,%010000,%011000,%100000,%110110,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%100100,%110110,%111111 ; Level 12
                                fcb %000000,%001000,%010000,%011000,%010000,%011000,%010000,%111000,%000111,%001011,%010111,%011011,%010111,%011011,%110110,%111111 ; Level 13
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%101101,%011011,%110110,%111111 ; Level 14
                                fcb %000000,%001000,%010000,%011000,%100000,%010000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%010111,%110110,%111111 ; Level 15
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 16
                                fcb %000000,%001000,%010000,%001000,%010000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%010111,%011011,%110110,%111111 ; Level 17
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 18
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 19
                                fcb %000000,%001000,%010000,%011000,%010000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%010111,%011011,%110110,%111111 ; Level 20
                                fcb %000000,%001000,%010000,%001000,%001000,%011000,%001011,%111000,%000111,%001011,%010111,%011011,%011000,%011011,%110110,%111111 ; Level 21
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%101101,%011011,%110110,%111111 ; Level 22
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 23
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 24
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 25
                                fcb %000000,%001000,%010000,%011000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%011011,%110110,%111111 ; Level 26
                                fcb %000000,%001000,%010000,%100000,%100000,%011000,%100010,%111000,%000111,%001011,%010111,%011011,%100100,%100100,%110110,%111111 ; Level 27
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%101101,%011011,%110110,%111111 ; Level 28
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%011000,%111000,%000111,%001011,%010111,%011011,%001011,%011011,%110110,%111111 ; Level 29
                                fcb %000000,%001000,%010000,%011000,%000111,%011000,%000111,%111000,%000111,%001011,%010111,%011011,%111000,%011011,%110110,%111111 ; Level 30
                                fcb %000000,%001000,%010000,%011000,%001000,%011000,%101000,%111000,%000111,%001011,%010111,%011011,%101101,%011011,%110110,%111111 ; Level 31

heroStartingMapCoordinates  ;    MapCoordinate
                            ;    ┌─────────── mapX
                            ;    │   ┌─────── mapY
                            ;    │   │     ┌─── Level
                            fcb $02,$02  ; 0
                            fcb $0F,$01  ; 1
                            fcb $0F,$08  ; 2
                            fcb $02,$02  ; 3
                            fcb $0B,$07  ; 4
                            fcb $18,$07  ; 5
                            fcb $01,$0E  ; 6
                            fcb $02,$02  ; 7
                            fcb $0F,$07  ; 8
                            fcb $10,$04  ; 9
                            fcb $10,$0B  ; 10
                            fcb $1E,$09  ; 11
                            fcb $02,$07  ; 12
                            fcb $0A,$08  ; 13
                            fcb $0C,$0D  ; 14
                            fcb $10,$07  ; 15
                            fcb $14,$06  ; 16
                            fcb $04,$01  ; 17
                            fcb $01,$03  ; 18
                            fcb $02,$0E  ; 19
                            fcb $10,$07  ; 20
                            fcb $02,$01  ; 21
                            fcb $1E,$0D  ; 22
                            fcb $15,$07  ; 23
                            fcb $19,$0B  ; 24
                            fcb $13,$08  ; 25
                            fcb $14,$08  ; 26
                            fcb $0B,$0D  ; 27
                            fcb $10,$09  ; 28
                            fcb $02,$0E  ; 29
                            fcb $10,$07  ; 30
                            fcb $10,$05  ; 31

baddiesLevelStartingData    ;     Enemy 0        Enemy 1        Enemy 2        Enemy 3        Enemy 4
                            ;     X    Y  D      X    Y  D      X    Y  D      X    Y  D      X    Y  D    Level  (D = Direction: 0=Left, 1=Right, 2=Up, 3=Down)
                            fcb   1,   7, 2,     6, $0E, 0,   $0B,   5, 0,   $14,   5, 0,   $19,   2, 3  ;   0
                            fcb   5,   1, 1,     5,   3, 1,   $0E, $0E, 0,   $11,   8, 2,   $1E,   8, 3  ;   1
                            fcb   1,   1, 1,     1,   9, 1,   $0F,   4, 2,   $18,   4, 1,   $1E,   9, 3  ;   2
                            fcb   2, $0E, 0,   $0A,   7, 2,   $0B, $0D, 2,   $16,   7, 0,   $16, $0E, 0  ;   3
                            fcb $0D,   7, 0,   $0F, $0A, 3,   $10,   5, 2,   $12,   8, 1,    -1,  -1, 0  ;   4
                            fcb   6,   2, 3,   $0A, $0C, 1,   $0C,   1, 1,   $10,   7, 2,   $1E,   5, 3  ;   5
                            fcb $0D,   8, 0,   $0F,   5, 0,   $10,   7, 0,   $11, $0A, 0,   $13,   7, 0  ;   6
                            fcb  -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;   7
                            fcb $0B,   4, 3,   $0C, $0B, 1,   $12,   3, 0,   $13, $0A, 2,    -1,  -1, 0  ;   8
                            fcb   6,   2, 1,     6, $0D, 0,   $0F, $0B, 0,   $19,   2, 1,   $19, $0D, 0  ;   9
                            fcb   2,   2, 0,     7, $0C, 0,   $0C,   9, 2,   $12, $0E, 2,   $1A,   7, 0  ;  10
                            fcb $1E,   1, 3,   $1E,   2, 3,   $1E,   3, 3,   $1E,   4, 3,   $1E,   5, 3  ;  11
                            fcb $1D,   6, 3,   $1D,   8, 3,   $1D, $0A, 3,   $1D, $0C, 3,   $1D, $0E, 3  ;  12
                            fcb   4,   2, 0,     4,   6, 0,     4, $0A, 0,   $0C,   7, 2,   $0E,   7, 3  ;  13
                            fcb   4,   7, 0,     1, $0B, 0,   $11,   1, 1,   $16, $0E, 0,    -1,  -1, 0  ;  14
                            fcb $1A, $0E, 0,   $1C,   6, 0,   $1C,   9, 0,   $1C,   2, 0,   $1E, $0C, 3  ;  15
                            fcb   2, $0B, 1,     2, $0C, 2,     2, $0D, 2,     3, $0D, 0,     4, $0D, 0  ;  16
                            fcb   2,   1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  17
                            fcb $15,   7, 0,   $17,   7, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  18
                            fcb   1, $0C, 0,   $0B, $0E, 0,   $12,   5, 0,   $17, $0E, 0,   $1A,   1, 0  ;  19
                            fcb  -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  20
                            fcb $0A, $0B, 0,   $0D, $0B, 0,   $10, $0B, 0,   $13, $0B, 0,   $16, $0B, 0  ;  21
                            fcb  -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  22
                            fcb $0E,   7, 0,   $0F,   7, 0,   $10,   7, 0,   $11,   7, 0,   $12,   7, 0  ;  23
                            fcb  -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  24
                            fcb  -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  25
                            fcb   3,   2, 2,     3, $0D, 2,     8,   7, 2,   $0E,   8, 3,   $1E, $0D, 2  ;  26
                            fcb  -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  27
                            fcb   3,   2, 3,   $1E,   2, 3,     3, $0D, 3,   $1E, $0D, 3,   $0F,   6, 1  ;  28
                            fcb  -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  29
                            fcb  -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0,    -1,  -1, 0  ;  30
                            fcb $0F,   2, 1,     6,   3, 2,     2,   6, 2,   $16,   9, 3,     3, $0D, 2  ;  31

levelToFloorTileLookup          fcb     $05     ; 0
                                fcb     $28     ; 1
                                fcb     $05     ; 2
                                fcb     $4D     ; 3
                                fcb     $4F     ; 4
                                fcb     $3F     ; 5
                                fcb     $3C     ; 6
                                fcb     $40     ; 7
                                fcb     $3D     ; 8
                                fcb     $40     ; 9
                                fcb     $47     ; 10
                                fcb     $4D     ; 11
                                fcb     $3F     ; 12
                                fcb     $4B     ; 13
                                fcb     $4C     ; 14
                                fcb     $05     ; 15
                                fcb     $3D     ; 16
                                fcb     $45     ; 17
                                fcb     $3F     ; 18
                                fcb     $40     ; 19
                                fcb     $4C     ; 20
                                fcb     $4B     ; 21
                                fcb     $4F     ; 22
                                fcb     $05     ; 23
                                fcb     $40     ; 24
                                fcb     $45     ; 25
                                fcb     $3C     ; 26
                                fcb     $3F     ; 27
                                fcb     $05     ; 28
                                fcb     $28     ; 29
                                fcb     $45     ; 30
                                fcb     $3D     ; 31

levelBcdGemCounts           ;    ┌─────────── BCD Gem Count
                            ;    │        ┌─────── Level
                            fcb $10     ; 0
                            fcb $12     ; 1
                            fcb $10     ; 2
                            fcb $07     ; 3
                            fcb $08     ; 4
                            fcb $01     ; 5
                            fcb $81     ; 6
                            fcb $01     ; 7
                            fcb $04     ; 8
                            fcb $08     ; 9
                            fcb $19     ; 10
                            fcb $01     ; 11
                            fcb $06     ; 12
                            fcb $08     ; 13
                            fcb $10     ; 14
                            fcb $02     ; 15
                            fcb $04     ; 16
                            fcb $09     ; 17
                            fcb $03     ; 18
                            fcb $04     ; 19
                            fcb $10     ; 20
                            fcb $04     ; 21
                            fcb $01     ; 22
                            fcb $05     ; 23
                            fcb $07     ; 24
                            fcb $08     ; 25
                            fcb $05     ; 26
                            fcb $12     ; 27
                            fcb $04     ; 28
                            fcb $03     ; 29
                            fcb $01     ; 30
                            fcb $02     ; 31

platformLevelNameLocations  ;   ┌─────────── First block of first video buffer
                            ;   │                               ┌───┬─── 16-bit Offset into that block
                            fcb BUFFER1_FIRST_PHYSICAL_BLOCK+0,$02,$1C  ; 0 = top platform
                            fcb BUFFER1_FIRST_PHYSICAL_BLOCK+3,$15,$48  ; 1 = bottom platform
                            fcb BUFFER1_FIRST_PHYSICAL_BLOCK+1,$02,$58  ; 2 = right platform
                            fcb BUFFER1_FIRST_PHYSICAL_BLOCK+2,$15,$08  ; 3 = left platform

nameCloudKingdom            fcn "CLOUD KINGDOM"
nameIceKingdom              fcn "ICE KINGDOM"
nameGhostKingdom            fcn "GHOST KINGDOM"
nameIslandKingdom           fcn "ISLAND KINGDOM"
nameJumpingKingdom          fcn "JUMPING KINGDOM"
nameUnseenKingdom           fcn "UNSEEN KINGDOM"
nameCrystalKingdom          fcn "CRYSTAL KINGDOM"
nameArrowKingdom            fcn "ARROW KINGDOM"
nameQuartetKingdom          fcn "QUARTET KINGDOM"
nameBoxKingdom              fcn "BOX KINGDOM"
nameFlyingKingdom           fcn "FLYING KINGDOM"
nameChaseKingdom            fcn "CHASE KINGDOM"
nameFireKingdom             fcn "FIRE KINGDOM"
nameCorridorKingdo          fcn "CORRIDOR KINGDOM"
nameDarkKingdom             fcn "DARK KINGDOM"
nameAcidKingdom             fcn "ACID KINGDOM"
namePyramidKingdom          fcn "PYRAMID KINGDOM"
namePuzzleKingdom           fcn "PUZZLE KINGDOM"
nameSkiKingdom              fcn "SKI KINGDOM"
nameStoneKingdom            fcn "STONE KINGDOM"
nameGreenKingdom            fcn "GREEN KINGDOM"
nameEnigmaKingdom           fcn "ENIGMA KINGDOM"
nameDiabolicKingdo          fcn "DIABOLIC KINGDOM"
nameBarrierKingdom          fcn "BARRIER KINGDOM"
nameSpikyKingdom            fcn "SPIKY KINGDOM"
nameHazardKingdom           fcn "HAZARD KINGDOM"
nameMagneticKingdo          fcn "MAGNETIC KINGDOM"
nameLavaKingdom             fcn "LAVA KINGDOM"
nameBridgeKingdom           fcn "BRIDGE KINGDOM"
nameForceKingdom            fcn "FORCE KINGDOM"
nameHopscotchKingd          fcn "HOPSCOTCH KINGDOM"
nameAerialKingdom           fcn "AERIAL KINGDOM"

levelNames                  fdb nameCloudKingdom,nameIceKingdom,nameGhostKingdom,nameIslandKingdom,nameJumpingKingdom,nameUnseenKingdom,nameCrystalKingdom,nameArrowKingdom
                            fdb nameQuartetKingdom,nameBoxKingdom,nameFlyingKingdom,nameChaseKingdom,nameFireKingdom,nameCorridorKingdo,nameDarkKingdom,nameAcidKingdom
                            fdb namePyramidKingdom,namePuzzleKingdom,nameSkiKingdom,nameStoneKingdom,nameGreenKingdom,nameEnigmaKingdom,nameDiabolicKingdo,nameBarrierKingdom
                            fdb nameSpikyKingdom,nameHazardKingdom,nameMagneticKingdo,nameLavaKingdom,nameBridgeKingdom,nameForceKingdom,nameHopscotchKingd,nameAerialKingdom

directionControlsToHeroIndexLookupForLevelSelectionScreen
                            ;   heroFrame index       control bits value          heroFrame index
                            fcb 0   ;                 0 = No controls,            0 = Look Straight
                            fcb 5   ;                 1 = Down,                   5 = Look Right Down
                            fcb 1   ;                 2 = Up,                     1 = Look Left Up
                            fcb 0   ;                 3 = Up Down,                0 = Look Straight
                            fcb 4   ;                 4 = Right,                  4 = Look Right
                            fcb 5   ;                 5 = Right Down,             5 = Look Right Down
                            fcb 4   ;                 6 = Right Up,               4 = Look Right
                            fcb 0   ;                 7 = Right Up Down,          0 = Look Straight
                            fcb 8   ;                 8 = Left,                   8 = Look Left
                            fcb 8   ;                 9 = Left Down,              8 = Look Left
                            fcb 1   ;                 10 = Left Up,               1 = Look Left Up
                            fcb 0   ;                 11 = Left Up Down,          0 = Look Straight
                            fcb 0   ;                 12 = Left Right,            0 = Look Straight
                            fcb 0   ;                 13 = Left Right Down,       0 = Look Straight
                            fcb 0   ;                 14 = Left Right Up,         0 = Look Straight
                            fcb 0   ;                 15 = Left Right Up down,    0 = Look Straight

directionControlsToPlatformLookupForLevelSelectionScreen
                            fcb $FF     ; 0 = No controls,           -1 = No platform
                            fcb   1     ; 1 = Down,                   1 = Bottom platform
                            fcb   0     ; 2 = Up,                     0 = Top platform
                            fcb $FF     ; 3 = Up Down,               -1 = No platform
                            fcb   2     ; 4 = Right,                  2 = Right platform
                            fcb   1     ; 5 = Right Down,             1 = Bottom platform
                            fcb   2     ; 6 = Right Up,               2 = Right platform
                            fcb $FF     ; 7 = Right Up Down,         -1 = No platform
                            fcb   3     ; 8 = Left,                   3 = Left platform
                            fcb   3     ; 9 = Left Down,              3 = Left platform
                            fcb   0     ; 10 = Left Up,               0 = Top platform
                            fcb $FF     ; 11 = Left Up Down,         -1 = No platform
                            fcb $FF     ; 12 = Left Right,           -1 = No platform
                            fcb $FF     ; 13 = Left Right Down,      -1 = No platform
                            fcb $FF     ; 14 = Left Right Up,        -1 = No platform
                            fcb $FF     ; 15 = Left Right Up down,   -1 = No platform

speedsToMoveToPlatformForLevelSelectionScreen
                            fdb -1,-1   ; 0
                            fdb  1,1    ; 1
                            fdb  2,0    ; 2
                            fdb -2,0    ; 3

levelCompletedFlags
                        ;                           1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 3 3
                        ; Level 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
                            fcb 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0


starLocations           ;   ┌──────────── block in first video buffer
                        ;   │    ┌───┬─── 16-bit Offset into that block (star is most-significant nibble)
                        fcb $40,$02,$4C,  $40,$04,$3C,  $40,$05,$0C,  $40,$0A,$2C,  $40,$0B,$88
                        fcb $40,$0D,$34,  $40,$12,$1C,  $40,$1B,$54,  $40,$1E,$44,  $40,$1E,$64
                        fcb $40,$1F,$3C,  $41,$00,$6C,  $41,$04,$34,  $41,$05,$60,  $41,$12,$38
                        fcb $41,$13,$58,  $41,$14,$24,  $41,$17,$64,  $41,$1A,$74,  $41,$1A,$7C
                        fcb $41,$1B,$1C,  $41,$1B,$54,  $41,$1C,$3C,  $41,$1D,$00,  $41,$1D,$98
                        fcb $41,$1E,$60,  $42,$01,$2C,  $42,$04,$20,  $42,$08,$18,  $42,$0A,$28
                        fcb $42,$0C,$60,  $42,$0C,$78,  $42,$0F,$6C,  $42,$15,$34,  $42,$16,$5C
                        fcb $42,$17,$48,  $42,$1A,$7C,  $42,$1C,$18,  $42,$1D,$58,  $43,$05,$7C
                        fcb $43,$0C,$04,  $43,$0E,$80,  $43,$0F,$3C,  $43,$0F,$88,  $43,$12,$6C
                        fcb $43,$16,$04,  $43,$17,$5C,  $43,$19,$70,  $43,$1A,$48,  $43,$1A,$74
                        fcb $44,$00,$94,  $44,$02,$04,  $44,$02,$5C,  $44,$02,$90,  $44,$04,$8C
                        fcb $44,$08,$10,  $44,$0B,$40,  $44,$0C,$5C,  $44,$0C,$5C,  $44,$0C,$60
                        fcb 0
