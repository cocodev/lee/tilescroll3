;
tunes.land.index            equ 0
tunes.land.length           equ 1
tunes.land.waveTable        equ squareWaveTable
tunes.land                  fcb 1,$2,$4F
;
tunes.hitBumper.index       equ 1
tunes.hitBumper.length      equ 1
tunes.hitBumper.waveTable   equ squareWaveTable
tunes.hitBumper             fcb 1,$0B,$B2   ;  238 Hz * 256 / 5210 * 256 = $0BB2
;
tunes.jump.index            equ 2
tunes.jump.length           equ 4
tunes.jump.waveTable        equ squareWaveTable
tunes.jump                  fcb 1,$08,$5A  ;  170 Hz * 256 / 5210 * 256 = $085A
                            fcb 1,$09,$BB  ;  198 Hz * 256 / 5210 * 256 = $09BB
                            fcb 1,$08,$FE  ;  183 Hz * 256 / 5210 * 256 = $08FE
                            fcb 1,$0A,$9D  ;  216 Hz * 256 / 5210 * 256 = $0A9D
;
tunes.hitWingItem.index     equ 3
tunes.hitWingItem.length    equ 1
tunes.hitWingItem.waveTable equ squareWaveTable
tunes.hitWingItem           fcb 40,$00,$E2  ;   18 Hz * 256 / 5210 * 256 = $00E2
;
tunes.hitGemItem.index      equ 4
tunes.hitGemItem.length     equ 3
tunes.hitGemItem.waveTable  equ squareWaveTable
tunes.hitGemItem            fcb 2,$0E,$72  ;  294 * 256 / 5210 * 256 = $0E72
                            fcb 2,$1C,$D8  ;  587 * 256 / 5210 * 256 = $1CD8
                            fcb 2,$39,$Bc  ; 1175 * 256 / 5210 * 256 = $39BC
;
tunes.jumpOpenDoor.index        equ 5
tunes.jumpOpenDoor.length       equ 2
tunes.jumpOpenDoor.waveTable    equ squareWaveTable
tunes.jumpOpenDoor              fcb 2,$1D,$49  ;  596 Hz * 256 / 5210 * 256 = $1D49
                                fcb 2,$3A,$9E  ; 1193 Hz * 256 / 5210 * 256 = $3A9E
;
tunes.hitKeyItem.index      equ 6
tunes.hitKeyItem.length     equ 1
tunes.hitKeyItem.waveTable  equ squareWaveTable
tunes.hitKeyItem            fcb 1,$1D,$49   ;  596 Hz * 256 / 5210 * 256 = $1D49
;
tunes.falling.index         equ 7
tunes.falling.length        equ 8
tunes.falling.waveTable     equ sineWaveTable
tunes.falling               fcb 2,$75,$04  ; $2386 Hz * 256 / 5220 * 256 = $7504
                            ;fcb 1,$73,$5D  ; $2352 Hz * 256 / 5220 * 256 = $735D
                            fcb 2,$71,$B6  ; $2318 Hz * 256 / 5220 * 256 = $71B6
                            ;fcb 1,$70,$0F  ; $2284 Hz * 256 / 5220 * 256 = $700F
                            fcb 2,$6E,$68  ; $2251 Hz * 256 / 5220 * 256 = $6E68
                            ;fcb 1,$6C,$C2  ; $2217 Hz * 256 / 5220 * 256 = $6CC2
                            fcb 2,$6B,$1B  ; $2183 Hz * 256 / 5220 * 256 = $6B1B
                            ;fcb 1,$69,$74  ; $2150 Hz * 256 / 5220 * 256 = $6974
                            fcb 2,$67,$CD  ; $2116 Hz * 256 / 5220 * 256 = $67CD
                            ;fcb 1,$66,$26  ; $2082 Hz * 256 / 5220 * 256 = $6626
                            fcb 2,$64,$80  ; $2049 Hz * 256 / 5220 * 256 = $6480
                            ;fcb 1,$62,$D9  ; $2015 Hz * 256 / 5220 * 256 = $62D9
                            fcb 2,$61,$32  ; $1981 Hz * 256 / 5220 * 256 = $6132
                            ;fcb 1,$5F,$8B  ; $1948 Hz * 256 / 5220 * 256 = $5F8B
                            fcb 4,$5D,$E4  ; $1914 Hz * 256 / 5220 * 256 = $5DE4
                            ;fcb 1,$5C,$3E  ; $1880 Hz * 256 / 5220 * 256 = $5C3E

tunes.hitPaintItem.index        equ 8
tunes.hitPaintItem.length       equ 8
tunes.hitPaintItem.waveTable    equ squareWaveTable
tunes.hitPaintItem              fcb 7,$02,$82  ;   51 Hz * 256 / 5210 * 256 = $0282
                                fcb 7,$02,$68  ;   49 Hz * 256 / 5210 * 256 = $0268
                                fcb 7,$02,$4F  ;   47 Hz * 256 / 5210 * 256 = $024F
                                fcb 7,$02,$36  ;   45 Hz * 256 / 5210 * 256 = $0236
                                fcb 7,$02,$29  ;   44 Hz * 256 / 5210 * 256 = $0229
                                fcb 7,$02,$10  ;   42 Hz * 256 / 5210 * 256 = $0210
                                fcb 7,$02,$04  ;   41 Hz * 256 / 5210 * 256 = $0204
                                fcb 7,$01,$EB  ;   39 Hz * 256 / 5210 * 256 = $01EB

tunes.hitHammerItem.index       equ 9
tunes.hitHammerItem.length      equ 2
tunes.hitHammerItem.waveTable   equ squareWaveTable
tunes.hitHammerItem             fcb 3,$01,$C5  ;   36 Hz * 256 / 5210 * 256 = $01C5
                                fcb 2,$00,$00  ;    0 Hz * 256 / 5210 * 256 = $0000

tunes.bounceDamage.index        equ $0A
tunes.bounceDamage.length       equ 1
tunes.bounceDamage.waveTable    equ squareWaveTable
tunes.bounceDamage              fcb 3,$08,$FE  ;  183 Hz * 256 / 5210 * 256 = $08FE

tunes.hitDamageFloor.index      equ $0B
tunes.hitDamageFloor.length     equ 3
tunes.hitDamageFloor.waveTable  equ squareWaveTable
tunes.hitDamageFloor            fcb 4,$01,$EB  ;   39 * 256 / 5210 * 256 = $01EB
                                fcb 2,$01,$DE  ;   38 * 256 / 5210 * 256 = $01DE
                                fcb 2,$01,$D1  ;   37 * 256 / 5210 * 256 = $01D1

tunes.hitFloorFall.index        equ $0C
tunes.hitFloorFall.length       equ 1
tunes.hitFloorFall.waveTable    equ squareWaveTable
tunes.hitFloorFall              fcb 2,$EA,$7A  ; 4772 * 256 / 5210 * 256 = $EA7A

tunes.hitCrazyBummper.index     equ $0D
tunes.hitCrazyBummper.length    equ 3
tunes.hitCrazyBummper.waveTable equ squareWaveTable
tunes.hitCrazyBummper           fcb 1,$EA,$7A  ; 1403 * 256 / 5210 * 256 = $EA7A
                                fcb 1,$EA,$7A  ; 1194 * 256 / 5210 * 256 = $EA7A
                                fcb 1,$EA,$7A  ; 1355 * 256 / 5210 * 256 = $EA7A

tunes.death.index           equ $0E
tunes.death.length          equ 6
tunes.death.waveTable       equ squareWaveTable
tunes.death                 fcb 3,$01,$C5  ;   36 * 256 / 5210 * 256 = $01C5
                            fcb 1,$00,$00  ;    0 * 256 / 5210 * 256 = $0000
                            fcb 2,$01,$C5  ;   36 * 256 / 5210 * 256 = $01C5
                            fcb 2,$00,$00  ;    0 * 256 / 5210 * 256 = $0000
                            fcb 1,$01,$C5  ;   36 * 256 / 5210 * 256 = $01C5
                            fcb 3,$00,$00  ;    0 * 256 / 5210 * 256 = $0000

tunes.levelSelectAndNewLevel.index      equ $0F
tunes.levelSelectAndNewLevel.length     equ 4
tunes.levelSelectAndNewLevel.waveTable  equ squareWaveTable
tunes.levelSelectAndNewLevel            fcb 3,$02,$4F  ;   47 * 256 / 5210 * 256 = $024F
                                        fcb 5,$00,$00  ;    0 * 256 / 5210 * 256 = $0000
                                        fcb 3,$02,$4F  ;   47 * 256 / 5210 * 256 = $024F
                                        fcb 5,$00,$00  ;    0 * 256 / 5210 * 256 = $0000

tunes.levelSelectedAndGameOver.index        equ $10
tunes.levelSelectedAndGameOver.length       equ 8
tunes.levelSelectedAndGameOver.waveTable    equ squareWaveTable
tunes.levelSelectedAndGameOver              fcb 2,$EA,$7A  ;  119 * 256 / 5210 * 256 = $EA7A
                                            fcb 2,$00,$00  ;  108 * 256 / 5210 * 256 = $0000
                                            fcb 2,$00,$00  ;   99 * 256 / 5210 * 256 = $0000
                                            fcb 2,$00,$00  ;   91 * 256 / 5210 * 256 = $0000
                                            fcb 2,$EA,$7A  ;   85 * 256 / 5210 * 256 = $EA7A
                                            fcb 2,$00,$00  ;   79 * 256 / 5210 * 256 = $0000
                                            fcb 2,$EA,$7A  ;   74 * 256 / 5210 * 256 = $EA7A
                                            fcb 2,$00,$00  ;   70 * 256 / 5210 * 256 = $0000

tunes.hitClockItem.index        equ $11
tunes.hitClockItem.length       equ 4
tunes.hitClockItem.waveTable    equ squareWaveTable
tunes.hitClockItem              fcb 2,$EA,$7A  ;  879 * 256 / 5210 * 256 = $EA7A
                                fcb 2,$00,$00  ;  698 * 256 / 5210 * 256 = $0000
                                fcb 2,$00,$00  ;  783 * 256 / 5210 * 256 = $0000
                                fcb 3,$00,$00  ;  523 * 256 / 5210 * 256 = $0000

tunes.hitSodaItem.index     equ $12
tunes.hitSodaItem.length    equ 6
tunes.hitSodaItem.waveTable equ squareWaveTable
tunes.hitSodaItem           fcb 1,$EA,$7A  ; 2386 * 256 / 5210 * 256 = $EA7A
                            fcb 1,$00,$00  ; 2435 * 256 / 5210 * 256 = $0000
                            fcb 1,$00,$00  ; 2485 * 256 / 5210 * 256 = $0000
                            fcb 1,$00,$00  ; 2538 * 256 / 5210 * 256 = $0000
                            fcb 1,$EA,$7A  ; 2593 * 256 / 5210 * 256 = $EA7A
                            fcb 1,$00,$00  ; 2651 * 256 / 5210 * 256 = $0000

tunes.pause.index           equ $13
tunes.pause.length          equ 2
tunes.pause.waveTable       equ squareWaveTable
tunes.pause                 fcb 2,$EA,$7A  ; 1046 * 256 / 5210 * 256 = $EA7A
                            fcb 2,$00,$00  ;  879 * 256 / 5210 * 256 = $0000

tunes.menu.index             equ $14
tunes.menu.length            equ 1
tunes.menu.waveTable         equ squareWaveTable
tunes.menu                   fcb 1,$EA,$7A  ; 1046 * 256 / 5210 * 256 = $EA7A

tunes.openTitleWinLevel.index       equ $15
tunes.openTitleWinLevel.length      equ 7
tunes.openTitleWinLevel.waveTable   equ squareWaveTable
tunes.openTitleWinLevel             fcb 2,$19,$A6  ;  523 * 256 / 5210 * 256 = $19A6
                                    fcb 2,$20,$52  ;  659 * 256 / 5210 * 256 = $2052
                                    fcb 2,$26,$66  ;  783 * 256 / 5210 * 256 = $2666
                                    fcb 2,$33,$4C  ; 1046 * 256 / 5210 * 256 = $334C
                                    fcb 2,$00,$00  ;    0 * 256 / 5210 * 256 = $0000
                                    fcb 2,$26,$66  ;  783 * 256 / 5210 * 256 = $2666
                                    fcb 8,$33,$4C  ; 1046 * 256 / 5210 * 256 = $334C

tunes.addresses             fdb tunes.land,          tunes.hitBumper,          tunes.jump,          tunes.hitWingItem,          tunes.hitGemItem,          tunes.jumpOpenDoor,          tunes.hitKeyItem,          tunes.falling,          tunes.hitPaintItem,          tunes.hitHammerItem,          tunes.bounceDamage,          tunes.hitDamageFloor,          tunes.hitFloorFall,          tunes.hitCrazyBummper,          tunes.death,          tunes.levelSelectAndNewLevel,          tunes.levelSelectedAndGameOver,          tunes.hitClockItem,          tunes.hitSodaItem,          tunes.pause,          tunes.menu,          tunes.openTitleWinLevel.length
tunes.lengths               fcb tunes.land.length,   tunes.hitBumper.length,   tunes.jump.length,   tunes.hitWingItem.length,   tunes.hitGemItem.length,   tunes.jumpOpenDoor.length,   tunes.hitKeyItem.length,   tunes.falling.length,   tunes.hitPaintItem.length,   tunes.hitHammerItem.length,   tunes.bounceDamage.length,   tunes.hitDamageFloor.length,   tunes.hitFloorFall.length,   tunes.hitCrazyBummper.length,   tunes.death.length,   tunes.levelSelectAndNewLevel.length,   tunes.levelSelectedAndGameOver.length,   tunes.hitClockItem.length,   tunes.hitSodaItem.length,   tunes.pause.length,   tunes.menu.length,   tunes.openTitleWinLevel.length
tunes.waveTables            fdb tunes.land.waveTable,tunes.hitBumper.waveTable,tunes.jump.waveTable,tunes.hitWingItem.waveTable,tunes.hitGemItem.waveTable,tunes.jumpOpenDoor.waveTable,tunes.hitKeyItem.waveTable,tunes.falling.waveTable,tunes.hitPaintItem.waveTable,tunes.hitHammerItem.waveTable,tunes.bounceDamage.waveTable,tunes.hitDamageFloor.waveTable,tunes.hitFloorFall.waveTable,tunes.hitCrazyBummper.waveTable,tunes.death.waveTable,tunes.levelSelectAndNewLevel.waveTable,tunes.levelSelectedAndGameOver.waveTable,tunes.hitClockItem.waveTable,tunes.hitSodaItem.waveTable,tunes.pause.waveTable,tunes.menu.waveTable,tunes.openTitleWinLevel.waveTable