10 CLS:PCLEAR 1:CLEAR 100,&H7FFF:GOSUB 300
15 PRINT"1. Load Tiles and Maps and prerender Level 0 first"
20 PRINT"2. Skip loading Tiles and Maps but Prerender Level 0 first"
40 OP$=INKEY$:IF OP$="" THEN 40
50 OP=INSTR("12",OP$):IF OP=0 THEN 10
60 ON OP GOTO 100,150
99 '
100 PRINT"Loading...":LOADM"LOADDATA.BIN":EXEC
110 PRINT"Loading Cloud Kingdoms ...":LOADM"CLOUDK.BIN":EXEC
120 GOTO 200
149 '
150 PRINT"Loading Cloud Kingdoms ...":
160 LOADM"CLOUDK.BIN":EXEC
170 GOTO 200
199 '
200 PRINT "Done."
210 GOSUB 500
220 END
299 '
300 PALETTE 0,0:PALETTE 8,63:POKE &HFF9A,0:WIDTH 80:RETURN
399 '
499 ' PRINT DEBUG OUTPUT
500 PRINT "--- DEBUG OUTPUT ---"
510 PRINT "GIME Check Count:"(PEEK(&H2000)*256+PEEK(&H2001))
520 PRINT "Timer Interval Start:"(PEEK(&H2002)*256+PEEK(&H2003))
530 PRINT "Timer Interval Final:"(PEEK(&H2004)*256+PEEK(&H2005))"(Only useful if score area displayed successfully on exit)"
530 PRINT "Timer Interval Final:"(PEEK(&H2004)*256+PEEK(&H2005))"(Only useful if score area displayed correctly)"
540 PRINT "Frame Time: "PEEK(&H2006)"v"PEEK(&H2007)*256+PEEK(&H2008)"f"
565 PRINT "Debug Data: ";:FOR I = 0 TO 255:PRINT RIGHT$("0"+HEX$(PEEK(&H2009+I)),2)" ";:NEXT I:PRINT

570 PRINT "--------------------"
580 RETURN
999 '
1000 WIDTH 80:PCLEAR 1:CLEAR 100,&H7FFF:PRINT "Enter Physical Block in hex: ";:LINE INPUT B$:B=VAL("&H"+B$)
1010 POKE &H2005,B:EXEC &H2000
1020 FOR A=&H400 TO &H4FF STEP 16:PRINT HEX$(A)": ";:FOR P=0 TO 15
1030 PRINT RIGHT$("0"+HEX$(PEEK(A+P)), 2)" ";
1040 NEXT P:PRINT:NEXT A
1050 POKE &HE097,&H34
1060 END
2000 PRINT "Frequency in Hz: ";:INPUT A$
2010 PRINT "$"HEX$(VAL(A$)*256/5220*256)
2030 GOTO 2000
