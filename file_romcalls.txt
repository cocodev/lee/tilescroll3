; Taken from William "lostwizard" Astle's Dungeons of Daggorath disk mod at http://dod.projects.l-w.ca/hg-lw/index.cgi/file/tip/dod.s

006F    DEVNUM            ; 1 byte
0070    CINBFL            ; 2 byte
0071    RSTFLG            ; 1 byte
094C    DNAMBF            ; 8 bytes
0954    DEXTBF            ; 3 bytes
FF00    PIA0
FF20    PIA1

; ------------------------------

        hook_openi      rmb 2       ; the address of the "open for input" routine in Disk ROM
        hook_openo      rmb 2       ; the address of the "open for output" routine in the Disk ROM
        error_handler   rmb 2       ; the installed error trap
        error_stack     rmb 2       ; the stack address to use when transferring control to the error trap

; ------------------------------

init_diskio                         ; Once at start up. Initializes points to call into ROM for disk i/o.
        ldx   $C004                 ; get DSKCON address pointer
        ldy   #$8955                ; point to IRQ handler
        cmpx  #$D75F                ; is it disk basic 1.1?
        beq   load_disk11           ; brif so

        cmpx  #$d66c                ; is it disk basic 1.0?
        beq   load_disk10           ; brif so

        ldx   #disk_error           ; point to the routine for no disk system
        ldu   #disk_error
        ...                         ; go finish initializing

; ------------------------------

load_disk10
        ldx   #$C956                ; address of "open system file for output" routine
        ldu   #$C959                ; address of "open system file for input" routine
        ldy   #$d7c4                ; point to Disk IRQ handler
        bra   load_000              ; go set vectors

load_disk11
        ldx   #$CA04                ; address of "open system file for output" routine
        ldu   #$CA07                ; address of "Open system file for input" routine
        ldy   #$d8b7                ; point to Disk IRQ handler

load_000
        stx   hook_openo            ; save address of "open for output"
        stu   hook_openi            ; save address of "open for input"
        sty   hook_irq              ; save IRQ hook for Basic

load_001
        clr   error_handler         ; mark the error handler as disabled
        clr   error_handler+1
        jmp   START                 ; transfer control to the main stream code

; This routine is called for disk routine traps if there is no recognized version of Disk Basic.
disk_error
        ldb #255          ; internal error code equivalent of "ENOSYS"
                          ; fall through to error handler is intentional.
; This is the error handler.
error_hook
        ldx   error_handler         ; is there a handler registered?
        bne   error_hook000         ; brif so
        clr   RSTFLG                ; force Basic to do a cold start
        jmp   [$FFFE]               ; force a reset if we got an error we weren't expecting

error_hook000
        lds   error_stack           ; reset the stack to a known state
        bsr   restore_dp            ; reset the direct page
        bsr   error_cleartrap       ; clear the error trap on error
        jsr   slowclock             ; force slow clock mode
        jmp   ,x                    ; transfer control to the handler

; This is the routine that installs a handler and the cleanup routine.
error_settrap
        stx   error_handler         ; set the handler
        puls  y                     ; get back caller so the stack is in the right place
        sts   error_stack           ; save the stack pointer for recovery
        jmp   ,y                    ; return to caller

; This is the shim routine that clean out the error handler.
error_cleartrap
        pshs  cc                    ; save flags
        clr   error_handler          ; clear the hander
        clr   error_handler+1
        puls  cc,pc                  ; return to original caller

; This is the error handler for opening files, both input and output.
file_openerr
        bsr   restore_dp            ; restore direct page
        lda   #1                    ; clear Z
        puls  d,x,y,u,pc            ; restore registers and return

; This is the IO error handler for file I/O
file_ioerror
        bsr   restore_dp          ; reset DP properly
        bsr   file_close          ; close the file
        comb                      ; flag error
        puls  d,x,y,u,pc          ; restore registers and return

; This routine sets up PIA1 for Basic (with interrupts disabled)
setpiabasic
        ldd   #$34fe                ; initializer for side A
        sta   PIA0+3                ; disable VSYNC interrupt, clear analogue mux MSB
        clr   PIA1+1                ; set direction mode for side A
        stb   PIA1                  ; set direction properly (sets RS232 to output)
        sta   PIA1+1                ; set back to data mode, no interrupts, cassette off
        ldb   #2                    ; set RS232 to "marking"
        stb   PIA1
        ldd   #$34f8                ; initializer for side B
        clr   PIA1+3                ; set direction mode for side B
        stb   PIA1+2                ; set direction properly, disable sound, one bit sound as input
        sta   PIA1+3                ; set back to data mode, no interrupts, sound on
        rts

; This routine sets the direct page properly for a Basic call
set_basdp
        pshs  a                     ; save register
        clra                        ; basic's DP is on page 0
        tfr   a,dp                  ; set DP
        puls  a,pc                  ; restore registers and return

; This routine restores the direct page properly for Daggorath
restore_dp
        pshs  cc,a                  ; save flags and temp
        lda   #zero/256             ; get proper DP value
        tfr   a,dp                  ; set DP
        puls  cc,a,pc               ; restore registers and return

; This routine forces the clock rate to the regular "slow" rate. The reason for the two bits is to make sure it gets
; slowed down on both Coco3 and Coco1/2. It will also properly stop "double speed" mode on a Coco1/2. According to
; the SAM data sheet, this is the correct sequence to force full slowdown.
slowclock
        sta   $FFD8                 ; slow the clock rate down
        sta   $FFD6
        rts

; This routine opens a file for INPUT. If the file does not exist or cannot be opened for some reason, it will return
; nonzero. Otherwise it returns zero. Enter with filename/extension in DNAMBF. On return,
; DEVNUM will be set to the correct file number.
file_openi
        pshs  d,x,y,u               ; save registers
        ldx   #file_openerr         ; point to error handler for opening files
        bsr   error_settrap         ; set Basic error handler
        bsr   set_basdp             ; set direct page for Basic call
        jsr   [hook_openi]          ; go call the file open handler in ROM
        bsr   restore_dp            ; restore direct page
        jsr   slowclock             ; force slow down
        clra                        ; set Z for success
        bsr   error_cleartrap       ; clear the error trap
        puls  d,x,y,u,pc            ; restore registers and return

; This routine closes the currently open file. It returns Z set on success and clear on error.
; Enter with the file number in DEVNUM.
file_close
        ldx   #file_openerr         ; use the same generic handler for failure as opening
        bsr   error_settrap         ; register error handler
        bsr   set_basdp             ; set up DP correctly
        jsr   $A426                 ; call the "close one file" handler
        jsr   slowclock             ; force slow down
        clra                        ; set Z for success
        bsr   error_cleartrap       ; clear the error trap
        bra   restore_dp            ; restore DP and return

; This routine writes the byte in A to the currently open file.
; In the event of an error, return C set and close the file. Otherwise, return C clear.
file_write
        pshs  b,x,y,u               ; save registers
        ldx   #file_ioerror         ; pointer to IO error trap
        jsr   error_settrap         ; set the trap
        bsr   set_basdp             ; set up DP properly
        jsr   $A282                 ; write byte
        bsr   restore_dp            ; restore direct page
        jsr   slowclock             ; force slow down
        clrb                        ; reset C for no error
        jsr   error_cleartrap       ; clear the error trap
        puls  b,x,y,u,pc            ; restore registers and return
    
; This routine reads a byte from the currently open file and returns it in A.
; In the event of an error, return C set and close the file. Otherwise, return C clear.
; On EOF, CINBFL will be nonzero.
file_readbyte
        pshs  d,x,y,u               ; save registers
        ldx   #file_ioerror         ; pointer to IO error handler
        jsr   error_settrap         ; set error handler
        bsr   set_basdp             ; set up DP correctly
        jsr   $A176                 ; go read a character
        sta   ,s                    ; save return value
        bsr   restore_dp            ; reset DP
        jsr   slowclock             ; force slow down
        clrb                        ; reset C for no error
        puls  d,x,y,u,pc            ; restore registers and return

; This routine reads b bytes from the open file and stores them in the buffer
; pointed to by x.  Returns with C set on error.  Otherwise, returns C clear.
file_readbytes
        jsr   file_readbyte         ; read byte
        bcs   file_readbytes_return ; brif error
        tst   CINBFL                ; was there anything to read?
        bne   file_readbytes_error  ; brif not
        sta   ,x+                   ; save byte in buffer
        decb                        ; done reading?
        bne   file_readbytes        ; brif not
        rts                         ; return success
file_readbytes_error
        coma                        ; set carry for failed read
file_readbytes_return
        rts                         ; return failure

; ------------------------------

load_file
        clrb                        ; mark current game still valid
        pshs  d,x,y,u               ; save registers
        ...         
        ...                         ; Store up to 8 character filename in DNAMBF
        ...                         ; Fill remainder of DNAMBF with 32 (space)
        ...                         ; Store up to 3 character extension in DEXTBF
        ...                         ; Fill remainder of DEXTBF with 32 (space)
        ...
        jsr   setpiabasic           ; set up PIA for Basic I/O
        jsr   file_openi            ; open file for output
        beq   load_filecontinue     ; brif no error opening file
        bra   load_file_error         ; throw error if open failed

load_fileerr
        jsr   file_close            ; close the file if it's open

load_file_error
        jsr   setpiadod             ; reset PIAs to DoD mode
        coma                        ; flag error on save
        puls  d,x,y,u,pc            ; return to caller

